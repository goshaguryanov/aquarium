module Lago
  class MobileUserSerializer < ActiveModel::Serializer
    attributes :email, :operator, :return_url
  end
end
