module Pigeon
  class ContactSerializer < ActiveModel::Serializer
    attr_reader :mailing

    attributes :id, :email, :unsubscribe

    def initialize(object, options = {})
      @mailing = options.delete(:mailing)
      super(object, options)
    end

    def unsubscribe
      mailing && "#{Settings.hosts.lago}/unsubscribe/#{object.unsubscribe_token(mailing.type.slug)}"
    end
  end
end