module Pigeon
  class MailingSerializer < ActiveModel::Serializer
    attributes :id, :subject, :template, :contacts, :status, :status_code

    def template
      uc = MailingTemplateRender.new(object)
      uc.template_with_banners
    end

    def contacts
      object.contacts.on.count
    end

    def status_code
      object.attributes['status']
    end
  end
end
