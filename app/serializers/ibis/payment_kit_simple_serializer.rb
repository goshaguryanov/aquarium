module Ibis
  class PaymentKitSimpleSerializer < ActiveModel::Serializer
    attributes :id, :product_id, :namespace_id, :subproduct_id, :period_id
    attributes :price, :crossed_price, :discount
    attributes :upsell_id, :renewal_id, :alternate_ids, :paymethod_ids
    attributes :autopay_timeout_hours, :syncable, :disabled, :texts, :user_conditions
    attributes :created_at, :updated_at
    attributes :product, :namespace, :subproduct, :period, :city_choice

    def price
      object.price.to_f
    end

    def crossed_price
      object.crossed_price.to_f
    end

    def product
      ProductSerializer.new(object.product)
    end
    
    def namespace
      NamespaceSimpleSerializer.new(object.namespace)
    end

    def subproduct
      SubproductSimpleSerializer.new(object.subproduct)
    end

    def period
      PeriodSerializer.new(object.period)
    end

    def autopay_timeout_hours
      object.autopay_timeout && object.autopay_timeout.to_i / 3600
    end
  end
end
