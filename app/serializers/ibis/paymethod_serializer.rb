module Ibis
  class PaymethodSerializer < ActiveModel::Serializer
    attributes :id, :slug, :title, :description
  end
end
