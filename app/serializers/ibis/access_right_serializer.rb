module Ibis
  class AccessRightSerializer < ActiveModel::Serializer
    attributes :start_date, :end_date, :order, :payment

    def order
      OrderSerializer.new(object.order)
    end

    def payment
      PaymentListSerializer.new(object.payment)
    end
  end
end
