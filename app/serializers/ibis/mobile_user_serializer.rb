class Ibis::MobileUserSerializer < ActiveModel::Serializer
  attributes :id, :status, :first_name, :last_name, :msisdn, :operator, :created_at, :updated_at
end
