module Ibis
  class PeriodSerializer < ActiveModel::Serializer
    attributes :id, :slug, :title
    attributes :measure_unit, :duration_number
  end
end
