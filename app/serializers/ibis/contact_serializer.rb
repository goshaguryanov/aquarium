module Ibis
  class ContactSerializer < ActiveModel::Serializer
    attributes :id, :email, :user_id, :contact_status, :subscription_status, :created_at, :updated_at, :type_ids
  end
end
