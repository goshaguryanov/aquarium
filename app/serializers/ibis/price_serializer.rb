module Ibis
  class PriceSerializer < ActiveModel::Serializer
    attributes :product, :period, :student, :value, :since, :expire, :upsell_ids, :crossed_price
  end
end

