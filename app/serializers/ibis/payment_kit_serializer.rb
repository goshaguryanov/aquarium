module Ibis
  class PaymentKitSerializer < PaymentKitSimpleSerializer
    attributes :alternates, :upsell, :renewal, :paymethods

    def alternate
      object.alternates.map { |p| PaymentKitSimpleSerializer.new(p) }
    end

    def upsell
      PaymentKitSimpleSerializer.new(object.upsell)
    end

    def renewal
      PaymentKitSimpleSerializer.new(object.renewal)
    end

    def paymethods
      object.paymethods.map { |paymethod| PaymethodSerializer.new(paymethod) }
    end
  end
end
