module Ibis
  class CitySerializer < ActiveModel::Serializer
    attributes :id, :title, :capability, :position, :contact
  end
end
