module Ibis
  class SubproductSerializer < SubproductSimpleSerializer
    attributes :product

    def product
      ProductSerializer.new(object.product)
    end
  end
end
