module Ibis
  class OptionSerializer
    attr_reader :option
    
    def initialize(option)
      @option = option
    end
    
    def as_json
      option.slice(:id, :type, :value)
    end
  end
end
