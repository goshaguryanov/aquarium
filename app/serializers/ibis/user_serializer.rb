module Ibis
  class UserSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name, :nickname, :email, :phone, :status, :avatar, :address
    attributes :company, :birthday, :company_speciality_id, :appointment_id, :income_id, :auto_id
    attributes :created_at, :updated_at, :autopay, :subscribe_base_id, :accepted_promo

    def avatar
      object.avatar.as_json
    end

    def address
      object.address.as_json
    end
  end
end
