module Ibis
  class ProductSerializer < ActiveModel::Serializer
    attributes :id, :slug, :title
  end
end
