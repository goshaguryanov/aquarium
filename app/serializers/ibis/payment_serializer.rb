module Ibis
  class PaymentSerializer < PaymentSimpleSerializer
    attributes :renewal, :manager_comment

    def renewal
      PaymentKitSimpleSerializer.new(object.renewal)
    end
  end
end