class PersonSerializer < ActiveModel::Serializer
  attributes :id, :title, :document_id, :bound_document_id, :processed, :created_at
end
