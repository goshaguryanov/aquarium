module Ibis
  class NamespaceSimpleSerializer < ActiveModel::Serializer
    attributes :id, :product_id, :slug, :title, :start_date, :end_date, :product
    attributes :base, :redirect_to_id, :texts, :picture

    def product
      ProductSerializer.new(object.product)
    end
  end
end
