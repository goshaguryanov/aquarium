module Metynnis
  class NamespaceSimpleSerializer < ActiveModel::Serializer
    attributes :slug, :title, :start_date, :end_date, :texts, :redirect_to_slug_path

    def redirect_to_slug_path
      if object.redirect_to
        "#{object.redirect_to.product.slug}/#{object.redirect_to.slug}"
      end
    end
  end
end
