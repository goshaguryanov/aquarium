module Metynnis
  class PaymentKitSimpleSerializer < ActiveModel::Serializer
    attributes :product, :namespace, :subproduct, :period, :slug_path
    attributes :price, :crossed_price, :discount, :paymethods, :texts, :city_choice

    def product
      ProductSerializer.new(object.product)
    end

    def namespace
      NamespaceSimpleSerializer.new(object.namespace)
    end

    def subproduct
      SubproductSerializer.new(object.subproduct)
    end

    def period
      PeriodSerializer.new(object.period)
    end

    def paymethods
      object.paymethods.map { |paymethod| PaymethodSerializer.new(paymethod) }
    end
  end
end
