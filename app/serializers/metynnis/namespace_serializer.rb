module Metynnis
  class NamespaceSerializer < NamespaceSimpleSerializer
    attributes :product, :redirect_to

    def product
      ProductSerializer.new(object.product)
    end

    def redirect_to
      NamespaceSimpleSerializer.new(object.redirect_to)
    end
  end
end
