module Metynnis
  class PaymentKitSerializer < PaymentKitSimpleSerializer
    attributes :upsell, :alternates, :renewal, :first_payment

    def upsell
      PaymentKitSimpleSerializer.new(object.upsell)
    end

    def alternates
      object.alternates.map {|alt| PaymentKitSimpleSerializer.new(alt) }
    end

    def renewal
      PaymentKitSimpleSerializer.new(object.renewal)
    end

    def first_payment
      result = {}
      if object.renewal
        result[:renewal] = {
            price: object.renewal.price,
            date: object.period.duration.from_now - object.renewal.autopay_timeout
        }
      end
      if object.upsell && object.upsell.renewal
        result[:upsell] = {
            price: object.upsell.renewal.price,
            date: object.upsell.period.duration.from_now - object.upsell.renewal.autopay_timeout
        }
      end
      result
    end

    def namespace
      NamespaceSimpleSerializer.new(object.namespace)
    end
  end
end
