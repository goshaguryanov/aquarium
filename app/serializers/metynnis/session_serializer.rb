module Metynnis
  class SessionSerializer < ActiveModel::Serializer
    attributes :id, :subscription_status, :account_type

    def account_type
      object.account.type if object.account
    end
  end
end
