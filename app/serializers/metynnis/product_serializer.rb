module Metynnis
  class ProductSerializer < ActiveModel::Serializer
    attributes :slug, :title
  end
end
