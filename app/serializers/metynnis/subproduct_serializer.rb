module Metynnis
  class SubproductSerializer < ActiveModel::Serializer
    attributes :slug, :title, :phone_text
  end
end
