module Metynnis
  class PeriodSerializer < ActiveModel::Serializer
    attributes :slug, :title, :duration
  end
end
