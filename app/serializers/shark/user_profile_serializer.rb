module Shark
  class UserProfileSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :last_name, :phone, :email, :nickname, :status, :next_billing
    attributes :avatar, :access_rights, :autopay, :accounts, :mail_types

    def access_rights
      # Исключаем access_rights, по которым был возврат (период действия < 2 дней)
      object.access_rights.paid.where("(access_rights.end_date - access_rights.start_date) > interval '2 days'").map { |ar| AccessRightSerializer.new(ar).as_json }
    end

    def accounts
      object.accounts.reject do |account|
        account.type == 'password'
      end.map do |account|
        AccountSerializer.new(account).as_json
      end
    end

    def next_billing
      next_payment = object.payments.joins(:payment_kit).paid.order('(payments.end_date-payment_kits.autopay_timeout)').find do |payment|
        payment.can_renewal?
      end
      if next_payment
        {
            summ: next_payment.payment_kit.renewal ? next_payment.payment_kit.renewal.price  : next_payment.renewal.price,
            date: next_payment.next_billing_date
        }
      end
    end

    def mail_types
      ::Mailing::MailType.is_visible.map do |mail_type|
        MailTypeSerializer.new(mail_type).as_json.merge({ checked: object.contact && object.contact.mail_types.include?(mail_type) })
      end
    end
  end
end
