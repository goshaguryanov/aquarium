module Shark
  class ContactSerializer < ActiveModel::Serializer
    attributes :email
  end
end
