module Shark
  class QuoteSerializer < ActiveModel::Serializer
    attributes :title, :slug, :price_time, :value, :prev_value

    def title
      Settings.quotes.title_replace[object.full_slug] || object.title
    end
  end
end
