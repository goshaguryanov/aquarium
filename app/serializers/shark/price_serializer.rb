module Shark
  class PriceSerializer < ActiveModel::Serializer
    attributes :id, :product, :period, :student, :value
    attributes :since, :expire, :crossed_price, :upsell, :renewal

    def upsell
      object.upsell.map { |u| PriceSerializer.new(u) }
    end

    def renewal
      PriceSerializer.new(object.renewal)
    end
  end
end
