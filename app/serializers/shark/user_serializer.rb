module Shark
  class UserSerializer
    attr_reader :user, :session

    def initialize(user, session = nil)
      @user    = user
      @session = session
    end

    def as_json
      user.slice(*%i(id first_name last_name phone email nickname avatar created_at)).merge(
          status: session_status,
          mail_types: mail_types,
          sub_end_date: user.last_unexpired_access_right.try(:end_date),
          renewal_date: user.autopay && user.payment.try(:next_billing_date)
      )
    end

    def mail_types
      user.confirmed? && user.contact && user.contact.mail_types.is_visible.map { |m| m.slice(*%i(id slug title)) }
    end

    private
    def session_status
      session.subscription_status unless session.nil?
    end
  end
end
