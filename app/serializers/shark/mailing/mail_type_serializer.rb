module Shark
  class Mailing::MailTypeSerializer < ActiveModel::Serializer
    attributes :slug, :title
  end
end
