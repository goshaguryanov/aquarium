module Shark
  class SessionShowSerializer < ActiveModel::Serializer
    attributes :user_id, :account_id, :ip, :account_type, :access_rights

    def access_rights
      ar = object.access_rights
      if object.user_id.present?
        ar += object.user.access_rights
      end
      ar.uniq! do |a|
        if a.order_id.present?
          a.order_id
        else
          Time.now.to_f
        end
      end
      ar.map { |a| AccessRightSerializer.new(a) }
    end

    def ip
      object.ip.to_s
    end
    
    def account_type
      object.account.type if object.account
    end
  end
end
