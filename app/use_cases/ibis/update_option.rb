module Ibis
  class UpdateOption
    include Procto.call
    
    attr_reader :option
    
    def initialize(option_id, params)
      @option = Option.find(option_id)
      option.assign_attributes(params.slice(:type, :value))
    end
    
    def call
      Option.save_type_to_redis(option.type) if option.save
    end
    
    def errors
      option.errors
    end
  end
end
