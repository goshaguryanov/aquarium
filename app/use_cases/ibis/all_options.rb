module Ibis
  class AllOptions
    include Procto.call
    
    def call
      Option.all
    end
  end
end
