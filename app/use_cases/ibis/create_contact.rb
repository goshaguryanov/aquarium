module Ibis
  class CreateContact
    attr_reader :contact, :editor_id
    
    def initialize(attributes, editor_id)
      @contact = Contact.new(attributes)
      @editor_id = editor_id
    end
    
    def call
      if contact.save
        create_snapshot
      end
    end
    
    def errors
      contact.errors
    end
    
  private
    def create_snapshot
      contact.snapshots.create!(
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.contact_created',
        data:      contact.attributes
      )
    end
  end
end
