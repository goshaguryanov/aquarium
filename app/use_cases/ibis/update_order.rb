module Ibis
  class UpdateOrder
    include Procto.call
    
    attr_reader :order, :editor_id, :reader, :use_case_errors, :access_right
    
    ALLOWED_READER_TYPES = %w(User LegalEntity)
    
    def initialize(order_id, params, editor_id)
      @order = Order.find(order_id)
      @reader = get_reader(params[:reader])
      order.payment_status    = Order.payment_statuses[params[:payment_status]] if Order.payment_statuses.include?(params[:payment_status])
      order.account           = params[:account]
      order.start_date        = Time.zone.parse(params[:start_date]).beginning_of_day
      order.end_date          = Time.zone.parse(params[:end_date]).end_of_day
      order.subscribe_base_id = params[:subscribe_base_id]
      order.manager_comment   = params[:manager_comment]
      order.manager_status    = params[:manager_status]
      @editor_id = editor_id
    end
    
    def call
      Order.transaction do
        if order.save
          create_snapshot
          if reader && !order.access_rights.find { |ar| ar.reader==reader }
            if order.access_rights.find { |ar| ALLOWED_READER_TYPES.include?(ar.reader.class.name) }
              # already have access for user or legal_entity
              @use_case_errors = { reader: [ "нельзя добавить еще одного подписчика" ] }
              raise ActiveRecord::Rollback
            end
            
            access = Common::GrantAccess.new(
              reader:     reader,
              order:      order,
              start_date: order.start_date,
              end_date:   order.end_date
            )
            
            @access_right = access.access_right
            unless access.call
              raise ActiveRecord::Rollback
            end
          end
          order.grant_access
          return true
        end
      end
    end
    
    def errors
      if order.errors.present?
        order.errors
      elsif access_right
        access_right.errors
      else
        use_case_errors || {}
      end
    end
    
  private
    def get_reader(params)
      if params && ALLOWED_READER_TYPES.include?(params[:reader_type])
        Object.const_get(params[:reader_type]).find(params[:reader_id])
      end
    end
    
    def create_snapshot
      Snapshot.create!(
        subject:   order,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.order_updated',
        data:      order.attributes
      )
    end
  end
end
