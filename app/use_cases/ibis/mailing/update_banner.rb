module Ibis
  class Mailing::UpdateBanner
    include Procto.call
    
    attr_reader :banner, :editor_id
    
    def initialize(banner_id, params, editor_id)
      @editor_id = editor_id
      @banner = ::Mailing::Banner.find(banner_id)
      banner.assign_attributes(params.slice(:title, :body, :url, :type, :embed, :picture, :type_ids, :start_date, :end_date, :position))
    end
    
    def call
      if banner.save
        create_snapshot
      end
    end
    
    def errors
      banner.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   banner,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mailing_banner_updated',
        data:      banner.attributes
      )
    end
  end
end
