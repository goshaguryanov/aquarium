module Ibis
  class Mailing::BuildTemplate
    attr_reader :mailing, :template_kit, :editor_id, :error

    def initialize(mailing_id, template_kit, editor_id)
      @mailing = ::Mailing.find(mailing_id)
      @template_kit = template_kit
      @editor_id = editor_id
    end

    def call
      action_view = ActionView::Base.new(Rails.configuration.paths['app/views'])
      action_view.class_eval do
        include Rails.application.routes.url_helpers
        include ApplicationHelper
        
        def default_url_options
          { host: Settings.hosts.shark }
        end
        
        def protect_against_forgery?
          false
        end
      end
      
      template = action_view.render(
        partial: 'mailing_generators/template_kit/kit',
        format: :html,
        locals: { :@mailing => mailing, :@template_kit => template_kit.each { |item| Hashie::Mash.new(item.to_h) } }
      )

      mailing.data[:template_kit] = template_kit
      mailing.data_will_change!
      mailing.template = template

      if mailing.save
        mailing.snapshots.create!(
          event:     'build_template',
          author_id: editor_id,
          data:      mailing.attributes
        )
      else
        @error = mailing.errors.full_messages.join(', ')
        false
      end

    rescue => e
      @error = e.message
      false
    end
  end
end