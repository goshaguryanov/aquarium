module Ibis
  class Mailing::UpdateMailType
    attr_reader :mail_type, :editor_id
    
    def initialize(mail_type_id, params, editor_id)
      @editor_id = editor_id
      @mail_type = Mailing::Banner.find(mail_type_id)
      mail_type.assign_attributes(params.slice(:slug, :title))
    end
    
    def call
      if mail_type.save
        create_snapshot
      end
    end
    
    def errors
      mail_type.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   mail_type,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mailing_type_updated',
        data:      mail_type.attributes
      )
    end
  end
end
