module Ibis
  class Mailing::CreateBanner
    include Procto.call
    
    attr_reader :banner, :editor_id
    
    def initialize(params, editor_id)
      @editor_id = editor_id
      @banner = ::Mailing::Banner.new(params)
    end
    
    def call
      if banner.save
        create_snapshot
      end
    end
    
    def errors
      banner.errors
    end
  
  private
    def create_snapshot
      Snapshot.create!(
        subject:   banner,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mailing_banner_created',
        data:      banner.attributes
      )
    end
  end
end
