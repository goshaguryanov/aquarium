module Ibis
  module Reports
    class Create
      include Procto.call
      
      attr_reader :report, :editor_id

      def initialize(params, editor_id)
        @editor_id = editor_id
        @report = Report.new(
          start_date: Time.zone.parse(params[:start_date]).beginning_of_day,
          end_date:   Time.zone.parse(params[:end_date]).end_of_day,
          kind:       params[:kind]
        )
      end
      
      def call
        if report.save
          create_snapshot
          Resque.enqueue(Workers::ReportGeneratorJob, report.id)
          report
        end
      end

      def errors
        report.errors
      end
    private

      def create_snapshot
        Snapshot.create!(
          subject:   report,
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.report_created',
          data:      report.attributes
        )
      end
    end
  end
end
