module Ibis
  class AllMobileUsers
    include Procto.call
    
    attr :page
    
    def initialize(page)
      @page = page
    end
    
    def call
      MobileUser.order(id: :desc).page(page)
    end
  end
end
