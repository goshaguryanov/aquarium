class Ibis::NotReadyMailing
  attr_reader :mailing, :editor_id

  def initialize(id, editor_id)
    @mailing = ::Mailing.find(id)
    @editor_id = editor_id
  end

  def call
    if mailing.status != 'ready'
      @errors = [ "can't change status from '#{mailing.status}' to 'created'" ]
      false
    else
      mailing.status = 'created'
      mailing.save && create_snapshot
    end
  end

  def errors
    @errors || mailing.errors
  end

private
  def create_snapshot
    mailing.snapshots.create(
      author_id: editor_id,
      service:   'ibis',
      event:     'ibis.mailing_not_ready',
      data:      mailing.attributes
    )
  end
end
