module Ibis
  class AllOrders
    include Procto.call
    
    attr :page
    
    def initialize(page)
      @page = page
    end
    
    def call
      Order.order(id: :desc).page(page)
    end
  end
end
