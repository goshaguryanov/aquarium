module Ibis
  class CreateMobileUser
    include Procto.call
    
    attr_reader :mobile_user, :editor_id
    
    ALLOWED_ATTRIBUTES = %i(
      status
      first_name
      last_name
      msisdn
      operator
    )
    
    def initialize(mobile_user_attributes, editor_id)
      @mobile_user = MobileUser.new(mobile_user_attributes.slice(*ALLOWED_ATTRIBUTES))
      @editor_id = editor_id
    end
    
    def call
      if mobile_user.save
        create_snapshot
      end
    end
    
    def errors
      mobile_user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   mobile_user,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mobile_user_created',
        data:      mobile_user.attributes
      )
    end
  end
end
