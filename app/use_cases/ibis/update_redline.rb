module Ibis
  class UpdateRedline
    include Procto.call
    
    attr_reader :redline, :editor_id
    
    def initialize(redline_id, params, editor_id)
      @editor_id = editor_id
      @redline = Redline.find(redline_id)
      redline.assign_attributes(params.slice(:label, :status, :title, :url, :editor_id))
    end
    
    def call
      if redline.valid?
        # Redline.update_all(status: 'off')
        redline.save
        create_snapshot
      end
    end
    
    def errors
      redline.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject:   redline,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.redline_updated',
        data:      redline.attributes
      )
    end
  end
end
