module Ibis
  class UpdateMailing
    attr_reader :mailing, :editor_id
        
    def initialize(mailing_id, attributes, editor_id)
      @mailing = ::Mailing.find(mailing_id)
      mailing.assign_attributes(attributes.slice(*%i(type_id template subject)))
      mailing.scheduled_at = Time.zone.parse(attributes[:scheduled_at]) if attributes[:scheduled_at].present?
      if attributes[:preheader].present?
        mailing.data[:preheader] = attributes[:preheader]
        mailing.data_will_change!
      end
      @editor_id = editor_id
    end
    
    def call
      if mailing.save
        create_snapshot
      end
    end
    
    def errors
      mailing.errors
    end
    
  private
    def create_snapshot
      mailing.snapshots.create!(
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.mailing_updated',
        data:      mailing.attributes
      )
    end
  end
end
