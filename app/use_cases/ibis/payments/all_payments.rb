module Ibis
  module Payments
    class AllPayments
      include Procto.call

      attr :page, :mobile

      def initialize(page, mobile=false)
        @page = page
        @mobile = mobile
      end

      def call
        mobile ? Payment.order(id: :desc).mobile.page(page) : Payment.not_mobile.order(id: :desc).page(page)
      end
    end
  end
end
