module Ibis
  module Payments
    class UpdateProduct
      attr_reader :product, :editor_id
      
      def initialize(product_id, params, editor_id)
        @product = Product.find(product_id)
        product.assign_attributes(params.slice(:slug, :title))
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if product.save
      end

      def errors
        product.errors
      end

    private

      def create_snapshot
        product.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.product_updated',
          data:      product.attributes
        )
      end
    end
  end
end
