module Ibis
  module Payments
    class UpdateCity
      attr_reader :city, :editor_id
      
      def initialize(city_id, params, editor_id)
        @city = City.find(city_id)
        city.assign_attributes(params.slice(:title, :position, :contact))
        city.capability = City.capabilities[params[:capability]]
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if city.save
      end

      def errors
        city.errors
      end

    private

      def create_snapshot
        city.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.city_updated',
          data:      city.attributes
        )
      end
    end
  end
end
