module Ibis
  module Payments
    class DestroyCity
      attr_reader :city
      
      def initialize(city_id)
        @city = City.find(city_id)
      end
      
      def call
        city.snapshots.destroy_all && city.destroy
      end

      def errors
        city.errors
      end
    end
  end
end
