module Ibis
  module Payments
    class UpdateNamespace
      attr_reader :namespace, :editor_id
      
      def initialize(namespace_id, params, editor_id)
        @namespace = Namespace.find(namespace_id)
        namespace.assign_attributes(params.slice(:slug, :title, :redirect_to_id, :texts, :picture))
        namespace.start_date = Time.parse(params[:start_date])
        namespace.end_date   = Time.parse(params[:end_date])
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if namespace.save
      end

      def errors
        namespace.errors
      end

    private

      def create_snapshot
        namespace.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.namespace_updated',
          data:      namespace.attributes
        )
      end
    end
  end
end
