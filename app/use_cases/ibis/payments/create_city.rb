module Ibis
  module Payments
    class CreateCity
      attr_reader :city, :editor_id
      
      def initialize(params, editor_id)
        @city = City.new do |c|
          c.assign_attributes(params.slice(:title, :position, :contact))
          c.capability = City.capabilities[params[:capability]]
        end
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if city.save
      end

      def errors
        city.errors
      end

    private

      def create_snapshot
        city.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.city_created',
          data:      city.attributes
        )
      end
    end
  end
end
