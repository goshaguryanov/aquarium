module Ibis
  module Payments
    class CreatePaymentKit
      attr_reader :payment_kit, :editor_id
      
      def initialize(params, editor_id)
        @payment_kit = PaymentKit.new do |p|
          p.assign_attributes(params.slice(*%i(product_id namespace_id subproduct_id period_id price crossed_price discount upsell_id texts user_conditions renewal_id)))
          p.autopay_timeout = params[:autopay_timeout_hours] && params[:autopay_timeout_hours].hours
          p.syncable        = TypeConversion.to_bool(params[:syncable])
          p.alternate_ids   = params[:alternate_ids] if params[:alternate_ids]
          p.paymethod_ids   = params[:paymethod_ids] if params[:paymethod_ids]
          p.city_choice     = TypeConversion.to_bool(params[:city_choice])
          p.disabled        = TypeConversion.to_bool(params[:disabled])
        end
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if payment_kit.save
      end

      def errors
        payment_kit.errors
      end

    private

      def create_snapshot
        payment_kit.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.payment_kit_created',
          data:      payment_kit.attributes
        )
      end
    end
  end
end
