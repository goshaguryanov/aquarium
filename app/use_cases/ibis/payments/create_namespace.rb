module Ibis
  module Payments
    class CreateNamespace
      attr_reader :namespace, :editor_id
      
      def initialize(params, editor_id)
        @namespace = Namespace.new do |namespace|
          namespace.assign_attributes(params.slice(:product_id, :slug, :title, :redirect_to_id, :texts, :picture))
          namespace.start_date = Time.parse(params[:start_date])
          namespace.end_date   = Time.parse(params[:end_date])
        end
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if namespace.save
      end

      def errors
        namespace.errors
      end

    private

      def create_snapshot
        namespace.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.namespace_created',
          data:      namespace.attributes
        )
      end
    end
  end
end
