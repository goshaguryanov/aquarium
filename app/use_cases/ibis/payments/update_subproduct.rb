module Ibis
  module Payments
    class UpdateSubproduct
      attr_reader :subproduct, :editor_id
      
      def initialize(subproduct_id, params, editor_id)
        @subproduct = Subproduct.find(subproduct_id)
        subproduct.assign_attributes(params.slice(:slug, :title, :phone_text, :position))
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if subproduct.save
      end

      def errors
        subproduct.errors
      end

    private

      def create_snapshot
        subproduct.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.subproduct_updated',
          data:      subproduct.attributes
        )
      end
    end
  end
end
