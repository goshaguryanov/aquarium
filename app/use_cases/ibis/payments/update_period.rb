module Ibis
  module Payments
    class UpdatePeriod
      attr_reader :period, :editor_id
      
      def initialize(period_id, params, editor_id)
        @period = Period.find(period_id)
        period.assign_attributes(params.slice(:title))
        if %w(years months weeks days hours minutes seconds).include? params[:measure_unit]
          period.duration = params[:duration_number].to_i.send(params[:measure_unit].to_sym)
          period.slug = params.values_at(:duration_number, :measure_unit).join
        end
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if period.save
      end

      def errors
        period.errors
      end

    private

      def create_snapshot
        period.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.period_updated',
          data:      period.attributes
        )
      end
    end
  end
end
