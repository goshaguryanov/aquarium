module Ibis
  module Payments
    class UpdatePayment
      include Reader
      attr_reader :payment, :reader, :editor_id, :access_right, :use_case_errors
      
      def initialize(payment_id, params, editor_id)
        @reader = get_reader(params[:reader])
        @payment = Payment.find(payment_id)
        payment.payment_kit_id = params[:payment_kit_id]
        payment.paymethod      = paymethod
        payment.price          = payment.payment_kit.price
        payment.status         = Payment.statuses[params[:status]] if Payment.statuses.key?(params[:status])
        payment.start_date     = Time.zone.parse(params[:start_date]).beginning_of_day
        if params[:end_date].present?
          payment.end_date = Time.zone.parse(params[:end_date]).end_of_day
        else
          payment.end_date = (payment.start_date + payment.payment_kit.period.duration - 1.day).end_of_day
        end
        payment.manager_comment   = params[:manager_comment]
        payment.manager_status    = params[:manager_status]
        payment.account           = params[:account]
        payment.subscribe_base_id = params[:subscribe_base_id]

        @editor_id = editor_id
      end
      
      def call
        Payment.transaction do
          if payment.save
            create_snapshot
            if reader && !payment.access_rights.find { |ar| ar.reader==reader }
              if payment.access_rights.find { |ar| ALLOWED_READER_TYPES.include?(ar.reader.class.name) }
                # already have access for user or legal_entity
                @use_case_errors = { reader: [ "нельзя добавить еще одного подписчика" ] }
                raise ActiveRecord::Rollback
              end
              
              access = Common::GrantAccess.new(
                reader:     reader,
                payment:    payment,
                start_date: payment.start_date,
                end_date:   payment.end_date
              )
              
              @access_right = access.access_right
              unless access.call
                raise ActiveRecord::Rollback
              end
            end
            payment.grant_access
            return true
          end
        end
      end

      def errors
        if payment.errors.present?
          payment.errors
        elsif access_right && access_right.errors.present?
          access_right.errors
        else
          use_case_errors || {}
        end
      end

    private
      def create_snapshot
        payment.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.payment_updated',
          data:      payment.attributes
        )
      end

      def paymethod
        @paymethod ||= Paymethod.find_by_slug('other')
      end
    end
  end
end
