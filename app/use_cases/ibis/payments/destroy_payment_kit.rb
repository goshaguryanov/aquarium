module Ibis
  module Payments
    class DestroyPaymentKit
      attr_reader :payment_kit
      
      def initialize(payment_kit_id)
        @payment_kit = PaymentKit.find(payment_kit_id)
        @editor_id = editor_id
      end
      
      def call
        payment_kit.snapshots.destroy_all && payment_kit.destoy
      end

      def errors
        payment_kit.errors
      end
    end
  end
end
