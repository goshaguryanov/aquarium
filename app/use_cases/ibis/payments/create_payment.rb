module Ibis
  module Payments
    class CreatePayment
      include Reader
      attr_reader :payment, :reader, :editor_id, :access_right
      
      def initialize(params, editor_id)
        @reader = get_reader(params[:reader])
        @payment = Payment.paid.new do |p|
          p.payment_kit_id    = params[:payment_kit_id]
          p.paymethod         = paymethod
          p.price             = p.payment_kit.price
          p.start_date        = Time.zone.parse(params[:start_date]).beginning_of_day
          if params[:end_date].present?
            p.end_date = Time.zone.parse(params[:end_date]).end_of_day
          else
            p.end_date = (p.start_date + p.payment_kit.period.duration - 1.day).end_of_day
          end
          p.manager_comment   = params[:manager_comment]
          p.manager_status    = params[:manager_status]
          p.synced            = true
          p.account           = params[:account]
          p.subscribe_base_id = params[:subscribe_base_id]
        end
        @editor_id = editor_id
      end
      
      def call
        Payment.transaction do
          if payment.save
            create_snapshot
            access = Common::GrantAccess.new(
              reader:     reader,
              payment:    payment,
            )

            @access_right = access.access_right
            unless access.call
              raise ActiveRecord::Rollback
            end

            payment.grant_access

            #InnerMailer.new_subscription(payment).deliver
            payment
          end
        end
      end

      def errors
        if payment.errors.present?
          payment.errors
        elsif access_right && access_right.errors.present?
          access_right.errors
        else
          {}
        end
      end

    private
      def create_snapshot
        payment.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.payment_created',
          data:      payment.attributes
        )
      end

      def paymethod
        @paymethod ||= Paymethod.find_by_slug('other')
      end
    end
  end
end
