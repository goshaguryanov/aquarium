module Ibis
  module Payments
    class SetBaseNamespace
      attr_reader :namespace, :editor_id
      
      def initialize(namespace_id, editor_id)
        @namespace = Namespace.find(namespace_id)
        @editor_id = editor_id
      end
      
      def call
        namespace.product.namespaces.baselist.each do |n|
          n.base = false
          next unless n.changed?
          if n.save
            n.snapshots.create!(
              author_id: editor_id,
              service:   'ibis',
              event:     'ibis.namespace_remove_base_flag',
              data:      n.attributes
            )
          else
            @error = "Can't remove base flag from namespace #{n.id}"
            return false
          end
        end
        create_snapshot if namespace.update(base: true)
      end

      def error
        @error || namespace.errors.full_messages.join(', ')
      end

    private

      def create_snapshot()
        namespace.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.namespace_set_base_flag',
          data:      namespace.attributes
        )
      end
    end
  end
end
