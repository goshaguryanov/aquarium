module Ibis
  module Payments
    class CreateSubproduct
      attr_reader :subproduct, :editor_id
      
      def initialize(params, editor_id)
        @subproduct = Subproduct.new(params.slice(:product_id, :slug, :title, :phone_text, :position))
        @editor_id = editor_id
      end
      
      def call
        create_snapshot if subproduct.save
      end

      def errors
        subproduct.errors
      end

    private

      def create_snapshot
        subproduct.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.subproduct_created',
          data:      subproduct.attributes
        )
      end
    end
  end
end
