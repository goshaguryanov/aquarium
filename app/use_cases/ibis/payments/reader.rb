module Ibis
  module Payments
    module Reader
      ALLOWED_READER_TYPES = %w(MobileUser User LegalEntity)

    private
      def get_reader(params)
        if params && ALLOWED_READER_TYPES.include?(params[:reader_type])
          Object.const_get(params[:reader_type]).find(params[:reader_id])
        end
      end
    end
  end
end
