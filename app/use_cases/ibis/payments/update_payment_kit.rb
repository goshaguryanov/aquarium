module Ibis
  module Payments
    class UpdatePaymentKit
      attr_reader :payment_kit, :editor_id
      
      def initialize(payment_kit_id, params, editor_id)
        @payment_kit = PaymentKit.find(payment_kit_id)
        payment_kit.assign_attributes(params.slice(*%i(product_id namespace_id subproduct_id period_id price crossed_price discount upsell_id alternate_ids paymethod_ids texts user_conditions renewal_id)))
        payment_kit.autopay_timeout = params[:autopay_timeout_hours] && params[:autopay_timeout_hours].hours
        payment_kit.syncable        = TypeConversion.to_bool(params[:syncable])
        payment_kit.alternate_ids   = params[:alternate_ids] ? params[:alternate_ids] : []
        payment_kit.paymethod_ids   = params[:paymethod_ids] ? params[:paymethod_ids] : []
        payment_kit.city_choice     = TypeConversion.to_bool(params[:city_choice])
        payment_kit.disabled        = TypeConversion.to_bool(params[:disabled])

        @editor_id = editor_id
      end
      
      def call
        create_snapshot if payment_kit.save
      end

      def errors
        payment_kit.errors
      end

    private

      def create_snapshot
        payment_kit.snapshots.create!(
          author_id: editor_id,
          service:   'ibis',
          event:     'ibis.payment_kit_updated',
          data:      payment_kit.attributes
        )
      end
    end
  end
end
