module Ibis
  class AllUsers
    include Procto.call
    
    attr :page
    
    def initialize(page)
      @page = page
    end
    
    def call
      User.order(id: :desc).page(page)
    end
  end
end
