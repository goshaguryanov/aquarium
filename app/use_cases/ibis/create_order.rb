module Ibis
  class CreateOrder
    include Procto.call
    
    attr_reader :editor_id, :reader, :order, :access_right

    ALLOWED_READER_TYPES = %w(User LegalEntity)
    
    def initialize(params, editor_id)
      @editor_id = editor_id
      @reader = get_reader(params[:reader])
      @order = Order.paid.new do |order|
        order.payment_method    = Order.payment_methods[:by_unknown]
        order.start_date        = Time.zone.parse(params[:start_date]).beginning_of_day
        order.end_date          = Time.zone.parse(params[:end_date]).end_of_day
        order.manager_comment   = params[:manager_comment]
        order.manager_status    = params[:manager_status]
        order.synced            = true
        order.account           = params[:account]
        order.subscribe_base_id = params[:subscribe_base_id]

        price_params = params.extract!(:product, :period, :student, :price_id)
        Common::SetPriceForOrder.new(order, price_params).call
      end
    end
    
    def call
      Order.transaction do
        if order.save
          create_snapshot
          access = Common::GrantAccess.new(
            reader:     reader,
            order:      order,
            start_date: order.start_date,
            end_date:   order.end_date
          )

          @access_right = access.access_right
          unless access.call
            raise ActiveRecord::Rollback
          end
          # InnerMailer.new_subscription(order).deliver
          order
        end
      end
    end
    
    def errors
      if order.errors.present?
        order.errors
      elsif access_right
        access_right.errors
      else
        {}
      end
    end

  private
    def get_reader(params)
      if params && ALLOWED_READER_TYPES.include?(params[:reader_type])
        Object.const_get(params[:reader_type]).find(params[:reader_id])
      end
    end

    def create_snapshot
      Snapshot.create!(
        subject:   order,
        author_id: editor_id,
        service:   'ibis',
        event:     'ibis.order_created',
        data:      order.attributes
      )
    end
  end
end
