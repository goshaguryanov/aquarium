class Ibis::ReadyMailing
  attr_reader :mailing, :editor_id

  def initialize(id, editor_id)
    @mailing = ::Mailing.find(id)
    mailing.status = 'ready'

    @editor_id = editor_id
  end

  def call
    mailing.save && create_snapshot
  end

  def errors
    mailing.errors
  end

private
  def create_snapshot
    mailing.snapshots.create(
      author_id: editor_id,
      service:   'ibis',
      event:     'ibis.mailing_ready',
      data:      mailing.attributes
    )
  end
end
