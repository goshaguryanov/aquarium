module Lago
  module Code
    class Create < Base

      attr_reader :mobile_user, :msisdn

      def initialize(msisdn, access_token, return_url)
        @access_token = access_token
        @msisdn = redis_token.blank? ? msisdn : redis_token['msisdn']
        @mobile_user = MobileUser.find_by_msisdn(@msisdn)
        @return_url = return_url
        @errors = []
      end

      def call
        if mobile_user
          if mobile_user.has_active_access_rights?
            save_to_redis!
            create_otp_code!
            @redirect = [Settings.hosts.metynnis, 'id', 'mobile', 'codes', "confirm?phone=#{@msisdn}"].join('/')
            Resque.enqueue(Workers::SendSmsJob, mobile_user.msisdn, mobile_user.operator, sms_message)
          else
            @errors.push(:inactive)
            false
          end
        else
          @errors.push(:not_found)
          false
        end
      end

      private

      def create_otp_code!
        @code ||= ROTP::TOTP.new(otp_token).now
      end

      def sms_message
        "Ваш код восстановления подписки: #{code}. Никому не сообщайте код!"
      end

      def save_to_redis!
        redis_client.del(redis_key)
        redis_client.mapped_hmset(redis_key, {
            otp_token: otp_token,
            msisdn: mobile_user.msisdn,
            operator: mobile_user.operator,
            return_url: @return_url
        }
        )
        redis_client.expire(redis_key, CODE_LIFETIME)
      end
    end
  end
end
