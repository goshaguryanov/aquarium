module Lago
  module Code
    class Check < Base

      attr_reader :msisdn

      def initialize(code, access_token, client_ip)
        @code = code
        @access_token = access_token
        @client_ip = client_ip
      end

      def call
        if !redis_token.blank? && ROTP::TOTP.new(redis_token['otp_token']).verify_with_drift(code, CODE_LIFETIME)
          @msisdn = redis_token['msisdn']
          use_case = Common::MobileUser.new(msisdn, redis_token['operator'], access_token, @client_ip)
          unless use_case.call
            @errors = use_case.errors
            return false
          end
          @redirect = redis_token['return_url']
          @redirect = Settings.hosts.shark if !@redirect || @redirect.empty?
          redis_client.del(redis_key)
        else
          @errors = ['Неверный одноразовый пароль!']
          return false
        end
        true
      end
    end
  end
end
