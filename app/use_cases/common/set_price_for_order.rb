module Common
  class SetPriceForOrder
    attr_reader :order, :price
    
    def initialize(order, attributes)
      @order = order
      attributes = attributes.symbolize_keys
      if attributes[:price_id]
        @price = Price.find(attributes[:price_id])
      else
        @price = Price.active.where(convert_price_attributes(attributes)).order(:since, :created_at).first
        unless @price
          PaymentProcess.logger.error("Use case '#{self.class.name}' set price failed. price_params: #{attributes}")
        end
      end
    end
    
    def call
      order.price = price
    end

  private
    def convert_price_attributes(attrs)
      {
        product: (Price.products[attrs[:product]] if attrs[:product]),
        period:  (Price.periods[attrs[:period]] if attrs[:period]),
        student: TypeConversion.to_bool(attrs[:student])
      }
    end
  end
end
