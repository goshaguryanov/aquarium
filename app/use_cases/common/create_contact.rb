# Этот use_case используется в случае неавторизированной подписки, поэтому он всегда будет высылать подтверждение
module Common
  class CreateContact
    attr_reader :contact, :type_slugs, :token
    
    def initialize(attributes, type_slugs=[])
      attributes[:email].downcase! if attributes[:email]
      @contact    = Contact.find_or_initialize_by(attributes)
      @type_slugs = type_slugs
    end
    
    def call
      type_ids = mail_types.pluck(:id)
      if type_slugs.present?
        if type_ids.size != type_slugs.size
          @use_case_errors = ['invalid mailing type(s)']
          return false
        end

        if Set.new(type_ids) <= Set.new(contact.type_ids)
          @use_case_errors = ['already subscribed']
          return false
        end
      end

      if contact.new_record?
        if contact.valid? && contact.save
          create_snapshot
        else
          return false
        end
      end

      send_confirmation_email(type_ids) if type_ids.present?
      true
    end
    
    def errors
      @use_case_errors || contact.errors.to_a
    end

    def mail_types
      @mail_types ||= Mailing::MailType.where(slug: type_slugs)
    end
    
  private
    def create_snapshot
      contact.snapshots.create!(
        event:   'contact_created',
        data:    contact.attributes
      )
    end
    
    def send_confirmation_email(type_ids)
      @token = UserToken.contact_confirmation.generate(contact_id: contact.id, type_ids: type_ids)
      Resque.enqueue(Workers::SubscriberMailer::ConfirmationEmailJob, @token.id)
    end
  end
end
