module Common
  class GrantAccess
    attr_reader :access_right
    
    def initialize(attributes)
      access_parameters = attributes.slice(:reader, :order, :start_date, :end_date, :payment)
      @access_right = AccessRight.new(access_parameters)
    end
    
    def call
      if access_right.save
        access_right.dump_to_redis
        access_right.confirm_users
        create_snapshot
      end
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: access_right,
        event:   'access_granted',
        data:    access_right.attributes
      )
    end
  end
end
