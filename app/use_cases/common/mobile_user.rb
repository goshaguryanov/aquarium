module Common
  class MobileUser

    attr_reader :mobile_user, :session, :attributes, :errors

    def initialize(msisdn, operator, access_token, client_ip)
      @mobile_user = ::MobileUser.find_or_initialize_by(msisdn: msisdn)
      @mobile_user.operator = operator if operator
      @access_token = access_token
      @client_ip = client_ip
    end

    def call
      unless @access_token.blank?
        use_case = ::Shark::RestoreSession.new(access_token: @access_token, ip: @client_ip)
        if use_case.call(persist: true)
          @session = use_case.session
          ::Shark::Sessions::DestroyMobileUserSession.new(mobile_user.id).call
        else
          @errors = use_case.errors
          return false
        end
      end

      mobile_user.sessions << session if session
      event = nil
      event = 'created' if mobile_user.new_record?
      event = 'updated' if mobile_user.changed?
      if mobile_user.save
        session.dump_to_redis if session
        create_snapshot(event) if event
        true
      else
        @errors = mobile_user.errors.to_a
        false
      end
    end

      def create_snapshot(event)
        mobile_user.snapshots.create!(
            event:   "mobile_user_#{event}",
            data:    mobile_user.attributes
        )
      end
  end
end
