module Common
  class CreateLegalEntity
    attr_reader :legal_entity
    
    def initialize(attributes)
      @legal_entity = LegalEntity.new(attributes.slice(:name, :ips, :user_ids))
    end
    
    def call
      legal_entity.save && create_snapshot
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: legal_entity,
        event:   'legal_entity_created',
        data:    legal_entity.attributes
      )
    end
  end
end
