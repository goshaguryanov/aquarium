module Common
  class UpdatePrice
    attr_reader :price
    
    def initialize(price_id, attributes)
      @price = Price.find(price_id)
      price.assign_attributes(attributes)
    end
    
    def call
      if price.save
        create_snapshot
        true
      end
    end
    
    def errors
      price.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: price,
        event:   'price_updated',
        data:    price.attributes
      )
    end
  end
end
