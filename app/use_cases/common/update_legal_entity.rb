module Common
  class UpdateLegalEntity
    attr_reader :legal_entity, :old_ips
    
    def initialize(entity_id, attributes)
      @legal_entity = LegalEntity.find(entity_id)
      @old_ips      = legal_entity.raw_ips
      
      legal_entity.assign_attributes(attributes.slice(:name, :ips, :user_ids))
    end
    
    def call
      obsolete_ips = old_ips - legal_entity.ips
      
      if legal_entity.save
        legal_entity.dump_sessions_to_redis
        create_snapshot
        
        if legal_entity.subscriber? && !obsolete_ips.empty?
          LegalEntity.remove_ips_from_redis(obsolete_ips)
        end
      end
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: legal_entity,
        event:   'legal_entity_updated',
        data:    legal_entity.attributes
      )
    end
  end
end
