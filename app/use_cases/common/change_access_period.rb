module Common
  class ChangeAccessPeriod
    attr_reader :access_right
    
    def initialize(access_right_id, attributes)
      @access_right = AccessRight.find(access_right_id)
      access_right.assign_attributes(attributes.slice(:start_date, :end_date))
    end
    
    def call
      if access_right.save
        access_right.dump_to_redis
        access_right.confirm_users
        create_snapshot
      end
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: access_right,
        event:   'access_period_changed',
        data:    access_right.attributes
      )
    end
  end
end
