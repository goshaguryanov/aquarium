module Common
  class CreateOrder
    attr_reader :price_params, :params, :user, :client_ip
    
    def initialize(params, user, client_ip = nil)
      @price_params = params.extract!(:product, :period, :student, :price_id)
      @params       = params
      @user         = user
      @client_ip    = client_ip
    end
    
    def call
      order.client_ip = client_ip if client_ip
      Common::SetPriceForOrder.new(order, price_params).call

      #set start_date and end_date when it's empty
      order.start_date = start_date unless order.start_date
      order.end_date = end_date unless order.end_date
      
      if order.save && order.payment
        Common::GrantAccess.new(order: order, reader: user).call
        Snapshot.create!(
          subject: order,
          author:  user,
          event:   'common_create_order',
          data:    order.attributes
        )
        order
      end
    end

    def start_date
      last_access_right = order.price.paper? ? nil : user.access_rights.where("end_date IS NOT NULL").order(end_date: :desc).first
      products = order.price.profi? ? Price.products.values : [Price.products[order.price.product], Price.products[:profi]]
      last_order = user.orders.paid.joins(:price).where("prices.product in (?)", products).order(end_date: :desc).first
      [ last_access_right && next_day(last_access_right.end_date), last_order && next_day(last_order.end_date), Time.now ].compact.max
    end

    def end_date
      ((order.start_date || start_date) + order.price.period_seconds - 1.day).end_of_day
    end

    def next_day(date)
      date && ((date + 1.day).beginning_of_day)
    end
    
    def order
      @order ||= Order.new(params)
    end
  end
end
