module Shark
  class AcceptPromo
    attr_reader :user
    
    def initialize(user, promo_name)
      @user = user
      
      user.accepted_promo = user.accepted_promo.merge(
        promo_name => Time.now.to_i
      )
    end
    
    def call
      if user.save
        user.dump_sessions_to_redis
        create_snapshot
      end
    end
    
    def errors
      user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: user,
        author:  user,
        service: 'shark',
        event:   'shark.promo_accepted',
        data:    user.attributes
      )
    end
  end
end
