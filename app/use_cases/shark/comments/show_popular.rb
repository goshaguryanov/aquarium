module Shark
  module Comments
    class ShowPopular
      attr_reader :widget_id, :xid, :comment

      def initialize(widget_id, xid)
        @widget_id = widget_id
        @xid = xid
      end

      def call
        @comment = PopularComment.find_or_create_by(widget_id: widget_id, xid: xid)
        if @comment.updated_at < 10.minutes.ago
          Resque.enqueue(Workers::Comments::UpdatePopular, widget_id, xid)
        end
      end
    end
  end
end
