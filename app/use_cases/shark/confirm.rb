module Shark
  class Confirm
    attr_reader :token_string
    
    def initialize(params)
      @token_string = params.require(:token)
    end
    
    def valid?
      token.present?
    end
    
    def call
      ActiveRecord::Base.transaction do
        token.user.update(status: 'confirmed')
        create_snapshot
        token.user.dump_sessions_to_redis
        mailing_confirm
        token.update(used: true)
        UserMailer.welcome_email(token.user).deliver
      end
    end

    def mailing_confirm
      if valid? && token.token_data['mailing_type_ids']
        use_case = ::Common::CreateContact.new({ email: token.user.email })
        if use_case.call
          contact = use_case.contact
          contact.update(
            contact_status: contact.deleted? ? 'deleted' : 'confirmed',
            subscription_status: 'on',
            type_ids: contact.type_ids | token.token_data['mailing_type_ids']
          )
          contact.snapshots.create!(
            event: 'subscribe_confirm',
            data:  contact.attributes
          )
        end
      end
    end

  private
    def token
      @token ||= UserToken.confirmation.unused.find_by(token: token_string)
    end

    def create_snapshot
      token.user.snapshots.create!(
        event: 'confirm',
        data:  token.user.attributes
      )
    end
  end
end
