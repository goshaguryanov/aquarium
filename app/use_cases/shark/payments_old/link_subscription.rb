module Shark
  class PaymentsOld
    class LinkSubscription < PaymentsOld
      include ::Payments::Logger
      attr_reader :session, :user, :status
      
      def initialize(session, user)
        @session = session
        @user    = user
        @status  = :ok
      end
      
      def call
        logger.info { "Use case: #{self.class.name}, parameters: {session: #{session.id}, user_id: #{user.id}}" }
        
        # ищем активный AccessRight для этой сессии
        unless order
          logger.error { "Use case: #{self.class.name}, error: Can't find active order for session: session: #{session.id}" }
          return error_response(:unprocessable_entity, 'You have no subscription')
        end
        
        if users.count>0 && !users.include?(user)
          logger.error { "Use case: #{self.class.name}, error: Already linked to another user. Session: #{session.id}, user_id: #{user.id}, another user id: #{users.map(&:id).join(',')}" }
          return error_response(:unprocessable_entity, 'Subscription already linked')
        end
        
        if users.empty?
          Common::GrantAccess.new(order: order, reader: user, start_date: order.start_date, end_date: order.end_date).call
        end
        
        order.payment.return_redirect(path: 'subscription_ok')
      end
      
      def access_right
        @access_right ||= session.access_rights.active.first
      end
      
      def order
        @order ||= (access_right && access_right.order)
      end
      
      def users
        @users ||= (order && order.users)
      end
    end
  end
end
