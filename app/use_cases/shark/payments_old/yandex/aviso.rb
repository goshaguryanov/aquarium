module Shark
  class PaymentsOld
    module Yandex
      class Aviso < PaymentsOld
        include ::Payments::Logger
        attr_reader :params, :transaction_id, :client_ip
        
        def initialize(params, client_ip)
          @params         = params
          @transaction_id = params.require(:invoiceId)
          @client_ip      = client_ip
          @status         = :ok
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {params: #{params}, client_ip: #{client_ip}}" }
          if order && order.payment && order.payment.respond_to?(:aviso)
            if response = order.payment.aviso(params, client_ip)
              response
            else
              logger.error { "Use case: #{self.class.name}, error: Forbidden, transaction_id: #{transaction_id}" }
              error_response :forbidden, 'Forbidden'
            end
          else
            logger.error { "Use case: #{self.class.name}, error: Order not found, transaction_id: #{transaction_id}" }
            error_response :not_found, 'Order not found'
          end
        end
        
        def order
          @order ||= Order.by_yandex.find_by_transaction_id(transaction_id)
        end
      end
    end
  end
end
