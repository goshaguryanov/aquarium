module Shark
  class PaymentsOld
    module Card
      class Check < PaymentsOld
        attr_reader :uuid
        
        def initialize(uuid)
          @uuid   = uuid
          @status = :ok
        end
        
        def call
          if !order
            error_response :not_found, 'Order not found'
          elsif order.payment && order.payment.respond_to?(:check)
            order.payment.check
          else
            error_response :unprocessable_entity, 'Invalid order'
          end
        end
        
        def order
          order ||= Order.by_card.find_by_uuid(uuid) 
        end
      end
    end
  end
end
