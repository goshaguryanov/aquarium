module Shark
  class PaymentsOld
    module Card
      class CreateOrder < PaymentsOld
        include ::Payments::Logger
        attr_reader :params, :user, :client_ip
        
        def initialize(params, user, client_ip)
          auto_renewing = params.extract!(:auto_renewing)[:auto_renewing]
          @params    = params.merge(payment_method: :by_card, data: { auto_renewing: auto_renewing.nil? || TypeConversion.to_bool(auto_renewing) })
          @user      = user
          @client_ip = client_ip
          @status    = :ok
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {params: #{params}, user_id: #{user.id}, client_ip: #{client_ip}}" }
          if order = Common::CreateOrder.new(params, user, client_ip).call
            order.payment.do_async(:start)
          else
            error = 'Invalid order'
            logger.error { "Use case: #{self.class.name}, error: #{error}" }
            error_response :unprocessable_entity, error
          end
        end
      end
    end
  end
end
