module Shark
  class PaymentsOld
    module Google
      class VerifyReceipt < PaymentsOld
        include ::Payments::Logger
        attr_reader :subscription_id, :token, :session, :client_ip
        
        def initialize(subscription_id, token, session, client_ip)
          @subscription_id = subscription_id
          @token        = token
          @session      = session
          @client_ip    = client_ip
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {subscription_id: #{subscription_id}, token: #{token}, session_id: #{session.id}, client_ip: #{client_ip}}" }
          Resque.enqueue(Workers::Payments::GoogleVerify, subscription_id, token, session.id, client_ip)
        end
      end
    end
  end
end
