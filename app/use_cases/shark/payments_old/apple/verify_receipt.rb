module Shark
  class PaymentsOld
    module Apple
      class VerifyReceipt < PaymentsOld
        include ::Payments::Logger

        attr_reader :receipt, :session, :client_ip
        
        def initialize(receipt, session, client_ip)
          @receipt   = receipt
          @session   = session
          @client_ip = client_ip
        end
        
        def call
          logger.info { "Use case: #{self.class.name}, parameters: {session_id: #{session.id}, client_ip: #{client_ip}}" }
          Resque.enqueue(Workers::Payments::AppleVerify, receipt, session.id, client_ip)
        end
      end
    end
  end
end
