module Shark
  class NewsreleaseZip
    include ActionView::Helpers::SanitizeHelper
    include Procto.call

    attr_reader :date, :newsrelease, :documents, :pages, :url, :target_filename

    TMP_PATH              = Pathname.new('/tmp').freeze
    NEWSPAPER_SOURCE_PATH = TMP_PATH.join('newspaper').freeze
    
    def initialize(params, image_type)
      @date            = newsrelease_date(params.symbolize_keys)
      @image_type      = image_type
      @newsrelease     = newsrelease
      @zip_filename    = zip_filename
      @target_filename = TMP_PATH.join(@zip_filename)
    end
    
    def call
      if newsrelease
        level = Rails.logger.level
        Rails.logger.level = 0
        newspaper = Newspaper.find_or_initialize_by(document_id: @newsrelease.id)
        prepare_filesystem
        @documents = Roompen.bound_documents(@newsrelease.id)
        fill_newspaper_pages(@documents)
        save_newstand_images(@newsrelease)
        download(documents)
        File.write(newspaper_source_path.join('newspaper.json'), newspaper_content)
        create_zip(target_filename)

        @url = "#{Settings.hosts.cdn}#{@url_path}"

        # update Newspaper @ aquarium
        newspaper.release_date = @newsrelease.published_at
        newspaper.urls[@image_type] = @url
        newspaper.urls_will_change!
        newspaper.save

        # update Document @ snipe
        create_box(root_box, @image_type, create_attachment)
        document_publish

        FileUtils.rm_rf @target_filename
        Rails.logger.level = level
      end
    end
    
    def newsrelease
      if date.nil?
        last_newsrelease
      else
        begin
          Roompen.document(newspaper_url)
        rescue Roompen::NotFound
          nil
        end
      end
    end
    
  private
    def prepare_filesystem
      FileUtils.rm_rf   @target_filename
      FileUtils.rm_rf   newspaper_source_path
      FileUtils.mkdir_p documents_source_path
      FileUtils.mkdir_p images_source_path
    end

    def newspaper_source_path
      NEWSPAPER_SOURCE_PATH.join(newsrelease.id.to_s)
    end

    def documents_source_path
      newspaper_source_path.join('documents')
    end

    def images_source_path
      newspaper_source_path.join('images')
    end
    
    def download(documents)
      documents.each do |document|
        document = Roompen.document_by_id(document.id)
        download_document(document.id)
        convert_html(document.id)
        save_document_image(document, @image_type)
        save_document_portrait(document, @image_type)
        save_document_boxes_images(document, @image_type)
      end
    end
    
    def last_newsrelease
      newsreleases = Roompen.categorized(%w(kinds newsrelease), without: [%w(parts news)], limit: 1)
      if newsreleases.one?
        newsreleases.first
      else
        raise ActionController::RoutingError.new('Not Found')
      end
    end
    
    def newsrelease_date(params)
      if [:year, :month, :day].all? { |key| params[key] }
        Date.new(*params.values_at(:year, :month, :day).map(&:to_i))
      else
        Date.today
      end
    end
    
    def newspaper_url
      [Settings.domain, 'newspaper', date.strftime('%Y/%m/%d')].join('/')
    end
    
    def newspaper_content
      Oj.dump({ newsrelease: @newsrelease, pages: pages })
    end
  
    def fill_newspaper_pages(documents)
      categories = Roompen.child_categories_of(%w(newspaper))
      hdocs = {}
  
      documents.reverse_each do |doc|
        if doc.categories.has_newspaper?
          hdocs[doc.categories.newspaper.slug] ||= []
          hdocs[doc.categories.newspaper.slug] << doc
        end
      end
  
      @pages = []
      categories.each do |category|
        if hdocs.has_key?(category.slug)
          @pages << Roompen::List.new({
            title: category.title,
            header: category.title,
            documents: hdocs[category.slug]
          })
        end
      end
    end

    def save_newstand_images(newsrelease)
      if newsrelease.has_newsstand?
        [
          :iphone_multitasking_1x,
          :iphone_multitasking_2x,
          :ipad_multitasking_1x,
          :ipad_multitasking_2x,
          :ipad_shelf_1x,
          :ipad_shelf_2x,
          :iphone_shelf_1x,
          :iphone_shelf_2x,
          :source
        ].each do |version|
          if newsrelease.newsstand.has_version?(version)
            download_image(newsrelease.newsstand.send(version).url)
          end
        end
      end
    end
    
    def save_document_image(document, image_type)
      save_object_image(document, image_type.to_sym)
    end

    def save_document_portrait(document, image_type)
      save_object_portrait(document, image_type.to_sym)
    end

    def save_document_boxes_images(document, image_type)
      type = image_type.to_sym
      document.boxes.each do |box|
        case box.type
        when 'inset_image', 'chronology', 'quote', 'inset_text'
          save_object_image(box, type)
        when 'gallery'
          box.children.each do |gallery_item|
            save_object_image(gallery_item, type)
          end
        end
      end
    end

    def save_object_image(object, image_type)
      if object.has_image? && object.image.has_version?(image_type)
        download_image(object.image.send(image_type).url)
      end
    end

    def save_object_portrait(object, image_type)
      if object.has_portrait? && object.portrait.has_version?(image_type)
        download_image(object.portrait.send(image_type).url)
      end
    end
    
    def download_image(url)
      cmd = %Q(cd #{images_source_path} && wget -xnH -nv #{Settings.hosts.agami}#{url})
      Rails.logger.debug("Download image: #{cmd}")
      system(cmd)
    end
    
    def download_document(document_id)
      cmd = %Q(cd #{documents_source_path} && wget -nv -O #{document_id}.json #{Settings.hosts.tanagra}/documents/#{document_id})
      Rails.logger.debug(cmd)
      system(cmd)
    end
    
    def zip_filename
      "newspaper_#{@newsrelease.id}.zip"
    end

    def create_zip(filename)
      Zipper.new(newspaper_source_path, filename).write
      FileUtils.chmod(0644, filename)
      
      @upload_response = AgamiUploader.upload(filename.to_s)
      Rails.logger.debug(@upload_response)
      if @upload_response.success?
        @file_md5 = Digest::MD5.file(filename.to_s).hexdigest
        @url_path = @upload_response.body[:versions][:original][:url]
      end
      
      FileUtils.rm(filename)
    end
    
    def convert_html(document_id)
      filename = "#{documents_source_path}/#{document_id}.json"
      
      document_data = Oj.load(File.read(filename))
      document_data[:boxes] = unescape_html(document_data[:boxes])
      
      File.write(filename, Oj.dump(document_data))
    end
    
    def unescape_html(boxes)
      coder = HTMLEntities.new
      
      boxes.map do |box|
        if box.has_key?(:body)
          box.merge(body: strip_tags(coder.decode(box[:body])))
        else
          box
        end
      end
    end
    
    def root_box(document_id = @newsrelease.id)
      snipe_client.get("/v1/documents/#{document_id}/root_box").body
    end
    
    def create_box(root_box, version, attachment)
      data = {
        attributes: {
          type:      'newsrelease_mobile_archive',
          parent_id: root_box[:id],
          position:  1,
          enabled:   true,
          version:   version,
          zip_id:    attachment[:id]
        }
      }
      
      Rails.logger.debug("Creating box for version #{version}")
      snipe_client.post('/v1/boxes', data).body
    end
    
    def create_attachment(upload_data = @upload_response.body)
      Rails.logger.debug("Creating attachment for #{zip_filename}")
      snipe_client.post('/v1/attachments', attributes: upload_data).body
    end
    
    def document_publish(document_id = @newsrelease.id)
      Rails.logger.debug("Publish document")
      snipe_client.patch("/v1/documents/#{document_id}/publish").body
    end
    
    def snipe_client
      @snipe_client ||= SnipeClient.new
    end
  end
end
