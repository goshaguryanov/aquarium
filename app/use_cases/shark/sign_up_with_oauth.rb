module Shark
  class SignUpWithOauth
    attr_reader :user, :account, :update_session, :mailing_type_ids
    
    PERMITTED_ATTRIBUTES = %i(
      first_name
      last_name
      nickname
      email
      phone
    )
    
    def initialize(params, ip)
      @user = build_from(params)
      
      @account = user.accounts.build do |account|
        account.type        = params.fetch(:provider)
        account.external_id = params.fetch(:external_id)
      end

      @update_session = UpdateSession.new(params.fetch(:access_token), ip)

      mailing_type_slugs = Array(params[:mailing_type_slugs]) + [ 'marketing' ]
      @mailing_type_ids = ::Mailing::MailType.where(slug: mailing_type_slugs).pluck(:id)
    end
    
    def save
      if user.save
      ::UserMailer.send_oath_confirmation_email(token_data).deliver
        update_session.call(
          user_id:    user.id,
          account_id: account.id
        )
        
        if contact
          contact.update(user_id: user.id)
          create_contact_snapshot('contact_updated')
        else
          user.create_contact(email: user.email)
          create_contact_snapshot('contact_created')
        end
        
        create_user_snapshot

        notify_newrelic
      end
    end
    
    def errors
      user.errors
    end
    
  private
    def build_from(params)
      User.new(params.slice(*PERMITTED_ATTRIBUTES))
    end
    
    def create_user_snapshot
      Snapshot.create!(
        subject: user,
        author:  user,
        service: 'shark',
        event:   'shark.user_signed_up_with_oauth',
        data:    user.attributes
      )
    end
    
    def create_contact_snapshot(event)
      user.contact.snapshots.create!(
        event:   event,
        data:    contact.attributes
      )
    end
    
    def notify_newrelic
      ::NewRelic::Agent.increment_metric('Custom/OAuthSignUp')
    end

    def token_data
      if mailing_type_ids.present?
        { mailing_type_ids: mailing_type_ids }
      else
        {}
      end
    end
    
    def contact
      Contact.find_by(email: user.email)
    end
  end
end
