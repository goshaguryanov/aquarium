module Shark
  class UpdateSession
    include Procto.call
    
    attr_reader :session
    
    def initialize(access_token, ip)
      @session = Session.find_or_initialize_by(access_token: access_token)
      
      type = Aquarium::Redis.connection.type(session_key)
      
      case type
      when 'hash'
        session.ip = Aquarium::Redis.connection.hget(session_key, :ip)
      when 'string'
        json = Aquarium::Redis.connection.get(session_key)
        session.ip = Oj.load(json)[:ip]
      else
        session.ip = ip
      end
    end
    
    def call(params = {})
      session.assign_attributes(params.slice(:user_id, :account_id))
      
      if session.save
        session.dump_to_redis
        Resque.enqueue(Workers::PurgeCacheJob, session.access_token)
        ::NewRelic::Agent.increment_metric('Custom/Sessions/create_in_pg')
      end
    end
    
  private
    def session_key
      "sessions:#{session.access_token}"
    end
  end
end
