module Shark
  class ResetPassword
    attr_reader :token, :update_session
    
    def initialize(password_recovery_token, access_token, ip)
      @token = UserToken.password_recovery.unused.find_by!(token: password_recovery_token)
      @update_session = UpdateSession.new(access_token, ip)
    end
    
    def call(new_password)
      ActiveRecord::Base.transaction do
        change_password(new_password)
        token.update(used: true)
      end
    end
    
  private
    def change_password(new_password)
      account.password = new_password
      account.save!
      
      update_session.call(
        user_id:    user.id,
        account_id: account.id
      )
      
      create_snapshot
    end
    
    def user
      token.user
    end
    
    def account
      @account ||= user.accounts.password.first_or_initialize
    end
    
    def create_snapshot
      Snapshot.create!(
        subject: account,
        author:  user,
        service: 'shark',
        event:   'shark.password_reset',
        data:    account.attributes
      )
    end
  end
end
