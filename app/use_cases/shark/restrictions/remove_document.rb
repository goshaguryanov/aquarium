module Shark
  module Restrictions
    class RemoveDocument < Base

      def initialize(document_url)
        @document_url = document_url
      end

      def call
        redis_cli.del(redis_key)
      end
    end
  end
end
