module Shark
  module Restrictions
    class AddDocument < Base

      def initialize(document_url, expire_date)
        @document_url = document_url
        @expire_date  = expire_date
      end

      def call
        redis_cli.del(redis_key)
        redis_cli.set(redis_key, @expire_date.to_s)
        redis_cli.expire(redis_key, expiration) if expiration
        true
      end

      def expiration
        @expire_date.blank? ? nil : (Time.parse(@expire_date) - Time.now).to_i
      end
    end
  end
end
