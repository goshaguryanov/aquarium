module Shark
  class SubscribeConfirm
    attr_reader :token_string, :contact
    
    def initialize(params)
      @token_string = params.require(:token)
    end
    
    def valid?
      token.present?
    end
    
    def call
      ActiveRecord::Base.transaction do
        @contact = Contact.find(token.token_data['contact_id'])
        contact.update(
          contact_status:      'confirmed',
          subscription_status: 'on',
          type_ids: contact.type_ids | token.token_data['type_ids']
        )
        create_snapshot
        token.update(used: true)
      end if valid?
    end

    def mail_types
      @mail_types ||= valid? && ::Mailing::MailType.where(id: token.token_data['type_ids'])
    end
    
  private
    def token
      @token ||= UserToken.contact_confirmation.unused.find_by(token: token_string)
    end

    def create_snapshot
      contact.snapshots.create!(
        event: 'subscribe_confirm',
        data: contact.attributes
      )
    end
  end
end
