module Shark
  module Sessions
    class DestroyMobileUserSession < Base
      RANGE = 0..-5

      attr_reader :mobile_user_id

      def initialize(mobile_user_id)
        @mobile_user_id = mobile_user_id
        @sessions ||= (mobile_user_id ? sessions(mobile_user_id: mobile_user_id) : [])
      end

      def call
        sessions[RANGE].each do |session|
          logger.info "access_token: #{session['access_token']}"
          unbind_session(session['access_token'], :mobile_user_id)
        end
        true
      end
    end
  end
end
