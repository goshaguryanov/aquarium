module Shark
  module Sessions
    class Base

      private

      def sessions(condition = {})
        @sessions ||= Session.where(condition).map(&:from_redis).sort { |x, y| last_visit(y) <=> last_visit(x) }
      end

      def notify_newrelic
        ::NewRelic::Agent.increment_metric('Custom/Sessions/cut_old_sessions')
      end

      def unbind_session(access_token, *keys)
        logger.info("Unbinding token: #{access_token}")
        session = Session.find_by_access_token(access_token)
        session.update_attributes(keys.each_with_object({}) { |e, obj| obj[e] = nil })
        session.dump_to_redis
        Resque.enqueue(Workers::PurgeCacheJob, access_token)
      end

      def logger
        @logger ||= Logger.new("#{Rails.root.join('log')}/cut_old_sessions.log")
      end

      def redis_session(access_token)
        (Aquarium::Redis.connection.hgetall("sessions:#{access_token}") || {}).merge('access_token' => access_token)
      end

      def last_visit(session)
        begin
          Time.parse(session['last_visited_at'].to_s)
        rescue Exception
          Time.new(1900, 1, 1)
        end
      end
    end
  end
end
