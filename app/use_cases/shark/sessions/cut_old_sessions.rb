module Shark
  module Sessions
    class CutOldSessions < Base
      DESKTOP_SESSIONS_LIMIT = 2
      MOBILE_SESSIONS_LIMIT  = 5
      APP_SESSIONS_LIMIT     = 3

      attr_reader :user_id
      private :user_id

      def initialize(user_id:)
        @user_id = user_id
      end

      def call
        if sessions(user_id: user_id).any?
          cut_sessions
          notify_newrelic
        end
      end

      private

      def cut_sessions
        desktop_sessions_count, mobile_sessions_count, app_sessions_count = 0, 0, 0
        sessions.each do |session|
          case session['view']
          when 'desktop'
            desktop_sessions_count += 1
            unbind_session(session['access_token'], :user_id, :account_id) if desktop_sessions_count > DESKTOP_SESSIONS_LIMIT
          when 'mobile'
            mobile_sessions_count += 1
            unbind_session(session['access_token'], :mobile_user_id) if mobile_sessions_count > MOBILE_SESSIONS_LIMIT
          when 'app'
            app_sessions_count += 1
            unbind_session(session['access_token'], :user_id, :account_id) if app_sessions_count > APP_SESSIONS_LIMIT
          else
            logger.info("Undefined session(#{session['access_token']}) type")
          end
        end
      end
    end
  end
end
