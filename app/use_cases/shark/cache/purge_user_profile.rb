module Shark
  module Cache
    class PurgeUserProfile < Base

      BASE_PATH = '/purge/includes/long_cache/user_profile/'

      attr_reader :token

      def initialize(token)
        @token = token
      end

    private
      def full_urls
        [Settings.hosts.shark, Settings.hosts.m_shark].map do |host|
          "#{host}#{BASE_PATH}authenticated/#{token}"
        end
      end
    end
  end
end
