module Shark
  module Cache
    class Base
      include Procto.call

      def call
        full_urls.each do |full_url|
          Settings.shark.srv_ids.each do |srv_id|
            Faraday.get(full_url) do |request|
              request.headers['Cookie'] = "srv_id=#{srv_id}"
            end
          end
        end
      end
    end
  end
end