module Shark
  class BlockUnconfirmedUsers
    include Procto.call
    
    attr_reader :users
    
    def initialize
      @users = User.unconfirmed
    end
    
    def call
      users.find_each do |user|
        user.update(status: 'blocked')
        create_snapshot(user)
        
        user.sessions.each do |session|
          SignOut.call(access_token: session.access_token)
        end
      end
    end
    
  private
    def create_snapshot(user)
      Snapshot.create! do |snapshot|
        snapshot.subject = user
        snapshot.service = 'shark'
        snapshot.event   = 'shark.block_unconfirmed'
        snapshot.data    = user.attributes
      end
    end
  end
end
