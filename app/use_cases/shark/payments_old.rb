module Shark
  class PaymentsOld
    attr_reader :status
    
    def error_response(st, error)
      @status = st
      { status: 'error', errors: Array(error) }
    end
  end
end
