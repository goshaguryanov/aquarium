module Shark
  class UpdateProfile
    include Procto.call
    
    attr_reader :user
    
    def initialize(user, params)
      @user = user
      user.assign_attributes(params)
    end
    
    def call
      alert = user.autopay_changed?(from: true, to: false)
      if user.save
        create_snapshot
        send_alert if alert
        return true
      end
    end
    
    def errors
      user.errors
    end
    
  private
    def create_snapshot
      Snapshot.create!(
        subject: user,
        author:  user,
        service: 'shark',
        event:   'shark.profile_updated',
        data:    user.attributes
      )
    end

    def send_alert
      InnerMailer.autopay_changed(user).deliver
    end
  end
end
