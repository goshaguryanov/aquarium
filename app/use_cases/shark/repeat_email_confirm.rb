module Shark
  class RepeatEmailConfirm
    attr_reader :user
    
    def initialize(params)
      @user = User.find_by_access_token(params.require(:token))
    end
    
    def success?
      @success
    end
    
    def call
      @success = user.send_confirmation_email if user
    end
  end
end
