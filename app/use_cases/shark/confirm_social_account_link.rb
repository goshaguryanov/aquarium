module Shark
  class ConfirmSocialAccountLink
    attr_reader :token_string, :update_session, :account
    
    def initialize(params, ip)
      @token_string = params.require(:confirmation_token)
      @update_session = UpdateSession.new(params.fetch(:access_token), ip)
    end
    
    def valid?
      token.present? && find_account.nil?
    end
    
    def success?
      @success
    end
    
    def call
      if valid?
        ActiveRecord::Base.transaction do
          token.update(used: true)
          @account = Account.create(account_data)
          update_session.call(user_id: account.user.id, account_id: account.id)
          create_snapshot
          notify_newrelic
          @success = true
        end
      end
    end
    
  private
    def token
      @token ||= UserToken.social_link.unused.find_by(token: token_string)
    end
    
    def find_account
      token.user.accounts.find_by(account_data)
    end
    
    def account_data
      {
        user_id: token.user.id,
        type: token.token_data['provider'],
        external_id: token.token_data['external_id']
      }
    end
    
    def create_snapshot
      Snapshot.create!(
        subject: account,
        author:  token.user,
        service: 'shark',
        event:   'shark.social_account_link_confirmed',
        data:    account.attributes
      )
    end
    
    def notify_newrelic
      ::NewRelic::Agent.increment_metric('Custom/SocialAccountLinkCreated')
    end
  end
end
