module Shark
  class ActivePrices
    include Procto.call
    
    def call
      Price.active.map do |p|
        Shark::PriceSerializer.new(p).as_json
      end
    end
  end
end
