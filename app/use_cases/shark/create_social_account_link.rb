module Shark
  class CreateSocialAccountLink
    attr_reader :params, :user
    
    def initialize(params)
      @params = params
    end
    
    def call
      if find_account.nil? && find_user.present?
        token = user.tokens.social_link.generate(token_data)
        # queue to resque
        UserMailer.social_account_link_email(token).deliver
        @success = true
      end
    end
    
    def success?
      @success
    end
  
  private
    def find_account
      Account.find_by(type: params[:provider], external_id: params[:external_id])
    end
    
    def find_user
      @user ||= User.find_by(email: params[:email])
    end
    
    def token_data
      params.slice(:provider, :external_id, :email)
    end
  end
end
