module Shark
  class RestoreSession
    include Procto.call
    
    attr_reader :session
    
    def initialize(params)
      @session = Session.find_or_initialize_by(access_token: params[:access_token]) do |session|
        session.ip = params[:ip]
      end
    end
    
    def call(persist: false)
      if persist
        if session.id
          ::NewRelic::Agent.increment_metric('Custom/Sessions/restore_in_pg')
        else
          ::NewRelic::Agent.increment_metric('Custom/Sessions/create_in_pg')
        end
        session.save && session.dump_to_redis
      else
        session.dump_to_redis
        ::NewRelic::Agent.increment_metric('Custom/Sessions/restore_in_redis')
      end
    end
    
    def errors
      session.errors
    end
  end
end
