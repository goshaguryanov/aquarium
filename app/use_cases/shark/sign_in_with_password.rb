module Shark
  class SignInWithPassword
    attr_reader :email, :password, :update_session
    
    def initialize(params, ip)
      @email    = params.require(:email)
      @password = params.require(:password)
      
      @update_session = UpdateSession.new(params.fetch(:access_token), ip)
    end
    
    def user_blocked?
      user && !user.allowed_to_sign_in?
    end
    
    def call
      sign_in if valid?
      notify_newrelic
    end
    
    def valid?
      !user_blocked? && password_account && password_account.authenticate(password)
    end
    
    def user
      @user ||= User.find_by(email: email.downcase)
    end
    
  private
    def sign_in
      update_session.call(
          user_id:    user.id,
          account_id: password_account.id
      )

      cutter = Shark::Sessions::CutOldSessions.new(user_id: user.id)
      cutter.call
    end
    
    def password_account
      @account ||= user && user.accounts.find_by(type: 'password')
    end
    
    def notify_newrelic
      ::NewRelic::Agent.increment_metric(newrelic_metric_name)
    end
    
    def newrelic_metric_name
      status = if valid?
        'successful'
      elsif user_blocked?
        'blocked'
      else
        'failed'
      end
      
      "Custom/PasswordSignIn/#{status}"
    end
  end
end
