module Shark
  class ChangePassword
    attr_reader :account
    
    def initialize(user, new_password)
      @account = password_account_for(user)
      account.password = new_password
    end
    
    def call
      account.save
    end
    
    def errors
      account.errors
    end
    
  private
    def password_account_for(user)
      user.accounts.password.first_or_initialize
    end
  end
end
