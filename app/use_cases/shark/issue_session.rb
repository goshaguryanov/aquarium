module Shark
  class IssueSession
    include Procto.call
    
    attr_reader :session
    
    def initialize(params)
      @session = Session.new do |session|
        session.ip = params[:ip]
      end
    end
    
    def call(persist: false)
      session.access_token = Session.generate_token
      
      if persist
        session.save && session.dump_to_redis
        ::NewRelic::Agent.increment_metric('Custom/Sessions/create_in_pg')
      else
        session.dump_to_redis
        ::NewRelic::Agent.increment_metric('Custom/Sessions/create_in_redis')
      end
    end
    
    def errors
      session.errors
    end
  end
end
