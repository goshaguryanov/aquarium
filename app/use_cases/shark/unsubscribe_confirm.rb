module Shark
  class UnsubscribeConfirm
    attr_reader :unsubscribe_token
    
    def initialize(unsubscribe_token)
      @unsubscribe_token = unsubscribe_token
    end
    
    def call
      if contact && mail_type
        if contact.type_ids.include?(mail_type.id)
          contact.type_ids -= [mail_type.id]
          contact.type_ids_will_change!
          contact.save

          create_snapshot
        end
        true
      end
    end
    
    def mail_type
      @mail_type ||= ::Mailing::MailType.find_by_slug(secret_hash[:type_slug])
    end

    def contact
      @contact ||= Contact.find_by_email_type_secret(*secret_hash.values_at(*%i(email type_slug secret)))
    end

    def secret_hash
      @secret_hash ||= Contact.secret_hash(unsubscribe_token)
    end

  private

    def create_snapshot
      contact.snapshots.create!(
        event: 'unsubscribe_confirm',
        data: contact.attributes
      )
    end
  end
end
