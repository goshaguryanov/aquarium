module Shark
  class SignInWithOauth
    attr_reader :type, :external_id, :old_external_id, :update_session
    
    def initialize(params, ip)
      @type            = params.fetch(:provider)
      @external_id     = params.fetch(:external_id)
      @old_external_id = params[:old_external_id]
      
      @update_session = UpdateSession.new(params.fetch(:access_token), ip)
    end
    
    def user_blocked?
      user && !user.allowed_to_sign_in?
    end
    
    def call
      sign_in if valid?
      notify_newrelic
    end
    
    def valid?
      !user_blocked? && account.present?
    end
    
    def user
      account && account.user
    end
    
  private
    def sign_in
      cutter = Shark::Sessions::CutOldSessions.new(user_id: user.id)
      cutter.call
      
      update_session.call(
        user_id:    user.id,
        account_id: account.id
      )
    end
    
    def account
      @account ||= if new_account.nil? && old_account.nil?
        nil
      elsif old_account.nil?
        new_account
      elsif new_account.nil?
        create_snapshot(old_account, 'before_external_id_update')
        old_account.update(external_id: external_id)
        create_snapshot(old_account, 'after_external_id_update')
        
        old_account
      elsif new_account.user == old_account.user
        old_account.destroy
        create_snapshot(old_account, 'old_account_destroyed')
        
        new_account
      else
        new_account
      end
    end
    
    def new_account
      @new_account ||= Account.find_by(type: type, external_id: external_id)
    end
    
    def old_account
      if type == 'facebook' && !old_external_id.nil?
        @old_account ||= Account.find_by(type: type, external_id: old_external_id)
      end
    end
    
    def create_snapshot(object, event)
      Snapshot.create!(
        subject: object,
        event:   "facebook_merge.#{event}",
        data:    object.attributes
      )
    end
    
    def notify_newrelic
      ::NewRelic::Agent.increment_metric(newrelic_metric_name)
    end
    
    def newrelic_metric_name
      status = if valid?
        'successful'
      elsif user_blocked?
        'blocked'
      else
        'failed'
      end
      
      "Custom/OAuthSignIn/#{status}"
    end
  end
end
