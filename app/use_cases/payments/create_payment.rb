module Payments
  class CreatePayment
    include Payments::Logger

    attr_reader :payment_kit_params, :paymethod_params, :params, :event_params, :reader, :client_ip, :errors, :access_right

    def initialize(params, reader, client_ip = nil)
      @payment_kit_params = params.extract!(:product, :namespace, :subproduct, :period, :payment_kit_path, :payment_kit_id)
      @paymethod_params   = params.extract!(:paymethod, :paymethod_id, :paymethod_slug)
      @reader             = reader
      @params             = params
      @client_ip          = client_ip
      @event_params       = extract_event_params!
    end

    def call
      logger.info { "Use case: #{self.class.name}, parameters: {params: #{params}, reader_type: #{reader.class.name}, reader_id: #{reader.id}, client_ip: #{client_ip}}" }

      unless payment_kit
        @errors = ['Can\'t find payment_kit']
        return nil
      end

      #set start_date and end_date when it's empty
      payment.start_date = start_date unless payment.start_date
      payment.end_date   = end_date unless payment.end_date

      if payment.save
        use_case = Common::GrantAccess.new(payment: payment, reader: reader)
        if use_case.call
          @access_right = use_case.access_right
          Snapshot.create!(
              subject: payment,
              event:   'payments_create_payment',
              data:    payment.attributes
          )
          payment.fire_event!(event_params)
          payment
        else
          @errors = use_case.access_right.errors.full_messages
          nil
        end
      else
        @errors = payment.errors.full_messages
        nil
      end
    end

    def start_date
      last_access_right = payment.payment_kit.subproduct.slug == 'paper' ? nil : reader.access_rights.where("end_date IS NOT NULL").order(end_date: :desc).first
      last_payment      = reader.payments.paid.joins(:payment_kit).where("payment_kits.subproduct_id in (?)", pending_subproducts.pluck(:id)).order(end_date: :desc).first
      [last_access_right && next_day(last_access_right.end_date), last_payment && next_day(last_payment.end_date), Time.now].compact.max
    end

    def end_date
      ((payment.start_date || start_date) + payment.payment_kit.period.duration - 1.day).end_of_day
    end

    def next_day(date)
      date && ((date + 1.day).beginning_of_day)
    end

    def pending_subproducts
      if payment.product.slug == 'subscription'
        payment.subproduct.slug == 'profi' ? payment.product.subproducts : payment.product.subproducts.where(slug: ['profi', payment.subproduct.slug])
      else
        [payment.subproduct]
      end
    end

    def payment_kit
      @payment_kit ||= begin
        pk = Payments::FindPaymentKit.new(payment_kit_params).call
        pk if pk && pk.paymethod_ids.include?(paymethod.id)
      end
    end

    def paymethod
      @paymethod ||= begin
        paymethod_params[:paymethod] ||
            (paymethod_params[:paymethod_slug] && Paymethod.find_by_slug(paymethod_params[:paymethod_slug])) ||
            (paymethod_params[:paymethod_id] && Paymethod.find(paymethod_params[:paymethod_id]))
      end
    end

    def payment
      @payment ||= begin
        data = params.fetch(:data, {}).merge(client_ip: client_ip)
        p    = Payment.new(params.merge(payment_kit: payment_kit, paymethod: paymethod, data: data))
        if !params[:price] && payment_kit.price
          p.price = payment_kit.price
        end
        if payment_kit.renewal_id
          p.renewal_id = payment_kit.renewal_id
        end
        p
      end
    end

    def log_errors
      logger.error { "Use case: #{self.class.name}, error: #{errors.join('; ')}" }
    end

    def extract_event_params!
      if reader.class == MobileUser
        @params.extract!(:access_token, :operator, :msisdn)
      elsif reader.class == User
        { user_id: reader.id }
      elsif reader.class == Session
        { session_id: reader.id }
      else
        {}
      end.merge(@params.extract!(:document_url))
    end
  end
end
