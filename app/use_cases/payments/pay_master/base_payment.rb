module Payments
  module PayMaster
    class BasePayment
      include Payments::Logger
      attr_accessor :errors, :params
      def initialize(params)
        @errors = []
        @params = params.symbolize_keys
      end
      def payment
        @payment ||= ::Payment.notpaid.bymethod(:paymaster).find_by(id: params[:PAYMENT_ID])
      end
    end
  end
end