require 'digest/md5'
module Payments
  module PayMaster
    class NotificationPayment < BasePayment
      attr_accessor :params
      def call
        if payment.present? && self.decode_hash == self.calculate_hash
          payment.status = :paid
          payment.grant_access
          payment.save!
          Snapshot.create!(
              subject: payment,
              event:   'payment_paymaster_notification',
              data:    payment.attributes
          )
        end
      end

      def decode_hash
        Base64.decode64(params[:LMI_HASH]).unpack('H*').first
      end

      def calculate_hash
        parts = []
        %i(LMI_MERCHANT_ID LMI_PAYMENT_NO LMI_SYS_PAYMENT_ID LMI_SYS_PAYMENT_DATE LMI_PAYMENT_AMOUNT LMI_CURRENCY
           LMI_PAID_AMOUNT LMI_PAID_CURRENCY LMI_PAYMENT_SYSTEM LMI_SIM_MODE).each do |key|
          parts << params[key].to_s
        end
        Digest::MD5.hexdigest((parts << Settings.paymaster.secret).join(';'))
      end
    end
  end
end
