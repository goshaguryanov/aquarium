module Payments
  module PayMaster
    class CreatePayment < Payments::CreatePayment
      def form_attributes
        {
            LMI_MERCHANT_ID: Settings.paymaster.merchant_id,
            LMI_PAYMENT_AMOUNT: get_amount,
            LMI_CURRENCY: 'RUB',
            LMI_PAYMENT_DESC_BASE64: Base64.encode64(get_description),
            LMI_EXPIRES: get_expiration,
            LMI_PAYMENT_NO: payment.id,
            PAYMENT_ID: payment.id
        }
      end

      def form_url
        Settings.paymaster.url
      end
      def get_amount
        payment.price.to_s.gsub(',','.')
      end
      def get_description
        "#{payment.product.title}: #{payment.subproduct.title}"
      end
      def get_expiration
        Settings.paymaster.expires.to_i.seconds.from_now.utc.strftime('%Y-%m-%dT%H:%M:%S')
      end
    end
  end
end
