module Payments
  module Card
    class GetResult < Base
      attr_reader :error, :response

      def initialize(payment)
        @payment = payment
      end
      
      def call
        @response = get_response(req_params_for_payment_result)
        if response && response.result=='OK'
          true
        else
          @error = 'can\'t get result'
          false
        end
      end

      # parameters for requesting transaction id
      def req_params_for_payment_result
        {
          'command'        => 'c',
          'trans_id'       => payment.transaction_id,
          'client_ip_addr' => payment.client_ip
        }
      end
    end
  end
end