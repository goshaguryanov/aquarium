module Payments
  module Card
    class Renewal < Base
      attr_reader :payment, :error, :response, :newpayment

      def initialize(payment)
        @payment = payment
      end

      def call
        logger.info { "Use case: #{self.class.name}, original payment_id: #{payment.id}" }

        # проверяем, можно ли делать продление
        unless payment.can_renewal?
          logger.error { "Use case: #{self.class.name}, can't renewal: #{payment.id}" }
          return
        end

        # создаем оплату с доступом
        payment_kit_renewal =  payment.payment_kit.renewal || payment.payment_kit

        params = {
          paymethod_id:   paymethod.id,
          payment_kit_id: payment_kit_renewal.id,
          renewal_id:     payment_kit_renewal.renewal_id,
          price:          payment_kit_renewal.price,
          data: {
            biller_client_id:    payment.biller_client_id,
            autopayment:         true,
            previous_payment_id: payment.id,
            client_ip:           payment.client_ip
          }
        }
        @newpayment = Payments::CreatePayment.new(params, payment.user, payment.client_ip).call

        payment.data[:next_payment_id] = newpayment.id
        payment.data_will_change!
        payment.save!


        @response = get_response(req_params_for_renewal)
        if response && response.result=='OK'
          true
        else
          @error = 'can\'t get transaction_id'
          false
        end
      end

      # parameters for requesting transaction id
      def req_params_for_renewal
        {
          'command'          => 'e',
          'amount'           => (newpayment.price * 100).round,
          'currency'         => currency,
          'client_ip_addr'   => newpayment.client_ip,
          'description'      => newpayment.id,
          'biller_client_id' => newpayment.biller_client_id,
          'perspayee_expiry' => '1220'
        }
      end

      def paymethod
        Paymethod.find_by_slug('card')
      end
    end
  end
end
