module Payments
  module Card
    class GetTransactionId < Base
      attr_reader :error, :response

      def initialize(payment)
        @payment = payment
      end

      def call
        @response = get_response(req_params_for_transaction_id)
        if response && response.key?(:transaction_id) && !response.transaction_id.blank?
          response.transaction_id
        else
          @error = 'can\'t get transaction_id'
          nil
        end
      end

      private

      # parameters for requesting transaction id
      def req_params_for_transaction_id
        req_values = {
            'command'        => 'v',
            'amount'         => (payment.price * 100).round,
            'currency'       => currency,
            'client_ip_addr' => payment.client_ip,
            'description'    => payment.id,
            'msg_type'       => 'SMS',
            'language'       => 'iframe'
        }

        if auto_renewing
          req_values.merge!(add_params_for_transaction_id)
        end

        req_values
      end

      # additional parameters when require recurrent payment
      def add_params_for_transaction_id
        {
            'command'             => 'z',
            'biller_client_id'    => biller_client_id,
            'perspayee_expiry'    => '1220',
            'perspayee_gen'       => 1,
            'perspayee_overwrite' => 1
        }
      end

      def auto_renewing
        payment.data.symbolize_keys[:auto_renewing]
      end
    end
  end
end
