module Payments
  module Card
    class CloseDay < Base
      attr_reader :response
      
      def call
        close_day || logger.error("Error closing financial day in RSB for not recurrent merchant")
        @auto_renewal = true
        close_day || logger.error("Error closing financial day in RSB for recurrent merchant")
      end

      # parameters for requesting transaction id
      def req_params_for_close_day
        {
          'command' => 'b'
        }
      end

      def close_day
        @response = get_response(req_params_for_close_day)
        response && response.result == 'OK'
      end
    end
  end
end