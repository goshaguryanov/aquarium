require 'faraday_middleware'
require 'faraday_middleware/response_middleware' 

module Payments
  module Card
    class Base
      attr_reader :payment
      include Payments::Logger

      def biller_client_id
        @biller_client_id ||= "#{Time.now.strftime('%Y%m%d')}_#{payment.user.id}"
      end
      
    private
      def connection
        ssl_params = {
          client_cert: client_cert,
          client_key:  client_key,
          ca_file:     ca_file,
          verify_mode: OpenSSL::SSL::VERIFY_NONE
        }
        
        Faraday.new(url: Settings.card.merchant_host, ssl: ssl_params) do |builder|
          builder.request  :url_encoded
          builder.response :mashify
          builder.response :rsb_response_decoder
          builder.adapter  Faraday.default_adapter
        end
      end
      
      def client_cert
        client_cert_file = auto_renewal ? Settings.card.ssl.client_cert_recurrent : Settings.card.ssl.client_cert
        client_cert_file = ERB.new(client_cert_file).result
        OpenSSL::X509::Certificate.new(File.read(client_cert_file)) unless client_cert_file.blank?
      end
      
      def client_key
        client_key_file = auto_renewal ? Settings.card.ssl.client_key_recurrent : Settings.card.ssl.client_key
        client_key_file = ERB.new(client_key_file).result
        OpenSSL::PKey::RSA.new(File.read(client_key_file)) unless client_key_file.blank?
      end

      def auto_renewal
        payment ? payment.renewal_id : @auto_renewal
      end
      
      def ca_file
        ERB.new(Settings.card.ssl.ca_file).result
      end
      
      def post(req_values)
        connection.post(Settings.card.merchant_url, req_values) do |req|
          req.options[:timeout] = 20
          req.options[:open_timeout] = 10
        end
      end
      
      def get_response(req_values)
        res = post(req_values).body
        logger.info { "#{self.class.name} request: #{req_values}, response: #{res}" }
        res
      end
      
      def currency
        '643' # RUB
      end
      
      def perspayee_expiry
        '1220' # TODO: храним шаблон до 31.12.2020, надо уточнить
      end
        
      def host_for_client
        Settings.card.client_host
      end
      
      def url_for_client
        Settings.card.client_url
      end

      class ResponseDecoder < FaradayMiddleware::ResponseMiddleware
        define_parser do |body|
          body.strip.split(/\r?\n/).each_with_object(Hash.new) do |line, hash|
            key, value = line.split(':', 2)
            hash[key.strip.downcase] = value.strip unless value.nil?
          end
        end
      end
      
      Faraday::Response.register_middleware rsb_response_decoder: ResponseDecoder       
    end
  end
end
