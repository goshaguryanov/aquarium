module Payments
  module Yandex
    class CreatePayment < Payments::CreatePayment
      # :money - оплата из кошелька в Яндекс.Деньгах
      # :card - оплата с произвольной банковской карты (по умолчанию)
      def form_attributes
        payment_type = case paymethod.slug.to_s
                         when 'yandex_kassa'
                           'AC'
                         when 'yandex_money'
                           'PC'
                         when 'qiwi'
                           'QW'
                         when 'web_money'
                           'WM'
                         else 'AC'
                       end
        {
            paymentType: payment_type,
            shopId: Settings.yandex.shop_id,
            scid: Settings.yandex.sc_id,
            sum: format('%.2f', payment.price),
            customerNumber: reader.id,
            orderNumber: payment.id,
            rebillingOn: payment.can_renewal_paymethod?.to_s,
            goods_name_N: payment.product.title,
            goods_description_N: payment.subproduct.title
        }
      end
      def form_url
        Settings.yandex.url
      end
    end
  end
end
