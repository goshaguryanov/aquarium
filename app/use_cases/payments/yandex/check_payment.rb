module Payments
  module Yandex
    class CheckPayment < BasePayment
      def call
        if valid?('checkOrder')
          # сохраняем invoiceId установленный в Yandex для возможных последующих операци
          payment.data[:invoiceId] = params[:invoiceId]
          payment.data_will_change!
          payment.save!
          logger.error { "Use case: #{self.class.name}, payment: #{payment.id}" }
        end
      end
      def xml_response
        super('checkOrderResponse')
      end
    end
  end
end