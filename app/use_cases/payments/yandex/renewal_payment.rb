module Payments
  module Yandex
    class RenewalPayment
      attr_reader :payment, :newpayment

      def initialize(payment)
        @payment = payment
      end

      def paymethod
        payment.paymethod
      end

      def call
        unless payment.can_renewal?
            logger.error { "Use case: #{self.class.name}, can't renewal: #{payment.id}" }
            return
        end
        Payment.transaction do
          params = {
              paymethod_id:   paymethod.id,
              payment_kit_id: payment.renewal_id,
              renewal_id:     payment.renewal_id,
              price:          payment.renewal.price,
              data: payment.data.merge({
                  biller_client_id:    payment.biller_client_id,
                  autopayment:         true,
                  previous_payment_id: payment.id,
                  client_ip:           payment.client_ip
              })
          }
          @newpayment = Payments::Yandex::CreatePayment.new(params, payment.user, payment.client_ip).call

          payment.data[:next_payment_id] = @newpayment.id
          payment.data_will_change!
          payment.save!
        end
      end
    end
  end
end
