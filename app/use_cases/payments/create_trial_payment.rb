module Payments
  class CreateTrialPayment
    attr_reader :payment, :base_use_case, :user, :errors, :access_right

    def initialize(payment_params, user, client_ip)
      @base_use_case = CreatePayment.new(payment_params.merge(paymethod: paymethod), user, client_ip)
      @user = user
      @errors = []
    end

    def call
      unless payment_kit
        @errors.push("Can't find PaymentKit")
        return nil
      end

      unless payment_kit.suitable_for?(user)
        @errors.push('PaymentKit not suitable for user')
        return nil
      end

      if base_use_case.call
        @payment = base_use_case.payment
        @access_right = base_use_case.access_right
        payment.update(status: 'paid')

      else
        @errors = base_use_case.errors
        return nil
      end

      payment.update(status: 'paid')
      payment.snapshots.create!(
        event: 'payments_trial_pay',
        data:  payment.attributes
      )

      access_case = Common::ChangeAccessPeriod.new(@access_right.id, start_date: payment.start_date, end_date: payment.end_date)
      unless access_case.call
        @errors = access_right.errors
        return nil
      end

      payment
    end

    def payment_kit
      base_use_case.payment_kit
    end

    def paymethod
      @paymethod ||= Paymethod.find_by_slug('trial')
    end
  end
end