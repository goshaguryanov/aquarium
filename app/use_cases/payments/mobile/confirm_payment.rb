module Payments
  module Mobile
    class ConfirmPayment < ::Payments::Mobile::Base

      attr_reader :subscription_id, :attributes, :access_token, :operator, :result, :msisdn

      def initialize(operator, access_token, attributes = {})
        @operator        = operator
        @access_token    = access_token
        @subscription_id = attributes[:subscription_id]
        @attributes      = attributes
      end

      def call
        if success?
          if redis_payment.blank?
            fire_paid_event!(:failed)
            @result = { redirect: create_new_payment_url }
          else
            @result = { redirect: wait_payment_url }
            fire_paid_event!(:ok)
            verify_payment!
          end
        else
          fire_paid_event!(:failed)
          yield if block_given?
          true
        end
      end

      def fire_paid_event!(event)
        Resque.enqueue(Workers::Statistics::FireEvent, 'pay_confirm', {
                                                         pay_status:   event,
                                                         operator:     operator,
                                                         msisdn:       msisdn,
                                                         redirect:     @result ? @result[:redirect] : nil,
                                                         return_url: redis_payment['return_url'],
                                                         access_token: access_token
                                                     })
      end

      def create_restore_access_url
        [Settings.hosts.metynnis, 'id', 'mobile', 'codes', 'new', "?return_url=#{redis_payment['return_url']}"].join('/')
      end

      def create_wifi_url
        [Settings.hosts.metynnis, 'id', 'mobile', 'wifi', 'new', "?return_url=#{redis_payment['return_url']}"].join('/')
      end

      def create_new_payment_url
        [Settings.hosts.metynnis, 'paywall_mobile', operator].join('/')
      end

      def wait_payment_url(id = subscription_id)
        [Settings.hosts.metynnis, 'id', operator, 'wait', id].join('/')
      end
    end
  end
end
