module Payments
  module Mobile
    class CreatePayment < ::Payments::Mobile::Base

      attr_reader :subscription_id, :operator, :payment, :errors

      def initialize(attributes)
        @operator        = attributes[:operator]
        @subscription_id = attributes.delete(:subscription_id)
        @msisdn          = attributes[:msisdn]
        @service_id      = attributes.delete(:service_id)
        @access_token    = attributes[:access_token]
        @attributes      = attributes
        @errors          = []
        merge_document
      end

      def call
        init_mobile_user! && init_payment!
      end

      def merge_document
        @attributes = @attributes.merge({document_url: redis_payment['return_url']})
      end

      def init_mobile_user!
        use_case = Common::MobileUser.new(@msisdn, @operator, @access_token, redis_payment['client_ip'])
        if use_case.call
          @mobile_user = use_case.mobile_user
        else
          @errors = use_case.errors
          false
        end
      end

      def init_payment!
        use_case = ::Payments::CreatePayment.new(@attributes, @mobile_user)
        if (@payment = use_case.call)
          true
        else
          @errors = use_case.errors
          false
        end
      end
    end
  end
end
