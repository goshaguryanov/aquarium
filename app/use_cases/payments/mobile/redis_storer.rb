module Payments
  module Mobile
    class RedisStorer < ::Payments::Mobile::Base

      attr_reader :operator, :subscription_id

      def initialize(operator, subscription_id, return_url, client_ip, attributes = {})
        @operator           = operator
        @subscription_id    = subscription_id
        @return_url         = return_url
        @client_ip          = client_ip
        @attributes         = attributes
      end

      def call
        redis_client.del(redis_key)
        redis_client.mapped_hmset(redis_key, @attributes.symbolize_keys.merge(return_url:     @return_url,
                                                                              client_ip:      @client_ip
                                           ))
        redis_client.expire(redis_key, 1.day.seconds)
      end
    end
  end
end
