module Payments
  module Mobile
    module Tele2
      class VerifyPayment < ::Payments::Mobile::Beeline::VerifyPayment

        def initialize(operator, subscription_id, access_token, client_ip, msisdn)
          @operator        = operator
          @subscription_id = subscription_id
          @access_token    = access_token
          @client_ip       = client_ip
          @msisdn          = redis_payment['msisdn'] || msisdn
          unless @msisdn.blank?
            @mobile_user     = MobileUser.where(operator: 'tele2').find_by_msisdn(@msisdn)
            @payment         = @mobile_user.active_payment if @mobile_user
          end
          @payment         ||= Payment.find_by_subscription_id(redis_key)
        end
      end
    end
  end
end
