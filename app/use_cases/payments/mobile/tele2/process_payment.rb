module Payments
  module Mobile
    module Tele2
      class ProcessPayment

        def initialize(sms_message)
          @sms_message = sms_message
        end

        def call
          Resque.enqueue(::Workers::Payments::Tele2CallbackVerification, @sms_message)
        end
      end
    end
  end
end
