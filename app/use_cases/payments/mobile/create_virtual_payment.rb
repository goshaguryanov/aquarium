module Payments
  module Mobile
    class CreateVirtualPayment < ::Payments::Mobile::Base
      include Payments::Logger

      attr_reader :redirect, :errors, :subscription_id, :operator, :msisdn, :access_token

      def initialize(operator, return_url, access_token = nil, client_ip = nil, msisdn = nil, attributes = {})
        @operator         = operator
        @return_url       = return_url
        @access_token     = access_token
        @client_ip        = client_ip
        @msisdn           = msisdn
        @attributes       = attributes
      end

      def call
        if @msisdn && @access_token && (mobile_user = MobileUser.find_by_msisdn(@msisdn)) && mobile_user.has_active_access_rights?
          use_case = ::Lago::Code::Create.new(@msisdn, @access_token, @return_url)
          if use_case.call
            @redirect = use_case.redirect
          else
            @errors = use_case.errors
            false
          end
        else
          begin
            yield if block_given?
          rescue Exception
            create_fake_payment!
            @redirect = wait_payment_url
          ensure
            pay_init_event
          end
        end
      end

      def pay_init_event
        Resque.enqueue(Workers::Statistics::FireEvent, 'pay_init', {
            subscription_id: subscription_id,
            paymethod: operator,
            product: Settings.paywall_mobile.product,
            namespace: operator,
            subproduct: Settings.paywall_mobile.subproduct,
            period: Settings.paywall_mobile.period,
            pay_status: redirect ? 'ok' : 'fail',
            redirect: redirect,
            operator: operator,
            msisdn: msisdn,
            access_token: access_token,
            referer: @return_url
        })
      end

      private

      def save_to_redis!
        attributes = { access_token: @access_token }
        attributes.merge!(msisdn: @msisdn) unless @msisdn.blank?
        ::Payments::Mobile::RedisStorer.new(@operator,
                                            subscription_id,
                                            @return_url,
                                            @client_ip,
                                            attributes
        ).call
      end

      def redirect_to_wait_error!
        create_fake_payment!
        @redirect = wait_payment_url
        true
      end

      def wait_payment_url(id = subscription_id)
        [Settings.hosts.metynnis, 'id', operator, 'wait', id].join('/')
      end

      def authorized?
        %w(authorized registered).include?(@attributes[:owl_state])
      end
    end
  end
end
