module Payments
  module Mobile
    class Base

      def redis_payment(id=redis_key)
        redis_client.hgetall(id)
      end

      def redis_key(id = subscription_id)
        Operators::Base.system_subscription_id(operator, id)
      end


      private

      def redis_client
        Aquarium::Redis.connection
      end

      def create_fake_payment!(error_code: 0)
        @subscription_id = SecureRandom.uuid
        redis_client.mapped_hmset(redis_key, error_code: error_code)
        redis_client.expire(redis_key, 12.hours)
      end
    end
  end
end
