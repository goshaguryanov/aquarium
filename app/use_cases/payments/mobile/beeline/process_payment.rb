module Payments
  module Mobile
    module Beeline
      class ProcessPayment

        def initialize(sms_message)
          @sms_message = sms_message
        end

        def call
          Resque.enqueue(::Workers::Payments::BeelineCallbackVerification, @sms_message)
        end
      end
    end
  end
end
