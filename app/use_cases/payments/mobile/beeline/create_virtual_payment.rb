module Payments
  module Mobile
    module Beeline
      class CreateVirtualPayment < ::Payments::Mobile::CreateVirtualPayment

        def call
          super do
            @subscription_id, @redirect = Operators.send(operator).create_subscription(@msisdn, @access_token)
            if @redirect
              save_to_redis!
            else
              logger.error { "Use case: #{self.class.name}, errors: There is an error in operator's response" }
              redirect_to_wait_error!
            end
          end
        end
      end
    end
  end
end
