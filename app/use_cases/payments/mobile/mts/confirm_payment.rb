module Payments
  module Mobile
    module Mts
      class ConfirmPayment < ::Payments::Mobile::ConfirmPayment
        include Payments::Logger

        attr_reader :payment, :error

        def call
          super do
            if subscribe_error_code
              if attributes.key?('ExistingSID') && subscribe_error_code == '9'
                create_linked_virtual_payment!
                @result = { redirect: wait_payment_url(active_subscription_id) }
                verify_payment!(active_subscription_id)
              elsif subscribe_error_code == '8'
                @result = { redirect: create_wifi_url }
                true
              else
                @result = { redirect: wait_payment_url }
                redis_client.hset(redis_key, :error_code, subscribe_error_code)
                verify_payment!
              end
            else
              @result = { redirect: wait_payment_url }
              verify_payment!
            end
          end
        end

        def success?
          !attributes['MSISDN'].blank? && attributes['SubscribeResult'] == 'true' && attributes['Result'] == 'true'
        end

        def subscribe_error_code
          attributes['SubscribeErrorCode']
        end

        def active_subscription_id
          attributes['ExistingSID'] || subscription_id
        end

        def verify_payment!(id = subscription_id)
          Resque.enqueue(::Workers::Payments::MtsManualVerification, id, access_token)
        end

        def create_linked_virtual_payment!
          ::Payments::Mobile::RedisStorer.new(
              operator,
              active_subscription_id,
              redis_payment['return_url'],
              redis_payment['client_ip']
          ).call
        end
      end
    end
  end
end
