module Payments
  module Mobile
    module Mts
      class VerifyPayment < ::Payments::Mobile::VerifyPayment

        def call
          super do
            case
            when payment.data.key?('fault_code')
              error_response!(::Operators.mts.internal_error(kind: :fault_codes, code: payment.data['fault_code']))
            when payment.data.key?('error_code')
              error_response!(::Operators.mts.internal_error(kind: :error_codes, code: payment.data['error_code']))
            else
              return false
            end
          end
        end

        private

        def proceed_without_payment
          if exists_in_redis?
            if redis_payment.key?('error_code')
              error_response!(::Operators.mts.internal_error(kind: :subscribe_errors, code: redis_payment['error_code']))
            else
              return false
            end
          else
            error_response!(error: 'Подписка не найдена')
          end
          true
        end
      end
    end
  end
end
