module Payments
  module Mobile
    class VerifyPayment < ::Payments::Mobile::Base
      include ::Payments::Logger

      attr_reader :payment, :result, :subscription_id, :operator

      def initialize(operator, subscription_id, access_token, client_ip, *)
        @operator        = operator
        @subscription_id = subscription_id
        @access_token    = access_token
        @client_ip       = client_ip
        @payment         = Payment.find_by_subscription_id(redis_key)
      end

      def call
        if payment
          case payment.status
            when payment.active? && 'paid'
              if mobile_user
                unless bind_session_to_user!
                  error_response!(error: 'Не удалось привязать сессию к пользователю')
                  return true
                end
                if mobile_user.email
                  @result = { status: :ok, redirect: return_url }
                else
                  @result = { status: :ok, redirect: update_mobile_user_path }
                end
              end
            when 'canceled'
              error_response!(error: 'Платеж отменен')
            else
              yield if block_given?
          end
        else
          return proceed_without_payment
        end
        true
      end

      private

      def update_mobile_user_path
        [Settings.hosts.lago, 'mobile', 'subscribers', 'new'].join('/')
      end

      def wifi_subscription_path
        [Settings.hosts.metynnis, 'paywall_mobile', 'wifi'].join('/')
      end

      def error_response!(options = {})
        @result = { status: :error }.merge(options)
      end

      def mobile_user
        @mobile_user ||= payment && payment.mobile_user
      end

      def bind_session_to_user!
        ::Common::MobileUser.new(mobile_user.msisdn, mobile_user.operator, @access_token, @client_ip).call
      end

      def exists_in_redis?
        redis_client.exists(redis_key)
      end

      def return_url
        redis_client.hget(redis_key, :return_url) || Settings.hosts.shark
      end
    end
  end
end
