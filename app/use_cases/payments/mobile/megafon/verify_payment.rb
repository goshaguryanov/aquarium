module Payments
  module Mobile
    module Megafon
      class VerifyPayment < ::Payments::Mobile::VerifyPayment

        def proceed_without_payment
          if exists_in_redis?
            if redis_payment.key?('error_code')
              error_response!(::Operators.megafon.internal_error(kind: :subscribe_errors, code: redis_payment['error_code']))
            else
              return false
            end
          else
            error_response!(error: 'Подписка не найдена')
          end
          true
        end
      end
    end
  end
end
