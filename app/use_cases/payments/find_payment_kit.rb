module Payments
  class FindPaymentKit
    include Payments::Logger

    attr_reader :payment_kit, :attributes
    
    def initialize(attributes)
      @attributes = attributes.symbolize_keys
    end
    
    def call
      unless @payment_kit = attributes[:payment_kit_id] ? PaymentKit.find_by_id(attributes[:payment_kit_id]) : PaymentKit.active.find_by_slug_path(slug_path)
        logger.error("Use case '#{self.class.name}' can't find payment_kit. Params: #{attributes}")
      end
      payment_kit
    end

    def slug_path
      attributes[:payment_kit_path] || attributes.values_at(*%i(product namespace subproduct period)).join('.')
    end
  end
end
