module Payments
  class LinkSubscription
    include Payments::Logger
    attr_reader :session, :user, :status, :error
    
    def initialize(session, user)
      @session = session
      @user    = user
      @status  = :ok
    end
    
    def call
      logger.info "Use case: #{self.class.name}, parameters: {session: #{session.id}, user_id: #{user.id}}"
      
      # ищем активный AccessRight для этой сессии
      unless payment
        logger.error "Use case: #{self.class.name}, error: Can't find active payment for session: session: #{session.id}"
        @error = 'You have no subscription'
        return nil
      end
      
      if users.count>0 && !users.include?(user)
        logger.error "Use case: #{self.class.name}, error: Already linked to another user. Session: #{session.id}, user_id: #{user.id}, another user id: #{users.map(&:id).join(',')}"
        @error = 'Subscription already linked'
        return nil
      end
      
      if users.empty?
        Common::GrantAccess.new(payment: payment, reader: user, start_date: payment.start_date, end_date: payment.end_date).call
      end
      
      payment
    end
    
    def access_right
      @access_right ||= session.access_rights.active.first
    end
    
    def payment
      @payment ||= (access_right && access_right.payment)
    end
    
    def users
      @users ||= (payment && payment.users)
    end
  end
end
