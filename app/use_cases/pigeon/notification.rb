module Pigeon
  class Notification

    def initialize(notify_params)
      @notify_params = notify_params
    end

    def call
      @notify_params.each do |mailing|
        mailing[:contacts].each do |contact|
          letters = Mailing::Letter.where(mailing_id: mailing[:mailing_id], contact_id: contact[:contact_id])
          letters.update_all(status: contact[:status])
          letters.map { |letter| letter.data['mail'] = contact['mail'] }
          Indexer::Letters.index_new(letters.to_a)

          if (record = Contact.find_by(id: contact[:contact_id]))
            if contact[:status].to_i == Mailing::Letter.statuses[:undelivered]
              record.update(subscription_status: Contact.subscription_statuses[:off])
              record.snapshots.create!(event: 'undelivered_mail', data: record.attributes)
            elsif contact[:status].to_i == Mailing::Letter.statuses[:error] && block_contact?(contact[:mail])
              record.update(subscription_status: Contact.subscription_statuses[:off])
              record.snapshots.create!(event: 'letter_error', data: record.attributes)
            end
          end
        end
      end
      true
    rescue Exception => e
      Rails.logger.error e.inspect
      false
    end

    private

    def block_contact?(message)
      @errors_parser ||= Mailings::Letter::ErrorsParser.new
      @errors_parser.message = message
      (diagnostic_code_fixed = @errors_parser.diagnostic_code_fixed) && (rules[diagnostic_code_fixed].to_s == 'block')
    end

    def rules
      unless @rules
        @rules = {}
        Mailing::Rule.all.each { |rule| @rules[rule.code] = rule.status }
      end
      @rules
    end
  end
end
