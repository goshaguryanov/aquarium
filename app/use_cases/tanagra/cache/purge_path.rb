module Tanagra
  module Cache
    class PurgePath < Base
      BASE_PATH = '/purge/'

      attr_reader :path

      def initialize(path)
        @path = path
      end

      private

      def full_urls
        [Settings.hosts.tanagra].map do |host|
          "#{host}" + BASE_PATH + path
        end
      end
    end
  end
end
