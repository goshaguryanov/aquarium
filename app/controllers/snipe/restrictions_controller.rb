module Snipe
  class RestrictionsController < ApplicationController

    # POST /shark/restrictions
    def create
      use_case = ::Shark::Restrictions::AddDocument.new(document_url, expire_date)

      if use_case.call
        head :ok
      else
        head :forbidden
      end
    end

    def destroy
      use_case = ::Shark::Restrictions::RemoveDocument.new(document_url)

      if use_case.call
        head :ok
      else
        head :forbidden
      end
    end

    private

    def document_url
      params.require(:url)
    end

    def expire_date
      params.fetch(:expire_date, nil)
    end
  end
end
