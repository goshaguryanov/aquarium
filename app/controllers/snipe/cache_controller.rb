module Snipe
  class CacheController < ApplicationController

    # POST /purge
    def purge
      if Tanagra::Cache::PurgePath.new(path).call && Shark::Cache::PurgePath.new(shark_path).call
        head :ok
      else
        head :forbidden
      end
    end

    private

    def path
      params.require(:path)
    end

    def shark_path
      path =~ /^[\w\.]+\/(.*)$/
      $1
    end
  end
end
