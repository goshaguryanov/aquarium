module Lago
  class CodesController < Lago::ApplicationController

    def create
      use_case = Lago::Code::Create.new(msisdn, access_token, return_url)
      call_use_case(use_case)
    end

    def check
      use_case = Lago::Code::Check.new(code, access_token, request.ip)
      call_use_case(use_case)
    end

    private

    def code
      attributes.require(:code)
    end

    def msisdn
      attributes.require(:msisdn)
    end

    def return_url
      attributes[:return_url]
    end

    def call_use_case(use_case)
      if use_case.call
        render json: { redirect: use_case.redirect }
      else
        render json: {
            status: :error,
            phone: use_case.msisdn,
            errors: use_case.errors
        }, status: :not_found
      end
    end
  end
end
