module Lago
  class ApplicationController < ::ApplicationController

    include GelfExt::Controller::Logger

    private

    def mobile_user
      @mobile_user ||= session.mobile_user
    end

    def session
      @session ||= Session.multi_find(access_token)
    end

    def access_token
      params[:token] || params[:mobile_user_token]
    end

    def authenticate_mobile_user!
      if !mobile_user
        render json: { status: :error, error: 'Not found' }
      end
    end
  end
end
