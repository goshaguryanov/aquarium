module Metynnis
  class NamespacesController < ApplicationController
    def show_by_slug
      render json: Namespace.joins(:product).where('products.slug = ?', params[:product]).find_by_slug(params[:namespace]), serializer: NamespaceSerializer
    end

    def base
      render json: Namespace.base, serializer: NamespaceSerializer
    end
  end
end
