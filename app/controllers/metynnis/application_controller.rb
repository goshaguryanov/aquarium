module Metynnis
  class ApplicationController < ::ApplicationController

    private

    def user
      @user ||= User.joins(:sessions).where(sessions: { access_token: params[:token] }).first if params[:token]
    end

    def session
      @session ||= Session.find_by_access_token(params[:token]) if params[:token]
    end
  end
end
