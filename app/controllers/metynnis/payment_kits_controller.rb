module Metynnis
  class PaymentKitsController < ApplicationController
    def index
      kits = PaymentKit.active_by_product_namespace(params[:product], params[:namespace]).joins(:subproduct, :period).order('subproducts.position, periods.duration')
      render json: kits, each_serializer: PaymentKitSerializer
    end

    def show
      kit = PaymentKit.active.find_by_slug_path(params.values_at(:product, :namespace, :subproduct, :period).join('.'))
      render json: kit, serializer: PaymentKitSerializer
    end

    def subproducts
      render json: Metynnis::ActivePaymentKits.call(params[:product], params[:namespace])
    end

    def suitable_index
      render json: Metynnis::SuitablePaymentKits.call(params[:product], params[:namespace], user), each_serializer: PaymentKitSerializer
    end

    def suitable_show
      payment_kit = PaymentKit.active.find_by_slug_path(params.values_at(:product, :namespace, :subproduct, :period).join('.'))
      if payment_kit && payment_kit.suitable_for?(user)
        render json: PaymentKitSerializer.new(payment_kit)
      else
        head :not_found
      end
    end

    # супер-контроллер, который предоставляет всю необходимую инфу для первого шага подписки
    def combined_index
      product_slug = params[:product]
      namespace_slug = params[:namespace]

      if namespace_slug.present? && product_slug.present?
        namespace = Namespace.joins(:product).where(namespaces: {slug: namespace_slug}, products: {slug: product_slug}).first
      else
        namespace = Namespace.base
        product_slug = namespace.product.slug
        namespace_slug = namespace.slug
      end

      if !namespace || namespace.redirect_to
        render json: { namespace: NamespaceSerializer.new(namespace) }
        return
      end

      render json: {
        namespace:    NamespaceSerializer.new(namespace),
        subproducts:  ActivePaymentKits.call(product_slug, namespace_slug).as_json,
        user:         UserSerializer.new(user),
        session:      SessionSerializer.new(session)
      }
    end

    #  супер-контроллер, который предоставляет всю необходимую инфу для 2 и 3 шага подписки
    def combined_show
      payment_kit = PaymentKit.active.find_by_slug_path(params.values_at(:product, :namespace, :subproduct, :period).join('.'))

      if payment_kit
        cities = payment_kit.city_choice ? City.order(:position, :title) : []
        render json: {
          payment_kit: PaymentKitSerializer.new(payment_kit),
          cities:      cities.map { |c| CitySerializer.new(c) },
          user:        UserSerializer.new(user),
          session:     SessionSerializer.new(session),
          suitable:    user && payment_kit.suitable_for?(user)
        }
      else
        head :not_found
      end
    end

  private
    def user
      if session = Session.find_by_access_token(params[:token])
        session.user
      end
    end
  end
end
