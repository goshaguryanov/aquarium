module Metynnis
  module Payments
    class MobileController < PaymentsController

      include GelfExt::Controller::Logger

      def create
        use_case = handler::CreateVirtualPayment.new(operator, return_url, access_token, request.ip, msisdn, attributes)

        if use_case.call
          render json: { redirect_url: use_case.redirect }, status: :ok
        else
          render json: { status: :error, errors: use_case.errors }, status: :unprocessable_entity
        end
      end

      #POST /:operator/confirm
      def confirm
        use_case = handler::ConfirmPayment.new(operator, access_token, attributes)

        if use_case.call
          render json: use_case.result
        else
          render json: { error: 'Что-то пошло не так' }
        end
      end

      # Проверка пеймента
      def check
        use_case = handler::VerifyPayment.new(operator, subscription_id, access_token, request.ip, msisdn)

        if use_case.call
          render json: use_case.result
        else
          render json: { status: :ok, wait: { subscription_id: subscription_id } }
        end
      end

      def callback
        handler::ProcessPayment.new(attributes).call
        head :ok
      end

      private

      def operator
        @operator ||= params.require(:operator)
      end

      def access_token
        params[:access_token]
      end

      def handler
        "#{::Payments::Mobile}::#{operator.capitalize}".constantize
      end

      def attributes
        params.require(:attributes)
      end

      def subscription_id
        params.require(:subscription_id)
      end

      def msisdn
        params.fetch(:msisdn, nil)
      end

      def return_url
        @return_url ||= params[:return_url].blank? ? Settings.hosts.shark : params[:return_url]
      end
    end
  end
end
