module Metynnis
  module Payments
    module Yandex
      class YandexKassaController < YandexBaseController
        private
        def paymethod
          @paymethod ||= Paymethod.find_by_slug('yandex_kassa')
        end
      end
    end
  end
end
