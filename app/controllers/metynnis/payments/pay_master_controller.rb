module Metynnis
  module Payments
    class PayMasterController < PaymentsController
      def create
        render(json: { status: 'error', errors: ['user not found'] }, status: :forbidden) and return unless user
        use_case = ::Payments::PayMaster::CreatePayment.new(payment_params.merge(paymethod: paymethod), user, request.ip)
        if use_case.call
          pay_init_event
          render json: { attributes: use_case.form_attributes, url: use_case.form_url, result: 'paymaster' }
        else
          render json: { status: 'error', errors: Array(use_case.errors) }, status: :unprocessable_entity
        end
      end
      def confirmation
        use_case = ::Payments::PayMaster::ConfirmationPayment.new(params.require(:attributes))
        if use_case.call
          render nothing: true, status: :ok
        else
          render nothing: true, status: :unprocessable_entity
        end
      end
      def notification
        use_case = ::Payments::PayMaster::NotificationPayment.new(params.require(:attributes))
        if use_case.call
          Resque.enqueue(Workers::Payments::PayMasterResult, use_case.payment.id, request.ip)
          render nothing: true, status: :ok
        else
          render nothing: true, status: :unprocessable_entity
        end
      end
      private
      def paymethod
        @paymethod ||= Paymethod.find_by_slug('paymaster')
      end
    end
  end
end
