module Metynnis
  module Payments
    class PaymentsController < ApplicationController

      private

      def user
        @user ||= (params.key?(:access_token) && User.find_by_access_token(params[:access_token]))
      end

      def session
        @session ||= Session.find_by_access_token(params[:access_token])
      end

      def payment_params
        params.permit(:product, :namespace, :subproduct, :period, :payment_kit_path)
      end

      def access_token
        params[:access_token]
      end

      def pay_init_event
        Resque.enqueue(Workers::Statistics::FireEvent, 'pay_init', {
            paymethod: paymethod.slug,
            access_token: access_token
        }.merge!(payment_params))
      end
    end
  end
end
