module Metynnis
  module Payments
    class SubscribeRequestsController < ApplicationController

      #POST /company
      def company
        if Resque.enqueue(::Workers::SubscribeRequest, :company, company_params)
          render json: { result: true }
        else
          render json: { result: false }, status: :unprocessable_entity
        end
      end

      #POST /gift
      def gift
        if Resque.enqueue(::Workers::SubscribeRequest, :gift, gift_params)
          render json: { result: true }
        else
          render json: { result: false }, status: :unprocessable_entity
        end
      end

      private

      def company_params
        params.permit(:name, :title, :email, :phone, :newspaper_count, :online_count)
      end

      def gift_params
        params.permit(:name, :email, :phone)
      end
    end
  end
end