module Metynnis
  class CitiesController < ApplicationController
    def index
      render json: City.order(:position, :title), each_serializer: CitySerializer
    end
  end
end
