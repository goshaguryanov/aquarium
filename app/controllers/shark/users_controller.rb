module Shark
  class UsersController < ApplicationController
    before_filter :check_user, only: %i(show profile update change_password accept_promo repeat_email_confirm)
    # before_action :check_redis
    
    def show
      render json: UserSerializer.new(session.user, session).as_json
    end
    
    def profile
      render json: UserProfileSerializer.new(session.user).as_json
    end
    
    def update
      use_case = UpdateProfile.new(session.user, params.require(:user))
      
      if use_case.call
        render json: UserSerializer.new(use_case.user).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def change_password
      use_case = ChangePassword.new(session.user, params.fetch(:password))
      
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def accept_promo
      use_case = AcceptPromo.new(session.user, params.require(:promo_name))
      
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def send_password_reset_link
      SendPasswordRecoveryLink.call(params[:email])
      head :ok
    end
    
    def check_password_reset_token
      if CheckPasswordRecoveryToken.new(params[:password_reset_token]).correct?
        head :ok
      else
        head :not_found
      end
    end
    
    def reset_password
      use_case = Shark::ResetPassword.new(params[:password_reset_token], params[:access_token], ip)
      use_case.call(params[:password])
      
      head :ok
    end
    
    def sign_up_with_password
      use_case = SignUpWithPassword.new(params.require(:user), ip)
      
      if Settings.registration_closed?
        head :gone
      elsif use_case.save
        render json: UserSerializer.new(use_case.user).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def sign_in_with_password
      use_case = SignInWithPassword.new(params, ip)
      use_case.call
      
      if use_case.user_blocked?
        use_case.user.send_confirmation_email
        render json: { error: 'User blocked' }, status: :forbidden
      elsif use_case.valid?
        render json: UserSerializer.new(use_case.user, use_case.update_session.session).as_json
      else
        render json: { error: 'Invalid credentials' }, status: :unprocessable_entity
      end
    end
    
    def sign_up_with_oauth
      use_case = SignUpWithOauth.new(params.require(:user), ip)
      
      if Settings.registration_closed?
        head :gone
      elsif use_case.save
        render json: UserSerializer.new(use_case.user).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def sign_in_with_oauth
      use_case = SignInWithOauth.new(params, ip)
      use_case.call
      
      if use_case.user_blocked?
        use_case.user.send_confirmation_email
        render json: { error: 'User blocked' }, status: :forbidden
      elsif use_case.valid?
        render json: UserSerializer.new(use_case.user, use_case.update_session.session).as_json
      else
        render json: { error: 'Invalid credentials' }, status: :unprocessable_entity
      end
    end
    
    def sign_out
      SignOut.call(params, ip)
      head :ok
    end
    
    def confirm
      use_case = Confirm.new(params)
      
      if use_case.valid?
        use_case.call
        head :ok
      else
        head :not_found
      end
    end
    
    def create_social_account_link
      use_case = CreateSocialAccountLink.new(params)
      use_case.call
      
      if use_case.success?
        head :created
      else
        head :not_found
      end
    end
    
    def confirm_social_account_link
      use_case = ConfirmSocialAccountLink.new(params, ip)
      use_case.call
      
      if use_case.success?
        head :created
      else
        head :not_found
      end
    end
    
    def repeat_email_confirm
      use_case = RepeatEmailConfirm.new(params)
      use_case.call
      
      if use_case.success?
        head :created
      else
        head :unprocessable_entity
      end
    end
    
    def update_mailing
      use_case = UpdateMailing.new(session.user, params)
      
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  
  private
    def session
      @session ||= Session.multi_find(params[:token])
    end
    
    def check_user
      head :unauthorized and return if session.nil?
      head :not_found and return if session.user.nil?
    end

    def ip
      request.ip
    end
  end
end
