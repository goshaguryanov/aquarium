module Shark
  module Orders
    class OrdersController < ApplicationController
      def link_subscription
        if session && user
          use_case = Payments::LinkSubscription.new(session, user)
          if use_case.call
            render json: {status: 'ok', redirect: { path: 'subscription_ok' } }
          else
            render json: { status: 'error', errors: [ use_case.error ] }, status: :unprocessable_entity
          end
        else
          render json: { status: 'error', errors: [ 'Incorrect session or user' ] }, status: :unprocessable_entity
        end
      end
  
      def mobile_subscriptions
        target = params.require(:target)
        if Settings.mobile_subscriptions.groups[target]
          render json: { groups: Settings.mobile_subscriptions.groups[target].select(&:enabled), paywall: Settings.mobile_subscriptions.paywall }
        else
          render json: { status: 'error', errors: [ 'Incorrect target' ] }, status: :unprocessable_entity
        end
      end
      
    private
      def user
        @user ||= (params.key?(:access_token) && User.find_by_access_token(params[:access_token]))
      end
  
      def session
        @session ||= Session.find_by_access_token(params[:access_token])
      end
      
      def order_params
        params.require(:order).permit(:product, :period, :student, :auto_renewing)
      end
      
      def payment_answer(use_case)
        result = use_case.call
        render json: result, status: use_case.status
      end
    end
  end
end
