module Shark
  module Orders
    class AppleController < OrdersController
      def verify
        if session
          Shark::PaymentsOld::Apple::VerifyReceipt.new(params.require(:receipt), session, request.ip).call
          render json: { status: 'ok' }
        else
          render json: { status: 'error', errors: [ 'Incorrect session' ] }, status: :unprocessable_entity
        end
      end
    end
  end
end
