module Shark
  module Orders
    class CardController < OrdersController
      def create
        use_case = Shark::PaymentsOld::Card::CreateOrder.new(order_params, user, request.ip)
        payment_answer(use_case)
      end
      
      def callback
        use_case = Shark::PaymentsOld::Card::Callback.new(params.require(:trans_id), request.ip)
        result = use_case.call
        result[:order] = use_case.order
        render json: result, status: use_case.status
      end
      
      def check
        use_case = Shark::PaymentsOld::Card::Check.new(params.require(:uuid))
        payment_answer(use_case)
      end
    end
  end
end
