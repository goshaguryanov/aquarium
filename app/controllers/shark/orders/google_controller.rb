module Shark
  module Orders
    class GoogleController < OrdersController
      def verify
        if session
          Shark::PaymentsOld::Google::VerifyReceipt.new(
            params.require(:subscription_id),
            params.require(:token),
            session,
            request.ip
          ).call
          render json: { status: 'ok' }
        else
          render json: { status: 'error', errors: [ 'Incorrect session' ] }, status: :unprocessable_entity
        end
      end
    end
  end
end
