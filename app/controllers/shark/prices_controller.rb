module Shark
  class PricesController < ApplicationController
    def active
      render json: Shark::ActivePrices.call
    end
  end
end
