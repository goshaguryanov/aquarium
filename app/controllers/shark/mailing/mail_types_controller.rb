module Shark
  class Mailing::MailTypesController < ApplicationController
    def show_by_slug
      if mail_type = ::Mailing::MailType.find_by_slug(params.require(:slug))
        render json: mail_type, serializer: Mailing::MailTypeSerializer
      else
        head :not_found
      end
    end
  end
end
