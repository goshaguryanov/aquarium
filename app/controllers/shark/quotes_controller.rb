module Shark
  class QuotesController < ApplicationController
    def main_page
      render json: Shark::Quotes::MainPage.call, each_serializer: QuoteSerializer
    end
  end
end
