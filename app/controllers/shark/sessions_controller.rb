module Shark
  class SessionsController < ApplicationController
    def show
      session = Session.multi_find(params[:token])
      
      if session
        render json: SessionShowSerializer.new(session)
      else
        head :not_found
      end
    end
    
    def create
      session = params.require(:session)
      use_case = session[:access_token] ? RestoreSession.new(session) : IssueSession.new(session)
      
      if use_case.call(persist: params[:mobile])
        response.headers['X-Access-Token'] = use_case.session.access_token
        render json: SessionSerializer.new(use_case.session).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
