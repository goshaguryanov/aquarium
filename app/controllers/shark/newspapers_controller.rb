module Shark
  class NewspapersController < ApplicationController
    def zip
      newspaper = Newspaper.find_by(release_date: release_date)
      
      if newspaper
        respond_with newspaper
      else
        head :not_found
      end
    end
    
    def generate_archive
      if Resque.enqueue(Workers::ZipperJob, date_params, %i(mobile_low mobile_high))
        head :accepted
      else
        head :unprocessable_entity
      end
    end
  
  private
    def date_params
      if date_params_complete?
        params.slice(:year, :month, :day)
      else
        {}
      end
    end
    
    def release_date
      if date_params_complete?
        Date.new(*params.values_at(:year, :month, :day).map(&:to_i))
      else
        nil
      end
    end
    
    def date_params_complete?
      %i(year month day).all? { |key| params[key] }
    end
  end
end
