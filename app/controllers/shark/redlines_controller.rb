module Shark
  class RedlinesController < ApplicationController
    def active
      render json: Redline.active
    end
  end
end
