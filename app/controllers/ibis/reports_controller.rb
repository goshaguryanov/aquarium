module Ibis
  class ReportsController < ApplicationController
    def index
      use_case = Reports::All.new(params[:page])
      render json: use_case.call, each_serializer: ReportSerializer
    end

    def create
      use_case = Reports::Create.new(attributes, editor_id)

      if use_case.call
        render json: use_case.report, serializer: ReportSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
