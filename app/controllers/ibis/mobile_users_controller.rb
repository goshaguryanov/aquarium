module Ibis
  class MobileUsersController < ApplicationController

    def index
      mobile_users = AllMobileUsers.call(params[:page])
      render json: mobile_users
    end

    def create
      use_case = CreateMobileUser.new(attributes, editor_id)

      if use_case.call
        render json: use_case.mobile_user
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = UpdateMobileUser.new(params[:id], attributes, editor_id)
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def show
      mobile_user = MobileUser.find(params[:id])
      render json: mobile_user
    end

    def search
      mobile_users = MobileUsersSearchQuery.new.search(params.require(:search_string))
      render json: mobile_users
    end

    def count
      render json: { mobile_users: MobileUser.count, pages: MobileUser.page.total_pages }
    end
  end
end

