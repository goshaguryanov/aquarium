module Ibis
  class ApplicationController < ::ApplicationController
  
    before_action :editor_id

    rescue_from SnipeClient::Unauthorized, with: :unknown_token
    rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  
  private
    def editor_id
      @editor_id ||= snipe_client.get('/v1/session').body[:editor_id]
    end
    
    def snipe_client
      @snipe_client ||= SnipeClient.new(request.headers['X-Access-Token'] || '')
    end

    def record_not_found
      render json: {error: 'Not found'}, status: :not_found
    end
  end
end
