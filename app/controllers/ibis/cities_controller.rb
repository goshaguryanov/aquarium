module Ibis
  class CitiesController < ApplicationController
    def show
      render json: City.find(params.require(:id)), serializer: CitySerializer
    end

    def index
      render json: City.order(:position, :title), each_serializer: CitySerializer
    end

    def create
      use_case = Payments::CreateCity.new(attributes, editor_id)
      if use_case.call
        render json: use_case.city, serializer: CitySerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdateCity.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.city, serializer: CitySerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def destroy
      use_case = Payments::DestroyCity.new(params[:id])
      if use_case.call
        head :ok
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end      
    end
  end
end
