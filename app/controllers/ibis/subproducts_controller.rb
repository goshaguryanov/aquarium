module Ibis
  class SubproductsController < ApplicationController
    def show
      subproduct = ::Subproduct.find(params.require(:id))
      render json: subproduct, serializer: SubproductSerializer
    end

    def create
      use_case = Payments::CreateSubproduct.new(attributes, editor_id)
      if use_case.call
        render json: use_case.subproduct, serializer: SubproductSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdateSubproduct.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.subproduct, serializer: SubproductSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def payment_kits
      render(
        json: PaymentKit.joins(:namespace, :period).where(subproduct_id: params[:subproduct_id]).order('namespaces.start_date, periods.duration'),
        each_serializer: PaymentKitSimpleSerializer
      )
    end
  end
end
