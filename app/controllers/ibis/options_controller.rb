module Ibis
  class OptionsController < ApplicationController
    def index
      options = AllOptions.call
      render json: options
    end
    
    def by_type
      render json: Option.where(type: params[:type])
    end
    
    def types
      render json: Option::TYPES
    end
    
    def create
      use_case = CreateOption.new(attributes)
      
      if use_case.call
        render json: OptionSerializer.new(use_case.option).as_json
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def update
      use_case = UpdateOption.new(params[:id], attributes)
      
      if use_case.call
        head :ok
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def destroy
      DestroyOption.call(params[:id])
      head :ok
    end
    
    def dump
      DumpOptions.call
      head :ok
    end
  end
end
