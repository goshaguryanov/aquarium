module Ibis
  class NewspaperController < ApplicationController
    def generate_flash
      if Resque.enqueue(Workers::FlasherJob, params[:document_id]) && Resque.enqueue(Workers::ProfKioskJob, params[:document_id])
        head :accepted
      else
        head :unprocessable_entity
      end
    end
  end
end
