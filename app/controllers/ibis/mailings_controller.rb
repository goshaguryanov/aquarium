module Ibis
  class MailingsController < ApplicationController
    def index
      mailings = ::Mailing.order(id: :desc).page(params[:page])
      render json: mailings, each_serializer: Ibis::MailingSerializer
    end
    
    def show
      mailing = ::Mailing.find(params[:id])
      render json: mailing, serializer: Ibis::MailingSerializer
    end

    def create
      use_case = CreateMailing.new(attributes, editor_id)
      
      if use_case.call
        render json: use_case.mailing, serializer: MailingSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = UpdateMailing.new(params[:id], attributes, editor_id)
      
      if use_case.call
        render json: use_case.mailing, serializer: MailingSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
    
    def count
      render json: { mailings: ::Mailing.count, pages: ::Mailing.page.total_pages }
    end

    def deliver_test
      mailing = ::Mailing.find(params[:id])
      emails = params[:emails] || []

      emails.each do |email|
        ::MailingMailer.test_email(mailing, email).deliver
      end

      head :ok
    end

    def build_template
      use_case = ::Ibis::Mailing::BuildTemplate.new(params[:id], params.require(:template_kit), editor_id)
      if use_case.call
        render json: { mailing: use_case.mailing }
      else
        render json: { error: use_case.error }, status: :unprocessable_entity
      end
    end

    def attach_contacts
      Resque.enqueue(Workers::Mailing::AttachContacts, params.require(:id), params.require(:conditions).map { |c| c.to_h.symbolize_keys })
      head :ok
    end

    def contacts_count
      count = ::Mailing.where(id: params[:id]).joins(:letters).count
      render json: { count: count }
    end

    def ready
      use_case = ReadyMailing.new(params[:id], editor_id)

      if use_case.call
        render json: use_case.mailing, serializer: MailingSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def not_ready
      use_case = NotReadyMailing.new(params[:id], editor_id)

      if use_case.call
        render json: use_case.mailing, serializer: MailingSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end      
    end
    
    # debug only
    def preview
      mailing = ::Mailing.last
      render html: Pigeon::MailingTemplateRender.new(mailing).template_preview.html_safe, content_type: 'text/html'
    end
  end
end
