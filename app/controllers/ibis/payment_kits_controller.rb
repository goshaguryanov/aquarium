module Ibis
  class PaymentKitsController < ApplicationController
    def index
      payment_kits = PaymentKit.joins(:product, :namespace, :subproduct, :period).order('products.id, namespaces.start_date desc, subproducts.position, periods.duration')
      render json: payment_kits, each_serializer: PaymentKitSimpleSerializer
    end

    def show
      payment_kit = ::PaymentKit.find(params.require(:id))
      render json: payment_kit, serializer: PaymentKitSerializer
    end

    def create
      use_case = Payments::CreatePaymentKit.new(attributes, editor_id)
      if use_case.call
        render json: use_case.payment_kit, serializer: PaymentKitSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdatePaymentKit.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.payment_kit, serializer: PaymentKitSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def destroy
      use_case = Payments::DestroyPaymentKit.new(params[:id])
      if use_case.call
        head :ok
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def associations
      payment_kit = ::PaymentKit.find(params.require(:payment_kit_id))
      render json: payment_kit.associations.uniq.order(id: :desc), each_serializer: PaymentKitSimpleSerializer
    end
  end
end
