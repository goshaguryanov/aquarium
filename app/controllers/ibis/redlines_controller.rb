module Ibis
  class RedlinesController < ApplicationController
    def index
      redlines = Redline.on
      render json: redlines
    end

    def count
      render json: { redlines: Redline.count, pages: Redline.page.total_pages }
    end

    def show
      order = Redline.find(params[:id])
      render json: order
    end

    def create
      use_case = CreateRedline.new(attributes, editor_id)
      
      if use_case.call
        render json: use_case.redline
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = UpdateRedline.new(params[:id], attributes, editor_id)
      
      if use_case.call
        render json: use_case.redline
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
