module Ibis
  class PeriodsController < ApplicationController
    def show
      period = ::Period.find(params.require(:id))
      render json: period, serializer: PeriodSerializer
    end

    def index
      render json: Period.order(:duration), each_serializer: PeriodSerializer
    end

    def create
      use_case = Payments::CreatePeriod.new(attributes, editor_id)
      if use_case.call
        render json: use_case.period, serializer: PeriodSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdatePeriod.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.period, serializer: PeriodSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end
  end
end
