module Ibis
  class OrdersController < ApplicationController
    def index
      orders = AllOrders.call(params[:page])
      render json: orders, each_serializer: Ibis::OrderSerializer
    end

    def count
      render json: { orders: Order.count, pages: Order.page.total_pages }
    end

    def show
      order = Order.find(params[:id])
      render json: order, serializer: Ibis::FullOrderSerializer
    end

    def create
      use_case = CreateOrder.new(attributes, editor_id)
      
      if use_case.call
        render json: use_case.order, serializer: Ibis::FullOrderSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = UpdateOrder.new(params[:id], attributes, editor_id)
      
      if use_case.call
        render json: use_case.order, serializer: Ibis::FullOrderSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
