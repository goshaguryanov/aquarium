module Ibis
  class ProductsController < ApplicationController
    def show
      product = ::Product.find(params.require(:id))
      render json: product, serializer: ProductSerializer
    end

    def index
      render json: Product.order(:title), each_serializer: ProductSerializer
    end

    def create
      use_case = Payments::CreateProduct.new(attributes, editor_id)
      if use_case.call
        render json: use_case.product, serializer: ProductSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def update
      use_case = Payments::UpdateProduct.new(params[:id], attributes, editor_id)
      if use_case.call
        render json: use_case.product, serializer: ProductSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def namespaces
      render json: Namespace.where(product_id: params[:product_id]).order(:title), each_serializer: NamespaceSimpleSerializer
    end

    def subproducts
      render json: Subproduct.where(product_id: params[:product_id]).order(:position, :title), each_serializer: SubproductSerializer
    end

    def active_payment_kits
      render(
        json: PaymentKit.joins(:namespace, :subproduct, :period).where('payment_kits.product_id=? and ? between namespaces.start_date and namespaces.end_date', params[:product_id], Time.now).order('namespaces.start_date, subproducts.position, periods.duration'),
        each_serializer: PaymentKitSimpleSerializer
      )
    end

    def active_namespaces
      render json: Namespace.where(product_id: params[:product_id]).active.order(:start_date), each_serializer: NamespaceSimpleSerializer
    end
  end
end
