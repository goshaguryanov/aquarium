module Ibis
  class ContactsController < ApplicationController

    def index
      @contacts = ::Contact.all
      @contacts = @contacts.where('"contacts"."email" LIKE ?' '%'+params[:email]+'%') if params[:email].present?
      @contacts = @contacts.where('? = ANY ("contacts"."type_ids")', params[:type_id]) if params[:type_id].present?
      @contacts = @contacts.joins(:mailings).where(mailings: { id: params[:mailing_id] }) if params[:mailing_id].present?
      @contacts = @contacts.page(params[:page]).per(params[:per])
      render json: @contacts, each_serializer: Ibis::ContactSerializer
    end

    def show
      @contact = ::Contact.find(params[:id])
      render json: @contact, serializer: Ibis::ContactSerializer
    end

    def update
      use_case = UpdateContact.new(params[:id], contact_params, editor_id)
      if use_case.call
        render json: use_case.contact, serializer: Ibis::ContactSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def create
      use_case = CreateContact.new(contact_params, editor_id)
      if use_case.call
        render json: use_case.contact, serializer: Ibis::ContactSerializer
      else
        render json: {error: use_case.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    def count
      render json: { contacts: Contact.count, pages: Contact.page.total_pages }
    end

    def destroy
      contact = Contact.find(params[:id])
      if contact.destroy
        render nothing: true
      else
        render json: {error: contact.errors.full_messages.join(', ')}, status: :unprocessable_entity
      end
    end

    # GET /letters
    # Получает фильтрованный список рассылок контакта
    # @return [Hash] отфильтрованные рассылки
    def letters
      start_date = Time.parse(params[:start_date]) rescue nil
      end_date   = Time.parse(params[:end_date])   rescue nil
      @letters = ::Mailing::Letter.where(contact_id: params[:id]).joins(:mailing).includes(:mailing).joins(:type).includes(:type)
      @letters.where!('mailings.scheduled_at > ?', start_date) if start_date
      @letters.where!('mailings.scheduled_at < ?', end_date)   if end_date
      @letters.where!(mailing_types: { id: params[:type] })   if params[:type].present?
      @letters.where!(status: params[:status])                if params[:status].present?
      render json: @letters.order(updated_at: :desc).page(params[:page]).per(params[:per]), each_serializer: Ibis::Mailing::LetterSerializer
    end

    # GET /letters/:letter_id/details
    # Детализированная информация о письме
    # @return [Hash] содержимое письма
    def letter_details
      @letter = ::Mailing::Letter.where(contact_id: params[:id], status: 2).find(params[:letter_id])
      render json: @letter
    end

  private
    def contact_params
      params.require(:attributes).permit(:email, :user_id, :contact_status, :subscription_status, type_ids: [] )
    end
  end
end
