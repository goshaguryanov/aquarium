class Ibis::Indexer::OrdersController < ApplicationController
  # Lists ids of orders filtered by updated_at after specified timestamp.
  #
  # `GET /ibis/indexer/orders/fresh/:timestamp`
  #
  def fresh
    respond_with Order.where('updated_at > ?', Time.at(params[:timestamp].to_i)).limit(1000).pluck(:id)
  end
  
  # Lists ids of documents by page and ordered by updated_at.
  #
  # `GET /ibis/indexer/orders/page/:page`
  #
  def page
    respond_with Order.order(updated_at: :desc).page(params[:page]).pluck(:id)
  end
end
