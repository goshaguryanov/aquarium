class Ibis::Indexer::ContactsController < ApplicationController
  # Lists ids of contacts filtered by updated_at after specified timestamp.
  #
  # `GET /ibis/indexer/contacts/fresh/:timestamp`
  #
  def fresh
    respond_with Contact.where('updated_at > ?', Time.at(params[:timestamp].to_i)).limit(1000).pluck(:id)
  end
  
  # Lists ids of documents by page and ordered by updated_at.
  #
  # `GET /ibis/indexer/contacts/page/:page`
  #
  def page
    respond_with Contact.order(updated_at: :desc).page(params[:page]).pluck(:id)
  end
end
