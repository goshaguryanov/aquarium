module Ibis
  class Mailing::MailTypesController < ApplicationController
    def index
      render json: ::Mailing::MailType.order(title: :asc).page(params[:page]), each_serializer: Mailing::MailTypeSerializer
    end

    def show
      render json: ::Mailing::MailType.find(params[:id]), serializer: Mailing::MailTypeSerializer
    end

    def create
      use_case = Mailing::CreateMailType.new(attributes, editor_id)
      
      if use_case.call
        render json: use_case.type, serializer: Mailing::MailTypeSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end

    def update
      use_case = Mailing::UpdateMailType.new(params[:id], attributes, editor_id)
      
      if use_case.call
        render json: use_case.type, serializer: Mailing::MailTypeSerializer
      else
        render json: use_case.errors, status: :unprocessable_entity
      end
    end
  end
end
