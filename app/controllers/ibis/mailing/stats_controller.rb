module Ibis
  class Mailing::StatsController < ApplicationController
    def links_clicks
      data = ReportKinds::MailingClicks.call(params[:id])
      render json: data
    end
    
    def banners_clicks
      data = ReportKinds::MailingBanners.call
      render json: data
    end
  end
end
