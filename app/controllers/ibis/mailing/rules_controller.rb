module Ibis
  module Mailing
    class RulesController < ApplicationController

      # GET /mailing/rules/current
      def current
        @rules = ::Mailing::Rule.all
        render json: @rules
      end

      # POST /mailing/rules
      def create
        @rule = ::Mailing::Rule.new(code: code, status: status)
        begin
          @rule.save
          render json: { rule: @rule, status: :ok }
        rescue ActiveRecord::RecordNotUnique
          head :unprocessable_entity
        end
      end

      # PATCH /mailing/rules/(:id)
      def update
        @rule = ::Mailing::Rule.find(params[:id])
        @rule.status = status
        if @rule.save
          render json: { rule: @rule, status: :ok }
        else
          head :unprocessable_entity
        end
      end

      private

      def code
        attributes.require(:code)
      end

      def status
        attributes.require(:status)
      end

      def attributes
        params.require(:attributes)
      end
    end
  end
end
