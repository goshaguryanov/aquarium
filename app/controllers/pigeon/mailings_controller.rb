module Pigeon
  class MailingsController < ApplicationController

    def show
      mailing = Mailing.where(status: Mailing.statuses.values_at('ready', 'scheduled', 'started')).find(params[:id])
      render json: mailing, serializer: Pigeon::MailingSerializer
    end

    def update
      ::Mailing.find(params.require(:id)).update!(status: update_params[:status], data: update_params.except(:status) )
      render nothing: true
    end

    def notify
      Rails.logger.info "GOT NOTIFY:\r\n#{notify_params}"
      use_case = Pigeon::Notification.new(notify_params)

      if use_case.call
        head :ok
      else
        head :forbidden
      end
    end
  
  private
  
    def update_params
      params.permit(:status, :editor_id, :error)
    end
  
    def notify_params
      params.permit(notifies: [ :mailing_id, contacts: [ :contact_id, :status, :error, :mail ] ]).require(:notifies)
    end
  end
end
