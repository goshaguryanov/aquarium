module Pigeon
  class ContactsController < ApplicationController
    def index
      @contacts = contacts.offset(params.fetch(:offset,0).to_i).limit(params.fetch(:limit,25).to_i)
      @mailing = Mailing.find(params[:mailing_id])
      render json: @contacts.map { |c| Pigeon::ContactSerializer.new(c, mailing: @mailing) }
    end
  private
    def contacts
      Contact.joins(:mailings).uniq.order(:id).where(mailings: { id: params[:mailing_id] }).on
    end
  end
end
