module StatisticHelper

  # Ссылка на писсель
  #
  # statistic_url :pixel, :mailing_view, mailing_id: 123, contact_id: 1234
  # statistic_pixel_url :mailing_view, mailing_id: 123, contact_id: 1234
  #
  # http://rigel.vedomosti.ru/pixel?event=mailing_view&url=&tags%3D%5Bmailing_id:123,contact_id:1234%5D

  # Редирект по ссылке
  #
  # statistic_url :redirect, :mailing_click, url: 'http://domain.local', mailing_id: 123, contact_id: '{{data "id"}}'
  # statistic_redirect_url :mailing_click, url: 'http://domain.local', mailing_id: 123, contact_id: '{{data "id"}}'
  #
  # http://rigel.vedomosti.ru/redirect?mailing_click&tags%5B%5D=mailing_id:123&tags%5B%5D=contact_id:1234&tags%5B%5D=link_type:title&tags%5B%5D=order:45

  def statistic_pixel_url(event, options = {})
    statistic_url(:pixel, event, options)
  end

  def statistic_redirect_url(url, event, options = {})
    statistic_url(:redirect, event, options.merge(url: url))
  end

  def statistic_mailing_view
    statistic_pixel_url('mail_hit', contact_id: '{{data_escape "id"}}', mailing_id: '{{mailing_escape "id"}}')
  end

  def statistic_mailing_click(url, link_type=nil)
    @link_order ||= 0;
    statistic_redirect_url(url, 'mail_click', contact_id: '{{data_escape "id"}}', mailing_id: '{{mailing_escape "id"}}', link_type: link_type, order: @link_order+=1)
  end

  def statistic_banner_view(banner)
    statistic_pixel_url('mail_banner_hit', contact_id: '{{data_escape "id"}}', mailing_id: '{{mailing_escape "id"}}', banner_id: banner.id, position: banner.position)
  end

  def statistic_banner_click(url, banner, link_type=nil)
    @link_order ||= 0;
    statistic_redirect_url(url, 'mail_banner_click', contact_id: '{{data_escape "id"}}', mailing_id: '{{mailing_escape "id"}}', link_type: link_type, banner_id: banner.id, position: banner.position)
  end

  def statistic_url(type, event, options = {})
    if [ :pixel ].include? type.to_sym
        path = '/pixel'
    elsif [ :redirect ].include? type.to_sym
        path = '/redirect'
    else
      raise "unsupported statistic event type '#{event}'"
    end
    parts = [event.to_query(:event)]
    if (url = options.delete(:url))
      parts << (url.to_s =~ /^{{.*}}$/ ? "url=#{url}" : url.to_query(:url))
    end
    parts << "tags=%5B#{to_tags(options)}%5D"
    [Settings.hosts.rigel, path, '?', parts.join('&')].join
  end

private
  def to_tags(hash)
    hash.map do |key, value|
      "%22#{CGI.escape(key.to_s)}:#{value.to_s =~ /^{{.*}}$/ ? value : CGI.escape(value.to_s)}%22" if value.present?
    end.compact.join('%2C')
  end
end
