module ApplicationHelper
  def full_url(url)
    if url.nil? || url.empty?
      '#'
    else
      if url.start_with?('vedomosti.ru')
        ['http://www.', url].join('')
      else
        ['http://www.vedomosti.ru', url].join('')
      end
    end
  end
  
  # Картинка с размерами и всеми необходимыми атрибутами
  def image_with_dimension(image, version, alt: nil, image_class: nil)
    image_version = image.public_send(version.to_sym)
    image_tag image_url("#{Settings.hosts.cdn}#{image_version.url}"), alt: alt, class: image_class, size: image_version_size(image_version)
  end
  
  def image_version_size(image_version)
    if image_version.has_width? && image_version.has_height?
      "#{image_version.width}x#{image_version.height}"
    end
  end
  
  def document(url)
    document = Roompen.document("vedomosti.ru#{url}")
    document.title.present? ? document : false
  rescue
    false
  end
  
  def percent(total, current)
    "#{(current.to_f * 100 / total).round(1)}%"
  end
end
