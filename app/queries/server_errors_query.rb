class ServerErrorsQuery
  class << self
    def result(from = 'now-5m', to = 'now')
      response = client.search(index: 'nginxlogs-*', body: query(from, to))
      result_process(response)
    end
  
  private
    def result_process(response)
      response['hits']['hits'].map do |hit|
        hit['_source'].symbolize_keys.slice(:status, :node, :request, :timestamp, :clientip)
      end
    end
    
    def query(from, to)
      {
        query: {
            filtered: {
                query: {
                    query_string: {
                        query: "status:[500 TO 510]",
                        analyze_wildcard: false
                    }
                },
                filter: {
                    bool: {
                        must: [ { range: { '@timestamp' => { gte: from, lte: to } } } ],
                    }
                }
            }
        },
        size: 500,
        sort: { '@timestamp' => 'desc' },
        fields: ['*', '_source']
      }
    end
    
    def client
      @client ||= Elasticsearch::Client.new(Settings.elasticsearch_logs)
    end
  end
end
