# Query object for users search.
class MobileUsersSearchQuery
  attr_reader :relation
  
  COLUMNS = %w(msisdn first_name last_name operator)
  
  def initialize(relation = MobileUser.all)
    @relation = relation
  end
  
  def search(search_string)
    relation.where(condition, query: "%#{search_string.strip}%")
  end
  
  def find_by_msisdn(msisdn)
    relation.find_by!(msisdn: msisdn.strip)
  end
  
private
  def condition
    COLUMNS.map do |column|
      "#{column} ILIKE :query"
    end.join(' OR ')
  end
end
