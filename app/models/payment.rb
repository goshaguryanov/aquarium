# create_table :payments, force: true do |t|
#   t.integer  :payment_kit_id,                                          null: false
#   t.integer  :status,                                  default: 0,     null: false
#   t.integer  :paymethod_id,                                            null: false
#   t.decimal  :price,          precision: 10, scale: 2,                 null: false
#   t.integer  :renewal_id
#   t.datetime :start_date
#   t.datetime :end_date
#   t.json     :data,                                    default: {},    null: false
#   t.hstore   :managers_data,                           default: {},    null: false
#   t.string   :transaction_id
#   t.boolean  :synced,                                  default: false, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :payments, [:payment_kit_id], name: :index_payments_on_payment_kit_id, using: :btree
# add_index :payments, [:paymethod_id], name: :index_payments_on_paymethod_id, using: :btree
# add_index :payments, [:renewal_id], name: :index_payments_on_renewal_id, using: :btree

class Payment < ActiveRecord::Base
  belongs_to :payment_kit
  belongs_to :renewal, class_name: 'PaymentKit'
  belongs_to :paymethod

  has_one :product, through: :payment_kit
  has_one :namespace, through: :payment_kit
  has_one :subproduct, through: :payment_kit
  has_one :period, through: :payment_kit

  has_many :access_rights
  has_many :sessions, through: :access_rights, source: :reader, source_type: 'Session'
  has_many :users, through: :access_rights, source: :reader, source_type: 'User'
  has_many :mobile_users, through: :access_rights, source: :reader, source_type: 'MobileUser'
  has_many :snapshots, as: :subject

  before_create do
    self.data[:guid] = SecureRandom.uuid
    self.data_will_change!
  end

  validates :paymethod_id, :status, :price, presence: true

  STATUSES = [
      [:notpaid, 'неоплачен'],
      [:paid, 'оплачен'],
      [:canceled, 'отменен']
  ].freeze

  MOBILE_OPERATOR_SLUGS = %w(mts tele2 beeline megafon)

  enum status: Hash[STATUSES.map.with_index { |status, idx| [status[0], idx] }]

  scope :bymethod, ->(slug) { joins(:paymethod).where(paymethods: { slug: slug }) }
  scope :unsynced, -> { where(synced: false) }
  scope :active, -> { paid.where('now() between payments.start_date and payments.end_date') }
  scope :expired, -> { paid.where('now() > payments.end_date') }
  scope :active_at, ->(timestamp) { paid.where('? between payments.start_date and payments.end_date', timestamp) }
  scope :mobile, -> { where(payment_kit_id: PaymentKit.where(namespace_id: Namespace.where(slug: MOBILE_OPERATOR_SLUGS).map(&:id))) }
  scope :not_mobile, -> { where.not(payment_kit_id: PaymentKit.where(namespace_id: Namespace.where(slug: MOBILE_OPERATOR_SLUGS).map(&:id))) }

  paginates_per 50

  %i(client_ip autopayment biller_client_id subscribe_base_id account).each do |a|
    define_method(a) do
      data.symbolize_keys[a.to_sym]
    end

    define_method("#{a}=") do |value|
      data[a.to_s] = value
      data_will_change!
    end
  end

  %i(comment status).each do |a|
    define_method("manager_#{a}") do
      managers_data.symbolize_keys[a.to_sym]
    end

    define_method("manager_#{a}=") do |value|
      managers_data[a.to_s] = value
      managers_data_will_change!
    end
  end

  def self.find_by_subscription_id(id)
    where("CAST(data->>'subscription_id' AS text) = ?", id).order(:updated_at).last
  end

  def user
    users.first
  end

  def mobile_user
    mobile_users.first
  end

  def session
    sessions.first
  end

  def can_renewal?
    can_renewal_base? && can_renewal_paymethod?
  end

  def can_renewal_base?
    data_sym = data.symbolize_keys
    paid? && renewal_id && !data_sym[:next_payment_id] && !data_sym[:renewal_failed]
  end

  def next_billing_date
    if paid? && can_renewal?
      end_date - renewal.autopay_timeout
    end
  end

  def can_renewal_paymethod?
    if %w(card qiwi web_money yandex_kassa paymaster).include? self.paymethod.slug
      !user || user.autopay
    else
      true
    end
  end

  def have_next_subscription?
    if !user
      false
    elsif online_subscription?
      last_access = user.last_access_right
      last_access && last_access.end_date > end_date
    elsif paper_subscription?
      last_payment = user.last_paper_payment
      last_payment && last_payment.end_date > end_date
    end
  end

  def paper_subscription?
    product.slug == 'subscription' && %w(paper profi).include?(subproduct.slug)
  end

  def online_subscription?
    product.slug == 'subscription' && %w(online profi student_online ).include?(subproduct.slug)
  end

  def mobile_subscription?
    product.slug == 'subscription' && subproduct.slug == Settings.paywall_mobile.subproduct
  end

  def grant_access
    new_start_date, new_end_date = nil
    if paid? && (online_subscription? || mobile_subscription?)
      new_start_date = start_date
      new_end_date   = end_date
    end

    access_rights.each do |a|
      if new_start_date != a.start_date || new_end_date != a.end_date
        Common::ChangeAccessPeriod.new(a.id, start_date: new_start_date, end_date: new_end_date).call
      end
    end
  end

  def synced!
    self.synced       = true
    data['synced_at'] = Time.now
    data_will_change!
    save
    Snapshot.create!(
        subject: self,
        event:   'payment_synced',
        data:    attributes
    )
  end

  def status_title
    STATUSES[attributes['status']].second
  end

  def send_user_mail
    PaymentMailer.new_subscription(user, self).deliver
  end

  def send_autopay_user_email
    PaymentMailer.new_autopayment(user, self).deliver
  end

  def redirect_url_for_mobile_user
    if mobile_user.contact
      data['return_url']
    else
      [Settings.hosts.lago, 'mobile_user', 'profile'].join('/')
    end
  end

  def subscription_id
    data['subscription_id']
  end

  def operator_subscription_id
    subscription_id.to_s.split(':').last
  end

  def active?
    start_date < Time.now && end_date > Time.now
  end

  def inactive?
    !active?
  end

  def fresh?(interval: 12.hours)
    start_date > (Time.now - interval)
  end

  def fire_event!(attributes = {})
    Resque.enqueue(Workers::Statistics::FirePaymentEvent, type, {
                                                            payment_id: id,
                                                            status:     status,
                                                            sum:        price,
                                                            paymethod:  paymethod.try(:slug),
                                                            product:    product.try(:slug),
                                                            namespace:  namespace.try(:slug),
                                                            subproduct: subproduct.try(:slug),
                                                            period:     period.try(:slug),
                                                            user_id:    user.try(:id),
                                                            session_id: session.try(:id),
                                                            msisdn:     mobile_user.try(:msisdn),
                                                            created_at: created_at,
                                                            updated_at: updated_at
                                                        }.merge(attributes))
  end

  def type
    return :pay_init if status == 'notpaid'
    if (reader = user || mobile_user || session)
      if reader.payments.expired.count > 0
        return :prolong if data['autopayment']
        (Time.now - end_date) > 30.days ? :return : :renewal
      else
        :first
      end
    end
  end
end
