# create_table :pressrelease_banners, force: true do |t|
#   t.integer  :document_id, limit: 8, null: false
#   t.integer  :banner_id,   limit: 8, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :pressrelease_banners, [:banner_id], name: :index_pressrelease_banners_on_banner_id, unique: true, using: :btree
# add_index :pressrelease_banners, [:document_id], name: :index_pressrelease_banners_on_document_id, unique: true, using: :btree

class PressreleaseBanner < ActiveRecord::Base
  validates :document_id, :banner_id, presence: true, uniqueness: true
end
