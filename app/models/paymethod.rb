# create_table :paymethods, force: true do |t|
#   t.string   :slug,        null: false
#   t.string   :title,       null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.text     :description
# end

class Paymethod < ActiveRecord::Base
  has_many :snapshots, as: :subject

  validates :slug, :title, presence: true
  validates :slug, uniqueness: true
end
