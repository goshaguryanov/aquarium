# create_table :prices, force: true do |t|
#   t.integer  :product,                                                null: false
#   t.integer  :period,                                                 null: false
#   t.boolean  :student,                                default: false, null: false
#   t.decimal  :value,         precision: 10, scale: 2,                 null: false
#   t.datetime :since,                                                  null: false
#   t.datetime :expire
#   t.integer  :upsell_ids,                             default: [],    null: false, array: true
#   t.decimal  :crossed_price, precision: 10, scale: 2
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :renewal_id
# end
#
# add_index :prices, [:product, :period, :student, :since, :expire], name: :index_prices_all_options, using: :btree
# add_index :prices, [:renewal_id], name: :index_prices_on_renewal_id, using: :btree
# add_index :prices, [:since, :expire], name: :index_prices_on_since_and_expire, using: :btree

class Price < ActiveRecord::Base
  belongs_to :renewal, class_name: 'Price', foreign_key: :renewal_id
  validates :product, :period, :value, :since, presence: true
  
  enum product: {
    online: 0,
    paper:  1,
    profi:  2,

    conf_translation: 100,
  }
  
  enum period: {
    for_ever:     0,
    for_month:    1,
    for_3months:  3,
    for_4months:  4,
    for_5months:  5,
    for_6months:  6,
    for_10months: 10,
    for_year:     12
  }
  
  scope :active, -> do
    where('(prices.expire IS NULL AND prices.since < :now) OR (:now BETWEEN prices.since AND prices.expire)', now: Time.now)
  end
  
  def upsell
    upsell_ids.map { |price_id| Price.find(price_id) }
  end

  def slx_product
    product.to_s == 'paper' ? 'offline' : product.to_s
  end

  def period_seconds
    case period.to_sym
    when :for_month    then 1.month
    when :for_3months  then 3.month
    when :for_4months  then 4.month
    when :for_5months  then 5.month
    when :for_6months  then 6.month
    when :for_10months then 10.month
    when :for_year     then 1.year
    when :for_ever     then 0
    end
  end
end
