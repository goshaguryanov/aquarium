# create_table :import_news, force: true do |t|
#   t.integer  :import_source_id
#   t.string   :title,                         null: false
#   t.string   :url
#   t.text     :body,             default: "", null: false
#   t.datetime :news_date,                     null: false
#   t.json     :data,                          null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :import_news, [:import_source_id], name: :index_import_news_on_import_source_id, using: :btree

class ImportNews < ActiveRecord::Base
  belongs_to :import_source
end
