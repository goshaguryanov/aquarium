# create_table :access_rights, force: true do |t|
#   t.integer  :order_id
#   t.datetime :start_date
#   t.datetime :end_date
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :reader_id,   null: false
#   t.string   :reader_type, null: false
#   t.integer  :payment_id
# end
#
# add_index :access_rights, [:order_id], name: :index_access_rights_on_order_id, using: :btree
# add_index :access_rights, [:payment_id], name: :index_access_rights_on_payment_id, using: :btree
# add_index :access_rights, [:reader_id, :reader_type], name: :index_access_rights_on_reader_id_and_reader_type, using: :btree

class AccessRight < ActiveRecord::Base
  belongs_to :reader, polymorphic: true
  belongs_to :order
  belongs_to :payment
  
  has_many :snapshots, as: :subject

  validates_presence_of :reader
  
  scope :active, -> do
    where('? BETWEEN access_rights.start_date AND access_rights.end_date', Time.now)
  end

  scope :unexpired, -> do
    where('access_rights.end_date > now()')
  end

  scope :paid, -> do
    where('access_rights.end_date IS NOT NULL')
  end
  
  def dump_to_redis
    reader.dump_sessions_to_redis
  end
  
  def active?
    start_date <= Time.now && Time.now <= end_date
  end
  
  def confirm_users
    reader.confirm_subscribers
  end
end
