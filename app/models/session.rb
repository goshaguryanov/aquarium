# create_table :sessions, force: true do |t|
#   t.string   :access_token,   null: false
#   t.integer  :user_id
#   t.integer  :account_id
#   t.inet     :ip,             null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :mobile_user_id
# end
#
# add_index :sessions, [:access_token], name: :index_sessions_on_access_token, unique: true, using: :btree
# add_index :sessions, [:account_id], name: :index_sessions_on_account_id, using: :btree
# add_index :sessions, [:mobile_user_id], name: :index_sessions_on_mobile_user_id, using: :btree
# add_index :sessions, [:user_id], name: :index_sessions_on_user_id, using: :btree

class Session < ActiveRecord::Base
  belongs_to :user
  belongs_to :account
  belongs_to :mobile_user
  has_many :access_rights, as: :reader
  has_many :snapshots, as: :subject
  has_many :payments, through: :access_rights

  validates :access_token, :ip, presence: true

  def dump_to_redis
    redis_dumper.dump(persist: persisted?)
  end

  alias_method :dump_sessions_to_redis, :dump_to_redis

  def subscription_status
    case
    when access_rights.active.any? || (user && user.has_active_access_rights?)
      'subscriber'
    when mobile_user && mobile_user.has_active_access_rights?
      'mobile_subscriber'
    else
      'not_subscriber'
    end
  end

  def auth_state
    case
    when user.nil?
      'guest'
    when user && user.registered?
      'registered'
    else
      'authenticated'
    end
  end

  def user_promo
    if user.present?
      user.accepted_promo.keys
    else
      []
    end
  end

  def confirm_subscribers; end

  def payment
    payments.paid.last
  end

#private
  def redis_dumper
    RedisDumper.new(self)
  end

  def destroy
    redis_dumper.delete_from_redis
    super
  end

  def from_redis
    begin
      Aquarium::Redis.connection.hgetall(redis_key).merge('access_token' => access_token)
    rescue Redis::CommandError
      {}
    end
  end

  def redis_key
    "sessions:#{access_token}"
  end

  class RedisDumper
    attr_reader :session

    def initialize(session)
      @session = session
    end

    def dump(persist:)
      redis.mapped_hmset(session_key, data)

      if persist
        redis.persist(session_key)
      else
        redis.expire(session_key, 90.days)
      end

      true
    end

    def delete_from_redis
      redis.del(session_key)
    end

    def data
      {
          status:          session.subscription_status,
          state:           session.auth_state,
          ip:              session.ip.to_s,
          promo_accepted:  user_accepted_promo?,
          last_visited_at: Time.now
      }
    end

    def user_accepted_promo?
      promo_active? && session.user_promo.include?(Settings.promo.slug)
    end

    def promo_active?
      Date.today < Date.parse(Settings.promo.ends_on) if Settings.promo?
    end

    def session_key
      "sessions:#{session.access_token}"
    end

    def redis
      Aquarium::Redis.connection
    end
  end

  class << self
    def generate_token
      salt = "#{Time.now.to_f}_#{Process.pid}"
      Digest::SHA1.hexdigest(salt)
    end

    def multi_find(access_token)
      Session.find_by_access_token(access_token) || begin
        data = Aquarium::Redis.connection.hgetall("sessions:#{access_token}")
        Session.new(data.slice(:ip).merge(access_token: access_token)) if data.present?
      end
    end
  end
end
