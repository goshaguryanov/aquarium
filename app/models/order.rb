# create_table :orders, force: true do |t|
#   t.string   :uuid,                           null: false
#   t.integer  :payment_status, default: 0,     null: false
#   t.integer  :payment_method,                 null: false
#   t.string   :transaction_id
#   t.datetime :start_date
#   t.datetime :end_date
#   t.json     :data,           default: {},    null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :price_id
#   t.boolean  :synced,         default: false, null: false
#   t.hstore   :managers_data,  default: {},    null: false
# end
#
# add_index :orders, [:payment_method, :transaction_id], name: :index_orders_on_payment_method_and_transaction_id, unique: true, using: :btree
# add_index :orders, [:price_id], name: :index_orders_on_price_id, using: :btree

class Order < ActiveRecord::Base
  belongs_to :price
  
  has_many :access_rights
  has_many :users, through: :access_rights, source: :reader, source_type: 'User'
  has_many :snapshots, as: :subject
  
  validates :payment_method, presence: true
  validates :uuid, presence: true, uniqueness: true
  
  before_validation do |order|
    order.uuid  ||= SecureRandom.uuid
  end
  
  enum payment_status: {
    notpaid:  0, # заказ еще неоплачен
    paid:     1, # заказ оплачен
    canceled: 2  # заказ отменен
  }
  
  enum payment_method: {
    by_card:    0,
    by_apple:   1,
    by_yandex:  2,
    by_google:  3,
    by_unknown: 404 # экспортирован из базы подписки, способ оплаты нам не важен
  }
  
  scope :unsynced, -> { where(synced: false) }
  
  paginates_per 50

  DATA_ATTRIBUTES = %i(client_ip autopayment subscribe_base_id account).freeze
  MANAGERS_DATA_ATTRIBUTES = %i(comment status).freeze
  
  def payment
    PaymentProcess.create(self)
  end
  
  DATA_ATTRIBUTES.each do |a|
    define_method(a) do
      data.symbolize_keys[a.to_sym]
    end
    
    define_method("#{a}=") do |value|
      data[a.to_s] = value
      data_will_change!
    end
  end

  MANAGERS_DATA_ATTRIBUTES.each do |a|
    define_method("manager_#{a}") do
      managers_data.symbolize_keys[a.to_sym]
    end
    
    define_method("manager_#{a}=") do |value|
      managers_data[a.to_s] = value
      managers_data_will_change!
    end
  end

  def paper_subscription?
    price && (price.paper? || price.profi?)
  end

  def online_subscription?
    price && (price.online? || price.profi?)
  end
  
  def autopayment?
    autopayment || false
  end
  
  def grant_access
    new_start_date, new_end_date = nil
    if paid? && price && (price.online? || price.profi?)
      new_start_date = start_date
      new_end_date = end_date
    end

    access_rights.each do |a|
      if new_start_date != a.start_date || new_end_date != a.end_date
        Common::ChangeAccessPeriod.new(a.id, start_date: new_start_date, end_date: new_end_date).call
      end
    end
  end
  
  def synced!
    self.synced = true
    data['synced_at'] = Time.now
    data_will_change!
    save
    Snapshot.create!(
      subject: self,
      event:   'order_synced',
      data:    attributes
    )
  end
  
  def user
    users.first
  end
  
  def send_user_mail
    if user
      if price.online?
        OrderMailer.order_online_email(user, self).deliver
      elsif price.profi?
        OrderMailer.order_profi_email(user, self).deliver
      elsif price.paper?
        OrderMailer.order_paper_email(user, self).deliver
      elsif price.conf_translation?
        OrderMailer.order_conf_translation_email(user, self).deliver
      end
    end
  end
end
