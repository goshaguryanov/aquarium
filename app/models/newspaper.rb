# create_table :newspapers, force: true do |t|
#   t.integer  :document_id,  limit: 8,              null: false
#   t.date     :release_date,                        null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.hstore   :urls,                   default: {}, null: false
# end
#
# add_index :newspapers, [:document_id], name: :index_newspapers_on_document_id, unique: true, using: :btree
# add_index :newspapers, [:release_date], name: :index_newspapers_on_release_date, unique: true, using: :btree

class Newspaper < ActiveRecord::Base
  IMAGE_VERSIONS = %w(mobile_low mobile_high).freeze
  
  validates :document_id, :release_date, :urls, presence: true
  validates :document_id, :release_date, uniqueness: true
end
