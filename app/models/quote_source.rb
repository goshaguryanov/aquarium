# create_table :quote_sources, force: true do |t|
#   t.string   :title,      null: false
#   t.string   :slug,       null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :quote_sources, [:slug], name: :index_quote_sources_on_slug, unique: true, using: :btree
# add_index :quote_sources, [:title], name: :index_quote_sources_on_title, unique: true, using: :btree

class QuoteSource < ActiveRecord::Base
  validates :title, :slug, presence: true, uniqueness: true
  has_many :tickers
  
  def updater
    @updater ||= begin
      classname = Settings.quotes[slug].updater
      classobj = Object.const_get(classname)
      classobj.new(self)
    end
  end
  
  def update
    updater.call
  end
end
