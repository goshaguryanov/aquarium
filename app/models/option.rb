# create_table :options, force: true do |t|
#   t.string   :type,       null: false
#   t.string   :value,      null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class Option < ActiveRecord::Base
  TYPES = %w(company_speciality appointment income auto)
  
  validates :type, :value, presence: true
  validates :type, inclusion: { in: TYPES }
  
  self.inheritance_column = :sti_disabled
  
  TYPES.each do |type|
    scope type, -> { where(type: type) }
  end
  
  def to_redis
    [id, value]
  end
  
  class << self
    def save_type_to_redis(type)
      data = send(type).map(&:to_redis)
      json = Oj.dump(data)
      Aquarium::Redis.connection.set("shark:user_options:#{type}", json)
    end
  end
end
