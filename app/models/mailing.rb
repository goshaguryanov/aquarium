# create_table :mailings, force: true do |t|
#   t.integer  :type_id
#   t.text     :template
#   t.datetime :scheduled_at
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :status,          default: 0,  null: false
#   t.json     :data,            default: {}, null: false
#   t.string   :subject
#   t.integer  :contacts_status, default: 0,  null: false
# end

class Mailing < ActiveRecord::Base
  belongs_to :type, class_name: 'Mailing::MailType'
  belongs_to :user
  has_many :letters, class_name: 'Mailing::Letter'
  has_many :contacts, through: :letters
  has_many :snapshots, as: :subject

  enum status: {
    created:   0, # создана, еще редактируется
    ready:     6, # готова к рассылке, как только наступит время в scheduled_at - разошлется
    scheduled: 4, # послана на рассылку в pigeon, забираются контакты
    started:   1, # контакты закачены рассыльщиком, начата рассылка
    finished:  2, # рассылка успешно завершена
    railed:    3  # рассылка прервалась с ошибкой
  }

  enum contacts_status: {
    contacts_empty:   0,
    contacts_linking: 10,
    contacts_ready:   20
  }

  validates :type_id, :subject, presence: true

  def perform!
    response = pigeon_client.get("/mailings/#{id}")
    if response.success?
      self.scheduled!
    end
  end

  def template_with_banners
    banners = Mailing::Banner.by_time(Time.now)
    active_template = ActiveTemplate.new
    template.gsub(/<!--\s*banner:\d+\s*-->/) do |element|
      if (banner = banners.by_type(type.id).find_by_position(element.scan(/\d+/)))
        active_template.render('mailing_generators/newsrelease/banner', banner: banner)
      end
    end
  end

  def banners_active
    Mailing::Banner.by_type(type.id).by_time(Time.now)
  end

private
  def pigeon_client
    @pigeon_client ||= PigeonClient.new
  end
end
