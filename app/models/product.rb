# create_table :products, force: true do |t|
#   t.string   :slug,       null: false
#   t.string   :title,      null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end

class Product < ActiveRecord::Base
  validates :slug, :title, presence: true
  validates :slug, uniqueness: true

  has_many :subproducts, dependent: :destroy
  has_many :namespaces,  dependent: :destroy
  has_many :payment_kits
  has_many :snapshots, as: :subject
end
