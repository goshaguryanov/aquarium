# create_table :mailing_types, force: true do |t|
#   t.string   :title,                              null: false
#   t.string   :template_classname
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :slug,                               null: false
#   t.boolean  :is_visible,         default: true,  null: false
#   t.boolean  :subscribers_only,   default: false, null: false
# end
#
# add_index :mailing_types, [:slug], name: :index_mailing_types_on_slug, unique: true, using: :btree

class Mailing::MailType < ActiveRecord::Base
  self.table_name = :mailing_types
  
  validates_presence_of :slug, :title
  validates_uniqueness_of :slug
  
  scope :is_visible, -> { where(is_visible: true) }
  scope :hidden, -> { where(is_visible: false) }
  
  def hidden?
    !is_visible
  end
end
