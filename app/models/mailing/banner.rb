# create_table :mailing_banners, force: true do |t|
#   t.string   :title,                       null: false
#   t.text     :body,       default: "",     null: false
#   t.string   :url,                         null: false
#   t.integer  :type_ids,   default: [],     null: false, array: true
#   t.datetime :start_date,                  null: false
#   t.datetime :end_date,                    null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :position,                    null: false
#   t.string   :type,       default: :text, null: false
#   t.text     :embed,      default: "",     null: false
#   t.json     :picture,    default: {},     null: false
# end

class Mailing::Banner < ActiveRecord::Base
  self.table_name = :mailing_banners
  self.inheritance_column = :sti_disabled
  paginates_per 50

  scope :by_time, ->(time) { current_scope.where('? BETWEEN "mailing_banners"."start_date" AND "mailing_banners"."end_date"', time) }
  scope :by_type, ->(type) { current_scope.where('"mailing_banners"."type_ids" @> ARRAY[?]::INTEGER[]', type) }
  
  validates_presence_of :title, :type, :url, :type_ids, :start_date, :end_date, :position
end
