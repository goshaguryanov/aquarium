# create_table :tickers, force: true do |t|
#   t.string   :title,                                                 null: false
#   t.string   :slug,                                                  null: false
#   t.integer  :quote_source_id,                                       null: false
#   t.datetime :price_time,                                            null: false
#   t.decimal  :value,           precision: 15, scale: 4,              null: false
#   t.decimal  :prev_value,      precision: 15, scale: 4
#   t.json     :data,                                     default: {}, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :tickers, [:quote_source_id, :slug], name: :index_tickers_on_quote_source_id_and_slug, unique: true, using: :btree

class Ticker < ActiveRecord::Base
  belongs_to :quote_source
  
  validates :title, :slug, :price_time, :value, presence: true
  validates :slug, uniqueness: { scope: :quote_source }

  def full_slug
    [quote_source.slug, slug].join('.')
  end
end
