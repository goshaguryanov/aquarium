# create_table :import_sources, force: true do |t|
#   t.string   :slug,       null: false
#   t.string   :title,      null: false
#   t.string   :url,        null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :import_sources, [:slug], name: :index_import_sources_on_slug, unique: true, using: :btree

class ImportSource < ActiveRecord::Base
  has_many :import_news
  
  validates :slug, :title, :url, presence: true
  validates :slug, uniqueness: true
  
  def importer
    @importer ||= begin
      classname = Settings.import_news[slug].importer
      classobj = Object.const_get(classname)
      classobj.new(self)
    end
  end
  
  def import
    importer.call
  end
end
