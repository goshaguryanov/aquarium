# create_table :namespaces, force: true do |t|
#   t.integer  :product_id
#   t.string   :slug,                           null: false
#   t.string   :title,                          null: false
#   t.datetime :start_date,                     null: false
#   t.datetime :end_date
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.boolean  :base,           default: false, null: false
#   t.integer  :redirect_to_id
#   t.json     :picture,        default: {},    null: false
#   t.hstore   :texts,          default: {},    null: false
# end
#
# add_index :namespaces, [:product_id], name: :index_namespaces_on_product_id, using: :btree
# add_index :namespaces, [:redirect_to_id], name: :index_namespaces_on_redirect_to_id, using: :btree

class Namespace < ActiveRecord::Base
  belongs_to :product
  belongs_to :redirect_to, class_name: 'Namespace'
  has_many :payment_kits
  has_many :snapshots, as: :subject

  validates :slug, :title, :start_date, :end_date, :product_id, presence: true
  validates :slug, uniqueness: { scope: :product_id }

  scope :active, -> do
    where('? BETWEEN start_date AND end_date', Time.now)
  end

  scope :baselist, -> do
    where(base: true)
  end
  
  class << self
    def base
      baselist.take
    end
  end
end
