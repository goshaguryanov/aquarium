# create_table :snapshots, force: true do |t|
#   t.integer  :subject_id,                          null: false
#   t.string   :subject_type,                        null: false
#   t.integer  :author_id,    limit: 8
#   t.string   :event,                               null: false
#   t.json     :data,                   default: {}, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :service
# end
#
# add_index :snapshots, [:author_id], name: :index_snapshots_on_author_id, using: :btree
# add_index :snapshots, [:subject_id, :subject_type], name: :index_snapshots_on_subject_id_and_subject_type, using: :btree

class Snapshot < ActiveRecord::Base
  SERVICES = %w(shark ibis).freeze
  
  belongs_to :subject, polymorphic: true
  belongs_to :author, class_name: 'User'
  
  validates :subject, :event, :data, presence: true
  validates :service, inclusion: { in: SERVICES }, allow_nil: true
end
