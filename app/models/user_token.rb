# create_table :user_tokens, force: true do |t|
#   t.integer  :user_id
#   t.string   :token,                      null: false
#   t.boolean  :used,       default: false, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :type,                       null: false
#   t.json     :token_data, default: {},    null: false
# end
#
# add_index :user_tokens, [:user_id], name: :index_user_tokens_on_user_id, using: :btree

class UserToken < ActiveRecord::Base
  VALID_TYPES = %w(
    confirmation
    password_recovery
    social_link
    contact_confirmation
  ).freeze
  
  belongs_to :user
  
  validates :token, :type, presence: true
  validates :type, inclusion: { in: VALID_TYPES }
  
  self.inheritance_column = :sti_disabled

  scope :unused, -> { where(used: false) }
  
  VALID_TYPES.each do |type|
    scope type, -> { where(type: type) }
  end
  
  class << self
    def generate(token_data = {})
      create! do |token|
        token.token      = Digest::SHA1.hexdigest(pepper)
        token.token_data = token_data
      end
    end
    
  private
    def pepper
      [Time.now.to_f, rand, Process.pid].join
    end
  end
end
