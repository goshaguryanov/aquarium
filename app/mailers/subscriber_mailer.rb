class SubscriberMailer < ActionMailer::Base
  default from: Settings.mailer.from
  
  def confirmation_email(confirmation_token, mail_type, unsubscribe_token)
    @contact = Contact.find(confirmation_token.token_data['contact_id'])
    @mail_type = mail_type
    @confirmation_url = "#{Settings.hosts.lago}/subscribe_confirm/#{confirmation_token.token}"
    @unsubscribe_url  = "#{Settings.hosts.lago}/unsubscribe_confirm/#{@contact.unsubscribe_token(mail_type.slug)}"
    
    mail(to: @contact.email, subject: 'Подписка на рассылки Vedomosti.ru')
  end
end
