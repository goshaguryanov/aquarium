class InnerMailer < ActionMailer::Base
  default from: '911@vedomosti.ru'
  
  def new_subscription(payment)
    @url = "#{Settings.hosts.ibis}/payments/#{payment.id}/edit"
    
    mail(to: Settings.mailer.subscribe_dept, subject: 'Новый заказ')
  end
  
  def renewal_failed(payment, reason)
    @url = "#{Settings.hosts.ibis}/payments/#{payment.id}/edit"
    @reason = reason
    
    mail(to: Settings.mailer.subscribe_dept, subject: 'Ошибка автопродления')
  end

  def autopay_changed(user)
    @user = user
    @url = "#{Settings.hosts.ibis}/users/#{@user.id}/edit"

    mail(to: Settings.mailer.subscribe_dept, subject: "Уведомление об отключении автопродления подписки")
  end
end
