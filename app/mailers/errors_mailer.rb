class ErrorsMailer < ActionMailer::Base
  default from: Settings.mailer.from,
          to:   Settings.mailer.admin
  
  def server_errors_email(errors)
    @errors = errors
    mail(subject: 'Server Errors HTTP 50x')
  end
end
