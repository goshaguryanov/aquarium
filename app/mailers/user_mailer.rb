class UserMailer < ActionMailer::Base
  default from: Settings.mailer.from
  
  def confirmation_email(token)
    @user = token.user
    @url = "#{Settings.hosts.lago}/confirm?token=#{token.token}"
    
    mail(default_headers_with(subject: 'Подтвердите регистрацию на vedomosti.ru', type: :email_confirmation))
  end

  def send_oath_confirmation_email(token)
    @user = token.user
    @url = "#{Settings.hosts.lago}/confirm?token=#{token.token}"

    mail(default_headers_with(subject: 'Подтвердите ваш аккаунт на сайте', type: :email_confirmation))
  end
  
  def welcome_email(user)
    @user = user
    @url = "http://buy.vedomosti.ru/subscription/base/?from=welcome-registration&utm_source=new-users&utm_medium=auto-email&utm_campaign=base"
    
    mail(default_headers_with(subject: 'Важная информация для читателей!', type: :welcome))
  end
  
  def password_recovery_email(token)
    @user = token.user
    @url = "#{Settings.hosts.lago}/password_reset/#{token.token}"
    
    mail(default_headers_with(subject: 'Изменение вашего пароля на сайте', type: :password_recovery))
  end
  
  def social_account_link_email(token)
    @user = token.user
    @url = "#{Settings.hosts.lago}/social_account_link/#{token.token}"
    # @account_type = I18n.t("account_type.#{token.token_data["provider"]}")
    
    mail(default_headers_with(subject: 'Ваш аккаунт на Vedomosti.ru', type: :social_account_link))
  end

  private

  def default_headers_with(subject: , type: , **options)
    cur_month = Time.now.strftime('%b-%Y')
    {
        to: @user.email,
        subject: subject,
        'X-Mailru-Msgtype' => "#{type}.#{cur_month}",
        'List-id' => "<#{type}.#{cur_month}>"
    }.merge(options)
  end
end
