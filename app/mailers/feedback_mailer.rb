class FeedbackMailer < ActionMailer::Base
  default from: Settings.mailer.from,
          to:   Settings.mailer.feedback_email
  
  def feedback_email(feedback_id)
    @feedback = Feedback.find(feedback_id)
    mail(subject: @feedback.mail_subject)
  end
end
