class SubscribeRequest < ActionMailer::Base

  def company(params)
    params.each { |k, v| instance_variable_set("@#{k}", v) }
    mail(subject: "Заявка на корпоративную подписку для #{@title}", from: @email, to: 'podpiska@vedomosti.ru')
  end

  def gift(params)
    params.each { |k, v| instance_variable_set("@#{k}", v) }
    mail(subject: "Заявка на подарочную подписку для #{@name}", from: @email, to: 'podpiska@vedomosti.ru')
  end
end
