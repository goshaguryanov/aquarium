class PigeonClient
  extend Forwardable
  
  def_delegators :connection, *%i(get post put patch delete)
  
private
  def connection
    Faraday.new(url: Settings.hosts.pigeon) do |builder|
      builder.use SnipeAuth
      builder.request  :json
      builder.response :mashify
      builder.response :oj, content_type: 'application/json'
      builder.adapter  :net_http_persistent
    end
  end
end
