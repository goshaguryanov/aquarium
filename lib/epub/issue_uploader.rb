module Epub
  class IssueUploader

    attr_reader :newsrelease_id, :epub_file, :attachment_id

    def initialize(newsrelease_id, epub_file)
      @newsrelease_id = newsrelease_id
      @epub_file      = epub_file
    end

    def call
      create_box
      "#{Settings.hosts.agami}#{uploaded_file[:versions][:original][:url]}"
    end

  private
    def uploaded_file
      @uploaded_file ||= AgamiUploader.upload(epub_file).body
    end

    def replace_file(directory)
      @uploaded_file = AgamiUploader.replace(epub_file, directory, 'converts={"original":""}').body
    end

    def attachment
      @attachment_data ||= snipe_client.post('/v1/attachments', { attributes: uploaded_file }).body
    end

    def reattach(attachment_id)
      @attachment_data ||= snipe_client.patch("/v1/attachments/#{attachment_id}", { attributes: uploaded_file }).body
    end

    def snipe_client
      @snipe_client ||= SnipeClient.new
    end

    def create_box
      # вдруг бокс с epub уже есть
      boxes = snipe_client.get("/v1/documents/#{newsrelease_id}/boxes").body
      epub_box = Array(boxes.first.children).find {|b| b.type=='newsrelease_epub' }

      if epub_box
        attachment_data = snipe_client.get("/v1/attachments/#{epub_box.epub_id}").body
        replace_file(attachment_data.directory)
        reattach(epub_box.epub_id)
      else
        data = {
          attributes: {
            type: 'newsrelease_epub',
            parent_id: root_box[:id],
            position: 1,
            enabled: true,
            epub_id: attachment[:id]
          }
        }
        snipe_client.post('/v1/boxes', data).body
      end
      
      snipe_client.patch("/v1/documents/#{newsrelease_id}/publish").body if published?
    end

    def published?
      if doc = snipe_client.get("/v1/documents/#{newsrelease_id}").body
        doc.published
      end
    end

    def root_box
      @root_box ||= snipe_client.get("/v1/documents/#{newsrelease_id}/root_box").body
    end

    def snipe_client
      @snipe_client ||= SnipeClient.new
    end
  end
end
