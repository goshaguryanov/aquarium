module Epub
  class IssueGenerator
    include ERB::Util

    attr_reader :cover, :newsrelease_date

    WEEKDAYS = %w(Воскресенье Понедельник Вторник Среда Четверг Пятница Суббота).freeze
    MONTHS = %w(января февраля марта апреля мая июня июля августа сентября октября ноября декабря).freeze

    def initialize(date = nil)
      @newsrelease_date = date ? Time.parse(date.to_s) : Time.now
    end

    def generate
      tmpfile = save_epub
      IssueUploader.new(newsrelease.id, tmpfile.path).call
    rescue
      nil
    ensure
      if tmpfile
        tmpfile.close
        tmpfile.unlink
      end
    end

    def save_epub
      Dir.mktmpdir('epub') do |dir|
        generator = self
        epub = EeePub.make do
          title      "Vedomosti #{generator.newsrelease_date.strftime('%e.%m.%Y')}"
          publisher  'Business News Media'
          date       generator.newsrelease_date.strftime('%Y-%m-%d')
          identifier "https://www.#{generator.newspaper_url}", :scheme => 'URL', :id => 'BookId'

          files generator.generate_static(dir) + generator.generate_content(dir) + generator.generate_images

          cover generator.cover.file
        end
        epub.save(epub_file.path)
      end
      
      epub_file
    end

    def newspaper_url
      [Settings.domain, 'newspaper', newsrelease_date.strftime('%Y/%m/%d')].join('/')
    end

    def generate_content(dir)
      @dir = dir
      files = []

      files << erb_process('cover.html', File.join(dir, 'cover.html'))
      files << erb_process('toc.html', File.join(dir, 'toc.html'))

      newspaper_pages.each_with_index do |page, i|
        @page = page
        @prev_page = i>0 ? newspaper_pages[i-1] : nil
        @next_page = i<newspaper_pages.length-1 ? newspaper_pages[i+1] : nil
        files << erb_process('page.html', File.join(dir, "p_#{page.slug}.html"))
      end

      issue_documents_ordered.each_with_index do |a, i|
        @article = Roompen.document(a.url)
        if @article.is_a? Roompen::Redirect
          @article = Roompen.document(@article.location)
        end
        @page = @article.categories.has_newspaper? && @article.categories.newspaper
        @prev_article = i>0 ? issue_documents_ordered[i-1] : nil
        @next_article = i<issue_documents_ordered.length-1 ? issue_documents_ordered[i+1] : nil
        files << erb_process('article.html', File.join(dir, "a#{a.id}.html"))
      end

      files
    end

    def generate_static(dir)
      files = []

      if cover_img
        files << download("#{Settings.hosts.cdn}#{cover_img.url}", File.join(dir, 'cover.png'))
        @cover = Hashie::Mash.new(
          file:   'cover.png',
          width:  cover_img.width,
          height: cover_img.height
        )
      else
        files << static_file('cover.jpg')
        @cover = Hashie::Mash.new(
          file:   'cover.jpg',
          width:  353,
          height: 470
        )
      end

      %w(
        style.css
        arrow-right.png
        arrow-left.png
        logo.png
        RobotoSlab-Regular-webfont.eot
        RobotoSlab-Regular-webfont.svg
        RobotoSlab-Regular-webfont.ttf
        RobotoSlab-Regular-webfont.woff
        RobotoSlab-Regular-webfont.otf
      ).each do |file|
        files << static_file(file)
      end

      files
    end

    def generate_images
      @images ||= {}
      @images.map do |article_id, imgs|
        imgs.map do |varsion, img|
          img.filename
        end
      end.flatten
    end

    def newsrelease_date_formatted
      @newsrelease_date_formatted = begin
        [
          WEEKDAYS[newsrelease_date.wday],
          newsrelease_date.mday,
          MONTHS[newsrelease_date.mon-1],
          newsrelease_date.year,
          'г.'
        ].join(' ')
      end
    end

    def article_image(article, version)
      @images ||= {}
      @images[article.id] ||= {}
      @images[article.id][version] ||= begin
        if article.has_image? && article.image.has_version?(version)
          image = article.image.send(version)
          ext = begin
            filename = image.url.split('/').last
            filename.include?('.') ? ".#{filename.split('.').last}" : ''
          end
          filename = "#{article.id}_#{version}#{ext}"
          Hashie::Mash.new(
            filename: download("#{Settings.hosts.cdn}#{image.url}", File.join(@dir, filename)),
            file:     filename,
            width:    image.width,
            height:   image.height
          )
        end
      end
    end

    def article_authors(article)
      Array(article.links[:authors]).map(&:bound_document).map(&:title)
    end

  private
    def newsrelease
      @newsrelease ||= Roompen.document(newspaper_url)
    end

    def download(url, filename)
      res = Net::HTTP.get_response(URI(url))
      raise "Can't download file #{url}, status: #{res.code}" unless res.code == '200'
      File.open(filename, 'wb') { |file| file.write(res.body) }
      filename
    end

    def static_file(file)
      Rails.root.join("lib/epub/static/#{file}").to_s
    end

    def cover_img
      newsrelease.newsstand.ipad_shelf_2x if newsrelease.has_newsstand?
    end

    def erb_process(template, filename)
      File.write(filename, ERB.new(File.read(Rails.root.join("app/views/epub/#{template}.erb"))).result(binding))
      filename
    end

    def newspaper_pages
      @newspaper_pages ||= begin
        categories = Roompen.child_categories_of(%w(newspaper))
        hdocs = {}

        issue_documents.each do |doc|
          if doc.categories.has_newspaper?
            hdocs[doc.categories.newspaper.slug] ||= []
            hdocs[doc.categories.newspaper.slug] << doc
          end
        end

        pages = []
        categories.each do |category|
          if hdocs.has_key?(category.slug)
            pages << Roompen::List.new({
              title:     category.title,
              header:    category.title,
              slug:      category.slug,
              documents: hdocs[category.slug]
            })
          end
        end
        pages
      end
    end

    def epub_file
      @epub_file ||= Tempfile.new(['issue', '.epub'])
    end

    def issue_documents
      @issue_documents ||= Roompen.bound_documents(newsrelease.id).reverse
    end

    def issue_documents_ordered
      newspaper_pages.map { |p| p.documents }.flatten
    end

    def xhtml_fix(text)
      text.gsub('&nbsp;', ' ').gsub(/<br\s*>/i, '<br/>').gsub(/&(?<![a-z]{2-5};)/, '&amp;').gsub('<...>', '[...]').gsub("\u0003", '')
    end
  end
end
