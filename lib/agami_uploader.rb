class AgamiUploader
  class << self
    def connection
      Faraday.new(url: Settings.hosts.agami) do |builder|
        builder.request  :multipart
        builder.request  :url_encoded
        builder.response :oj, content_type: 'application/json'
        builder.adapter  Faraday.default_adapter
      end
    end
    
    def upload(file_path, params = nil)
      agami_upload(file_path, '', 'application/zip', params)
    end

    def replace(file_path, directory, params = nil)
      agami_upload(file_path, directory, 'application/zip', params)
    end

    def upload_from_url(url, params = nil)
      agami_upload_from_url(url, '', 'application/json', params)
    end

    def replace_from_url(url, directory, params = nil)
    agami_upload_from_url(url, directory, 'application/json', params)
    end

  private
    def agami_upload(file_path, directory, type, params = nil)
      upload = Faraday::UploadIO.new(file_path, type)
      path = "/files#{directory}"
      path = [path, params].join('?') if params.present?
      connection.post path, file: upload
    end

    def agami_upload_from_url(url, directory, type, params = nil)
      path = "/files#{directory}"
      path = [path, params].join('?') if params.present?
      connection.post path do |req|
        req.headers['Content-Type'] = type
        req.headers['X-File-Location'] = url
      end
    end
  end
end
