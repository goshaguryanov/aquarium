STATUSES = [
    "first",
    "prolong",
    "stop",
    'renewal'
]

def elastic
  @elastic ||= Elasticsearch::Client.new({
                                             host:    Settings.statistics.host,
                                             port:    Settings.statistics.port,
                                             adapter: :net_http_persistent,
                                         })
end

def add(payment, status, duration, i)
  @data       ||= []
  mobile_user = payment.mobile_users.first
  if status == STATUSES[2] # "stop"
    return if payment.end_date.strftime('%T') == '23:59:59'
    event_time = payment.end_date
  elsif status == STATUSES[0]
    mobile_user.payments.each do |p|
      if p.end_date < payment.end_date
        status = STATUSES[3]
        break
      end
    end
    event_time = payment.start_date
  else
    event_time = payment.start_date + 60*60*24*i
  end
  puts format("ID%i  %17s AT %10s for %2i  (%10s %10s)", payment.id, status, event_time, duration, payment.start_date, payment.end_date)
  @data.push index: {
                 data: {
                     payment_id: payment.id,
                     event:      status,
                     time:       event_time,
                     status:     payment.status,
                     sum:       Settings.paywall_mobile.price,
                     product:    Settings.paywall_mobile.product,
                     namespace:  payment.data["operator"],
                     paymethod:  Settings.paywall_mobile.paymethod_slug,
                     period:     Settings.paywall_mobile.period,
                     subproduct: Settings.paywall_mobile.subproduct,
                     msisdn:     mobile_user.msisdn,
                     operator:   payment.data["operator"],
                     # user_id:      mobile_user.id
                     # access_token: "access_token",
                     # session_id:   "session_id"
                 }
             }
end

def push(index_name, document_type)
  elastic.bulk index: index_name, type: document_type, body: @data
end

collect = Proc.new do |payment|
  collect_till = Time.parse(ENV['collect_till']) rescue Time.now
  from         = payment.start_date.to_date
  to           = payment.end_date.to_date
  duration     = (to - from).ceil
  (from..to).each_with_index do |_, i|
    push = (payment.start_date + i.days) < collect_till
    if push
      if i == 0
        status = STATUSES[0] # "first"
      else
        status = STATUSES[1] # "prolong"
      end
      add(payment, status, duration, i)
      if i == duration
        status = STATUSES[2] # "stop"
        add(payment, status, duration, i)
      end
    end
  end
end

namespace :payments do
  desc 'Count payments per date stats.'
  task stats: :environment do
    paymethod_id = Paymethod.find_by_slug(Settings.paywall_mobile.paymethod_slug)
    Payment.paid.where(paymethod_id: paymethod_id).order(:start_date, :end_date).each(&collect)
    push("payments", "payment")
  end
end
