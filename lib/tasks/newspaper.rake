namespace :newspaper do
  desc 'Generate newspaper archive for mobile app.'
  task archive: :environment do
    Workers::ZipperJob.perform({}, %i(mobile_low mobile_high))
  end
  
  desc 'Generate JPG/SWF pages from PDF archive.'
  task flasher: :environment do
    Workers::FlasherJob.perform
  end
end
