namespace :paywall do
  desc 'Reset all lists of visited documents'
  task reset_visited: :environment do
    options = "-h #{Settings.redis.host} -p #{Settings.redis.port} -n #{Settings.redis.db}"
    cmd = %Q(redis-cli #{options} KEYS "visited:*" | xargs redis-cli #{options} DEL)
    system(cmd)
  end
  
  desc 'Dump all sessions to redis.'
  task dump_sessions: :environment do
    puts "#{Session.count} sessions in DB."
    Session.find_each.with_index do |session, index|
      puts "#{index.succ} sessions processed." if (index.succ % 1000).zero?
      session.dump_to_redis
    end
  end
end
