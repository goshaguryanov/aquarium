namespace :mailing do
  desc 'Send newsrelease mailing'
  task newsrelease: :environment do
    Workers::Mailing::NewsreleaseDailyJob.perform
  end
end
