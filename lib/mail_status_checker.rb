class MailStatusChecker
  def by_address(email, from = 'now-24h', to = 'now')
    query_string = "to:\"#{email}\""
    response = client.search(index: 'mail-logs-*', body: query(query_string, from, to))
    result_process(response)
  end
  
  def not_sent(from = 'now-1h', to = 'now')
    query_string = 'NOT status:"sent"'
    response = client.search(index: 'mail-logs-*', body: query(query_string, from, to))
    result_process(response)
  end
  
private
  def client
    @client ||= Elasticsearch::Client.new(Settings.elasticsearch_logs)
  end
  
  def result_process(response)
    response['hits']['hits'].map do |hit|
      hit['_source'].symbolize_keys.slice(:to, :status, :reason)
    end
  end
  
  def query(query_string, from, to)
    {
      query: {
          filtered: {
              query: {
                  query_string: {
                      query: query_string,
                      analyze_wildcard: false
                  }
              },
              filter: {
                  bool: {
                      must: [ { range: { '@timestamp' => { gte: from, lte: to } } } ],
                  }
              }
          }
      },
      size: 500,
      sort: { '@timestamp' => 'desc' },
      fields: ['*', '_source']
    }
  end
end
