require 'google/api_client'

module Google
  class Verifier
    class << self
      def client
        @client ||= begin
          cl = Google::APIClient.new(
            application_name:    Settings.google.application_name,
            application_version: Settings.google.application_version
          )
          cl.authorization = Signet::OAuth2::Client.new(
            token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
            audience:             'https://accounts.google.com/o/oauth2/token',
            scope:                Settings.google.scope_url,
            issuer:               Settings.google.service_account_email,
            signing_key:          signing_key
          )
          cl.authorization.fetch_access_token!
          cl
        end
      end
      
      def key_file
        ERB.new(Settings.google.key_file).result
      end
      
      def signing_key
        Google::APIClient::KeyUtils.load_from_pkcs12(key_file, Settings.google.key_secret)
      end
      
      def publisher
        @publisher ||= client.discovered_api('androidpublisher', 'v2')
      end
      
      def verify(package_name, subscription_id, token)
        parameters = {
          api_method: publisher.purchases.subscriptions.get,
          parameters: {
            packageName:    package_name,
            subscriptionId: subscription_id,
            token:          token
          }
        }
        result = client.execute(parameters)
        if result.status == 401
          # The access token expired, fetch a new one and retry once.
          client.authorization.fetch_access_token!
          result = client.execute(parameters)
        end
        Google::Receipt.new(result, token)
      end
    end
  end
end
