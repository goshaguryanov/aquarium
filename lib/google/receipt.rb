module Google
  class Receipt
    extend Forwardable
    def_delegators :@receipt, :error, :startTimeMillis, :expiryTimeMillis, :autoRenewing, :kind, :to_hash
    def_delegators :@response, :success?

    attr_reader :receipt, :response, :token
    
    def initialize(response, token)
      @response = response
      @receipt  = Hashie::Mash.new(Oj.load(response.body))
      @token    = token
    end

    def transaction_id
      "#{token}:#{expiryTimeMillis}"
    end
  end
end
