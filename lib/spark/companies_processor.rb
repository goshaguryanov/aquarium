module Spark
  class CompaniesProcessor
    COMPANIES_LIMIT = 1000

    def initialize
      @snipe_client = SnipeClient.new
    end

    def process
      companies = Roompen.categorized(%w(kinds companies), limit: COMPANIES_LIMIT)
      result = true
      Spark::Connection.start do |connection, auth_cookies|
        companies.each do |company|
          next unless (company.respond_to?(:ogrn) && company.ogrn)
          spark_company_doc = connection.call(:get_company_extended_report, message: { ogrn: company.ogrn }, cookies: auth_cookies)
          spark_coowners_doc = connection.call(:get_company_structure , message: { ogrn: company.ogrn }, cookies: auth_cookies)
          result &= Spark::Company.new(spark_company_doc, spark_coowners_doc).process_and_upload(@snipe_client, company.id)
        end
      end
      result
    end
  end
end