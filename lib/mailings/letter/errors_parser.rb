module Mailings
  module Letter
    class ErrorsParser
      attr_accessor :message

      def header(name)
        $1.strip if message =~ /^#{name}:(.*?)^[A-Z]/xm
      end

      def body
        $1.strip if message =~ /^(.*?)^[A-Za-z\-]+:/xm
      end

      def email
        header('To')
      end

      def domain
        $1 if (contact_email = email) && contact_email =~ /@([\w\dа-яё\.\-]+)/i
      end

      def date
        Time.parse(header('Date')) rescue nil
      end

      def diagnostic_code
        header('Diagnostic-Code')
      end

      def diagnostic_code_fixed
        diagnostic_code && diagnostic_code.split(/\s+/).compact.select { |word| word =~ /^[\[\(\"\']?([A-Z][a-z\'\-]+|[A-Z\'\-]+|[a-z\'\-]+|\d{3,3})[\.\,\:\;\*\(\)\!\?\$\%\+\'\"]*?$/ }.join(' ')
      end
    end
  end
end
