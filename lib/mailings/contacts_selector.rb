module Mailings
  class ContactsSelector
    include Enumerable

    attr_reader :contacts, :base_mail_type

    def initialize(base_type_slug)
      @base_mail_type = Mailing::MailType.find_by_slug(base_type_slug)
      @contacts = Contact.confirmed.on.where('contacts.type_ids && ARRAY[?]', base_mail_type.id).pluck(:id)
    end
        
    def by_status(statuses = [])
      newcontacts = []
      newcontacts |= all_contacts.where(user_id: nil).pluck(:id) if statuses.include? :unregistered
      newcontacts |= all_contacts.joins(:access_rights).where("now() between access_rights.start_date and access_rights.end_date").pluck(:id) if statuses.include? :subscribed

      if statuses.include? :unsubscribed
        newcontacts |= all_contacts.
          joins("LEFT JOIN access_rights ON access_rights.reader_type='User' AND access_rights.reader_id=contacts.user_id AND now() between access_rights.start_date and access_rights.end_date").
          where(access_rights: {id: nil}).pluck(:id)
      end

      union newcontacts
    end

    def by_mailing(mail_type_ids = [])
      union all_contacts.where('contacts.type_ids && ARRAY[?]', mail_type_ids).pluck(:id)
    end
    
    def by_payment_namespace(namespace_ids = [])
      union all_contacts.
        joins("JOIN access_rights ON access_rights.reader_type='User' AND access_rights.reader_id=contacts.user_id").
        joins("JOIN payments ON payments.id=access_rights.payment_id").
        joins("JOIN payment_kits ON payment_kits.id=payments.payment_kit_id").
        where(payments: {status: Payment.statuses[:paid]}, payment_kits: {namespace_id: namespace_ids}).
        group('contacts.id').
        pluck(:id)
    end
    
    def by_register_date(start_date, end_date)
      union all_contacts.joins(:user).where(users: {created_at: start_date..end_date}).pluck(:id)
    end

    def by_subscription_end(start_date, end_date)
      union all_contacts.joins(:access_rights).
        joins("LEFT JOIN access_rights as ar_after ON ar_after.reader_type='User' AND ar_after.reader_id=contacts.user_id AND ar_after.end_date > access_rights.end_date").
        where(ar_after: {id: nil}, access_rights: {end_date: start_date..end_date}).
        pluck(:id)
    end

    def each(&block)
      @contacts.each(&block)
    end

  private
    def all_contacts
      Contact.confirmed.on
    end

    def union(contacts_list)
      @contacts &= contacts_list
      self
    end
  end
end
