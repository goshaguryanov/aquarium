class FlashProcessor
  attr_accessor :document_id, :download_path, :pdf_path, :swf_path, :jpg_path, :cover_path, :archive_box, :pdf_attachment
  
  TMP_PATH = Pathname.new('/tmp').freeze
  
  def initialize(document_id)
    @document_id   = document_id

    @download_path = TMP_PATH.join('newspaper_pdf', document_id.to_s).freeze
    @pdf_path      = download_path.join('pdf').freeze
    @swf_path      = download_path.join('swf').freeze
    @jpg_path      = download_path.join('jpg').freeze
    @cover_path    = jpg_path.join('cover.png').freeze
  end
  
  def process
    level = Rails.logger.level
    Rails.logger.level = 0
    
    prepare_filesystem
    
    if select_box
      select_attachment
      
      # download and covert PDF archive
      if pdf_attachment.present?
        puts pdf_archive_url
        download(pdf_archive_url)
        unpack
        convert
      end
      
      # newsstand image attachment
      if File.exists?(cover_path)
        newsstand_data       = upload_newsstand_cover
        newsstand_attachment = create_newsstand_attachment(newsstand_data)
        document_update_newsstand(newsstand_attachment[:id])
      end
      
      document_publish if published?
    end
    
    Rails.logger.level = level
  end
  
private
  def prepare_filesystem
    FileUtils.rm_rf download_path
    [download_path, pdf_path, swf_path, jpg_path].each do |dir|
      FileUtils.mkdir_p(dir)
    end
  end

  def document_boxes
    snipe_client.get("/v1/documents/#{document_id}/boxes").body
  end
  
  def select_box
    root_box = document_boxes.find {|box| box[:type] == 'root' }
    if root_box[:children].present?
      @archive_box ||= root_box[:children].find {|box| box[:type] == 'newsrelease_archive' }
    else
      Rails.logger.debug('Box newsrelease_archive not found')
      false
    end
  end
  
  def select_attachment
    if archive_box[:pdf_id].present?
      response = snipe_client.get("/v1/attachments/#{archive_box[:pdf_id]}")
      if response.success?
        @pdf_attachment = response.body
      else
        Rails.logger.debug('PDF attachment not found')
      end
    end
  end
  
  def pdf_archive_url
    "#{Settings.hosts.agami}#{pdf_attachment[:versions][:original][:url]}"
  end
  
  def pdf_archive_filename
    uri_path = URI.parse(pdf_archive_url).path
    download_path.join(File.basename(uri_path))
  end
  
  def download(url)
    cmd = %Q(cd #{download_path} && wget -nv -nc #{url})
    Rails.logger.debug("Download image: #{cmd}")
    system(cmd)
  end
  
  def unpack
    unzipper = Unzipper.new(pdf_path, pdf_archive_filename)
    unzipper.unzip
  end
  
  def pdf_list
    Dir.glob("#{pdf_path}/*.pdf").sort
  end
  
  def convert
    pdf_list.each_with_index do |pdf_file, index|
      Rails.logger.debug("[=====================================]")
      filename = '%03d' % (index + 1)
      
      create_cover(pdf_file) if index == 0
      create_swf(pdf_file, filename)
      create_jpg(pdf_file, filename)
      
      jpg_data = upload_jpg(jpg_path.join("#{filename}.jpg"))
      jpg_attachment = create_attachment(jpg_data, filename)
      
      swf_data = upload_jpg(swf_path.join("#{filename}.swf"))
      swf_attachment = create_attachment(swf_data, filename)
      
      create_page_box(jpg_attachment[:id], swf_attachment[:id], index)
    end
  end
  
  def create_cover(pdf_file)
    [
      %Q(gs -q -sDEVICE=pngalpha -dNOPAUSE -dBATCH -dBackgroundColor=16#f7cba3 -r150 -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -sOutputFile=#{jpg_path.join('cover_tmp.png')} #{pdf_path.join(pdf_file)}),
      %Q(convert -geometry 1024x #{jpg_path.join('cover_tmp.png')} #{jpg_path.join('cover_small.png')}),
      %Q(convert -size 1024x1452 xc:'#f7cba360' #{jpg_path.join('cover_pink.png')}),
      %Q(convert #{jpg_path.join('cover_small.png')} #{jpg_path.join('cover_pink.png')} -compose Multiply -composite #{cover_path})
    ].each do |command|
      Rails.logger.debug("Create cover image: #{command}")
      system(command)
    end
  end
  
  def create_swf(pdf_file, filename)
    full_pdf_file = pdf_path.join(pdf_file)
    full_swf_file = swf_path.join(filename + '.swf')
    [
      %Q(pdf2swf -q -p 1 -G -s jpegquality=30 -s fontquality=50 -T 6 #{full_pdf_file} -o #{full_swf_file}),
      %Q(pdf2swf -q -p 1 -s jpegquality=30 -s fontquality=50 -T 6 #{full_pdf_file} -o #{full_swf_file}),
      %Q(pdf2swf -q -p 1 -G -s poly2bitmap -s fontquality=50 -T 6 #{full_pdf_file} -o #{full_swf_file}),
      %Q(pdf2swf -q -p 1 -G -s bitmap -T 6 $PIC -o #{full_pdf_file} -o #{full_swf_file})
    ].each do |cmd|
      Rails.logger.debug("Convert PDF to SWF [#{filename}]: #{cmd}")
      system(cmd) and break
      Rails.logger.error("Convert PDF to SWF failed, command: \"#{cmd}\"")
    end
  end
  
  def create_jpg(pdf_file, filename)
    cmd = %Q(gs -q -sDEVICE=jpeg -dNOPAUSE -dBATCH -g290x420 -dFIXEDMEDIA -dPDFFitPage -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -sOutputFile=#{jpg_path.join(filename + '.jpg')} #{pdf_path.join(pdf_file)})
    Rails.logger.debug("Convert PDF to JPG [#{filename}]: #{cmd}")
    system(cmd)
  end
  
  def upload_jpg(filename)
    converts = { _preview: "110x68^" }
    upload(filename, converts)
  end
  
  def upload_newsstand_cover
    converts = {
      iphone_multitasking_1x: "57x57>",
      iphone_multitasking_2x: "114x114>",
      ipad_multitasking_1x:   "72x72>",
      ipad_multitasking_2x:   "144x144>",
      ipad_shelf_1x:          "126x126>",
      ipad_shelf_2x:          "252x252>",
      iphone_shelf_1x:        "90x90>",
      iphone_shelf_2x:        "180x180>",
      source:                 "1024x1024>",
      _preview:               "110x68^"
    }
    upload(cover_path.to_s, converts)
  end
  
  def upload(filename, converts = {})
    Rails.logger.debug("Upload #{filename}")
    AgamiUploader.upload(filename.to_s, "converts=#{Oj.dump(converts)}").body
  end
  
  def create_page_box(jpg_id, swf_id, index)
    data = {
     attributes: {
       type:        'newsrelease_page',
       parent_id:   archive_box[:id],
       position:    index * 10,
       enabled:     true,
       jpg_id:      jpg_id,
       swf_id:      swf_id
     }
    }
    Rails.logger.debug("Creating box for [#{index}] page")
    snipe_client.post('/v1/boxes', data).body
  end
  
  def create_attachment(upload_data, filename)
    data = {
      attributes: {
        type:      'application',
        directory: upload_data[:directory],
        versions:  upload_data[:versions]
      }
    }
    Rails.logger.debug("Creating attachment for [#{filename}] page")
    
    tries ||= 3
    snipe_client.post('/v1/attachments', data).body
  rescue Net::HTTP::Persistent::Error => e
    if (tries -= 1).zero?
      raise
    else
      Rails.logger.debug('retrying')
      retry
    end
  end
  
  def create_newsstand_attachment(upload_data)
    Rails.logger.debug("Creating attachment for newsstand image")
    snipe_client.post('/v1/attachments', attributes: upload_data).body
  end
  
  def document_update_newsstand(newsstand_attachment_id)
    data = {
      attributes: {
        newsstand_id: newsstand_attachment_id
      }
    }
    Rails.logger.debug("Update document with newsstand image")
    snipe_client.patch("/v1/documents/#{document_id}", data).body
  end
  
  def document_publish
    Rails.logger.debug("Publish document")
    snipe_client.patch("/v1/documents/#{document_id}/publish").body
  end

  def published?
    if doc = snipe_client.get("/v1/documents/#{document_id}").body
      doc.published
    end
  end
  
  def snipe_client
    @snipe_client ||= SnipeClient.new
  end
end
