class SnipeAuth
  def initialize(app)
    @app = app
    @requests_counter = 0
  end

  def logger
    @logger ||= Logger.new("#{Rails.root.join('log')}/indexer.log")
  end

  def authorize(refresh: false)
    if Settings.bot? && Settings.bot.login? && Settings.bot.password?
      resp = nil
      unless refresh
        resp = Faraday.post(
            "#{Settings.hosts.snipe}/v1/access_token",
            editor: {
                login:    Settings.bot.login,
                password: Settings.bot.password
            }
        )

        logger.info 'tried authenticate on Snipe'
      else
        resp = Faraday.patch(
            "#{Settings.hosts.snipe}/v1/access_token",
            refresh_token: @auth[:refresh_token]
        )

        logger.info 'tried refresh token on Snipe'
      end

      if 200 == resp.status
        @auth = Oj.load(resp.body)
      else
        logger.error 'failed authenticate on Snipe'
        raise 'failed authenticate on Snipe'
      end
    else
      logger.error 'Settings.bot not defined'
      raise 'Settings.bot not defined!'
    end
  end

  def call(env)
    @requests_counter += 1
    authorize unless @auth
    if 1000 == @requests_counter
      authorize
      @requests_counter = 0
    end

    env.request_headers['X-Access-Token'] = @auth[:access_token]

    @app.call(env)
  end
end
