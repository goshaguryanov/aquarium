require 'faraday_middleware'
require 'faraday_middleware/response_middleware' 

module Itunes
  class Client
    extend Forwardable
    def_delegators :@client, :post

    def initialize
      @client = Faraday.new do |builder|
        builder.request  :json
        builder.response :mashify
        builder.response :oj
        builder.adapter Faraday.default_adapter
      end
    end
  end
end
