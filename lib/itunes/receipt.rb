module Itunes
  class Receipt
    extend Forwardable
    def_delegators :@receipt, *%i(status environment receipt latest_receipt_info latest_receipt)

    def initialize(receipt)
      @receipt = receipt
      @receipt.latest_receipt_info = Array(@receipt.latest_receipt_info)
      @receipt.latest_receipt_info.each do |r|
        r.raw_receipt = r.dup
        r.purchase_date = Time.at(r.purchase_date_ms.to_f / 1000)
        r.expires_date = Time.at(r.expires_date_ms.to_f / 1000)
      end
      if @receipt.latest_receipt_info.count>0 && @receipt.latest_receipt
        @receipt.latest_receipt_info.last.receipt_data = @receipt.latest_receipt
      end
    end

    def success?
      status == 0
    end

    def error
      case status
      when 0 then nil
      when 21005 then :server_not_available
      when 21006 then :subscription_expired
      when 21007 then :sandbox_receipt
      when 21008 then :production_receipt
      else :verification_failed
      end
    end

    def latest
      latest_receipt_info.last
    end

    def previous
      latest_receipt_info.count>1 && latest_receipt_info[-2]
    end
    
    class << self
      def get(receipt_data, sandbox = false)
        url = sandbox ? 'https://sandbox.itunes.apple.com/verifyReceipt' : 'https://buy.itunes.apple.com/verifyReceipt'
        data = {
          'receipt-data' => receipt_data,
          'password'     => Settings.apple.shared_secret
        }
        resp = ::Itunes::Client.new.post(url, data)
        resp.success? && new(resp.body)
      end
    end
  end
end
