class PaymentProcess
  class << self
    attr_accessor :logger

    def create(order)
      if order.by_card?
        PaymentProcess::Card.new(order)
      elsif order.by_yandex?
        PaymentProcess::Yandex.new(order)
      elsif order.by_apple?
        PaymentProcess::Apple.new(order)
      elsif order.by_google?
        PaymentProcess::Google.new(order)
      end
    end
  end

  attr_reader :order

  def initialize(order=nil)
    if order
      @order = order
    end
  end

  def do_async(method_name)
    Resque.enqueue(Workers::PaymentJob, order.id, method_name)
    return_wait(uuid: order.uuid)
  end

  def can_renewal?
    false
  end

  def add_access_right(reader)
    Common::GrantAccess.new(
      order:      order,
      reader:     reader,
      start_date: order.start_date,
      end_date:   order.end_date
    ).call
  end

  def snapshot_order(event)
    Snapshot.create!(
      subject: order,
      event:   event,
      data:    order.attributes
    )
  end

  # responses to Shark
  def return_redirect(options)
    {
      status: 'ok',
      redirect: options
    }
  end

  def return_form(url, attrs)
    {
      status: 'ok',
      post: url,
      attributes: attrs
    }
  end

  def return_body(body)
    {
      status: 'ok',
      body: body
    }
  end

  def return_error(*errors)
    {
      status: 'error',
      errors: errors
    }
  end

  def return_wait(options)
    {
      status: 'ok',
      wait: options
    }
  end

  def price
    Rails.env == 'production' ? order.price.value : 1
  end
end
