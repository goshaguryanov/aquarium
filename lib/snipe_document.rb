module SnipeDocument

  def document
    @document||= begin
      url = "/v1/documents/#{document_id}"
      if (response = snipe_client.get(url)).success?
        response.body
      else
        raise "Bad response from snipe by #{url} (status: #{response.status})"
      end
    end
  end

  def document_boxes
    @document_boxes ||= begin
      url = "/v1/documents/#{document_id}/boxes"
      if (response = snipe_client.get(url)).success?
        response.body
      else
        raise "Bad response from snipe by #{url} (status: #{response.status})"
      end
    end
  end

  def box_by_path(*types)
    reversed_types = types.reverse
    boxes = document_boxes
    box = nil
    while (type = reversed_types.pop) && boxes
      box = boxes.find {|box| box[:type] == type.to_s }
      boxes = box[:children]
    end
    box
  end

  def snipe_client
    @snipe_client ||= SnipeClient.new
  end
end