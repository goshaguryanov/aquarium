module DultonMedia
  class Base
    [:get, :post, :put, :delete].each do |method|
      define_method(method) do |path, values|
        request(method, path, values)
      end
    end

    private

    def request(method, path, values={})
      connection.send(method) do |request|
        request.url path
        request.params = values
        request.params[:account] = Settings.dulton_media.account
        request.params[:auth_token] = Settings.dulton_media.auth_token
      end.body
    end

    def connection
      @connection ||= Faraday.new(Settings.dulton_media.url) do |builder|
        builder.request :url_encoded
        builder.response :mashify
        builder.response :oj, content_type: 'application/json'
        builder.adapter Faraday.default_adapter
      end
    end
  end
end
