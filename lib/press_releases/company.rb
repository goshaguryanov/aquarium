module PressReleases
  class Company
    attr_reader :document

    extend Forwardable
    def_delegators :@document, :id, :press_releases_feed, :messages_feed, :title

    def initialize(document)
      @document = document
    end

    def press_releases(params = nil)
      @press_releases ||= snipe_client.get("/v1/documents/#{id}/full_bound_section/company?limit=#{self.class.limit}").body
    end

    def messages(params = nil)
      @messages ||= snipe_client.get("/v1/documents/#{id}/full_bound_section/messages_partner?limit=#{self.class.limit}").body
    end

    def add_press_release_by_item(item)
      snipe_doc = snipe_client.post('/v1/documents', {
        attributes: {
          title:           item.title,
          subtitle:        item.description,
          original_url:    item.link,
          email:           item.author,
          contact:         item.contact,
          category_ids:    self.class.press_release_category_ids,
          participant_ids: [ snipe_client.editor_id ]
        }
      })

      news_id = snipe_doc.body[:id]
      root_box = snipe_client.get("/v1/documents/#{news_id}/root_box").body

      snipe_client.post("/v1/documents/#{news_id}/links", {
        attributes: {
          section:           'company',
          bound_document_id: id,
          position:          0
        }
      })

      box_position = 0
      box = item.full_text.to_s.split(/[\r\n]+/).each do |text|
        snipe_client.post('/v1/boxes', {
          attributes: {
            parent_id: root_box[:id],
            position:  box_position,
            enabled:   true,
            type:      'paragraph',
            kind:      'plain',
            body:      text
          }
        })
        box_position += 5
      end
    end

    def add_message_by_item(item)
      snipe_doc = snipe_client.post('/v1/documents', {
        attributes: {
          title:           item.title,
          subtitle:        item.description,
          original_url:    item.link,
          email:           item.author,
          themes:          Array[item.category].join("\n"),
          category_ids:    self.class.company_message_category_ids,
          participant_ids: [ snipe_client.editor_id ]
        }
      })

      news_id = snipe_doc.body[:id]
      root_box = snipe_client.get("/v1/documents/#{news_id}/root_box").body

      snipe_client.post("/v1/documents/#{news_id}/links", {
        attributes: {
          section:           'messages_partner',
          bound_document_id: id,
          position:          0
        }
      })

      box_position = 0
      box = item.full_text.to_s.split(/[\r\n]+/).each do |text|
        snipe_client.post('/v1/boxes', {
          attributes: {
            parent_id: root_box[:id],
            position:  box_position,
            enabled:   true,
            type:      'paragraph',
            kind:      'plain',
            body:      text
          }
        })
        box_position += 5
      end

      item_date = Time.zone.parse(item.pubDate)

      # формируем url
      url = snipe_client.post("/v1/documents/#{news_id}/urls", {
        attributes: {
          domain: 'vedomosti.ru',
          date:   item_date.strftime('%Y-%m-%d'),
          slug:   (news_id - 140737488355328 + 123456).to_s + '-' + Russian::transliterate(item.title).downcase.gsub(/[^a-z\d]+/, '-').gsub(/\-$/, ''),
        }
      })

      # ставим как основной урл документа
      snipe_client.patch("/v1/documents/#{news_id}", { attributes: { url_id: url.body.id } })

      # публикуем
      snipe_client.patch("/v1/documents/#{news_id}/publish", attributes: { published_at: item_date.strftime('%FT%T.000%:z') })
    end

    def snipe_client
      @snipe_client ||= SnipeClient.new
    end

    class << self
      def find(id)
        new(Roompen.document_by_id(id))
      end

      def with_feeds
        Roompen.categorized(%w(kinds companies), %w(domains vedomosti.ru), limit: 2000).select(&:has_press_releases_feed?).map { |c| new(c) }
      end

      def with_message_feeds
        Roompen.categorized(%w(kinds companies), %w(domains vedomosti.ru), limit: 2000).select(&:has_messages_feed?).map { |c| new(c) }
      end

      def press_release_category_ids
        @press_release_category_ids ||= [ %(project), %w(project vedomosti), %w(kinds), %w(kinds press_releases), %w(domains), %w(domains vedomosti.ru) ].map { |c| Roompen.category(c).id }
      end

      def company_message_category_ids
        @press_release_category_ids ||= [ %(project), %w(project vedomosti), %w(kinds), %w(kinds company_messages), %w(domains), %w(domains vedomosti.ru) ].map { |c| Roompen.category(c).id }
      end

      def limit
        Settings.companies.import_limit.to_i
      end
    end
  end
end