class Rails::Application::Configuration
  def database_configuration
    ::Settings.map(&:postgresql).stringify_keys
  end
end
