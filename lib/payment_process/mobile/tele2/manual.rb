class PaymentProcess
  module Mobile
    module Tele2
      class Manual < ::PaymentProcess::Mobile::Beeline::Manual

        attr_reader :subscription_id, :data, :msisdn, :operator, :attributes, :error, :service_id

        def initialize(subscription_id, access_token)
          @operator        = :tele2
          @subscription_id = subscription_id
          @access_token    = access_token
        end
      end
    end
  end
end
