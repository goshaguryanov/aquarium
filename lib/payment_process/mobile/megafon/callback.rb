class PaymentProcess
  module Mobile
    module Megafon
      class Callback < ::PaymentProcess::Mobile::Base
        attr_reader :body, :errors, :subscription_id, :operator, :msisdn, :attributes

        def initialize(body)
          @body = body
          @operator = 'megafon'
          @errors = []
          @msisdn       = nil
          @access_token = nil
          @attributes   = { data: { 'operator' => 'megafon' } }
        end

        def new_success?
          %w(new new_minus new_undef).include? body['status']
        end

        def success_prolongation?
          %w(prolong).include? body['status']
        end

        def unsuccessful_prolongation?
          %w(suspend).include? body['status']
        end

        def unsubscribe?
          body['status'] == 'exit'
        end

        def error
          !body['error'].blank?
        end

        def verify!
          @subscription_id = body['transactid']
          @msisdn = body['msisdn']

          unless subscription_id
            if !msisdn.blank? && mobile_user && mobile_user.active_payment
              @subscription_id = mobile_user.active_payment.operator_subscription_id
            end
          end

          unless subscription_id
            @errors << ["Error processing megafon callback #{body}"]
            return false
          end
          event = nil
          if new_success? || success_prolongation?
            attributes[:status] = :paid
            attributes[:end_date] = subscribe_end_date
            event = :prolong if success_prolongation? && mobile_user && mobile_user.active_payment
          else
            attributes[:end_date] = Time.now
            event = :stop
          end

          if error
            @errors =  ['Что-то пошло не так']
            redis_client.hset(redis_key, :error_code, body['error'])
            false
          else
            save_to_database!
            fire_event!(event)
          end
        end

        private

        def mobile_user
          @mobile_user ||= MobileUser.find_by_msisdn(msisdn)
        end
      end
    end
  end
end
