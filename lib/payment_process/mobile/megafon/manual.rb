class PaymentProcess
  module Mobile
    module Megafon
      class Manual < ::PaymentProcess::Mobile::Base

        attr_reader :subscription_id, :data, :msisdn, :operator, :attributes, :error

        def initialize(subscription_id, access_token)
          @operator        = :megafon
          @subscription_id = subscription_id
          @access_token    = access_token
          @attributes = {}
        end

        def verify!
          response = Operators.megafon.verify_subscription!(subscription_id, access_token)
          unless response.status.eql?(200)
            redis_client.hset(redis_key, :error_code, response.status)
            return false
          end
          true
        end
      end
    end
  end
end
