class PaymentProcess
  module Mobile
    module Mts
      class Manual < ::PaymentProcess::Mobile::Base

        attr_reader :subscription_id, :data, :msisdn, :operator, :attributes, :error

        def initialize(subscription_id, access_token)
          @operator        = :mts
          @subscription_id = subscription_id
          @access_token    = access_token
        end

        def verify!
          attempts = 0
          begin
            if (@data = Operators.mts.verify_subscription(subscription_id)).blank?
              @error = "Payment with subscription_id #{subscription_id} was not found"
              false
            else
              data.merge!(redis_payment).merge!('subscription_id' => redis_key)
              @attributes = if data['approved'] == 'true'
                              { status: :paid, end_date: subscribe_end_date }
                            elsif data['approved'] == 'false'
                              raise ::PaymentProcess::Mobile::Mts::NotPaidYet if data['error_code'].to_i == 11
                              { end_date: Time.now }
                            else
                              {}
                            end
              if data['msisdn'].blank?
                @error = 'MSISDN undefined'
                false
              else
                @msisdn = data['msisdn']
                ::Payments::Mobile::UpdatePayment.new(payment, attributes.merge(data: data)).call if payment
              end
            end
          rescue ::PaymentProcess::Mobile::Mts::NotPaidYet
            sleep 5
            attempts += 1
            return false if attempts > 5
            retry
          end
        end
      end

      class NotPaidYet < Exception
      end
    end
  end
end
