class PaymentProcess
  module Mobile
    module Mts
      class Callback
        class Tariffication < Base

          def call
            node.each do |notify_child|
              if notify_child.key?('notifyData')
                notify_child['notifyData'].each { |notify_raw_data| parse_notify_data!(notify_raw_data) }
              else
                @errors << "Undefined node #{notify_child}"
                @status = false
              end
            end
            @status
          end

          private

          def parse_notify_data!(raw_data)
            if (data = read_raw_data(raw_data))
              data['TarifficationNotify'].each { |notify_node| parse_notify_node!(notify_node) }
            else
              @errors << "Error parsing notify data: #{raw_data}"
              @status = false
            end
          end

          def parse_notify_node!(notify_node)
            reset_cache! # Сбрасываем значения в памяти, т.к. за коллбэк могут прийти списания с более, чем 1 абонента
            notify_node.each do |key, value|
              value = value.first
              case key
              when 'SubscriptionId'
                @subscription_id                    = value
              when 'TransactionId'
                attributes['transaction_id']        = value
                attributes[:data]['transaction_id'] = value
              when 'ContentId'
                attributes[:data]['content_id']     = value
              when 'MSISDN'
                attributes[:data]['msisdn']         = value
                @msisdn                             = value
              when 'AttemptDate'
                attributes[:data]['attempt_date']   = value
              when 'FaultCode'
                attributes[:data]['fault_code']     = value
                attributes[:end_date]               = Time.now if value.to_i > 0
              when 'Result'
                attributes[:data]['result']         = value.to_s == 'true'
                attributes[:status]                 = :paid if attributes[:data]['result']
              else
                puts "Unknown key #{key}"
              end
            end
            attributes[:end_date] = subscribe_end_date if attributes[:status] == :paid
            save_to_database!
            fire_event!(:prolong) if prolong?
          end

          def prolong?
            mobile_user && (payment = mobile_user.payment) && payment.active? && !payment.fresh?
          end
        end
      end
    end
  end
end
