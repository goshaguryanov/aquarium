class PaymentProcess
  module Mobile
    module Beeline
      class Callback < ::PaymentProcess::Mobile::Base
        attr_reader :body, :errors, :subscription_id, :operator, :msisdn, :attributes

        REGEXPS = {
            block:   /^block_subscription\s+(?<msisdn>\d+)$|^block_subscriber\s+(?<msisdn>\d+)$|^subscription\shas\sbeen\sblocked\sfor\sabonent\s(?<msisdn>\d+)$|^.!.".{4,4}1$/i,
            unblock: /^unblock_subscription\s+(?<msisdn>\d+)$|^unblock_subscriber\s+(?<msisdn>\d+)$|^subscription\shas\sbeen\sunblocked\sfor\sabonent\s(?<msisdn>\d+)$/i,
            charged: /^charged\s+\d+\.?\d+?\s+\w+\./i
        }

        def initialize(body)
          @body         = body
          @operator     = 'beeline'
          @errors       = []
          @msisdn       = nil
          @access_token = nil
          @attributes   = { data: { 'operator' => 'beeline' } }
        end

        def verify!
          @msisdn           = body['source_addr']
          message           = convert_short_message(body['short_message'])
          attributes[:data] = attributes[:data].merge!(body)
          case message
          when REGEXPS[:block]
            block_subscriber!
          when REGEXPS[:unblock]
            unblock_subscriber!
          when REGEXPS[:charged]
            charge_subscription!
          else
            raise "[Beeline Callback] Undefined message type: #{message}"
          end
        end

        def block_subscriber!
          # Формат block_subscription 79036120764
          if mobile_user
            @subscription_id      = mobile_user.active_payment.operator_subscription_id
            attributes[:end_date] = Time.now
            save_to_database! && fire_event!(:stop)
          else
            @errors << ["Mobile User with msisdn #{msisdn} is not found"]
            false
          end
        end

        def unblock_subscriber!
          # Формат block_subscription 79036120764
          if mobile_user
            @subscription_id      = mobile_user.payment.operator_subscription_id
            attributes[:status]   = :paid
            attributes[:end_date] = subscribe_end_date
            save_to_database!
          else
            @errors << ["Mobile User with msisdn #{msisdn} is not found"]
            false
          end
        end

        def charge_subscription!
          # Формат charged <сумма списания> rub. Next pay date <дата и время след. списания>

          if mobile_user
            @subscription_id      = mobile_user.active_payment.operator_subscription_id
            attributes[:end_date] = subscribe_end_date
            save_to_database! && fire_event!(:prolong)
          else
            @errors << ["Mobile User with msisdn #{msisdn} is not found"]
            false
          end
        end

        def mobile_user
          @mobile_user ||= MobileUser.find_by_msisdn(@msisdn)
        end

        def subscribe_end_date
          (Time.now + 30.days).end_of_day
        end
      end
    end
  end
end