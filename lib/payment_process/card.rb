require 'faraday_middleware'
require 'faraday_middleware/response_middleware'

# Payment by card via Russian Standart bank
class PaymentProcess
  class Card < self
    attr_reader :auto_renewing
    
    def initialize(order = nil)
      super
      @auto_renewing = order.data.symbolize_keys[:auto_renewing] if order
    end

    def can_renewal?
       order_can_renewal? && !have_next_subscription?
    end

    def renewal_products
      %w(online paper profi)
    end

    def order_can_renewal?
      data = order.data.symbolize_keys
      order.paid? && data[:auto_renewing] && data[:biller_client_id] && !data[:next_order_id] && !data[:renewal_failed] && order.user.autopay && renewal_products.include?(order.price.product.to_s)
    end

    def have_next_subscription?
      if !order.user
        false
      elsif order.online_subscription?
        last_access = order.user.last_access_right
        last_access && last_access.end_date > order.end_date
      elsif order.paper_subscription?
        last_order = order.user.last_paper_order
        last_order && last_order.end_date > order.end_date
      end
    end
    
    def start
      resp = get_transaction_id
      if resp && resp.key?(:transaction_id) && !resp.transaction_id.blank?
        order.transaction_id = resp.transaction_id
        order.data['response_for_client'] = return_form(host_for_client + url_for_client, {trans_id: order.transaction_id})
      else
        order.payment_status = :canceled
        order.data['status_for_client'] = :unprocessable_entity
        order.data['response_for_client'] = return_error
      end
      order.data['payment_start_response'] = resp.to_hash if resp
      order.data_will_change!
      
      order.save!
      snapshot_order('payment_card_start')
    end
    
    def final
      resp = get_payment_result
      if resp && resp.result=='OK'
        order.payment_status = :paid
        order.grant_access
        order.send_user_mail
        order.data['response_for_client'] = return_redirect(path: 'subscription_ok')
        # InnerMailer.new_subscription(order).deliver

        if order.data.symbolize_keys[:auto_renewing] && !order.user.autopay
          user = order.user
          user.autopay = true
          user.save!
          Snapshot.create!(
            subject: user,
            event:   'payment_card_final',
            data:    user.attributes
          )
        end
      else
        order.payment_status = :canceled
        order.data['status_for_client'] = :unprocessable_entity
        order.data['response_for_client'] = return_error
      end
      
      order.data['payment_final_response'] = resp.to_hash if resp
      order.data_will_change!
      
      order.save!
      snapshot_order('payment_card_final')
    end
    
    def check
      order.data['response_for_client'] || return_wait(uuid: order.uuid)
    end
    
    def renewal
      resp = get_renewal_result
      if resp && resp.result=='OK'
        order.payment_status = :paid
        order.transaction_id = resp.transaction_id
        # make access
        order.grant_access
        order.send_user_mail
      else
        order.payment_status = :canceled
        # InnerMailer.renewal_failed(order, 'Оплата не прошла').deliver
      end
      
      order.data['payment_renewal_response'] = resp.to_hash if resp
      order.data_will_change!
      
      order.save!
      snapshot_order('payment_card_renewal')
    end
    
    def response_status
      order.data['status_for_client'] || :ok
    end

    def close_day
      resp = get_response(req_params_for_close_day)
      resp && resp.result == 'OK'
    end
    
  private
    def connection
      ssl_params = {
        client_cert: client_cert,
        client_key:  client_key,
        ca_file:     ca_file,
        verify_mode: OpenSSL::SSL::VERIFY_NONE
      }
      
      Faraday.new(url: host_for_merchant, ssl: ssl_params) do |builder|
        builder.request  :url_encoded
        builder.response :mashify
        builder.response :rsb_response_decoder
        builder.adapter  Faraday.default_adapter
      end
    end
    
    def client_cert
      client_cert_file = auto_renewing ? Settings.card.ssl.client_cert_recurrent : Settings.card.ssl.client_cert
      client_cert_file = ERB.new(client_cert_file).result
      OpenSSL::X509::Certificate.new(File.read(client_cert_file)) unless client_cert_file.blank?
    end
    
    def client_key
      client_key_file = auto_renewing ? Settings.card.ssl.client_key_recurrent : Settings.card.ssl.client_key
      client_key_file = ERB.new(client_key_file).result
      OpenSSL::PKey::RSA.new(File.read(client_key_file)) unless client_key_file.blank?
    end
    
    def ca_file
      ERB.new(Settings.card.ssl.ca_file).result
    end
    
    def post(req_values)
      connection.post(url_for_merchant, req_values) do |req|
        req.options[:timeout] = 20
        req.options[:open_timeout] = 10
      end
    end
    
    def get_response(req_values)
      res = post(req_values).body
      PaymentProcess.logger.info { "#{self.class.name} request: #{req_values}, response: #{res}" }
      res
    rescue Exception => e
      order.data['payment_connect_error'] = e.message
      order.data_will_change!
      # TODO: save
      nil
    end
    
    def get_transaction_id
      get_response(req_params_for_transaction_id)
    end
    
    def get_payment_result
      get_response(req_params_for_payment_result)
    end
    
    def get_renewal_result
      get_response(req_params_for_renewal)
    end
    
    # parameters for requesting transaction id
    def req_params_for_transaction_id
      req_values = {
        'command'        => 'v',
        'amount'         => (price * 100).round,
        'currency'       => currency,
        'client_ip_addr' => order.client_ip,
        'description'    => order.id,
        'msg_type'       => 'SMS',
        'language'       => 'ru'
      }
      
      if auto_renewing
        req_values.merge!(add_params_for_transaction_id)
      end
      
      req_values
    end
    
    # additional parameters when require recurrent payment
    def add_params_for_transaction_id
      {
        'command'             => 'z',
        'biller_client_id'    => biller_client_id,
        'perspayee_expiry'    => '1220',
        'perspayee_gen'       => 1,
        'perspayee_overwrite' => 1
      }
    end
    
    # parameters for requesting payment result
    def req_params_for_payment_result
      {
        'command'        => 'c',
        'trans_id'       => order.transaction_id,
        'client_ip_addr' => order.client_ip
      }
    end
    
    # request parameters for renewal
    def req_params_for_renewal
      {
        'command'          => 'e',
        'amount'           => (price * 100).round,
        'currency'         => currency,
        'client_ip_addr'   => order.client_ip,
        'description'      => order.id,
        'biller_client_id' => biller_client_id,
        'perspayee_expiry' => '1220'
      }
    end

    # request parametes for close day
    def req_params_for_close_day
      {
        'command' => 'b'
      }
    end
    
    def currency
      '643' # RUB
    end
    
    def perspayee_expiry
      '1220' # TODO: храним шаблон до 31.12.2020, надо уточнить
    end
    
    # template name for recurrent payment
    def biller_client_id
      order.data['biller_client_id'] ||= begin
        order.data_will_change!
        "#{Time.now.strftime('%Y%m%d')}_#{order.user.id}"
      end
    end
    
    def host_for_merchant
      Settings.card.merchant_host
    end
    
    def url_for_merchant
      Settings.card.merchant_url
    end
    
    def host_for_client
      Settings.card.client_host
    end
    
    def url_for_client
      Settings.card.client_url
    end

    class ResponseDecoder < FaradayMiddleware::ResponseMiddleware
      define_parser do |body|
        body.strip.split(/\r?\n/).each_with_object(Hash.new) do |line, hash|
          key, value = line.split(':', 2)
          hash[key.strip.downcase] = value.strip unless value.nil?
        end
      end
    end
    
    Faraday::Response.register_middleware rsb_response_decoder: ResponseDecoder
  end
end
