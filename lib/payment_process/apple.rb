# Payment by Itunes

class PaymentProcess
  class Apple < self
    class << self
      def verify(receipt_data, client_ip)
        receipt = Itunes::Receipt.get(receipt_data)
        if receipt && receipt.error == :sandbox_receipt
          receipt = Itunes::Receipt.get(receipt_data, true)
        end
        if !receipt || receipt.error
          raise receipt && receipt.error.to_s || 'unknown error'
        end

        last_receipt = receipt.latest
        PaymentProcess.logger.info { "#{self.name} receipt: #{receipt_data}, response: #{last_receipt}" }
        
        # Найдем заказ:
        order = Order.by_apple.find_by_transaction_id(last_receipt.transaction_id)
        unless order
          order = new_order_by_receipt(last_receipt, client_ip)
          
          previous_order = receipt.previous && Order.by_apple.find_by_transaction_id(receipt.previous.transaction_id)
          if previous_order
            price_params = { price_id: previous_order.price_id }
            order.data[:previous_order_id] = previous_order.id
            order.autopayment = true
          else
            price_params = {
              product: :online,
              student: false,
              period:  period_by_receipt(last_receipt)
            }
          end
          Common::SetPriceForOrder.new(order, price_params).call
          order.save!
          order.payment.snapshot_order('payments_apple_verify')
          
          if previous_order
            previous_order.data[:next_order_id] = order.id
            previous_order.data_will_change!
            previous_order.save
            previous_order.payment.snapshot_order('payments_apple_verify_renewal')
            
            # копируем доступы из прошлого заказа
            previous_order.access_rights.each do |ar|
              order.payment.add_access_right(ar.reader)
            end
          end
        end
        order
      rescue Exception => e
        PaymentProcess.logger.error { "#{self.name} error: #{e}, receipt: #{receipt_data}" }
        nil
      end
      
      def new_order_by_receipt(receipt, client_ip)
        params = {
          transaction_id: receipt.transaction_id,
          start_date:     receipt.purchase_date,
          end_date:       receipt.expires_date,
          data: {
            receipt:      receipt.receipt_data,
            product_id:   receipt.product_id,
            receipt_info: receipt.raw_receipt,
            client_ip:    client_ip,
          }
        }
        Order.by_apple.paid.new(params)
      end
      
      def period_by_receipt(receipt)
        product = Settings.mobile_subscriptions.apple.map(&:subscriptions).flatten.find { |p| p.product_id == receipt.product_id }
        product && product.period.to_sym
      end

      def create_snapshot(order, event)
        Snapshot.create!(
          subject: order,
          event:   event,
          data:    order.attributes
        )
      end
    end
    
    def can_renewal?
      data = order.data.symbolize_keys
      order.paid? && data[:receipt] && !data[:next_order_id] && !data[:renewal_failed]
    end
    
    def renewal
      if can_renewal?
        neworder = self.class.verify(order.data.symbolize_keys[:receipt], order.client_ip)
        unless neworder && neworder!=order
          # пролонгация не удалась
          order.data[:renewal_failed] = true
          order.data_will_change!
          order.save

          snapshot_order('payments_apple_renewal_failed')
        end
      end
    end
  end
end
