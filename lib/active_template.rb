class ActiveTemplate < ActionView::Base
  include AbstractController::Helpers
  include Rails.application.routes.url_helpers
  include ApplicationHelper
  include StatisticHelper
  def initialize
    super(Rails.configuration.paths['app/views'])
  end

  # def default_url_options
  #   { host: Settings.hosts.shark }
  # end

  # class FooBarTemplate < ActiveTemplate
  #   def default_url_options
  #     { host: Settings.hosts.shark }
  #   end
  # end
end
