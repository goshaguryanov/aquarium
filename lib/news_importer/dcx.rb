module NewsImporter
  class DCX < Base
    def call
      last_date = source.import_news.maximum(:news_date) || Time.at(0)
      source_content.select { |n| Time.parse(n.document.head.DateVisible|| n.published) > last_date }.each do |n|
        body = n.document.body ? n.document.body.p : ''
        body = body.join("\n") if body.is_a? Array
        source.import_news.create(
          title:     n.document.head.Title,
          body:      body,
          news_date: Time.parse(n.document.head.DateVisible || n.published),
          data:      n.slice(*%i(priority description type category enclosure))
        )
      end
    end
    
  private
    def connection_feature(conn)
      conn.response :xml
      conn.ssl.verify = false
    end

    def source_feed
      'https://dcx.vedomosti.ru/dcx_Vedomosti/atom/documents'
    end

    def source_content
      parameters = {
        q: {
          channel:  [ 'ch040dcxsystempoolfeeds' ],
          fulltext: [ config.search_string ]
        },
        itemsPerPage: 200
      }
      content = connection.get(source_feed, parameters).body
      begin
        content.feed.entry ? content.feed.entry.reverse : []
      rescue NoMethodError => e
        error_log(e.message)
        []
      end
    end
  end
end
