require 'faraday_middleware/response_middleware'

module NewsImporter
  class AKM < Base
    def call
      last_date = source.import_news.maximum(:news_date) || Time.at(0)
      content_conn = Faraday.new do |conn|
        conn.response :mashify
        conn.response :xml
        conn.response :akm_fix_xml
        conn.response :convert_w2u
        conn.options.params_encoder = DoNotEncoder
        conn.adapter Faraday.default_adapter
      end
      source_content.select { |n| Time.parse(n.pubDate) > last_date }.each do |n|
        begin
          info = content_conn.get(n.xmlurl).body.message
          body = info.body
          body = body.__content__ if body.is_a? Hash
          body = '' if body.blank?
          source.import_news.create(
            title:     info.header,
            url:       info.link,
            body:      body.strip,
            news_date: Time.parse(n.pubDate),
            data:      info.slice(*%i(shortheader rubrics source))
          )
        rescue Faraday::ParsingError => e
          error_log(e.message)
        end
      end
    end
    
  private
    def source_content
      super.reverse
    end

    def connection_feature(conn)
      conn.response :akm_response_decoder
      conn.response :convert_w2u
      conn.options.params_encoder = DoNotEncoder
    end

    class ResponseDecoder < FaradayMiddleware::ResponseMiddleware
      define_parser do |body|
        rows = body.split("\n")
        columns_names = %i(pubDate xmlurl title)
        rows.map do |r|
          res = {}
          r.split("\t").each_with_index { |v, i| res[columns_names[i]] = v }
          res
        end
      end
    end

    class ResponseConvertW2U < FaradayMiddleware::ResponseMiddleware
      define_parser do |body|
        body.force_encoding('cp1251').encode!('utf-8', invalid: :replace, undef: :replace, replace: '?')
      end
    end

    class ResponseFixXML < FaradayMiddleware::ResponseMiddleware
      define_parser do |body|
        body.gsub(/&(?!amp;)/, '&amp;')
      end
    end

    class DoNotEncoder
      class << self
        def encode(params)
          buffer = ''
          params.each do |key, value|
            buffer << "#{key}=#{value}&"
          end
          return buffer.chop
        end

        def decode(params)
          Hash[[params.split(/\=/, 2)]]
        end
      end
    end

    Faraday::Response.register_middleware akm_response_decoder: ResponseDecoder
    Faraday::Response.register_middleware akm_fix_xml: ResponseFixXML
    Faraday::Response.register_middleware convert_w2u: ResponseConvertW2U
  end
end
