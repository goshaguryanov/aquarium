require 'faraday/digestauth'

module NewsImporter
  class Base
    attr_reader :source
    
    def initialize(source)
      @source = source
    end
    
    def config
      @config ||= Settings.import_news[source.slug]
    end
    
    def source_feed
      config.feed
    end
    
    def connection
      @connection ||= Faraday.new do |conn|
        conn.response :mashify
        connection_feature(conn)
        conn.basic_auth(config.user, config.password) if config.user && config.password
        conn.request(:digest, config.digest_user, config.digest_password) if config.digest_user && config.digest_password
        conn.adapter Faraday.default_adapter
      end
    end
    
    def connection_feature(conn)
      # override in child class if needed
    end
    
    def source_content
      @source_content ||= connection.get(source_feed).body
    end

  private
    def error_log(message)
      Resque.logger.error("news import error, source: #{source.slug}, message: #{message}")
    end
  end
end
