module NewsImporter
  class Prime < Base
    def call
      last_date = source.import_news.maximum(:news_date) || Time.at(0)
      source_content.select { |n| Time.parse(n.pubDate) > last_date }.each do |n|
        source.import_news.create(
          title:     n.title,
          url:       n.link,
          body:      n.full_text,
          news_date: Time.parse(n.pubDate),
          data:      n.slice(*%i(pdalink category))
        )
      end
    end
    
  private
    def source_content
      super.rss.channel.item.reverse
    rescue NoMethodError, Faraday::ParsingError => e
      error_log(e.message)
      []
    end
    
    def connection_feature(conn)
      conn.response :xml
    end
  end
end
