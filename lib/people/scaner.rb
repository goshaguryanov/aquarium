module People
  class Scaner

    def initialize
      @people = {}
    end

    def scan_and_upload!
      Roompen.categorized(%w(domains vedomosti.ru), from: Date.yesterday, limit: 2000).each do |document|
        @current_document_id = document.id
        document = People::Document.new(document)
        scan_for_people!(document.content) if document.boxes
      end
      People::Uploader.new(@people).create_potential_people
    end

    def scan_for_people!(str)
      position = 0
      loop do
        potential_person, offset = find_first_person(str, position)
        position += offset + 1
        break unless potential_person
        store_person(potential_person) unless People::synonyms.include?(potential_person)
      end
    end

    private

    def find_first_person(str, position = 0)
      match = str[position..-1].match(/[А-ЯЁ][а-яё]+\s+[А-ЯЁ][а-яё]+/)
      match ? [match[0], $~.offset(0).first] : [nil, 0]
    end

    def store_person(person)
      analyze = YandexMystem::Raw.stem(person)
      first_names = process_analyze(analyze[0], People::FirstName) { |name| first_name?(name) }
      last_names = process_analyze(analyze[1], People::LastName) { |name| last_name?(name) }

      first_names.each do |first_name|
        last_names.each do |last_name|
          if (first_name.male? && last_name.male?) || (first_name.female? && last_name.female?)
            @people["#{first_name} #{last_name}"] = @current_document_id
            return
          end
        end
      end

      last_names = process_analyze(analyze[1], People::LastName)
      male_last_name   = last_names.find(&:male?)
      female_last_name = last_names.find(&:female?)
      #Бежим циклом ещё раз, если не удалось распознать персону сразу. Ищем по признаку: имя? && (пол имени == пол фамилии)
      first_names.each do |first_name|
        if first_name.male? && male_last_name
          @people["#{first_name} #{male_last_name}"] = @current_document_id
        elsif first_name.female? && female_last_name
          @people["#{first_name} #{female_last_name}"] = @current_document_id
        end
      end
    end

    def process_analyze(analyze, handler, &validator)
      analyze[:analysis].select do |name|
        if block_given?
          validator.call(name)
        else
          true
        end
      end.map { |name| handler.new(name) }
    end

    def first_name?(name)
      name[:gr] =~ /^S,persn(,|$)/
    end

    def last_name?(name)
      name[:gr] =~ /^S,(persn|famn)(,|$)/
    end
  end
end