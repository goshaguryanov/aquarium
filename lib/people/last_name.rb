module People
  class LastName < BaseName

    def person?
      (%w(S famn) - @gr).empty? || (%w(S persn) - @gr).empty?
    end
  end
end
