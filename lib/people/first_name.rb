module People
  class FirstName < BaseName

    def person?
      (%w(S persn) - @gr).empty?
    end
  end
end