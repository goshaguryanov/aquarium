module People
  class Document

    attr_reader :boxes

    def initialize(document)
      @boxes = People::snipe_client.get("/v1/documents/#{document.id}/boxes").body[0].try(:children) || []
    end

    def content
      @boxes.select { |box| box.type == 'paragraph' }.map { |box| box.body.to_s }.join("\n")
    end
  end
end
