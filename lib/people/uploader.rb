module People
  class Uploader

    def initialize(people)
      @people = people
    end

    def create_potential_people
      people_created = 0
      @people.each do |person, bound_document_id|
        begin
          people_created += 1 if Person.new(title: person, bound_document_id: bound_document_id).save
        rescue ActiveRecord::RecordNotUnique
          next
        end
      end
      people_created
    end
  end
end