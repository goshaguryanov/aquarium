class MartletClient

  extend Forwardable

  def_delegators :connection, :post, :get

  def send(msisdn, operator, text)
    post('/sms/send', dst: msisdn, operator: operator, text: text)
  end

  private
  def connection
    Faraday.new(url: Settings.hosts.martlet) do |builder|
      builder.request  :url_encoded
      builder.response :mashify
      builder.response :oj, content_type: 'application/json'
      builder.adapter  :net_http_persistent
      builder.use :gelf_logger, 'martlet'
    end
  end
end
