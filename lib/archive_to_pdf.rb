class ArchiveToPDF
  attr_accessor :url

  # @param [String] url to zip file with collection of PFD files for merge to one
  def initialize(url)
    self.url = url
  end

  # @return [File] the PDF tempfile of merged PDF documents from zip
  def convert
    result = Tempfile.new('export-')
    Dir.mktmpdir do |dir|
      file = download(self.url, dir)

      Unzipper.new(dir, file.path).unzip

      options = %w(-q -sDEVICE=pdfwrite -dFirstPage=1 -dAntiAliasColorImage=false -dAntiAliasGrayImage=false
                   -dAntiAliasMonoImage=false -dAutoFilterColorImages=false -dAutoFilterGrayImages=false
                   -dDownsampleColorImages=false -dDownsampleGrayImages=false -dDownsampleMonoImages=false
                   -dColorConversionStrategy=/LeaveColorUnchanged -dConvertCMYKImagesToRGB=false
                   -dConvertImagesToIndexed=false -dUCRandBGInfo=/Preserve -dPreserveHalftoneInfo=true
                   -dPreserveOPIComments=true -dPreserveOverprintSettings=true)

      pages = Dir.glob(File.join(dir,'*.pdf'))
      unless system("gs -o #{result.path} #{options.join(' ')} #{pages.sort.join(' ')}")
        raise 'Can\'t merge collection of PF to one PDF'
      end
    end
    result
  end

private


  def download(url, directory)
    res = Net::HTTP.get_response(URI(url))
    raise "Can't download file #{url}, status: #{res.code}" unless res.code == '200'
    file = Tempfile.new('original', directory, :encoding => 'ascii-8bit')
    file.write(res.body)
    file.flush
    file.seek(0)
    file
  end
end