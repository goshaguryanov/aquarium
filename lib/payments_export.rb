class PaymentsExport
  ENCODING = 'windows-1251'.freeze
  BASE_PATH = Rails.root.join('files','subscriptions').freeze

  attr_reader :payments

  def initialize(start_date, end_date = Time.now)
    @payments = Payment.paid.joins(:payment_kit).where(payment_kits: {syncable: true}).where("payments.created_at between ? and ?", start_date, end_date).includes(:subproduct, :namespace, :users).order(:created_at)
  end

  def zip
    @zip ||= begin
      buffer = Zip::ZipOutputStream.write_buffer do |out|
        out.put_next_entry payments_filename
        out.write payments_content

        out.put_next_entry users_filename
        out.write users_content
      end

      buffer.string
    end
  end

  def payments_filename
    @payments_filename ||= "payments_#{Time.now.strftime('%Y-%m-%d-%H%M')}.csv"
  end

  def users_filename
    @users_filename ||= "users_#{Time.now.strftime('%Y-%m-%d-%H%M')}.csv"
  end
  
  def payments_content
    payments.map do |p|
      res = [ payment_fields(p).join("\t") ]
      res << payment_fields(p, true).join("\t") if p.subproduct.slug=='profi' # разделение на 2 отдельных продукта
      res
    end.flatten.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end
  
  def users_content
    users.map do |u|
      user_fields(u).join("\t")
    end.join("\n").encode(ENCODING, undef: :replace, invalid: :replace, replace: '?')
  end
  
  def users
    @users = payments.map(&:user).compact.uniq
  end
  
  def payment_fields(payment, double=false)
    res = []
    user = payment.user
    data = payment.data.symbolize_keys
    prev_payment = data[:previous_payment_id] && Payment.find_by_id(data[:previous_payment_id])

    template_id = case payment.subproduct.slug
    when 'online'
      payment.namespace.slug=='student' ? 3 : 1
    when 'paper'
      payment.namespace.slug=='student' ? 6 : 4
    else
      double ? 5 : 2
    end

    res << 3  # источник данных
    res << data[:guid] # ID Заказа во внешней системе в формате GUID
    res << (user ? user.data['guid'] : '') # ID клиента во внешней системе в формате GUID
    res << '' # номер сессии загрузки, выдается системой при загрузке. В файле указываем пустое поле.
    res << (payment.subproduct.slug == 'paper' || double ? 1 : 8)  # Группа доступа к карточкам
    res << user.subscribe_base_id # ID клиента в базе SubsSQL
    res << 5  # ID нашей компании
    res << (payment.subproduct.slug == 'paper' || double ? 11 : 75) # ID Издания
    res << (payment.start_date && payment.start_date.strftime('%F') || ' ') # дата начала подписки. Формат '2016-02-29', текст ГГГГ-ММ-ДД
    res << (payment.end_date && payment.end_date.strftime('%F') || ' ') # дата окончания подписки. Формат '2016-02-29', текст ГГГГ-ММ-ДД
    res << '' # количество выпусков. Используется для изданий продающихся по выпускам
    res << 1  # Количество копий издания
    res << (payment.subproduct.slug=='profi' ? payment.price/2 : payment.price) # Сумма за подписку. Формат текст с разделителем дробной части точка "." '2568.25'
    res << 1  # ID валюты
    res << -1  # признак НДС вкл\выкл
    res << (payment.subproduct.slug == 'paper' || double ? 2 : 3)  # НДС (1-нет, 2-10%, 3-18%)
    res << 1  # тип кредитного лимита. По умолчанию "100% предопплата", в экзотических случаях Безлимитный (?)
    res << '' # Прайс лист (?)
    res << (prev_payment ? -1 : 0) # Признак автопродления. На основании это признака может включаться автоматическая запись карточки в рабочу таблицу, при условии что указан ID предыдущей карточки (?)
    res << (prev_payment ? prev_payment.data['guid'] : '') # номер карточки из которой продлилась. В формате GUID
    res << (payment.subproduct.slug == 'paper' || double ? 1 : 6) # канал продаж, для бумаги - 1, для web - 6
    res << '' # тип подписки. Заполняется для дополнительного "нарезания подписки в отчетах". (?)
    res << '' # Тип доставки (?)
    res << '' # Агент, сделку заключивший (?)
    res << '' # Сделку подготовил (?)
    res << '' # Группа продаж (1- Клиентсервис, 2-Коллцентр) (?)
    res << '' # Акция (?)
    res << '' # Метка о подарке текстом. Используется для поиска (?)
    res << '' # ID подарка из справочника подарков (?)
    res << '' # Целевая аудитория (?)
    res << payment.id # Номер заказа с сайта в старом формате. Необходим для связи с платежками
    res << (user ? user.email : '') # Комментарий (?)
    res << '' # Флаг для автоматического обработчика. Использутеся для метки записей, которые можно заливать автоматически на основе ранее созданных КП. Используется в автопродлении (?)
    res << '' # дополнительное поле
    res << '' # дополнительное поле
    res << '' # дополнительное поле
    res << (payment.paid? ? -1 : 0) # признак полной оплаты
    res << template_id # Шаблон автозаполнения. Можно использовать в связке с флагом автопродления, автообработки и указанием предыдущей КП (Previos_CardGUID). В этом случае КП создаться автоматически

    res
  end
  
  def user_fields(user)
    res = []

    res << 3  # источник данных
    res << user.data['guid'] # ID клиента во внешней системе в формате GUID
    res << '' # номер сессии загрузки, выдается системой при загрузке. В файле указываем пустое поле.
    res << user.last_name  # фамилия контактного лица
    res << user.first_name # имя контактного лица
    res << '' # Отчество контактного лица
    res << '' # обращение к контактному лицу (?)
    res << '' # должность контактного лица (?)
    res << user.phone # номер основного телефона контактного лица
    res << '' # номер дополнительного телефона контактного лица
    res << '' # номер мобильного телефона контактного лица
    res << '' # номер факса контактного лица
    res << '' # язык обращения к контактному лицу (?)
    res << user.email # имя электронного почтового ящика контактного лица
    res << 0  # Признак юридического лица
    res << '' # Название юридического лица
    res << '' # поисковое имя
    res << '' # юридический адрес текстом
    res << '' # ИНН
    res << '' # КПП
    res << '' # наименование банка юр. лица
    res << '' # расчетный счет
    res << '' # корреспондентский счет
    res << '' # БИК
    res << '' # ОКПО
    res << '' # ОГРН
    res << '' # SWIFT-BIC банка 
    res << '' # международный номер банковского счета. Используется при международных расчетах
    res << '' # тип бизнеса (?)
    res << '' # адрес доставки текстом (?)
    res << '' # комментарий (?)
    res << '' # признак Резидент\Нерезидент. По умолчанию Резидент (?)

    res
  end
end
