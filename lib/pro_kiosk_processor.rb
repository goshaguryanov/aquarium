require 'net/ftp'
class ProKioskProcessor
  include SnipeDocument

  attr_accessor :document_id

  def initialize(document_id)
    self.document_id = document_id
  end
  
  def process
    if (box = box_by_path(:root, :newsrelease_archive))
      url = "/v1/attachments/#{box[:pdf_id]}"
      response = self.snipe_client.get(url)
      if response.success?

        ftp = Net::FTP.new(Settings.profkiosk.host)

        ftp.login(Settings.profkiosk.user, Settings.profkiosk.password)
        ftp.passive = true

        pdf_file = ArchiveToPDF.new("#{Settings.hosts.agami}#{response.body.versions.original.url}").convert

        ftp.chdir(Settings.profkiosk.directory.encode('windows-1251'))

        newsrelease_date = document.published_at ? Time.zone.parse(document.published_at) : Time.now
        ftp.put(pdf_file.path, "#{newsrelease_date.strftime('%Y%m%d')}.pdf")

        ftp.close

      else
        raise "Bad response from snipe by #{url} (status: #{response.status})"
      end
    end
  end
  
end
