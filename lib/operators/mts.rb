module Operators
  class MTS < Base
    def initialize
      super
      @client ||= Savon.client(wsdl:                 Settings.mts.schema_url,
                               soap_header:          { 'ns1:AuthSoapHeader' => { 'ns1:Login'    => Settings.mts.login,
                                                                                 'ns1:Password' => Settings.mts.password } },
                               env_namespace:        'SOAP-ENV',
                               namespace_identifier: :ns1,
                               log_level:            :debug,
                               log: true,
                               logger:                  Rails.application.config.gelf_ext.enabled ? GelfExt::Savon::Logger.new('mts') : Rails.logger
      )
    end

    def create_subscription(msisdn = nil, access_token = '')
      guid = generate_guid
      response = client.call(:subscribe, message: { subscriberData: subscribe_data(Settings.mts.return_url, guid, msisdn) }, headers: {'Gelf-Access-Token' => access_token})
      get_subscription_id(response)
    end

    def redirect_url_for(subscription_id, template_id = 2)
      Settings.mts.redirect_url + "?SID=#{subscription_id}&RETURN_URL=#{Settings.mts.return_url}&tmpl=landing#{template_id}"
    end

    def verify_subscription(subscription_id)
      response = client.call(:get_subscription_status, message: { subscriptionId: subscription_id })
      parse_subscription_status(response)
    end

    private

    def subscribe_data(return_url, subscription_id, msisdn)
      Nokogiri::XML::Builder.new do |xml|
        xml.SmsSubscriptionsSubscribersData(xmlns: 'https://moipodpiski.ssl.mts.ru/xsd/SmsSubscriptionsSubscribersData.xsd') do
          xml.SMS_SUBSCRIPTION_SUBSCRIPTIONS do
            xml.SUBSCRIPTION_ID(subscription_id)
            xml.CONTENT_ID(Settings.mts.content_id)
            xml.MSISDN(msisdn.to_s) if msisdn
            xml.SUBSCRIPTION_DATE(Time.now.iso8601)
            xml.SUBSCRIPTION_SOURCE(4)
            xml.RETURN_URL(return_url)
          end
        end
      end.doc.root.to_xml
    end

    def get_subscription_id(response)
      begin
        xml = XmlSimple.xml_in(response.body[:subscribe_response][:subscribe_result])
        xml['RequestResult'].first['Result'].first == 'true' && xml['RequestResult'].first['SubscriptionId'].first
      rescue Exception => e
        logger.error e.backtrace
        nil
      end
    end

    def get_existing_subscription_id(fault_description)
      $1 if fault_description =~ /existing\sSubscriptionId:\s(.+)\.$/
    end

    def parse_subscription_status(response)
      begin
        xml = XmlSimple.xml_in(response.body[:get_subscription_status_response][:get_subscription_status_result])
        if xml.key?('SMS_SUBSCRIPTION_SUBSCRIPTIONS')
          xml['SMS_SUBSCRIPTION_SUBSCRIPTIONS'].first.each_with_object({}) do |(key, value), data|
            data[key.downcase] = value.first
          end
        else
          nil
        end
      rescue Exception => e
        logger.error e.backtrace
        nil
      end
    end
  end
end
