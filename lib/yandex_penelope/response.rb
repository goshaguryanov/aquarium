module YandexPenelope
  class Response
    attr_accessor :payment, :status, :error, :processedDT, :techMessage, :clientOrderId
    def initialize(payment, attributes)
      @payment       = payment
      @status        = attributes[:status].to_i
      @error         = attributes[:error].to_i
      @processedDT   = DateTime.parse(attributes[:processedDT])
      @techMessage   = attributes[:techMessage]
      @clientOrderId = attributes[:clientOrderId].to_i
    end
    def need_repeat?
      self.status == 1
    end
    def success?
      self.status == 0
    end
    def error?
      self.status == 3
    end
  end
end