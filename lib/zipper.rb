require 'zip'

class Zipper
  attr_reader :input_dir, :output_file
  
  # Initialize with the directory to zip and the location of the output archive.
  def initialize(input_dir, output_file)
    @input_dir   = input_dir
    @output_file = output_file
  end
  
  # Zip the input directory.
  def write
    entries = Dir.entries(input_dir)
    entries.delete('.')
    entries.delete('..')
    io = Zip::File.open(output_file, Zip::File::CREATE);
    write_entries(entries, '', io)
    io.close
  end

private
  # A helper method to make the recursion work.
  def write_entries(entries, path, io)
    entries.each do |entry|
      zip_file_path = path == '' ? entry : File.join(path, entry)
      disk_file_path = File.join(input_dir, zip_file_path)
      
      Rails.logger.debug 'Deflating ' + disk_file_path
      
      if File.directory?(disk_file_path)
        io.mkdir(zip_file_path)
        subdir = Dir.entries(disk_file_path)
        subdir.delete('.')
        subdir.delete('..')
        write_entries(subdir, zip_file_path, io)
      else
        io.get_output_stream(zip_file_path) { |f| f.puts(File.read(disk_file_path))}
      end
    end
  end
end
