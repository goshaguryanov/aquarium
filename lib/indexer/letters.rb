module Indexer
  class Letters < Indexer::Base
    load_settings_from_file

    transform do |document|
      @errors_parser ||= Mailings::Letter::ErrorsParser.new
      @errors_parser.message = document.data['mail']
      {
          'id' => document.id,
          'contact_id' => document.contact_id,
          'mailing_id' => document.mailing_id,
          'to' => @errors_parser.email,
          'diagnostic_code' => @errors_parser.diagnostic_code,
          'diagnostic_code_fixed' => @errors_parser.diagnostic_code_fixed,
          'domain' => @errors_parser.domain,
          'date' => @errors_parser.date,
          'message' => @errors_parser.message
      }
    end

    class << self

      def index_new(letters)
        index_documents(letters)
      end
    end
  end
end
