module Indexer
  class Contacts < Indexer::Base
    load_settings_from_file

    sources({
        one:   "#{Settings.hosts.aquarium}/ibis/contacts/:id",
        all:   "#{Settings.hosts.aquarium}/ibis/indexer/contacts/page/:page",
        fresh: "#{Settings.hosts.aquarium}/ibis/indexer/contacts/fresh/:from"
    })

    transform do |document|
      document.serializable_hash
    end

    class << self

      def load_documents(document_ids)
        Contact.where('id IN (?)', document_ids).to_a
      end
    end
  end
end
