module Indexer
  class Orders < Indexer::Base
    load_settings_from_file

    sources({
      one:   "#{Settings.hosts.aquarium}/ibis/orders/:id", 
      all:   "#{Settings.hosts.aquarium}/ibis/indexer/orders/page/:page", 
      fresh: "#{Settings.hosts.aquarium}/ibis/indexer/orders/fresh/:from"
    })

    transform do |document|
      document['users'].map! do |user|
        user.delete('orders')
        user.delete('accounts')
        user.delete('avatar')

        user
      end

      document
    end
  end
end
