module Indexer
  class Documents < Indexer::Base
    load_settings_from_file

    sources({
                one:   "#{Settings.hosts.snipe}/v1/documents/:id/preview",
                all:   "#{Settings.hosts.snipe}/v1/search/documents/index/:page",
                fresh: "#{Settings.hosts.snipe}/v1/search/documents/fresh/:from/"
            })

    transform do |document|
      document.delete('taggings')

      # Вырезаем конечные переводы строки и HTML теги
      text_blocks                       = document.values_at(*%w(subtitle newspaper_title newspaper_subtitle blog_quote announce)).compact
      text_blocks                       += document['boxes'].map do |box|
        ActionController::Base.helpers.strip_tags(case box['type']
                                                  when 'gallery'
                                                    if box.key?('children') && box['children'].is_a?(Array)
                                                      box['children'].map { |child| child['description'].to_s }.join(' ')
                                                    else
                                                      ''
                                                    end
                                                  else
                                                    box['body'] || ''
                                                  end).gsub(/\r/, " ").gsub(/\n/, " ")
      end
      document['boxes']                 = text_blocks.join(' ').strip
      document['admin_url']             = "#{Settings.hosts.ibis}/documents/#{document['id']}/edit"

      # Разворачиваем категории в список
      categories                        = categories_list(document['categories']).map(&:to_s).sort.uniq
      document['categories_list']       = categories
      document['categories_list_count'] = categories.size

      document
    end

    class << self
      def categories_list(structure)
        categories = []

        if structure.kind_of?(Hash)
          categories << structure['slug'] if structure['slug'].present?
          structure.each_pair do |key, branch|
            if branch.kind_of?(Hash) || branch.kind_of?(Array)
              categories << key
              categories = categories + categories_list(branch)
            end
          end
        end

        if structure.kind_of?(Array)
          structure.each do |branch|
            categories = categories + categories_list(branch)
          end
        end

        categories
      end
    end
  end
end
