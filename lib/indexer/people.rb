module Indexer
  class People < Indexer::Entity
    CATEGORY_SLUG = 'people'

    load_settings_from_file

    sources({
                one:   "#{Settings.hosts.snipe}/v1/documents/:id/preview",
                all:   "#{Settings.hosts.snipe}/v1/search/documents/index/:page?category_ids[]=#{category_id_by_slug}",
                fresh: "#{Settings.hosts.snipe}/v1/search/documents/fresh/:from/"
            })

    transform do |document|
      next unless person_valid?(document)

      attributes = %w(id synonyms published_at published url last_name first_name disable_on_main_page)
      document.keep_if { |k, _| attributes.include?(k) }
      document['first_letter'] = document['last_name'][0].mb_chars.upcase.to_s
      document
    end

    class << self

      def person_valid?(document)
        document['categories']['kinds']['slug'] == CATEGORY_SLUG && !document['first_name'].blank? && !document['last_name'].blank?
      end

      def group_separator_attribute
        'last_name'
      end

      def sort_attributes
        [
            { last_name: :asc }
        ]
      end
    end
  end
end
