module Indexer
  class Payments < Indexer::Base
    load_settings_from_file

    sources({
      one:   "#{Settings.hosts.aquarium}/ibis/payments/:id?view=search", 
      all:   "#{Settings.hosts.aquarium}/ibis/indexer/payments/page/:page", 
      fresh: "#{Settings.hosts.aquarium}/ibis/indexer/payments/fresh/:from"
    })
  end
end
