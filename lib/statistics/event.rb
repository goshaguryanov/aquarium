module Statistics
  class Event < Base

    attr_reader :options, :event, :user_agent, :referer, :rnd, :tags, :access_token, :status

    def initialize(options = {})
      @options    = options
      @event      = @options.delete(:event)
      @user_agent = @options.delete('user_agent')
      @referer    = @options.delete('referer')
      @access_token = @options.delete('access_token')
      @status = @options.delete('status')
      @rnd        = Time.now.to_i * 1000 + rand(1000) / 1000.0
      @tags       = "[\"#{options.to_a.map { |tag| tag.join(':') }.join('","')}\"]"
    end

    def send!
      real_referer = (referer && !referer.empty?) ? referer : 'http://vedomosti.ru'
      response = rigel_client.get('/boom', event_params,{
          'User-Agent' => user_agent,
          'HTTP_REFERER' => real_referer,
          'Referer' => real_referer,
          'Referrer' => real_referer
      })
      response.success?
    end

    private

    def event_params
      {
          event: event,
          rnd: rnd,
          referer: (referer && !referer.empty?) ? referer : 'http://vedomosti.ru',
          tags: tags,
          access_token: access_token
      }
    end
  end
end
