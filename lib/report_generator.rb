class ReportGenerator
  attr_accessor :report
  
  def initialize(report_id)
    @report = Report.find(report_id)
  end
  
  def process
    level = Rails.logger.level
    Rails.logger.level = 0
    
    if report_kind = "report_kinds/#{@report.kind}".camelize.safe_constantize
      reporter = report_kind.new(@report)
      reporter.call
      
      @report.update(ready: true, url: reporter.url)
      Rails.logger.debug("Report #{@report.kind} (#{@report.id}) was generated")
    else
      Rails.logger.debug("Error! Report #{@report.kind} (#{@report.id}) is not generated")
    end
    
    Rails.logger.level = level
  end
end
