class AdriverClient
  extend Forwardable
  
  def_delegators :connection, *%i(get post put patch delete)
  
  attr_reader :user_id, :password
  
  def authorize!
    response = get('/login') do |request|
      request.headers['X-Auth-Login'] = Settings.adriver.login
      request.headers['X-Auth-Passwd'] = Settings.adriver.password
    end
    
    @user_id, @password = response.body.login.values_at(:userId, :token)
  end
  
private
  def connection
    Faraday.new(url: Settings.hosts.adriver) do |builder|
      builder.request  :authentication, user_id, password if authenticated?
      builder.response :mashify
      builder.response :xml, content_type: /\bxml$/
      builder.response :unauthorized_handler
      builder.adapter  Faraday.default_adapter
    end
  end
  
  def authenticated?
    user_id && password
  end
  
  class AdriverAuth < Faraday::Middleware
    attr_reader :user_id, :password
    
    def initialize(app, user_id, password)
      super(app)
      @user_id  = user_id
      @password = password
    end
    
    def call(env)
      env.request_headers['X-Auth-UserID'] = user_id
      env.request_headers['X-Auth-Passwd'] = password
      env.request_headers['Content-Type'] = 'application/atom+xml'
      
      @app.call(env)
    end
  end
  
  Faraday::Request.register_middleware(authentication: AdriverAuth)
  
  class Unauthorized < StandardError; end
  
  class UnauthorizedHandler < Faraday::Response::Middleware
    def on_complete(response)
      raise Unauthorized if response.status == 401
    end
  end
  
  Faraday::Response.register_middleware(unauthorized_handler: UnauthorizedHandler)
end
