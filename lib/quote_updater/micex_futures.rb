module QuoteUpdater
  class MicexFutures < MicexBase
    def call
      nearest = {}
      securities.rows.row.each do |sec|
        if !nearest[sec.ASSETCODE] || sec.LASTTRADEDATE < nearest[sec.ASSETCODE].LASTTRADEDATE
          nearest[sec.ASSETCODE] = sec
        end
      end
      ActiveRecord::Base.transaction do
        nearest.values.each do |curr|
          price = marketdata.rows.row.find { |r| r.SECID==curr.SECID }
          if price && price.LAST.present?
            ticker = Ticker.find_or_initialize_by(quote_source: source, slug: curr.ASSETCODE)
            ticker.update(
              title:      curr.SHORTNAME,
              price_time: Time.parse(price.SYSTIME),
              value:      price.LAST.to_f,
              prev_value: 100 * price.LAST.to_f / (price.LASTTOPREVPRICE.to_f + 100),
              data:       curr.to_h.merge(price.to_h)
            )
          end
        end
      end
    end
  end
end
