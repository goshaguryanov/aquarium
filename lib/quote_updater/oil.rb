module QuoteUpdater
  class Oil < Base
    def call
      ActiveRecord::Base.transaction do
        oil_types.each do |slug, short|
          curr = source_content.select { |q| /^#{short}_\d+$/.match(q.tradecode) }.sort { |a, b| b.tradecode <=> a.tradecode }.first
          if curr
            ticker = Ticker.find_or_initialize_by(quote_source: source, slug: slug)
            ticker.update(
              title:      slug,
              price_time: Time.parse(curr.date),
              value:      price_translate(curr.rate),
              prev_value: price_translate(curr.rate) - price_translate(curr.change),
              data:       { fintoolid: curr.fintoolid }
            )
          end
        end
      end
    end
    
  private
    def connection_feature(conn)
      conn.response :xml
    end
    
    def source_content
      res = super.OIL.ITEM
      res.is_a?(Array) ? res : [ res ]
    end
    
    def oil_types
      {
        'BRENT' => 'BR',
        'URALS' => 'UR'
      }
    end
  end
end
