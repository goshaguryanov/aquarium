module QuoteUpdater
  class Metals < Base
    def call
      ActiveRecord::Base.transaction do
        source_content.each do |curr|
          slug = ticker_slugs[curr.fintoolid]
          if slug
            ticker = Ticker.find_or_initialize_by(quote_source: source, slug: slug)
            ticker.update(
              title:      curr.tradecode,
              price_time: Time.parse(curr.date),
              value:      price_translate(curr.rate),
              prev_value: price_translate(curr.rate) - price_translate(curr.change),
              data:       { fintoolid: curr.fintoolid }
            )
          end
        end
      end
    end
    
  private
    def connection_feature(conn)
      conn.response :xml
    end
    
    def source_content
      items = super.GOOD.ITEM
      items.is_a?(Array) ? items : [ items ]
    end
    
    def ticker_slugs
      {
        '11797' => 'GOLD',
        '11800' => 'PALLADIUM',
        '11799' => 'PLATINUM',
        '11798' => 'SILVER'
      }
    end
  end
end
