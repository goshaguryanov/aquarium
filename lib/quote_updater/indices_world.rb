module QuoteUpdater
  class IndicesWorld < Base
    def call
      ActiveRecord::Base.transaction do
        source_content.each do |curr|
          slug = ticker_slugs[curr.fintoolid]
          if slug
            ticker = Ticker.find_or_initialize_by(quote_source: source, slug: slug)
            ticker.update(
              title:      curr.tradecode,
              price_time: Time.parse(curr.date),
              value:      price_translate(curr.rate),
              prev_value: price_translate(curr.rate) - price_translate(curr.change),
              data:       { fintoolid: curr.fintoolid }
            )
          end
        end
      end
    end
    
  private
    def connection_feature(conn)
      conn.response :xml
    end
    
    def source_content
      items = super.INDEXES.ITEM
      items.is_a?(Array) ? items : [ items ]
    end
    
    def ticker_slugs
      {
        '5119' => 'DJIA',
        '5118' => 'SPX'
      }
    end
  end
end
