module QuoteUpdater
  class MicexCurrencies < MicexBase
    def call
      ActiveRecord::Base.transaction do
        securities.rows.row.each do |curr|
          price = marketdata.rows.row.find { |r| r.SECID==curr.SECID }
          if price && price.LAST.present?
            ticker = Ticker.find_or_initialize_by(quote_source: source, slug: curr.SECID)
            ticker.update(
              title:      curr.SHORTNAME,
              price_time: Time.parse(price.SYSTIME),
              value:      price.LAST.to_f,
              prev_value: price.LAST.to_f - price.CHANGE.to_f,
              data:       curr.to_h.merge(price.to_h)
            )
          end
        end
      end
    end
  end
end
