class RigelClient
  extend Forwardable

  def_delegators :connection, :get

  attr_reader :connection

  def initialize
    @connection = Faraday.new(url: Settings.hosts.rigel) do |builder|
      builder.adapter  :net_http_persistent
    end
  end

  def get(path, params, headers = {})
    connection.get(path, params) do |req|
      req.headers.merge!(headers.reject { |key,value| !value ||  value.empty? })
    end
  end
end
