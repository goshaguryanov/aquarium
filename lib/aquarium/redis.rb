module Aquarium
  module Redis
    class << self
      def connect!
        @connection = ::Redis.new(Settings.redis.to_hash)
      end
      
      def disconnect!
        @connection = nil
      end
      
      def connection
        connect! unless @connection
        @connection
      end
    end
  end
end
