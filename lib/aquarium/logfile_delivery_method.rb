module Aquarium
  class LogfileDeliveryMethod
    def initialize(settings)
      @settings = {
        filename: Rails.root.join('log', 'mail.log')
      }.merge(settings)
    end

    attr_accessor :settings

    def deliver!(mail)
      File.open(settings[:filename], 'a') do |f|
        f.write "#{mail.encoded}\n\n"
      end
    end
  end
end
