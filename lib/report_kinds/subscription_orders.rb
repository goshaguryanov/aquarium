module ReportKinds
  class SubscriptionOrders < Base
    attr_reader :url
    
    def call
      response = elastic.search(body: query(@report.start_date, @report.end_date))
      result = response['aggregations']['ids']['buckets']
      
      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(
          ID
          Фамилия
          Имя
          Email
          Телефон
          Компания
          Статус
          Автоплатёж
          Дата\ регистрации
          Число\ заходов
          Продукт
          Акция
          Цена
          Период
          Статус\ оплаты
          Способ\ оплаты
          Последний\ Payment\ ID
          Число\ оплат
        )
      
        result.each do |item|
          if user = User.find(item['key'])
            payments = user.payments.where(created_at: @report.start_date..@report.end_date).order(created_at: :desc)
            payment = payments.first
            
            user_data = [
              user.id,
              user.last_name,
              user.first_name,
              user.email,
              user.phone,
              user.company,
              user.status,
              user.autopay,
              user.created_at,
              item['doc_count']
            ]
            
            payment_data = if payment
              [
                payment.subproduct.slug,
                payment.namespace.slug,
                payment.price,
                payment.period.slug,
                payment.status,
                payment.paymethod.slug,
                payment.id,
                payments.count
              ]
            else
              []
            end
            
            csv << user_data + payment_data
          end
        end
        
        csv.close
      end
      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end
    
    def query(start_date, end_date)
      {
        query: {
          bool: {
            must: [
                {
                  range: {
                    time: {
                      lt: end_date.strftime('%FT%T%:z'),
                      gt: start_date.strftime('%FT%T%:z')
                    }
                  }
                },
                { term: { domain: 'buy.vedomosti.ru' } },
                { term: { event:  'subscription' } }
            ]
          }
        },
        aggs: {
          ids: { terms: { field: 'tid', size: 0 } }
        },
        size: 0
      }
    end
  end
end
