module ReportKinds
  class NewPaymentsSales < ReportKinds::PaymentsSales
    def call
      @filename.sub!('.csv','.xlsx')
      @filepath = @filepath.to_s.sub('.csv','.xlsx')

      payments.find_each.with_index do |payment, idx|
        user = payment.user
        prev_payment_id = payment.data['previous_payment_id']
        row = idx + 1

        worksheet.write(row, 0,   payment.id)
        worksheet.write(row, 1,   payment.created_at.strftime('%Y-%m-%d %H:%M'))
        worksheet.write(row, 2,   user && user.last_name)
        worksheet.write(row, 3,   user && user.first_name)
        worksheet.write(row, 4,   user && user.email)
        worksheet.write(row, 5,   payment.subproduct.title)
        worksheet.write(row, 6,   payment.namespace.title)
        worksheet.write(row, 7,   payment.period.title)
        worksheet.write(row, 8,   payment.paymethod.title)
        worksheet.write(row, 9,   payment.price)
        worksheet.write(row, 10,  payment.autopayment ? 'Да': 'Нет')
        worksheet.write(row, 11,  prev_payment_id)
      end
      workbook.close
    end

    private

    def payments
      excluded_paymethod_ids = [ Paymethod.find_by_slug('mobile').id ]
      Payment.paid.joins(:payment_kit).where(payment_kits: {syncable: true}).
          where('payments.paymethod_id NOT IN(?)', excluded_paymethod_ids).
          where(payments: {created_at: (@report.start_date..@report.end_date)}).
          includes(:subproduct, :namespace, :period, :users).order(:created_at)
    end

    def worksheet
      @worksheet ||= begin
        format = workbook.add_format
        format.set_bold
        format.set_align('center')

        ws = workbook.add_worksheet('Заказы')

        ws.write(0, 0,  'ID заказа',          format)
        ws.write(0, 1,  'Время заказа',       format)
        ws.write(0, 2,  'Фамилия',            format)
        ws.write(0, 3,  'Имя',                format)
        ws.write(0, 4,  'E-mail',             format)
        ws.write(0, 5,  'Продукт',            format)
        ws.write(0, 6,  'Акция',              format)
        ws.write(0, 7,  'Период',             format)
        ws.write(0, 8,  'Способ платежа',     format)
        ws.write(0, 9 , 'Цена',               format)
        ws.write(0, 10, 'Автопродление',      format)
        ws.write(0, 11, 'Предыдущий платеж',  format)
        ws
      end
    end
  end
end
