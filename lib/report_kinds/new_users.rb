module ReportKinds
  class NewUsers < Base
    attr_reader :url
    
    def call
      result = User.where(created_at: (@report.start_date..@report.end_date))
            
      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(ID Фамилия Имя Email Телефон Компания Статус Автоплатёж Дата\ регистрации)
      
        result.each do |user|
            csv << [
              user.id,
              user.last_name,
              user.first_name,
              user.email,
              user.phone,
              user.company,
              user.status,
              user.autopay,
              user.created_at
            ]
        end
        
        csv.close
      end
      
      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end
  end
end
