module ReportKinds
  class PaywallSubscriptions < Base
    attr_reader :url

    def call
      response = elastic.search(body: query(@report.start_date, @report.end_date))
      result = response['aggregations']['paywall_subscription']['buckets']

      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(ID Фамилия Имя Email Дата\ события)

        result.each do |item|
          if (user = User.find(item['key']))
            csv << [
                user.id,
                user.last_name,
                user.first_name,
                user.email,
                Time.at(item['max_time']['value'].to_i / 1000)
            ]
          end
        end

        csv.close
      end

      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end

    def query(start_date, end_date)
      {
          query: {
              bool: {
                  must: [
                      {range:
                           {
                               time: {
                                   lt: end_date.strftime('%FT%T%:z'),
                                   gt: start_date.strftime('%FT%T%:z')
                               }
                           }
                      },
                      {term: {towl_status: 'forbidden'}},
                      {term: {event: 'paywall_subscription'}},
                      {regexp: {path: '\/newspaper\/(last|\d+\/\d+\/\d+)'}}
                  ]
              }
          },
          aggs: {
              paywall_subscription: {
                  terms: {field: 'tid', size: 0},
                  aggs: {
                      max_time:
                          {
                              max: {field: 'time'}
                          }
                  }
              }
          },
          size: 0
      }
    end
  end
end
