module ReportKinds
  class MailingIssue < Base
    attr_reader :url

    def call
      result = Mailing.where(created_at: (@report.start_date..@report.end_date))
      # dev mode
      # result = [Mailing.last]

      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(ID Дата\ отправки Отправлено,\ шт. Доставлено,\ шт. Открыто,\ шт. Переходов,\ шт. Отписано,\ шт. Open\ Rate,\ % Click\ Rate,\ %)

        result.each do |mailing|
          csv << [
              mailing.id,
              mailing.scheduled_at,
              mailing.letters.count,
              mailing.letters.unknown.count,
              open_count(mailing.id),
              click_count(mailing.id),
              unsubscribe_count(mailing.id),
              (open_count(mailing.id).to_f  / mailing.letters.unknown.count * 100).round(2).to_s + '%',
              (click_count(mailing.id).to_f / mailing.letters.unknown.count * 100).round(2).to_s + '%'
          ]
        end

        csv.close
      end

      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end

    private
    def open_count(mailing_id)
      elastic.search(body: query('mail_hit', mailing_id))['aggregations']['uniq_contact_events']['value']
    end

    def click_count(mailing_id)
      elastic.search(body: query('mail_click', mailing_id))['aggregations']['uniq_contact_events']['value']
    end

    def unsubscribe_count(mailing_id)
      elastic.search(body: unsubscribe_query(mailing_id))['hits']['total']
    end

    def query(event, mailing_id)
      {
          query: {
              bool: {
                  must: [
                      { term: { tmailing_id: mailing_id } },
                      { term: { event: event } }
                  ]
              }
          },
          aggs: {
              uniq_contact_events:{
                  cardinality: {
                      field: 'tcontact_id'
                  }
              }
          },
          size: 0
      }
    end

    def unsubscribe_query(mailing_id)
      {
          query: {
              bool: {
                  must: [
                      { term: { tags:  "mailing_id:#{mailing_id}" } },
                      { term: { domain: 'id.vedomosti.ru' } }
                  ]
              }
          },
          size: 0
      }
    end
  end
end
