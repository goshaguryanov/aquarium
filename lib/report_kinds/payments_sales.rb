module ReportKinds
  class PaymentsSales < Base
    def call
      @filename.sub!('.csv','.xlsx')
      @filepath = @filepath.to_s.sub('.csv','.xlsx')

      payments.find_each.with_index do |payment, idx|
        user = payment.user
        prev_payment_id = payment.data['previous_payment_id']
        row = idx + 1

        worksheet.write(row, 0,  payment.data['guid'])    # GUID
        worksheet.write(row, 1,  '')                      # пустое значение для FreelancerID
        worksheet.write(row, 2,  '')                      # пустое значение для UserID
        worksheet.write(row, 3,  '')                      # пустое значение для CardIncominTypeID
        worksheet.write(row, 4,  '')                      # пустое значение для SalesGroupID
        worksheet.write(row, 5,  user && user.last_name)  # Фамилия
        worksheet.write(row, 6,  user && user.first_name) # Имя
        worksheet.write(row, 7,  user && user.email)      # Email
        worksheet.write(row, 8,  payment.subproduct.slug) # Продукт
        worksheet.write(row, 9,  payment.namespace.slug)  # Акция
        worksheet.write(row, 10, payment.price)           # Цена
        worksheet.write(row, 11, payment.period.slug)     # Период
        worksheet.write(row, 12, payment.id)              # ID
        worksheet.write(row, 13, prev_payment_id)         # Последний Payment ID
      end
      workbook.close
    end

    def url
      "#{Settings.hosts.files}/reports/#{@filename}"
    end

  private

    def payments
      Payment.paid.joins(:payment_kit).where(payment_kits: {syncable: true}).
        where(payments: {created_at: (@report.start_date..@report.end_date)}).
        includes(:subproduct, :namespace, :period, :users).order(:created_at)
    end

    def workbook
      @workbook ||= WriteXLSX.new(@filepath)
    end

    def worksheet
      @worksheet ||= begin
        format = workbook.add_format
        format.set_bold
        format.set_align('center')

        ws = workbook.add_worksheet('Заказы')

        ws.write(0, 0,  'impCardGUID',          format)
        ws.write(0, 1,  'FreelancerID',         format)
        ws.write(0, 2,  'UserID',               format)
        ws.write(0, 3,  'CardIncominTypeID',    format)
        ws.write(0, 4,  'SalesGroupID',         format)
        ws.write(0, 5,  'Фамилия',              format)
        ws.write(0, 6,  'Имя',                  format)
        ws.write(0, 7,  'Email',                format)
        ws.write(0, 8,  'Продукт',              format)
        ws.write(0, 9,  'Акция',                format)
        ws.write(0, 10,  'Цена',                 format)
        ws.write(0, 11, 'Период',               format)
        ws.write(0, 12, 'Payment ID',           format)
        ws.write(0, 13, 'Последний Payment ID', format)
        ws
      end
    end
  end
end
