require 'csv'

module ReportKinds
  class Base
    include Procto.call
    
    FILES_PATH = Rails.root.join(Settings.reports_path).freeze
    
    def initialize(report)
      @report = report
      @filename = "report_#{report.id}_#{Time.now.strftime("%Y-%m-%d_%H-%M")}.csv"
      @filepath = FILES_PATH.join(@filename)
    end
    
    def elastic
      @elastic ||= Elasticsearch::Client.new(
        host: Settings.statistics.host,
        port: Settings.statistics.port,
        adapter: :net_http_persistent,
      )
    end
  end
end
