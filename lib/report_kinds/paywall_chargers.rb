module ReportKinds
  class PaywallChargers < Base
    attr_reader :url
    
    def call
      # production
      response = elastic.search(body: query(@report.start_date, @report.end_date))
      result = response['aggregations']['authenticated_limit_exceeded']['buckets']
      
      # development
      # result = User.all.map do |user|
      #   { 'key' => user.id, 'doc_count' => 10 }
      # end
      #
      File.open(@filepath, 'wb', encoding: 'cp1251', undef: :replace, replace: '?') do |file|
        csv = CSV.new(file, col_sep: ';', encoding: 'cp1251')
        csv << ['sep=', nil]
        csv << %W(ID Фамилия Имя Email Телефон Компания Статус Автоплатёж Дата\ регистрации Число\ пэйволлов)
      
        result.each do |item|
          if user = User.find(item['key'])
            csv << [
              user.id,
              user.last_name,
              user.first_name,
              user.email,
              user.phone,
              user.company,
              user.status,
              user.autopay,
              user.created_at,
              item['doc_count']
            ]
          end
        end
        
        csv.close
      end
      
      @url = "#{Settings.hosts.files}/reports/#{@filename}"
    end
    
    def query(start_date, end_date)
      {
        query: {
          bool: {
            must: [
              { range:
                {
                  time: {
                    lt: end_date.strftime('%FT%T%:z'),
                    gt: start_date.strftime('%FT%T%:z')
                  }
                }
              },
 #             { term:  { towl_reason: 'authenticated_limit_exceeded' } },
              { term:  { event: 'paywall' } }
            ]
          }
        },
        aggs: {
          authenticated_limit_exceeded: { terms: { field: 'tid', size:  0 } }
        },
        size: 0
      }
    end
  end
end
