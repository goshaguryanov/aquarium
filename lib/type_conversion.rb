module TypeConversion
  def self.to_bool(value)
    if value.is_a? String
      value = value =~ /^(true|t|yes|y|1)$/i
    elsif value.is_a? Integer
      value = value != 0
    end
    !!value
  end
end
