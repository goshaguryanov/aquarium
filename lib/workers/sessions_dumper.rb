module Workers
  class SessionsDumper
    extend Workers::Logger

    @queue = :payments

    class << self
      def perform
        logger.info "Check access for sessions with expired access_right"
        access_rights.find_each do |access_right|
          access_right.dump_to_redis
          if access_right.reader_type == 'LegalEntity'
            legal_entity = access_right.reader
            logger.info "LegalEntity \"#{legal_entity.name}\", id: #{legal_entity.id}, access: #{legal_entity.subscriber? ? 'yes' : 'no'}"
            unless legal_entity.subscriber?
              LegalEntity.remove_ips_from_redis(legal_entity.raw_ips)
            end
          end
        end
      end

      def pending_start_date
        1.day.ago - 1.minute
      end

      def access_rights
        AccessRight.where(
          "start_date between ? and ? or end_date between ? and ?",
          pending_start_date,
          Time.now,
          pending_start_date,
          Time.now
        )
      end
    end
  end
end
