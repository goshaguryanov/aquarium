require 'resque-lock-timeout'

module Workers
  module Indexer
    class Documents
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::Documents.index_changed
        logger.info "index #{count} changed documents"
      end
    end
  end
end
