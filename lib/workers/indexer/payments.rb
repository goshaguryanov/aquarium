require 'resque-lock-timeout'

module Workers
  module Indexer
    class Payments
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::Payments.index_changed
        logger.info "index #{count} changed payments"
      end
    end
  end
end
