require 'resque-lock-timeout'

module Workers
  module Indexer
    class Contacts
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::Contacts.index_changed
        logger.info "index #{count} changed contacts"
      end
    end
  end
end
