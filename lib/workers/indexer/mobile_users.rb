require 'resque-lock-timeout'

module Workers
  module Indexer
    class MobileUsers
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::MobileUsers.index_changed
        logger.info "index #{count} changed mobile_users"
      end
    end
  end
end
