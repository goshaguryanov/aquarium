require 'resque-lock-timeout'

module Workers
  module Indexer
    class Orders
      extend Workers::Logger
      extend Resque::Plugins::LockTimeout
      
      @queue        = :indexer
      
      @loner        = true
      @lock_timeout = 3600

      def self.perform
        count = ::Indexer::Orders.index_changed
        logger.info "index #{count} changed orders"
      end
    end
  end
end
