module Workers
  module Statistics
    class FireEvent
      extend Workers::Logger

      @queue = :payments

      def self.perform(event, options = {})
        if ::Statistics::Event.new({ event: event }.merge(options)).send!
          logger.info 'Event has been sent successfully'
        else
          logger.info 'Event has been sent unsuccessfully'
        end
      end
    end
  end
end
