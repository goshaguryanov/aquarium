module Workers
  class ReportGeneratorJob
    extend Workers::Logger
    
    @queue = :report_generator
    
    class << self
      def perform(report_id)
        logger.info "Start generating report #{report_id}"
        report_generator = ReportGenerator.new(report_id)
        report_generator.process
        logger.info "Report #{report_id} was generated"
      end
    end
  end
end
