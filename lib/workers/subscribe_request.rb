module Workers
  class SubscribeRequest
    extend Workers::Logger

    @queue = :mailer

    def self.perform(slug, params)
      ::SubscribeRequest.send(slug, params).deliver
      logger.info "Subscribe request for #{slug} email: name #{params[:name]} email: #{params[:email]}"
    end
  end
end
