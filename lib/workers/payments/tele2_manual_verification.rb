module Workers
  module Payments
    class Tele2ManualVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(subscription_id, access_token)
        logger.info 'Start verifying Tele2 subscription status manually'
        use_case = ::PaymentProcess::Mobile::Tele2::Manual.new(subscription_id, access_token)
        if use_case.verify!
          logger.info 'Successfully finished verifying Tele2 subscription manually'
          true
        else
          logger.error use_case.error
          logger.info 'Unsuccessfully finished verifying Tele2 subscription manually'
          false
        end
      end
    end
  end
end
