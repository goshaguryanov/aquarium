module Workers
  module Payments
    class MegafonCallbackVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(body)
        logger.info 'Start verifying Megafon subscription status by callback'
        use_case = ::PaymentProcess::Mobile::Megafon::Callback.new(body)
        if use_case.verify!
          logger.info 'Successfully finished verifying Megafon callback'
          true
        else
          logger.error use_case.errors
          logger.info 'Unsuccessfully finished verifying Megafon callback'
          false
        end
      end
    end
  end
end
