module Workers
  module Payments
    class PayMasterResult
      extend Workers::Logger
      @queue = :payments
      def self.perform(payment_id, client_ip = nil)
        logger.info "get result for payment #{payment_id}"
        payment = Payment.notpaid.bymethod(:paymaster).find(payment_id)
        payment.status = :paid
        payment.grant_access
        payment.send_user_mail
        payment.save!

        payment.fire_event!

        Snapshot.create!(
            subject: payment,
            event:   'payment_paymaster_final',
            data:    payment.attributes
        )
      end
    end
  end
end
