module Workers
  module Payments
    class GoogleRenewalFind
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        logger.info 'Find payments for google renewal'
        Payment.bymethod(:google).paid.where(end_date: (Time.now .. 6.hour.from_now)).each do |payment|
          if payment.can_renewal?
            Resque.enqueue(Workers::Payments::GoogleVerify, payment.data['subscription_id'], payment.data['token'], payment.data['session_id'], payment.client_ip, payment.id)
          end
        end
      end
    end
  end
end
