module Workers
  module Payments
    class CardRenewal
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform(payment_id)
        logger.info "Renewal card payment: #{payment_id}"
        use_case = ::Payments::Card::Renewal.new(Payment.bymethod(:card).find(payment_id))
        renewal_status = use_case.call
        payment = use_case.newpayment

        unless payment
          logger.error "Renewal failed for payment: #{payment_id}, can't create new payment"
          return
        end

        if renewal_status
          payment.status = :paid
          payment.grant_access
          payment.send_autopay_user_email
        else
          payment.status = :canceled
        end

        payment.fire_event!

        payment.data['payment_renewal_response'] = use_case.response.to_hash if use_case.response
        payment.data_will_change!
        payment.save!
        Snapshot.create!(
          subject: payment,
          event:   'payment_card_renewal',
          data:    payment.attributes
        )

      end
    end
  end
end
