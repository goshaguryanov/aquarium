module Workers
  module Payments
    class CardCloseDay
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        logger.info "Close financial day in RSB"
        ::Payments::Card::CloseDay.new.call
      end
    end
  end
end
