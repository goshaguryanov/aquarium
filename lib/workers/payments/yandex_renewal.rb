module Workers
  module Payments
    class YandexRenewal
      extend Workers::Logger
      
      @queue = :payments

      class << self
        def perform(payment_id, repeat)
          logger.info "Renewal yandex payment: #{payment_id}"
          payment = Payment.bymethod([:yandex_kassa, :yandex_money]).find(payment_id)

          response = client.repeatCardPayment(payment)
          if response.success?
            logger.info "Success renewal: #{payment.id}"
          elsif response.need_repeat?
            logger.info "Need repeat renewal: #{payment.id}, because: #{response.techMessage}"

            run_at = if repeat.to_i == 0 # Первый повтор через минуту
              Time.now + 1.minute
            elsif repeat.to_i < 4 # Следующие три через 5 минут
              Time.now + 1.minute
            else # Остальные через 30 минут
              Time.now + 30.minute
             end
            
            Resque.enqueue_at(run_at, Workers::Payments::YandexRenewal, payment_id, repeat.to_i + 1)

          elsif response.error?
            logger.error "Can't renewal: #{payment.id}, because: #{response.techMessage}"
            payment.status = :canceled
          else
            logger.error "Can't renewal: #{payment.id}, because unsupported status code #{response.status} with '#{response.techMessage}'"
            payment.status = :canceled
          end

          if payment.changed? and payment.status == :canceled
            payment.save!
            Snapshot.create!(
                subject: payment,
                event:   'payment_cancel_renewal',
                data:    payment.attributes
            )
          end

        end

        def client
          @client ||= YandexPenelope::Client.new
        end
      end
    end
  end
end
