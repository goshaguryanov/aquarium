module Workers
  module Payments
    class CardCheckPayments
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        cnt = Payment.bymethod(:card).notpaid.where(created_at: 1.day.until..15.minute.until).where('transaction_id is not null').each do |payment|
          Resque.enqueue(Workers::Payments::CardGetResult, payment.id)
        end.count
        logger.info("Checked #{cnt} unpaid card orders") if cnt>0
      end
    end
  end
end