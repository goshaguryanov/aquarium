module Workers
  module Payments
    class YandexResult
      extend Workers::Logger
      @queue = :payments
      def self.perform(payment_id, client_ip = nil)
        logger.info "get result for payment #{payment_id}"
        payment = Payment.notpaid.bymethod([:yandex_kassa, :yandex_money]).find(payment_id)
        payment.status = :paid
        payment.grant_access
        payment.send_user_mail
        payment.save!
        Snapshot.create!(
          subject: payment,
          event:   'payment_yandex_final',
          data:    payment.attributes
        )
      end
    end
  end
end
