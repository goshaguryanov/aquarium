module Workers
  module Payments
    class Tele2CallbackVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(body)
        logger.info 'Start verifying Tele2 subscription status by callback'
        use_case = ::PaymentProcess::Mobile::Tele2::Callback.new(body)
        if use_case.verify!
          logger.info 'Successfully finished verifying Tele2 callback'
          true
        else
          logger.error use_case.errors
          logger.info 'Unsuccessfully finished verifying Tele2 callback'
          false
        end
      end
    end
  end
end
