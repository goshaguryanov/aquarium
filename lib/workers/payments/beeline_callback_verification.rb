module Workers
  module Payments
    class BeelineCallbackVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(body)
        logger.info 'Start verifying Beeline subscription status by callback'
        use_case = ::PaymentProcess::Mobile::Beeline::Callback.new(body)
        if use_case.verify!
          logger.info 'Successfully finished verifying Beeline callback'
          true
        else
          logger.error use_case.errors
          logger.info 'Unsuccessfully finished verifying Beeline callback'
          false
        end
      end
    end
  end
end
