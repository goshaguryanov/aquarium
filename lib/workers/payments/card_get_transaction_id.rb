module Workers
  module Payments
    class CardGetTransactionId
      extend Workers::Logger

      @queue = :payments

      def self.perform(payment_id)
        logger.info "get transaction_id for payment #{payment_id}"
        payment = Payment.notpaid.bymethod(:card).find(payment_id)
        use_case = ::Payments::Card::GetTransactionId.new(payment)
        if transaction_id = use_case.call
          payment.transaction_id = transaction_id
        else
          payment.status = :canceled
        end
        payment.data['biller_client_id'] = use_case.biller_client_id
        payment.data['payment_start_response'] = use_case.response.to_hash if use_case.response
        payment.data_will_change!
        payment.save!
        Snapshot.create!(
          subject: payment,
          event:   'payment_card_start',
          data:    payment.attributes
        )
      end
    end
  end
end