module Workers
  module Payments
    class AppleVerify
      extend Workers::Logger

      @queue = :payments

      class << self
        def perform(receipt_data, session_id, client_ip)
          logger.info "Verifing apple receipt; session_id: #{session_id}, client_ip: #{client_ip}, receipt_data: #{receipt_data}"

          receipt = Itunes::Receipt.get(receipt_data)
          if receipt && receipt.error == :sandbox_receipt && !Rails.env.production?
            receipt = Itunes::Receipt.get(receipt_data, true)
          end
          if !receipt || receipt.error
            logger.error "apple verify failed, session_id: #{session_id}, client_ip: #{client_ip}, error: #{receipt && receipt.error.to_s || 'unknown error'}"
            return
          end

          last_receipt = receipt.latest
          logger.info "#{self.name} session_id: #{session_id}, response: #{last_receipt}"
          
          # Найдем платеж:
          payment = Payment.bymethod('apple').find_by_transaction_id(last_receipt.transaction_id)
          unless payment
            payment = new_payment_by_receipt(last_receipt, client_ip)
            
            previous_payment = receipt.previous && Payment.bymethod('apple').find_by_transaction_id(receipt.previous.transaction_id)
            if previous_payment
              payment.data[:previous_payment_id] = previous_payment.id
              payment.payment_kit_id = previous_payment.renewal_id
              payment.autopayment = true
              payment.save

              previous_payment.data[:next_payment_id] = payment.id
              previous_payment.data_will_change!
              previous_payment.save
              previous_payment.snapshots.create(
                event: 'payments_apple_verify_renewal',
                data:  previous_payment.attributes
              )
              
              # копируем доступы из прошлого заказа
              previous_payment.access_rights.each do |ar|
                Common::GrantAccess.new(
                  payment:    payment,
                  reader:     ar.reader,
                  start_date: payment.start_date,
                  end_date:   payment.end_date
                ).call
              end
            else
              payment.save
            end

            payment.snapshots.create(
              event: 'payments_apple_verify',
              data:  payment.attributes
            )
          end

          if payment && session_id
            # привязываем текущую сессию к подпискам.
            session = Session.find(session_id)
            unless payment.access_rights.find { |ar| ar.reader == session }
              Common::GrantAccess.new(
                payment:    payment,
                reader:     session,
                start_date: payment.start_date,
                end_date:   payment.end_date
              ).call
            end

            if session.user
              # попробуем привязать подписку к пользователю
              ::Payments::LinkSubscription.new(session, session.user).call
            end
          end
        end

        def namespace
          @namespace ||= Namespace.joins(:product).where('products.slug=? and namespaces.slug=?', Settings.apple.product, Settings.apple.namespace).first
        end

        def new_payment_by_receipt(receipt, client_ip)
          payment_kit = namespace.payment_kits.joins(:period).order("abs(extract('epoch' from periods.duration) - #{receipt.expires_date - receipt.purchase_date})").first
          params = {
            payment_kit_id: payment_kit.id,
            price:          payment_kit.price,
            renewal_id:     payment_kit.renewal_id,
            paymethod:      Paymethod.find_by_slug('apple'),
            transaction_id: receipt.transaction_id,
            start_date:     receipt.purchase_date,
            end_date:       receipt.expires_date,
            data: {
              auto_renewing: true,
              receipt:       receipt.receipt_data,
              product_id:    receipt.product_id,
              receipt_info:  receipt.raw_receipt,
              client_ip:     client_ip,
            }
          }
          Payment.paid.new(params)
        end
      end
    end
  end
end