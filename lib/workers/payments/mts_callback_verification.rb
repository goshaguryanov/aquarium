module Workers
  module Payments
    class MtsCallbackVerification
      extend Workers::Logger

      @queue = :payments

      def self.perform(body)
        logger.info 'Start verifying MTS subscription status by callback'
        use_case = ::PaymentProcess::Mobile::Mts::Callback.new(body)
        if use_case.verify!
          logger.info 'Successfully finished verifying MTS callback'
          true
        else
          logger.error use_case.errors
          logger.info 'Unsuccessfully finished verifying MTS callback'
          false
        end
      end
    end
  end
end
