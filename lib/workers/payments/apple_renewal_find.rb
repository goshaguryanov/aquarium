module Workers
  module Payments
    class AppleRenewalFind
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        logger.info "Find payments for apple renewal"
        Payment.bymethod(:apple).paid.where(end_date: (Time.now .. 23.hour.from_now)).each do |payment|
          if payment.can_renewal?
            Resque.enqueue(Workers::Payments::AppleVerify, payment.data['receipt'], nil, payment.client_ip)
          end
        end
      end
    end
  end
end
