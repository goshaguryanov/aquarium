module Workers
  module Payments
    class YandexRenewalFind
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        logger.info 'Find payments for yandex renewal'
        Payment.bymethod([:yandex_kassa, :yandex_money]).paid.joins(:payment_kit).where("payments.end_date > now() and (payments.end_date - payment_kits.autopay_timeout) <= now()").each do |payment|
          if payment.can_renewal? && !payment.have_next_subscription? && payment.data.symbolize_keys[:invoiceId].present?
            use_case = ::Payments::Yandex::RenewalPayment.new(payment)
            Resque.enqueue(Workers::Payments::YandexRenewal, use_case.newpayment.id, 0) if use_case.call
          elsif payment.can_renewal? && payment.have_next_subscription?
            # у пользователя оказалась другая подписка
            payment.data[:renewal_failed] = true
            payment.data_will_change!
            payment.save
            Snapshot.create!(
              subject: payment,
              event:   'workers_yandex_renewal_failed',
              data:    payment.attributes
            )
            InnerMailer.renewal_failed(payment, 'У пользователя уже есть оформленная подписка').deliver
          end
        end
      end
    end
  end
end
