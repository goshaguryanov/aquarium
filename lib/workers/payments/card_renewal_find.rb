module Workers
  module Payments
    class CardRenewalFind
      extend Workers::Logger
      
      @queue = :payments
      
      def self.perform
        logger.info 'Find payments for card renewal'
        Payment.bymethod(:card).paid.joins(:payment_kit).where("payments.end_date > now() and (payments.end_date - payment_kits.autopay_timeout) <= now()").each do |payment|
          if payment.can_renewal? && !payment.have_next_subscription? && payment.data.symbolize_keys[:biller_client_id].present? && payment.payment_kit.renewal_id
            Resque.enqueue(Workers::Payments::CardRenewal, payment.id)
          elsif payment.can_renewal? && payment.have_next_subscription?
            # у пользователя оказалась другая подписка
            payment.data[:renewal_failed] = true
            payment.data_will_change!
            payment.save
            Snapshot.create!(
              subject: payment,
              event:   'workers_card_renewal_failed',
              data:    payment.attributes
            )
           # InnerMailer.renewal_failed(payment, 'У пользователя уже есть оформленная подписка').deliver
          end
        end
      end
    end
  end
end
