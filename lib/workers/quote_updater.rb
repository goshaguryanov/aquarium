module Workers
  class QuoteUpdater
    extend Workers::Logger
    
    @queue = :quotes
    
    class << self
      def perform(slug)
        QuoteSource.find_by_slug(slug).update
        logger.info "Quotes '#{slug}' updated"
      end
    end
  end
end
