module Workers
  class DummyJob
    extend Workers::Logger

    @queue = :indexer

    class << self
      def perform
        sleep(5)
        logger.debug 'Run dummy job!'
      end
    end
  end
end
