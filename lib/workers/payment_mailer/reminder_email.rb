module Workers
  module PaymentMailer
    class ReminderEmail
      extend Workers::Logger

      @queue = :mailer

      class << self
        def perform(payment_id,billing_date)
          payment = Payment.find(payment_id)
          ::PaymentMailer.subscription_reminder(payment, billing_date).deliver
          logger.info "Send reminder of subscription to: #{payment.user.email}"
        end
      end
    end
  end
end
