module Workers
  class PaymentJob
    extend Workers::Logger
    
    @queue = :payments
    
    class << self
      def perform(order_id, method_name)
        logger.info "Process order #{order_id} (payment method: #{method_name})"
        order   = Order.find(order_id)
        payment = order.payment if order
        payment.send(method_name) if payment && payment.respond_to?(method_name)
      end
    end
  end
end
