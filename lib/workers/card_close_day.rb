module Workers
  class CardCloseDay
    extend Workers::Logger
    
    @queue = :payments
    
    class << self
      def perform
        logger.info "Close financial day in RSB"
        close_day || logger.error("Error closing financial day in RSB for not recurrent merchant")
        close_day(auto_renewing: true) || logger.error("Error closing financial day in RSB for recurrent merchant")
      end

      def close_day(order_data = {})
        Order.by_card.new(data: order_data).payment.close_day
      end
    end
  end
end
