module Workers
  class GoogleVerifyJob
    extend Workers::Logger
    
    @queue = :payments
    
    class << self
      def perform(subscription_id, token, session_id, client_ip)
        logger.info "Process google verify job subscription_id: #{subscription_id}, token: #{token} for session_id: #{session_id}, client_ip: #{client_ip}"
        if order = PaymentProcess::Google.verify(subscription_id, token, client_ip)
          #откроем доступ:
          session = Session.find(session_id)
          unless order.access_rights.find { |access_right| access_right.reader == session }
            order.payment.add_access_right(session)
          end

          if session.user
           # попробуем привязать подписку к пользователю
           Shark::Payments::LinkSubscription.new(session, session.user).call
          end
        end
      end
    end
  end
end
