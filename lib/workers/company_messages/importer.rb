module Workers
  module CompanyMessages
    class Importer
      extend Workers::Logger

      @queue = :news_import
      
      class << self
        def perform(company_id)
          company = ::PressReleases::Company.find(company_id)

          imported_cnt = 0
          connection.get(company.messages_feed).body.rss.channel.item.reverse.take(limit).each do |item|
            next if company.messages.find { |n| n.original_url == item.link }
            company.add_message_by_item(item)
            imported_cnt += 1
          end
          logger.info("Imported #{imported_cnt} messages for company '#{company.title}'") if imported_cnt > 0

        rescue NoMethodError => e
          logger.error("Can't parse feed '#{company.messages_feed}' for company '#{company.title}'. Error: #{e.message}")
        end

        def connection
          @connection ||= Faraday.new do |conn|
            conn.response :mashify
            conn.response :xml
            conn.adapter Faraday.default_adapter
          end
        end

        def limit
          Settings.companies.import_limit.to_i
        end
      end
    end
  end
end
