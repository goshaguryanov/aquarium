module Workers
  module CompanyMessages
    class FindCompaniesFeeds
      
      @queue = :news_import
      
      class << self
        def perform
          ::PressReleases::Company.with_message_feeds.each do |c|
            Resque.enqueue(Workers::CompanyMessages::Importer, c.id)
          end
        end
      end
    end
  end
end
