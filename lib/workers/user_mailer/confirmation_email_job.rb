module Workers
  module UserMailer
    class ConfirmationEmailJob
      extend Workers::Logger
      
      @queue = :mailer
      
      class << self
        def perform(token_id)
          token = UserToken.find(token_id)
          ::UserMailer.confirmation_email(token).deliver
          logger.info "Confirmation email: token=#{token.token} user=#{token.user_id} email=#{token.user.email}"
        end
      end
    end
  end
end
