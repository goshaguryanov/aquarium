module Workers
  class FeedbackSendJob
    extend Workers::Logger
    
    @queue = :mailer
    
    class << self
      def perform(feedback_id)
        feedback = Feedback.find(feedback_id)
        
        if feedback.created?
          if FeedbackMailer.feedback_email(feedback_id).deliver
            feedback.update(status: :sent)
            logger.info "Feedback #{feedback_id} sent :)"
          else
            logger.info "Feedback #{feedback_id} failed :("
          end
        else
          logger.info "Feedback #{feedback_id} has been sent already. Please repeat manually."
        end
      end
    end
  end
end
