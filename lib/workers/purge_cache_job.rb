module Workers
  class PurgeCacheJob
    extend Workers::Logger
    
    @queue = :purge_cache
    
    class << self
      def perform(access_token)
        Shark::Cache::PurgeUserProfile.call(access_token)
        logger.info "PurgeCache.call(#{access_token}) enqueued"
      end
    end
  end
end
