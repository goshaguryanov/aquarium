module Workers
  module Logger
    def logger
      @logger ||= Resque.logger
    end
  end
end
