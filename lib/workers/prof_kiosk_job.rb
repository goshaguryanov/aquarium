require 'net/ftp'

module Workers
  class ProfKioskJob
    extend Workers::Logger

    @queue = :flasher

    def self.perform(document_id)
      if document_id.nil?
        logger.info "Connect to Snipe..."
        snipe_client = SnipeClient.new
        logger.info "Getting newspaper from Snipe"
        newspapers = snipe_client.get("/v1/sandboxes/#{Settings.snipe.newspaper_sandbox_id}/documents").body
        document_id = newspapers.first[:id]
      end
      logger.info "Exporting document(#{document_id}) to profkiosk.ru"
      ProKioskProcessor.new(document_id).process
    end
  end
end
