module Workers
  class AppleVerifyJob
    extend Workers::Logger
    
    @queue = :payments
    
    class << self
      def perform(receipt, session_id, client_ip)
        logger.info "Process apple verify job for session_id: #{session_id}, client_ip: #{client_ip}"
        if order = PaymentProcess::Apple.verify(receipt, client_ip)
          # откроем доступ:
          session = Session.find(session_id)
          unless order.access_rights.find { |ar| ar.reader == session }
            order.payment.add_access_right(session)
          end

          if session.user
           # попробуем привязать подписку к пользователю
           Shark::Payments::LinkSubscription.new(session, session.user).call
          end
        end
      end
    end
  end
end
