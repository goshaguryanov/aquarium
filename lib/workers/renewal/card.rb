module Workers
  module Renewal
    class Card
      extend Workers::Logger
      
      @queue = :payments
      
      class << self
        def perform(order_id)
          logger.info "Renewal card order: #{order_id}"
          Common::CardRenewal.new(Order.by_card.find(order_id)).call
        end
      end
    end
  end
end
