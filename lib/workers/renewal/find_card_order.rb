module Workers
  module Renewal
    class FindCardOrder
      extend Workers::Logger
      
      @queue = :payments
      
      class << self
        def perform
          logger.info "Find orders for card renewal"
          Order.by_card.paid.where(end_date: (Time.now ... 72.hour.from_now)).each do |order|
            if order.payment.can_renewal?
              Resque.enqueue(Workers::Renewal::Card, order.id)
            elsif order.payment.order_can_renewal? && order.payment.have_next_subscription?
              # у пользователя оказалась другая подписка
              order.data[:renewal_failed] = true
              order.data_will_change!
              order.save
              Snapshot.create!(
                subject: order,
                event:   'workers_card_renewal_failed',
                data:    order.attributes
              )
              # InnerMailer.renewal_failed(order, 'У пользователя уже есть оформленная подписка').deliver
            end
          end
        end
      end
    end
  end
end
