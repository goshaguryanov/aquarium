module Workers
  module Renewal
    class FindAppleOrder
      extend Workers::Logger
      
      @queue = :payments
      
      class << self
        def perform
          logger.info "Find orders for apple renewal"
          Order.by_apple.paid.where(end_date: (Time.now .. 23.hour.from_now)).each do |order|
            if order.payment.can_renewal?
              Resque.enqueue(Workers::Renewal::Apple, order.id)
            end
          end
        end
      end
    end
  end
end
