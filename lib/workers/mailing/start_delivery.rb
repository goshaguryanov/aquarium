module Workers
  module Mailing
    class StartDelivery
      extend Workers::Logger
      
      @queue = :mailer
      
      def self.perform
        ::Mailing.ready.contacts_ready.where('scheduled_at < NOW()').each do |mailing|
          if mailing.perform!
            logger.info "Started mailing #{mailing.id} '#{mailing.subject}'"
          else
            logger.error "Fail to start mailing #{mailing.id} '#{mailing.subject}'"
          end
        end
      end
    end
  end
end
