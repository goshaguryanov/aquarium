module Workers
  module Mailing
    class AttachContacts
      extend Workers::Logger
      
      @queue = :mailer
      
      class << self
        def perform(mailing_id, conditions)
          use_case = Ibis::Mailing::AttachContacts.new(mailing_id, conditions.map(&:symbolize_keys))
          if use_case.call
            logger.info "Attach #{use_case.mailing.letters.count} contacts to mailing #{mailing_id}"
          else
            logger.error "Fail to attach contacts for mailing #{mailing_id}"
          end
        end
      end
    end
  end
end
