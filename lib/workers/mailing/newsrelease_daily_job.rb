module Workers
  module Mailing
    class NewsreleaseDailyJob
      extend Workers::Logger

      @queue = :mailer

      class << self
        def perform
          begin
            if generate
              logger.info 'Mailing started'
            else
              logger.warn 'Mailing start failed'
            end
          rescue Roompen::NotFound => e
            logger.warn("Can't find newsrelease: #{e}")
          end
        end

        def generate
          newsrelease_message      = ::MailingGenerators::Newsrelease.generate
          newsrelease_free_message = ::MailingGenerators::Newsrelease.generate(base_template: 'newsrelease_free')

          mailing = ::Mailing.create(
              type_id:         mail_type.id,
              template:        newsrelease_message,
              subject:         "Свежий номер «Ведомостей» за #{Russian::strftime(Time.now, "%e %B").strip}",
              contacts_status: :contacts_linking
          )

          mailing_free = ::Mailing.create(
              type_id:         mail_type.id,
              template:        newsrelease_free_message,
              subject:         "Свежий номер «Ведомостей» за #{Russian::strftime(Time.now, "%e %B").strip}",
              contacts_status: :contacts_linking
          )

          logger.info 'Begin creating mails'
          ActiveRecord::Base.connection.execute("INSERT INTO mailing_letters (contact_id, mailing_id, created_at, updated_at)
          SELECT
            contacts.id, case when count(access_rights.id) > 0 then #{mailing.id} else #{mailing_free.id} end, now(), now()
          FROM contacts
            LEFT JOIN users ON users.id = contacts.user_id
            LEFT JOIN access_rights ON access_rights.reader_type = 'User'
                                       AND access_rights.reader_id = users.id
                                       AND NOW() BETWEEN access_rights.start_date AND access_rights.end_date
          WHERE contacts.\"contact_status\" = #{Contact.contact_statuses[:confirmed]} AND (type_ids @> ARRAY [#{mail_type.id}])
          GROUP BY contacts.id")
          logger.info 'Finished creating mails'
          send_mailing(mailing)
          send_mailing(mailing_free)

          logger.info "Daily newsrelease mailing was generated, mailing_id = #{mailing.id}, mailing_free_id = #{mailing_free.id}"

          mailing && mailing_free
        end

        def mail_type
          @mail_type ||= ::Mailing::MailType.find_by(slug: 'newsrelease')
        end

        def send_mailing(mailing)
          mailing.scheduled_at = Time.now
          mailing.contacts_ready!
          mailing.ready!
          mailing.snapshots.create(
              event: 'mailing generated',
              data:  mailing.attributes
          )
        end
      end
    end
  end
end
