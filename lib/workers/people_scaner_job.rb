module Workers
  class PeopleScanerJob
    extend Workers::Logger

    @queue = :people_scan

    class << self
      def perform
        logger.info "PeopleScaner started"
        cnt = People::Scaner.new.scan_and_upload!
        logger.info "PeopleScaner created #{cnt} people"
      end
    end
  end
end