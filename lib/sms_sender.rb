class SmsSender

  def initialize(msisdn, operator, message)
    @msisdn, @operator, @message = msisdn, operator, message
  end

  def send!
    martlet_client.send(@msisdn, @operator, @message).success?
  end

  private

  def martlet_client
    @martlet_client ||= MartletClient.new
  end
end
