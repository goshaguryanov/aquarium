class Scribble::Request

  def initialize(api_additive)
    @api_additive = api_additive
  end

  def get(path, options = {})
    uri = URI(request_url(path))
    options['Format'] ||= 'json'
    uri.query = URI.encode_www_form(options.merge('Token' => Scribble.config.token))
    req = Net::HTTP::Get.new(uri)
    yield(req) if block_given?
    attempts = 0
    response = begin
      Net::HTTP.start(uri.hostname, uri.port) { |http|
        http.request(req)
      }
    rescue TimeoutError => timeout_exception
      attempts += 1
      retry if attempts < 4
      raise "Can't GET to #{path} because of TimeoutError: #{timeout_exception}"
    end
    raise Scribble::Unauthorized if response.code == '401'
    response
  end

  def request_url(path)
    "#{Scribble.config.host.chomp('/')}/#{@api_additive}/#{path}"
  end
end