class Scribble::User

  def initialize
    @request = Request.new('user')
  end

  def authenticate
    @request.get('user') { |req| req.basic_auth Scribble.config.username, Scribble.config.password }
  end
end