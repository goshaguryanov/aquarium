class Scribble::Broadcasting
  SCRIBBLE_VIDEO_DESCRIPTION_REGEXP = /([^(<br>)]*)(<br>)?(.*)/m
  DEFAULT_VIDEO_TITLE = 'Ведомости'

  attr_reader :current_last_update_time

  def initialize(scribble_document)
    @posts =    []
    @edits =    []
    @deletes =  []
    @document = scribble_document
    @boxes =    scribble_document.root_box.children || []
  end

  def last_page?(page_size)
    ((@body['Posts'] || []) + (@body['Edits'] || []) + (@body['Deletes'] || [])).length < page_size
  end

  def store_next_scribble_posts_chunk(options)
    parse_scribble_response(Scribble.event.posts(@document.scribble_id, options))
  end

  def process_and_upload
    begin
      unless current_last_update_time.to_s == @document.scribble_check_time.to_s
        process_and_upload_posts
        document_attributes = {scribble_check_time: current_last_update_time }
        snipe_client.patch("/v1/documents/#{@document.id}", attributes: document_attributes)
        snipe_client.patch("/v1/documents/#{@document.id}/publish", attributes: { published_at: Time.now })
      end
      true
    rescue Exception => e
      ::Resque.logger.error("Error processing ScribbleLive broadcasting.\nDocument ID: #{@document.id}.\nError:#{e}")
      return false
    end
  end

  private

  def parse_scribble_response(response)
    @body =                     JSON.parse(response.body)
    @posts +=                   @body['Posts'] || []
    @edits +=                   @body['Edits'] || []
    @deletes +=                 @body['Deletes'] || []
    @current_last_update_time = scribble_date_to_time((@posts + @edits + @deletes).max_by { |post| post['LastModified'] }['LastModified']) rescue nil
  end

  def position_offset
    @offset ||= @boxes.min_by(&:position).position.to_i rescue 0
  end

  def process_and_upload_posts
    # Собираем измененные и новые посты в один массив и упорядочиваем по дате создания (чтобы выводить в правильном порядке)
    # Необходимо, чтобы обрабатывать ситуацию, когда с момента последнего обновления был добавлен и отредактирован пост
    # В этом случае он приходит в Edits, в Posts его не будет.
    (@posts + @edits).sort { |x, y| x['Created'] <=> y['Created'] }.each_with_index do |post, index|
      if post_exists?(post['Id'])
        box_updated_at = Time.parse(find_box(scribble_post_box_associations[post['Id']]).updated_at).strftime('%FT%T.%L%:z')
        snipe_client.patch("v1/boxes/#{scribble_post_box_associations[post['Id']]}",
                           attributes: post_to_box_data(post).except(:parent_id, :scribble_post_id).merge(updated_at: box_updated_at))
      else
        next if post['IsDeleted'].to_s == '1'
        snipe_client.post('v1/boxes', attributes: post_to_box_data(post).merge(position: -1000 * (index + 1) + position_offset))
      end
    end
    @deletes.each do |post|
      snipe_client.delete("v1/boxes/#{scribble_post_box_associations[post['Id']]}") if post_exists?(post['Id'])
    end
  end

  def find_box(id)
    @boxes.each { |box| return box if box.id == id }
  end

  def post_to_box_data(post)
    data = {
        scribble_post_id:     post['Id'],
        scribble_post_author: post['Creator'].try(:[], 'Name'),
        parent_id:            @document.root_box.id,
        created_at:           scribble_date_to_time(post['Created']).strftime('%FT%T%:z')
    }
    case post['Type']
      when 'IMAGE'
        data[:type] = 'inset_image'
        data[:description] = post['Content']
        data[:image_id] = create_attachment_image(post['Media'].first['Url'])
        data[:author] = ''
        data[:credits] = ''
      when 'TEXT'
        data[:type] = 'paragraph'
        data[:body] = post['Content']
      when 'HTML', 'EMBED'
        data[:type] = 'inset_media'
        data[:body] = post['Content']
      when 'VIDEO'
        begin
          post['Content'].to_s =~ SCRIBBLE_VIDEO_DESCRIPTION_REGEXP
          title, description = ($1 || DEFAULT_VIDEO_TITLE).to_s, $3.to_s
          dulton_video = upload_video_to_dulton(title, post['Media'].first['Url'])
          data[:type] = 'dulton_media'
          data[:title] = title
          data[:description] = description
          data[:dulton_id] = dulton_video.data.record.id
        rescue Exception => e
          puts e.backtrace.join("\n")
          data[:type] = 'inset_media'
          data[:body] = post['Content']
        end
      else
        data[:type] = 'inset_media'
        data[:body] = post['Content']
    end
    data
  end

  def scribble_date_to_time(scribble_date)
    Time.at(scribble_date[/\d+/].to_i / 1000)
  end

  def scribble_post_box_associations
    unless @associations
      @associations = {}
      @boxes.each { |box| @associations[box.scribble_post_id] = box.id }
    end
    @associations
  end

  def post_exists?(id)
    scribble_post_box_associations.key?(id)
  end

  def upload_video_to_dulton(title, url)
    DultonMedia.client.upload_video(title, url)
  end

  def create_attachment_image(url)
    attributes = upload_image_to_agami(url)
    attachment = snipe_client.post('/v1/attachments', attributes: attributes).body
    attachment.id
  end

  def upload_image_to_agami(url)
    unless @converts
      @converts = @document.settings.attachments.inset_image.versions.map {|k, v| "\"#{k}\":\"#{v.width}x#{v.height}^\""}
      @converts.push('"_preview":"110x151^"')
      @converts = @converts.join(',')
    end
    AgamiUploader.upload_from_url(url, "converts={#{@converts}}").body
  end

  def snipe_client
    @snipe_client ||= SnipeClient.new
  end
end