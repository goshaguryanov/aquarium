source 'https://rubygems.org'

gem 'rails', '~> 4.1.6'
gem 'rails-api'
gem 'rake', '< 11.0'

gem 'pg'
gem 'redis'

gem 'bcrypt'
gem 'oj'

gem 'procto'

gem 'ruby-ip', require: 'ip'

gem 'slim'

gem 'awesome_pry'

gem 'settings', github: 'roomink/settings'
gem 'roompen', git: 'git@github.com:vedomosti/roompen.git'

gem 'unicorn', group: :production
gem 'rack-cors'

gem 'kaminari'

# Templates
gem 'active_model_serializers'

group :development, :test do
  gem 'thin'
  gem 'rspec-rails'
  gem 'guard-rspec'
  gem 'terminal-notifier'
  gem 'terminal-notifier-guard'
  gem 'active_record-annotate'
  gem 'foreman'
  gem 'byebug'
end

group :test do
  gem 'resque_spec'
  gem 'faker'
end

gem 'faraday', '~> 0.9.0'
gem 'faraday_middleware', '~> 0.9.1'

gem 'resque', '~> 1.25.2'
gem 'resque-lock-timeout'
gem 'resque-scheduler'

gem 'elasticsearch'

gem 'newrelic_rpm'
gem 'rbtrace'

gem 'rubyzip'
gem 'zip-zip' # will load compatibility for old rubyzip API.
gem 'multi_xml'
gem 'faraday-digestauth'

gem 'google-api-client'

gem 'koala', '~> 1.11.0rc'

gem 'htmlentities', '~> 4.3.3'
gem 'em-synchrony'
gem 'em-http-request'

gem 'eeepub'
gem 'savon'
gem 'xml-simple', require: 'xmlsimple'
gem 'russian'

gem 'yandex_mystem'
gem 'petrovich'
gem 'ruby-smpp', require: 'smpp'
gem 'iconv'

gem 'ipaddress'
gem 'write_xlsx'
# Генерация ОТП-кодов для воостановления подписки моб. юзеров
gem 'rotp'
# Шифрование ОТП-токенов
gem 'aescrypt'

gem 'addressable'

gem 'gelf_ext', git: 'git@github.com:vedomosti/gelf_ext.git'
