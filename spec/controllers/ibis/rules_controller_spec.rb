RSpec.describe Ibis::Mailing::RulesController, :type => :controller do
  let(:attributes) { { code: Faker::Lorem.sentence, status: %w(allow block).sample } }
  let(:rule) { Mailing::Rule.create!(attributes) }

  before :each do
    allow_any_instance_of(SnipeClient).to receive_message_chain(:get, :body) { {editor_id: 1} }
  end

  it '#current' do
    rule_id = rule.id
    get :current
    expect(assigns(:rules).count).to eql(1)
    expect(assigns(:rules).first.id).to eql(rule_id)
  end

  it '#create' do
    post :create, attributes: attributes
    expect(response.status).to eql(200)
    expect(assigns(:rule).code).to eq(attributes[:code])
    expect(assigns(:rule).status).to eq(attributes[:status])
    post :create, attributes: attributes
    expect(response.status).to eq(422)
  end

  it '#update' do
    patch :update, id: rule.id, attributes: attributes.merge(status: :allow)
    expect(response.status).to eql(200)
    expect(assigns(:rule).status).to eq('allow')
    patch :update, id: rule.id, attributes: attributes.merge(status: :block)
    expect(assigns(:rule).status).to eq('block')
  end
end
