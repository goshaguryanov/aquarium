RSpec.describe Pigeon::ContactsController, :type => :controller do
  let (:user1) { User.create!(email: 'mail1@domain.local', nickname: 'test1') }
  let (:user2) { User.create!(email: 'mail2@domain.local', nickname: 'test2') }
  let (:mail_type) { Mailing::MailType.create(slug: 'test', title: 'Тестовая рассылка') }
  let (:mailing) { Mailing.create!(type: mail_type, subject: 'Test') }
  let (:contact1) { Contact.create!(user: user1, email: user1.email, subscription_status: 'on') }
  let (:contact2) { Contact.create!(user: user2, email: user2.email, subscription_status: 'on') }
  let! (:letter1) { Mailing::Letter.create!(contact: contact1, mailing: mailing) }
  let! (:letter2) { Mailing::Letter.create!(contact: contact2, mailing: mailing) }
  context '#index' do
    it 'first contact' do
      get :index, mailing_id: mailing.id, offset: 0, limit: 1
      expect(assigns(:contacts)).to include(contact1)
    end
    it 'second contact' do
      get :index, mailing_id: mailing.id, offset: 1, limit: 1
      expect(assigns(:contacts)).to include(contact2)
    end
  end
end
