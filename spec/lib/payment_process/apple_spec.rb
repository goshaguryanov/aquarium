RSpec.describe PaymentProcess::Apple do
  let(:start_date) { 1.month.until }
  let(:end_date) { Time.now }
  let(:data) do
    { receipt: 'test' }
  end
  let(:order) { Order.by_apple.paid.create!(start_date: start_date, end_date: end_date, data: data) }

  subject { order.payment }

  describe '#can_renewal?' do
    context 'with standart order' do
      it 'returns true' do
        expect(subject.can_renewal?).to be_truthy
      end
    end

    context 'with order without receipt' do
      let(:data) do
        {}
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end

    context 'with failed renewal' do
      let(:data) do
        { receipt: 'test', renewal_failed: true }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end

    context 'with already renewed order' do
      let(:data) do
        { receipt: 'test', next_order_id: 123 }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end
  end
end
