RSpec.describe PaymentProcess::Google do
  let(:start_date) { 1.month.until }
  let(:end_date) { Time.now }
  let(:data) do
    { subscription_id: 'test', token: 'test', auto_renewing: true }
  end
  let(:order) { Order.by_google.paid.create!(start_date: start_date, end_date: end_date, data: data) }

  subject { order.payment }

  describe '#can_renewal?' do
    context 'with recurrent order' do
      it 'returns true' do
        expect(subject.can_renewal?).to be_truthy
      end
    end

    context 'with not recurrent order' do
      let(:data) do
        { subscription_id: 'test', token: 'test', auto_renewing: false }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end

    context 'with order without token' do
      let(:data) do
        { subscription_id: 'test', auto_renewing: true }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end

    context 'when renewal failed' do
      let(:data) do
        { subscription_id: 'test', token: 'test', auto_renewing: true, renewal_failed: true }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end

    context 'with already renewed order' do
      let(:data) do
        { subscription_id: 'test', token: 'test', auto_renewing: true, next_order_id: 123 }
      end

      it 'returns false' do
        expect(subject.can_renewal?).to be_falsey
      end
    end
  end
end
