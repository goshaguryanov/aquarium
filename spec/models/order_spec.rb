RSpec.describe Order do
  describe '#call' do
    context 'with payment_method = by_card' do
      subject { described_class.by_card.new }
      
      it 'has right payment instance' do
        expect(subject.payment).to be_a(PaymentProcess::Card)
      end
    end

    context 'with correct parameters' do
      subject { described_class.by_card.new }

      it 'valid' do
        expect(subject).to be_valid
      end
    end

    context 'with incorrect parameters' do
      subject { described_class.new }

      it 'not valid' do
        expect(subject).not_to be_valid
      end
    end
  end
end
