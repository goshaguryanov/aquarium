RSpec.describe LegalEntity, type: :model do
  let(:ips)    { %w(0.0.0.0 127.0.0.1 192.168.1.0/24) }
  let(:entity) { LegalEntity.create(name: 'roomink', ips: ips) }
  
  describe '#dump_sessions_to_redis' do
    let(:user) { User.create(email: 'i@zaur.io') }
    
    before(:each) do
      allow(entity).to receive(:users).and_return([user])
    end
    
    it 'calls #dump_sessions_to_redis on each user' do
      expect(user).to receive(:dump_sessions_to_redis)
      entity.dump_sessions_to_redis
    end
    
    context 'with access' do
      before(:each) do
        allow(entity).to receive(:subscriber?).and_return(true)
      end
      
      it 'dumps ips and masks' do
        entity.dump_sessions_to_redis
        
        ips = Aquarium::Redis.connection.smembers('ips')
        masks = Aquarium::Redis.connection.smembers('ip_masks')
        
        expect(ips).to include('0.0.0.0')
        expect(ips).to include('127.0.0.1')
        expect(ips.count).to eq(2)
        
        expect(masks).to include('192.168.1.0/24')
        expect(masks.count).to eq(1)
      end
    end
  end
  
  describe '.remove_ips_from_redis' do
    it 'removes the specified ips from redis' do
      described_class.add_ips_to_redis(entity.ips)
      described_class.remove_ips_from_redis(%w(127.0.0.1 192.168.1.0/24))
      
      ips = Aquarium::Redis.connection.smembers('ips')
      masks = Aquarium::Redis.connection.smembers('ip_masks')
      
      expect(ips).to include('0.0.0.0')
      expect(ips.count).to eq(1)
      expect(masks).to be_empty
    end
  end
  
  describe '#subscriber?' do
    context 'without access' do
      it 'returns false' do
        expect(entity).not_to be_subscriber
      end
    end
    
    context 'with access' do
      before(:each) do
        entity.access_rights.create(start_date: 1.day.ago, end_date: 1.month.from_now)
      end
      
      it 'returns true' do
        expect(entity).to be_subscriber
      end
    end
  end
end
