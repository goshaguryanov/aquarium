RSpec.describe Session, type: :model do
  def redis
    Aquarium::Redis.connection
  end
  
  describe '.generate_token' do
    it 'generates a unique token' do
      tokens = 500.times.map { Session.generate_token }
      
      expect(tokens.count).to eq(tokens.uniq.count)
    end
  end
  
  describe '#authenticated_status' do
    subject { Session.new(ip: '0.0.0.0') }
    
    before(:each) do
      subject.access_token = Session.generate_token
    end
    
    context 'on session with active access_rights' do
      let(:access_rights) { double('AccessRight', active: [true]) }
      
      before(:each) do
        allow(subject).to receive(:access_rights).and_return(access_rights)
      end
        
      it 'returns "subscriber"' do
        expect(subject.subscription_status).to eq('subscriber')
      end
    end
    
    context 'on session of guest user' do
      before(:each) do
        allow(subject).to receive(:user_id).and_return(nil)
      end
        
      it 'returns "not_subscriber"' do
        expect(subject.subscription_status).to eq('not_subscriber')
      end
    end
    
    context 'on session of user with active access_rights' do
      let(:user) { double('User', has_active_access_rights?: true) }
      
      before(:each) do
        allow(subject).to receive(:user).and_return(user)
        allow(subject).to receive(:user_id).and_return(1)
      end
        
      it 'returns "subscriber"' do
        expect(subject.subscription_status).to eq('subscriber')
      end
    end
    
    context 'on session of registered user without active access_rights' do
      let(:user) { double('User', has_active_access_rights?: false, registered?: true) }
      
      before(:each) do
        allow(subject).to receive(:user).and_return(user)
        allow(subject).to receive(:user_id).and_return(1)
      end
        
      it 'returns "registered"' do
        expect(subject.auth_state).to eq('registered')
      end
    end
    
    context 'on session of confirmed user without active access_rights' do
      let(:user) { double('User', has_active_access_rights?: false, registered?: false) }
      
      before(:each) do
        allow(subject).to receive(:user).and_return(user)
        allow(subject).to receive(:user_id).and_return(1)
      end
        
      it 'returns "authenticated"' do
        expect(subject.auth_state).to eq('authenticated')
      end
    end
  end
  
  describe '#dump_to_redis' do
    let(:promo_ends_on) { 1.week.ago.strftime('%Y.%m.%d') }
    
    before(:each) do
      allow(Settings.promo).to receive(:ends_on).and_return(promo_ends_on)
    end
    
    subject { Session.new(ip: '0.0.0.0') }
    
    before(:each) do
      subject.access_token = Session.generate_token
    end
    
    context 'on a signed in session' do
      let(:user) { double('User', has_active_access_rights?: false, registered?: false) }
      
      before(:each) do
        allow(subject).to receive(:user).and_return(user)
        
        allow(subject).to receive(:user_id).and_return(1)
        allow(subject).to receive(:account_id).and_return(2)
      end
      
      it 'dumps all fields to redis' do
        subject.dump_to_redis
        
        expect(redis.hgetall("sessions:#{subject.access_token}").symbolize_keys).to eq(
          status:         'not_subscriber',
          state:          'authenticated',
          ip:             subject.ip.to_s,
          promo_accepted: 'false',
          user_id:        '1',
          account_id:     '2'
        )
      end
    end
    
    context 'on a signed out session' do
      before(:each) do
        redis.hset("sessions:#{subject.access_token}", :user_id, 1)
        redis.hset("sessions:#{subject.access_token}", :account_id, 2)
      end
      
      it 'removes the user_id & account_id keys from the session' do
        subject.dump_to_redis
        
        expect(redis.hexists("sessions:#{subject.access_token}", :user_id)).to be_falsy
        expect(redis.hexists("sessions:#{subject.access_token}", :account_id)).to be_falsy
      end
    end
    
    context 'without active promo action' do
      before(:each) do
        redis.set("sessions:#{subject.access_token}", 'abc')
      end
      
      it 'writes the user data' do
        subject.dump_to_redis
        
        data = redis.hgetall("sessions:#{subject.access_token}")
        expect(data.symbolize_keys).to eq(
          status:         'not_subscriber',
          state:          'guest',
          ip:             subject.ip.to_s,
          promo_accepted: 'false'
        )
      end
    end
    
    context 'with active promo action' do
      let(:promo_ends_on) { 1.week.from_now.strftime('%Y.%m.%d') }
      let(:user) { double('User') }
      
      before(:each) do
        allow(subject).to receive(:user).and_return(user)
      end
      
      context 'accepted by user' do
        before(:each) do
          allow(user).to receive(:accepted_promo).and_return(Settings.promo.slug => Time.now.to_i)
        end
        
        it 'writes the user data along with promo information' do
          subject.dump_to_redis
          
          data = redis.hgetall("sessions:#{subject.access_token}")
          expect(data.symbolize_keys).to eq(
            status:         'not_subscriber',
            state:          'guest',
            ip:             subject.ip.to_s,
            promo_accepted: 'true'
          )
        end
      end
      
      context 'not accepted by user' do
        before(:each) do
          allow(user).to receive(:accepted_promo).and_return({})
        end
        
        it 'writes the user data along with promo information' do
          subject.dump_to_redis
          
          data = redis.hgetall("sessions:#{subject.access_token}")
          expect(data.symbolize_keys).to eq(
            status:         'not_subscriber',
            ip:             subject.ip.to_s,
            promo_accepted: 'false'
          )
        end
      end
    end
  end
end
