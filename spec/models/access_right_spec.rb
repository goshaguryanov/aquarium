RSpec.describe AccessRight, type: :model do
  let (:user) { User.create!(email:'test@test.com', first_name: 'Vasya', last_name: 'Pupkin') }
  describe '.active' do
    context 'with a fresh access right' do
      before(:each) do
        subject.reader = user
        subject.start_date = Time.now
        subject.end_date = 1.day.from_now
        subject.save
      end
      
      it 'must be active' do
        expect(described_class.active).to include(subject)
      end
    end
  end
end
