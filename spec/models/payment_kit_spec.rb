RSpec.describe PaymentKit do
  describe '#suitable_for?' do
    subject { described_class.new(user_conditions: {'registered' => 'true'} ).suitable_for?(user) }

    context 'with suitable conditions' do
      let (:user) { User.create(email: 'test@test.com') }
      it 'returns true' do
        expect(subject).to be_truthy
      end
    end

    context 'with unsuitable conditions' do
      let (:user) { User.new }
      it 'returns false' do
        expect(subject).to be_falsey
      end
    end
  end
end
