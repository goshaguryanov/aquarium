RSpec.describe User do
  let(:user) { User.create(email: 'test@example.com') }
  
  describe '#dump_sessions_to_redis' do
    before(:each) do
      user.sessions.create(access_token: 'abc', ip: '0.0.0.0')
      user.sessions.create(access_token: 'def', ip: '127.0.0.1')
    end
    
    it 'dumps all sessions to redis using their #dump_to_redis method' do
      user.sessions.each do |session|
        expect(session).to receive(:dump_to_redis)
      end
      
      user.dump_sessions_to_redis
    end
    
    it 'ignores the email case' do
      expect {
        User.create!(email: 'Test@example.com')
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
  
  describe '#destroy' do
    it 'does not delete the record from the DB, only changes status' do
      user.destroy
      
      expect(user).to be_persisted
      expect(user).to be_deleted
    end
    
    it 'writes crap to email' do
      real_email = user.email
      user.destroy
      
      expect(user.email).to match(/deleted\+[a-z0-9]{32}\+#{real_email}/)
    end
  end

  describe '#is_online_subscriber_now?' do
    it "returns false when user hasn't any access_right" do
      expect(user.is_online_subscriber_now?).to be_falsey
    end

    it "returns false when user hasn't active access_right" do
      Common::GrantAccess.new(reader: user, start_date: 1.year.ago, end_date: 1.month.ago).call
      expect(user.is_online_subscriber_now?).to be_falsey
    end

    it "returns true when user has active access_right" do
      Common::GrantAccess.new(reader: user, start_date: 1.month.ago, end_date: 1.year.from_now).call
      expect(user.is_online_subscriber_now?).to be_truthy
    end
  end

  describe '#is_online_subscriber_last_year?' do
    it "returns false when user hasn't any access_right" do
      expect(user.is_online_subscriber_last_year?).to be_falsey
    end

    it "returns false when user hasn't fresh access_right" do
      Common::GrantAccess.new(reader: user, start_date: 2.years.ago, end_date: 13.months.ago).call
      expect(user.is_online_subscriber_last_year?).to be_falsey
    end

    it "returns true when user has fresh access_right" do
      Common::GrantAccess.new(reader: user, start_date: 1.year.ago, end_date: 11.months.from_now).call
      expect(user.is_online_subscriber_last_year?).to be_truthy
    end

    it "returns true when user has active access_right" do
      Common::GrantAccess.new(reader: user, start_date: 1.month.ago, end_date: 1.year.from_now).call
      expect(user.is_online_subscriber_last_year?).to be_truthy
    end
  end

  describe '#is_subscriber_of_namespace?' do
    let (:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
    let (:subproduct_online) { Subproduct.create!(product_id: product.id, slug: 'online', title: 'online') }
    let (:subproduct_paper) { Subproduct.create!(product_id: product.id, slug: 'paper', title: 'paper') }
    let (:namespace1) { Namespace.create!(product_id: product.id, slug: 'namespace1', title: '1', start_date: 1.month.ago, end_date: 1.month.ago) }
    let (:namespace2) { Namespace.create!(product_id: product.id, slug: 'namespace2', title: '2', start_date: 1.month.ago, end_date: 1.month.ago) }
    let (:period1) { Period.create!(slug: '1months', duration: 1.month, title: '1 month') }
    let (:period2) { Period.create!(slug: '2months', duration: 2.month, title: '2 month') }
    let (:paymethod) { Paymethod.create!(slug: 'card', title: 'card') }
    let (:exist_payment_kit) do
      PaymentKit.create!(
        product_id:    product.id,
        namespace_id:  exist_namespace.id,
        subproduct_id: exist_subproduct.id,
        period_id:     period1.id,
        price:         100,
        paymethod_ids: [ paymethod.id ]
      )
    end
    let (:payment_kit) do
      PaymentKit.create!(
        product_id:    product.id,
        namespace_id:  namespace.id,
        subproduct_id: subproduct.id,
        period_id:     period2.id,
        price:         100,
        paymethod_ids: [ paymethod.id ]
      )
    end

    context "user hasn't inactive any payments" do
      let (:namespace) { namespace1 }
      let (:subproduct) { subproduct_online }

      it "returns false" do
        expect(user.is_subscriber_of_namespace?(payment_kit)).to be_falsey
      end
    end

    context 'user has inactive payment with same namespace and product' do
      let (:exist_namespace) { namespace1 }
      let (:exist_subproduct) { subproduct_online }
      let (:namespace) { exist_namespace }
      let (:subproduct) { exist_subproduct }

      before (:each) do
        uc = Payments::CreatePayment.new( { payment_kit_id: exist_payment_kit, paymethod_id: paymethod.id, start_date: 2.month.ago, end_date: 1.month.ago }, user )
        uc.call
        uc.payment.update(status: 'paid')
      end

      it "returns false" do
        expect(user.is_subscriber_of_namespace?(payment_kit)).to be_falsey
      end
    end

    context 'user has active payment with another namespace' do
      let (:exist_namespace) { namespace1 }
      let (:exist_subproduct) { subproduct_online }
      let (:namespace) { namespace2 }
      let (:subproduct) { exist_subproduct }

      before (:each) do
        uc = Payments::CreatePayment.new( { payment_kit_id: exist_payment_kit, paymethod_id: paymethod.id, start_date: 1.month.ago, end_date: 1.month.from_now }, user )
        uc.call
        uc.payment.update(status: 'paid')
      end

      it "returns false" do
        expect(user.is_subscriber_of_namespace?(payment_kit)).to be_falsey
      end
    end

    context 'user has active payment with same namespace and product' do
      let (:exist_namespace) { namespace1 }
      let (:exist_subproduct) { subproduct_online }
      let (:namespace) { exist_namespace }
      let (:subproduct) { exist_subproduct }

      before (:each) do
        uc = Payments::CreatePayment.new( { payment_kit_id: exist_payment_kit, paymethod_id: paymethod.id, start_date: 1.month.ago, end_date: 1.month.from_now }, user )
        uc.call
        uc.payment.update(status: 'paid')
      end
      it "returns true" do
        expect(user.is_subscriber_of_namespace?(payment_kit)).to be_truthy
      end
    end

    context 'user has unpaid payment with same namespace and product' do
      let (:exist_namespace) { namespace1 }
      let (:exist_subproduct) { subproduct_online }
      let (:namespace) { exist_namespace }
      let (:subproduct) { exist_subproduct }

      before (:each) do
        uc = Payments::CreatePayment.new( { payment_kit_id: exist_payment_kit, paymethod_id: paymethod.id, start_date: 1.month.ago, end_date: 1.month.from_now }, user )
        uc.call
      end

      it "returns false" do
        expect(user.is_subscriber_of_namespace?(payment_kit)).to be_falsey
      end
    end
  end

  describe '#create' do
    it 'will generate guid' do
      user = described_class.create!(email: 'test@example.com')
      expect(user.reload.data['guid']).to_not be_nil
    end
  end
end
