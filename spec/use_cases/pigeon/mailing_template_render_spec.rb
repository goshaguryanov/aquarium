RSpec.describe Pigeon::MailingTemplateRender do
  before(:each) do
    @banner2 = Mailing::Banner.create!(title:'banner_title2', body:'banner_text2', url:'banner_url2', position: 2, type_ids: [issue.id], start_date: 1.month.ago, end_date: 1.month.from_now)
    @banner100 = Mailing::Banner.create!(title:'banner_title100', body:'banner_text100', url:'banner_url100', position: 100, type_ids: [issue.id], start_date: 1.month.ago, end_date: 1.month.from_now)
  end

  let (:issue) { Mailing::MailType.create(slug:'newsrelease', title:'newsrelease') }
  let (:mailing) { Mailing.create(type_id: issue.id, template: '<!--banner:2--><a href="link" --><!--banner:100--><!--banner:1-->') }

  subject { described_class.new(mailing) }
  
  describe '#template_with_banners' do
    it 'include banners' do
      expect(subject.template_with_banners).to include('banner_title2')
      expect(subject.template_with_banners).to include('banner_text2')
      expect(subject.template_with_banners).to include('banner_url2')

      expect(subject.template_with_banners).to include('banner_title100')
      expect(subject.template_with_banners).to include('banner_text100')
      expect(subject.template_with_banners).to include('banner_url100')
    end

    it 'not include unexisten banner' do
      expect(subject.template_with_banners).not_to include('<!--banner:1-->')
    end

    it 'include banners statistics' do
      expect(subject.template_with_banners).to include(%Q|<img src="#{Settings.hosts.rigel}/pixel?event=mail_banner_hit&tags=%5B%22contact_id:{{data_escape "id"}}%22%2C%22mailing_id:{{mailing_escape "id"}}%22%2C%22banner_id:#{@banner2.id}%22%2C%22position:2%22%5D"|)
      expect(subject.template_with_banners).to include(%Q|<img src="#{Settings.hosts.rigel}/pixel?event=mail_banner_hit&tags=%5B%22contact_id:{{data_escape "id"}}%22%2C%22mailing_id:{{mailing_escape "id"}}%22%2C%22banner_id:#{@banner100.id}%22%2C%22position:100%22%5D"|)

      expect(subject.template_with_banners).to include(%Q|href="#{Settings.hosts.rigel}/redirect?event=mail_banner_click&url=banner_url2&tags=%5B%22contact_id:{{data_escape "id"}}%22%2C%22mailing_id:{{mailing_escape "id"}}%22%2C%22banner_id:#{@banner2.id}%22%2C%22position:2%22%5D"|)
      expect(subject.template_with_banners).to include(%Q|href="#{Settings.hosts.rigel}/redirect?event=mail_banner_click&url=banner_url100&tags=%5B%22contact_id:{{data_escape "id"}}%22%2C%22mailing_id:{{mailing_escape "id"}}%22%2C%22banner_id:#{@banner100.id}%22%2C%22position:100%22%5D"|)
    end

    it 'include click statistics' do
      expect(subject.template_with_banners).to include(%Q|href="#{Settings.hosts.rigel}/redirect?event=mail_click&url=link&tags=%5B%22contact_id:{{data_escape "id"}}%22%2C%22mailing_id:{{mailing_escape "id"}}%22%2C%22order:1%22%5D"|)
    end
  end
end
