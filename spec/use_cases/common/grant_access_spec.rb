RSpec.describe Common::GrantAccess do
  let(:attributes) do
    {
      reader:     User.create(email: 'user@example.com'),
      order:      Order.create(payment_method: :by_card),
      start_date: Time.now,
      end_date:   1.month.from_now
    }
  end
  
  subject { described_class.new(attributes) }
  
  it 'grants access to the specified reader' do
    expect {
      subject.call
    }.to change { AccessRight.count }.by(1)
    
    expect(AccessRight.last.reader).to eq(attributes[:reader])
  end
  
  it 'creates a snapshot' do
    expect {
      subject.call
    }.to change { Snapshot.count }.by(1)
    
    snapshot = subject.access_right.snapshots.last
    expect(snapshot.event).to eq('access_granted')
  end
  
  it 'confirms users' do
    expect(subject.access_right).to receive(:confirm_users)
    subject.call
  end
  
  it 'dumps data to redis' do
    expect(subject.access_right).to receive(:dump_to_redis)
    subject.call
  end
end
