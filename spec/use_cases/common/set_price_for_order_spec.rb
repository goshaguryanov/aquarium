RSpec.describe Common::SetPriceForOrder do
  let(:order) { Order.paid.by_unknown.create! }
  
  subject { described_class.new(order, price_params) }

  before(:each) do
    Price.online.for_month.create!(student: true, value: 1, since: Time.now, expire: 1.month.from_now)
    Price.online.for_month.create!(student: false, value: 2, since: Time.now, expire: 1.month.from_now)
    Price.online.for_month.create!(student: true, value: 3, since: Time.now, expire: Time.now)
    Price.online.for_month.create!(student: false, value: 4, since: Time.now, expire: Time.now)
  end
  
  context 'with price_id' do
    let(:price_params) do
      { price_id: Price.find_by_value(4) }
    end

    it 'set price' do
      subject.call
      expect(order.price.value).to eq(4)
    end
  end

  context 'with parameters for nonexistent price' do
    let(:price_params) do
      { product: 'online', period: 'for_6months', student: false }
    end

    it 'don\'t set price' do
      subject.call
      expect(order.price).to be_nil
    end

    it 'log error' do
      expect(PaymentProcess.logger).to receive(:error)
      subject.call
    end
  end

  context 'with parameters with student:true' do
    let(:price_params) do
      { product: 'online', period: 'for_month', student: true }
    end

    it 'set price for a student' do
      subject.call
      expect(order.price.value).to eq(1)
    end
  end
  
  context 'with missing student flag' do
    let(:price_params) do
      { product: 'online', period: 'for_month' }
    end

    it 'set price for not a student' do
      subject.call
      expect(order.price.value).to eq(2)
    end
  end

  context 'with student flag as a string "true"' do
    let(:price_params) do
      { product: 'online', period: 'for_month', student: 'true' }
    end

    it 'set price for a student' do
      subject.call
      expect(order.price.value).to eq(1)
    end
  end

  context 'with student flag as a string "F"' do
    let(:price_params) do
      { product: 'online', period: 'for_month', student: 'F' }
    end

    it 'set price for not a student' do
      subject.call
      expect(order.price.value).to eq(2)
    end
  end
end
