RSpec.describe Common::UpdateLegalEntity do
  let(:first_user)  { User.create(email: '7@7vn.io') }
  let(:second_user) { User.create(email: 'lesha@kurepin.com') }
  
  let(:entity) do
    Common::CreateLegalEntity.new(
      name:     'Roomink',
      ips:      %w(123.123.123.0/24 0.0.0.0 192.168.1.0/24),
      user_ids: [first_user.id]
    ).tap(&:call).legal_entity
  end
  
  let(:attributes) do
    {
      name:     'Vedomosti',
      ips:      %w(192.168.1.0/24),
      user_ids: [second_user.id]
    }
  end
  
  subject { described_class.new(entity.id, attributes) }
  
  it 'updates the entity' do
    expect {
      subject.call
    }.to change { entity.reload.slice(*attributes.keys) }.to(attributes)
  end
  
  it 'dumps sessions to redis' do
    expect(subject.legal_entity).to receive(:dump_sessions_to_redis)
    subject.call
  end
  
  it 'creates a snapshot' do
    subject
    
    expect {
      subject.call
    }.to change { Snapshot.count }.by(1)
    
    snapshot = subject.legal_entity.snapshots.last
    expect(snapshot.event).to eq('legal_entity_updated')
  end
  
  context 'with access' do
    before(:each) do
      allow(subject.legal_entity).to receive(:subscriber?).and_return(true)
    end
    
    it 'removes old ips from redis' do
      expect(LegalEntity).to receive(:remove_ips_from_redis).with(%w(123.123.123.0/24 0.0.0.0))
      subject.call
    end
  end
end
