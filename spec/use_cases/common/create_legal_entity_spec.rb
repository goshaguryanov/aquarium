RSpec.describe Common::CreateLegalEntity do
  let(:user) { User.create(email: '7@7vn.io') }
  
  let(:attributes) do
    {
      name:     'Roomink',
      ips:      %w(123.123.123.0/24),
      user_ids: [user.id]
    }
  end
  
  subject { described_class.new(attributes) }
  
  it 'creates a new legal entity' do
    expect {
      subject.call
    }.to change { LegalEntity.count }.by(1)
    
    expect(subject.legal_entity).to be_persisted
    expect(subject.legal_entity.name).to eq(attributes[:name])
    expect(subject.legal_entity.ips).to eq(attributes[:ips])
    expect(subject.legal_entity.user_ids).to eq(attributes[:user_ids])
  end
  
  it 'creates a snapshot' do
    expect {
      subject.call
    }.to change { Snapshot.count }.by(1)
    
    snapshot = subject.legal_entity.snapshots.last
    expect(snapshot.event).to eq('legal_entity_created')
  end
end
