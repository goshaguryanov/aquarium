RSpec.describe Common::CreateOrder do
  let (:params) do
    {
      payment_method: :by_card,
      product:        'online',
      period:         'for_month',
      student:        false
    }
  end
  let(:user) { User.create(email: 'test@test.com') }
  let(:client_ip) { '127.0.0.1' }

  subject { described_class.new(params, user, client_ip) }

  before(:each) do
    [
      [ :online, :for_month, 1 ],
      [ :profi,  :for_month, 2 ],
      [ :paper,  :for_month, 3 ],
    ].each do |p|
      Price.create!(
        product: p[0],
        period:  p[1],
        value:   p[2],
        since:   Time.at(0)
      )
    end
  end
  
  it 'creates new order' do
    expect {
      subject.call
    }.to change { Order.count }.by(1)
  end

  it 'creates new access_right' do
    expect {
      subject.call
    }.to change { AccessRight.count }.by(1)

    expect(subject.order.access_rights.last.reader).to eq(user)
    expect(subject.order.access_rights.last.start_date).to be_nil
    expect(subject.order.access_rights.last.end_date).to be_nil
  end

  it 'creates 2 new snapshots' do
    expect {
      subject.call
    }.to change { Snapshot.count }.by(2)
  end
  
  context 'when online order' do
    it 'correct order dates' do
      allow(Time).to receive(:now).and_return(Time.parse('2000-01-01 13:00:00'))
      subject.call
      expect(subject.order.start_date).to eq(Time.parse('2000-01-01 13:00:00'))
      expect(subject.order.end_date).to eq(Time.parse('2000-01-31').end_of_day)
    end

    it 'correct order date with already exists active accessright' do
      allow(Time).to receive(:now).and_return(Time.new(1999))

      AccessRight.create!(
        reader: user,
        start_date: Time.parse('2000-01-01 13:00:00'),
        end_date: Time.parse('2000-01-02 23:59:59'),
      )

      subject.call
      expect(subject.order.start_date).to eq(Time.parse('2000-01-03'))
      expect(subject.order.end_date).to eq(Time.parse('2000-02-02').end_of_day)
    end

  end

  context 'when paper order' do
    let (:params) do
      {
        payment_method: :by_card,
        product:        'paper',
        period:         'for_month',
        student:        false
      }
    end

    it 'correct order date with already exists active paper order' do
      allow(Time).to receive(:now).and_return(Time.new(1999))

      old_order = Common::CreateOrder.new({
        start_date:     Time.parse('2000-01-01 13:00:00'),
        end_date:       Time.parse('2000-01-02 23:59:59'),
        payment_method: :by_card,
        product:        'paper',
        period:         'for_month',
        student:        false
      }, user, client_ip).call

      old_order.payment_status = Order.payment_statuses[:paid]
      old_order.save!

      # binding.pry

      subject.call
      expect(subject.order.start_date).to eq(Time.parse('2000-01-03'))
      expect(subject.order.end_date).to eq(Time.parse('2000-02-02').end_of_day)
    end

  end
end
