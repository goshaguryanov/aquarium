RSpec.describe Shark::UpdateProfile do
  let(:user) do
    User.create(
      email: 'lesha@kurepin.com',
      first_name: 'A',
      last_name:  'K'
    )
  end
  
  let(:params) do
    { first_name: 'Vasya', last_name: 'Pupkin' }
  end
  
  subject { described_class.new(user, params) }
  
  describe '#call' do
    it 'updates the user profile' do
      subject.call
      
      expect(User.find(user.id).first_name).to eq(params[:first_name])
      expect(User.find(user.id).last_name).to  eq(params[:last_name])
    end
    
    it 'creates a snapshot' do
      expect {
        subject.call
      }.to change { Snapshot.count }.by(1)
      
      snapshot = subject.user.snapshots.last
      expect(snapshot.event).to eq('shark.profile_updated')
    end
  end
end
