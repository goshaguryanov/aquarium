RSpec.describe Shark::SubscribeConfirm do
  let (:params) { ActionController::Parameters.new(token: token) }
  subject { described_class.new(params) }

  context 'with valid token' do
    before(:each) do
      @mail_type = Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )
      create_use_case = Common::CreateContact.new({email: 'test@test.com'}, ['issue'])
      create_use_case.call
      @contact = create_use_case.contact
      @token = create_use_case.token.token
    end

    let (:token) { @token }

    it 'valid' do
      expect(subject.valid?).to eq(true)
    end
    
    it 'returns truthy value' do
      expect(subject.call).to be_truthy
    end

    it 'updates contact status' do
      expect { subject.call }.to change { @contact.reload.contact_status }.from('registered').to('confirmed')
    end

    it 'updates subscription status' do
      expect { subject.call }.to change { @contact.reload.subscription_status }.from('off').to('on')
    end

    it 'creates snapshot' do
      expect { subject.call }.to change { Snapshot.count }.by(1)
    end

    it 'adds type_ids to contact' do
      expect { subject.call }.to change { @contact.reload.type_ids }.from([]).to([@mail_type.id])
    end
  end

  context 'with invalid token' do
    let (:token) { 'invalid' }

    it 'not valid' do
      expect(subject.valid?).to eq(false)
    end
    
    it 'returns falsey value' do
      expect(subject.call).to be_falsey
    end
  end

end
