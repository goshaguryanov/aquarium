RSpec.describe Shark::Confirm do
  let(:user) { User.create!(email:'pupkin@test.com') }
  let(:token) { user.tokens.confirmation.generate }
  let(:params) { ActionController::Parameters.new(token: token.token) }
  
  subject { described_class.new(params) }
  
  describe '#call' do
    context 'by a user without mailing' do
      it 'changes user status' do
        expect {
          subject.call
        }.to change { user.reload.status }.from('registered').to('confirmed')
      end

      it 'creates 1 snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
      end

      it 'mark token as used' do
        expect {
          subject.call
        }.to change { token.reload.used }.from(false).to(true)
      end
    end

    context 'by a user with mailing subscription' do
      let(:mail_type) { Mailing::MailType.create!(slug: 'newsrelease', title: 'newsrelease') }
      let(:token) { user.tokens.confirmation.generate(mailing_type_ids: [mail_type.id]) }

      it 'changes user status' do
        expect {
          subject.call
        }.to change { user.reload.status }.from('registered').to('confirmed')
      end

      it 'creates 3 snapshots' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(3)
      end

      it 'mark token as used' do
        expect {
          subject.call
        }.to change { token.reload.used }.from(false).to(true)
      end

      it 'create contact' do
        expect {
          subject.call
        }.to change { Contact.count }.by(1)
      end

      it 'subscribe contact to mailing' do
        subject.call
        expect(user.contact.contact_status).to eq('confirmed')
        expect(user.contact.subscription_status).to eq('on')
        expect(user.contact.type_ids).to eq([mail_type.id])
      end      
    end
    
    # context 'by a user without password account' do
    #   before(:each) do
    #     @user = Shark::SignUpWithOauth.new(
    #       first_name:   'Pavel',
    #       last_name:    'Durov',
    #       email:        'test@example.com',
    #       provider:     'vkontakte',
    #       external_id:  1,
    #       access_token: session.access_token
    #     ).tap(&:save).user
    #   end
      
    #   it 'creates a password account with the suplied password' do
    #     expect {
    #       subject.call
    #     }.to change { @user.accounts.count }.by(1)
    #   end
    # end
  end
end
