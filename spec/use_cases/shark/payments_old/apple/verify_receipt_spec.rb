RSpec.describe Shark::PaymentsOld::Apple::VerifyReceipt do
  let(:session) do
    use_case = Shark::IssueSession.new(ip: '127.0.0.1')
    use_case.call
    use_case.session
  end
  
  let(:purchase_info) do
    Base64.strict_encode64(%Q!{
      "original-purchase-date-pst" = "2014-12-29 21:49:43 America/Los_Angeles";
      "purchase-date-ms" = "1419918581000";
      "unique-identifier" = "22e81475882ff77bd962c99db3bcbb526adcfea5";
      "original-transaction-id" = "360000100388876";
      "expires-date" = "1421128181000";
      "app-item-id" = "521051076";
      "transaction-id" = "360000100388876";
      "quantity" = "1";
      "original-purchase-date-ms" = "1419918583000";
      "unique-vendor-identifier" = "00AEA206-9AEB-47EF-8274-E86C92FA202C";
      "web-order-line-item-id" = "360000006359232";
      "item-id" = "789629373";
      "expires-date-formatted" = "2015-01-13 05:49:41 Etc/GMT";
      "purchase-date" = "2014-12-30 05:49:41 Etc/GMT";
      "original-purchase-date" = "2014-12-30 05:49:43 Etc/GMT";
      "product-id" = "com.vedomosti.inapp.2014_1_maccess";
      "expires-date-formatted-pst" = "2015-01-12 21:49:41 America/Los_Angeles";
      "is-trial-period" = "true";
      "purchase-date-pst" = "2014-12-29 21:49:41 America/Los_Angeles";
      "bid" = "bid";
      "bvrs" = "3.2.1";
    }!)
  end
  
  let(:receipt) do
    Base64.strict_encode64(%Q!{
      "signature" = "8iN4rY5iGNaTUrE==";
      "purchase-info" = "#{purchase_info.strip}";
      "pod" = "22";
      "signing-status" = "0";
    }!)
  end
  
  subject { described_class.new(receipt, session, '0.0.0.0') }
  
  describe '#call' do
    before do
      ResqueSpec.reset!
    end
    
    context 'with correct receipt' do
      it 'adds task to queue' do
        res = subject.call
        expect(Workers::Payments::AppleVerify).to have_queued(receipt, session.id, '0.0.0.0')
      end
    end
  end
end
