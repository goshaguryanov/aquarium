RSpec.describe Shark::PaymentsOld::LinkSubscription do
  let(:order) do
    Order.by_apple.paid.create!(start_date: Time.now, end_date: 1.day.from_now)
  end
  
  let(:user) do
    User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com')
  end
  
  let(:session) do
    use_case = Shark::IssueSession.new(user: user, ip: '127.0.0.1')
    use_case.call
    session = use_case.session
    session
  end
  
  subject { described_class.new(session, user) }
  
  describe '#call' do
    before do
      Common::GrantAccess.new(order: order, reader: session, start_date: order.start_date, end_date: order.end_date).call
    end
    
    context 'with normal condition' do
      it 'creates new AccessRight' do
        expect { subject.call }.to change { AccessRight.count }.by(1)
      end
      
      it 'returns ok message' do
        res = subject.call
        expect(res[:status]).to eq('ok')
        expect(res).to include(:redirect)
      end
    end
    
    context 'when user already linked' do
      before :each do
        Common::GrantAccess.new(order: order, reader: user, start_date: order.start_date, end_date: order.end_date).call
      end
      
      it 'no creates new AccessRight' do
        expect { subject.call }.not_to change { AccessRight.count }
      end
      
      it 'returns ok message' do
        res = subject.call
        expect(res[:status]).to eq('ok')
        expect(res).to include(:redirect)
      end
    end
  end
end
