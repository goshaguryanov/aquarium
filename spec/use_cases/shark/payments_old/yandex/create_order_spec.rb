RSpec.describe Shark::PaymentsOld::Yandex::CreateOrder do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com') }
  let(:price) do
    Price.create!(
      product: :online,
      period:  :for_month,
      student: false,
      value:   1,
      since:   Time.now(),
      expire:  Time.now()+1.day
    )
  end
  subject { described_class.new({ price_id: price.id }, user, '0.0.0.0') }
  
  describe '#call' do
    it 'creates a Order' do
      expect { subject.call }.to change { Order.count }.by(1)
    end
    
    it 'creates a AccessRight' do
      expect { subject.call }.to change { AccessRight.count }.by(1)
    end
    
    it 'returns post response' do
      res = subject.call
      expect(res[:status]).to eq('ok')
      expect(res).to include(:post)
    end
  end
end
