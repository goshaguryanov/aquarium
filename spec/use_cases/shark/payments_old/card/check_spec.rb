RSpec.describe Shark::PaymentsOld::Card::Check do
  let(:user) { User.create!(first_name: 'Vasya', last_name: 'Pupkin', email: 'v@example.com') }
  let(:order) { Order.by_card.create() }
  subject { described_class.new(order.uuid) }
  
  describe '#call' do
    it 'returns wait response when data is empty' do
      res = subject.call
      
      expect(res[:status]).to eq('ok')
      expect(res).to include(:wait)
    end
    
    it 'returns data when it exists' do
      test_response = 'test'
      order.data[:response_for_client] = test_response
      order.data_will_change!
      order.save
      expect(subject.call).to eq test_response
    end
  end
end
