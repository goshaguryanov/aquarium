RSpec.describe Shark::UnsubscribeConfirm do
  let (:contact) { Contact.create(email: 'test@test.com') }
  subject { described_class.new(token) }

  context 'with valid token and have been subscribed' do
    before(:each) do
      @mail_type = Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )

      contact.update(type_ids: [@mail_type.id])
    end

    let (:token) { contact.unsubscribe_token('issue') }

    it 'returns truthy value' do
      expect(subject.call).to be_truthy
    end

    it 'updates type_ids' do
      expect { subject.call }.to change { contact.reload.type_ids }.from([@mail_type.id]).to([])
    end

    it 'creates snapshot' do
      expect { subject.call }.to change { Snapshot.count }.by(1)
    end
  end

  context "with valid token and have not been subscribed" do
    before(:each) do
      Mailing::MailType.create!(
        slug:  'issue',
        title: 'Выпуск'
      )

      contact.update(type_ids: [])
    end

    let (:token) { contact.unsubscribe_token('issue') }

    it 'returns truthy value' do
      expect(subject.call).to be_truthy
    end

    it "don't update type_ids" do
      expect { subject.call }.not_to change { contact.reload.type_ids }
    end

    it "don't create any snapshots" do
      expect { subject.call }.not_to change { Snapshot.count }
    end
  end
end
