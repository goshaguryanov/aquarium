RSpec.describe Shark::ChangePassword do
  let(:session) { Shark::IssueSession.new(ip: '8.8.8.8').tap(&:call).session }

  subject { described_class.new(@user, 'confidential') }

  describe '#call' do
    context 'by a user with a password account' do
      before(:each) do
        @user = Shark::SignUpWithPassword.new(
            {
                first_name:   'Vasya',
                last_name:    'Pupkin',
                email:        'test@example.com',
                password:     'secret',
                access_token: session.access_token
            },
            '127.0.0.1'
        ).tap(&:save).user
      end

      it 'changes_the password' do
        expect {
          subject.call
        }.to change { @user.accounts.password.first.password_digest }
      end
    end

    context 'by a user without password account' do
      before(:each) do
        @user = Shark::SignUpWithOauth.new(
            {
                first_name:   'Pavel',
                last_name:    'Durov',
                email:        'test@example.com',
                provider:     'vkontakte',
                external_id:  1,
                access_token: session.access_token
            },
            '127.0.0.1'
        ).tap(&:save).user
      end

      it 'creates a password account with the suplied password' do
        expect {
          subject.call
        }.to change { @user.accounts.count }.by(1)
      end
    end
  end
end
