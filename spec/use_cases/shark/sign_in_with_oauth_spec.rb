RSpec.describe Shark::SignInWithOauth do
  let(:user) { User.create!(email: 'mail@example.com') }

  before(:each) do
    user.accounts.facebook.create!(external_id: 123)
    @session = Shark::IssueSession.new(ip: '8.8.8.8').tap(&:call).session
  end

  describe '#valid?' do
    context 'with correct parameters' do
      subject do
        described_class.new(
            {
                provider:     'facebook',
                external_id:  '123',
                access_token: @session.access_token
            },
            '127.0.0.1'
        )
      end

      it 'returns true and authenticates the user' do
        expect(subject).to be_valid
        subject.call
        expect(Session.find_by(access_token: @session.access_token).user_id).to eq(user.id)
      end
    end

    context 'with incorrect parameters' do
      subject do
        described_class.new(
            {
                provider:     'facebook',
                external_id:  '321',
                access_token: @session.access_token
            },
            '127.0.0.1'
        )
      end

      it 'returns false and does not authenticate the user' do
        expect(subject).not_to be_valid
        expect(Session.where(access_token: @session.access_token)).not_to exist
      end
    end

    context 'with an old_external_id parameter' do
      context 'with a user having only old account' do
        subject do
          described_class.new(
              {
                  provider:        'facebook',
                  external_id:     '12345',
                  old_external_id: '123',
                  access_token:    @session.access_token
              },
              '127.0.0.1'
          )
        end

        it 'updates the old account with new external_id' do
          expect {
            subject.valid?
          }.to change { user.accounts.first.external_id }.from('123').to('12345')
        end

        it 'creates a snapshot for the changed account' do
          expect {
            subject.valid?
          }.to change { Snapshot.count }.by(2)

          expect(Snapshot.first.subject).to eq(user.accounts.first)
          expect(Snapshot.first.event).to eq('facebook_merge.before_external_id_update')

          expect(Snapshot.last.subject).to eq(user.accounts.first)
          expect(Snapshot.last.event).to eq('facebook_merge.after_external_id_update')
        end
      end

      context 'with a user having both accounts' do
        before(:each) do
          user.accounts.facebook.create!(external_id: 321)
        end

        subject do
          described_class.new(
              {
                  provider:        'facebook',
                  external_id:     '123',
                  old_external_id: '321',
                  access_token:    @session.access_token
              },
              '127.0.0.1'
          )
        end

        it 'destroys the old account' do
          expect {
            subject.valid?
          }.to change { user.accounts(true).count }.by(-1)
        end

        it 'creates a snapshot for a destroyed account' do
          expect {
            subject.valid?
          }.to change { Snapshot.count }.by(1)

          expect(Snapshot.last.subject).to be_nil
          expect(Snapshot.last.data['external_id']).to eq('321')
        end
      end
    end
  end

  after(:each) do
    Session.destroy_all
  end
end
