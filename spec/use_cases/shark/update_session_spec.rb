RSpec.describe Shark::UpdateSession do
  let(:user) { User.create(email: 'test@example.com') }
  
  let(:session) do
    Session.new(ip: '8.8.8.8').tap do |session|
      session.access_token = Session.generate_token
      session.dump_to_redis
    end
  end
  
  let(:params) do
    { user_id: user.id, account_id: 2 }
  end
  
  subject { described_class.new(session.access_token, '127.0.0.1') }
  
  before(:each) do
    allow(Shark::Cache::PurgeUserProfile).to receive(:call)
  end
  
  describe '#call' do
    it 'updates the session' do
      subject.call(params)
      expect(Session.find_by(access_token: session.access_token).user_id).to eq(params[:user_id])
      expect(Session.find_by(access_token: session.access_token).account_id).to eq(params[:account_id])
    end
    
    it 'dumps the session into redis' do
      expect(subject.session).to receive(:dump_to_redis)
      subject.call(params)
    end
    
    it 'set "purges the cache" job to resque' do
      subject.call(params)
      expect(Workers::PurgeCacheJob).to have_queued(subject.session.access_token)
    end
    
    context 'with an already persisted session' do
      before(:each) do
        session.save
      end
      
      it 'updates the session in postgres' do
        subject.call(params)
        expect(session.reload.account_id).to eq(2)
      end
    end
  end
end
