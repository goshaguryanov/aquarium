RSpec.describe Ibis::Payments::CreatePayment do
  let(:editor_id) { 1 }
  let(:user) {
    User.create!(email: 'test@test.com', first_name: 'Vasya', last_name: 'Pupkin')
  }
  let(:product) { Product.create!(slug: 'product', title: 'Product') }
  let(:namespace) { Namespace.create!(product: product, slug: 'namespace', title: 'Namespace', start_date: 1.month.ago, end_date: 1.month.from_now) }
  let(:subproduct) { Subproduct.create!(product: product, slug: 'subproduct', title: 'Subproduct') }
  let(:period) { Period.create!(slug: 'period', title: 'Period', duration: 1.month) }
  let(:payment_kit) { PaymentKit.last }

  subject { described_class.new(attributes, editor_id) }
  
  before(:each) do
    Paymethod.create!(slug: 'other', title: 'Other')

    payment_kit_params = {
      product_id:    product.id,
      namespace_id:  namespace.id,
      subproduct_id: subproduct.id,
      period_id:     period.id,
      price:         1
    }
    Ibis::Payments::CreatePaymentKit.new(payment_kit_params, editor_id).call
  end
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        {
          reader:            { reader_type: 'User', reader_id: user.id },
          payment_kit_id:    payment_kit.id,
          start_date:        Time.utc(1999,12,31,21).to_s,
          account:           'VIN-007',
          subscribe_base_id: 12345,
          manager_comment:   'test payment, arrrgh!'      
        }
      end
      
      it 'saves the payment' do
        expect {
          subject.call
        }.to change { Payment.count }.by(1)
        
        expect(Payment.last.account).to eq('VIN-007')
        expect(Payment.last.subscribe_base_id).to eq(12345)
        expect(Payment.last.start_date.to_s).to eq(Time.new(2000,1,1).to_s)
        expect(Payment.last.end_date.to_s).to eq(Time.new(2000,1,31).end_of_day.to_s)
        expect(Payment.last.price).to eq(payment_kit.price)
      end
      
      it 'creates 2 snapshots' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(2)
        
        snapshot = subject.payment.snapshots.last
        expect(snapshot.event).to eq('ibis.payment_created')
        expect(snapshot.author_id).to eq(editor_id)
      end
      
      it 'creates an access right' do
        expect {
          subject.call
        }.to change { AccessRight.count }.by(1)
        
        expect(subject.payment.access_rights[0].reader).to eq(user)
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        {
          payment_kit_id:  payment_kit.id,
          start_date:      Time.now.to_s,
          account:         'VIN-007',
          manager_comment: 'test payment, arrrgh!'
        }
      end
      
      it "doesn't saves the payment" do
        expect {
          subject.call
        }.not_to change { Payment.count }
      end
    end
  end
end
