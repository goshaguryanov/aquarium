RSpec.describe Ibis::Payments::DestroyCity do
  subject { described_class.new(city.id) }

  let(:city) { City.new(title: 'Moscow', capability: 'main', position: 1) }

  describe '#call' do
    before(:each) do
      city.save!
      city.snapshots.create!(event: 'first snapshot', data: city.attributes)
    end

    it 'delete city' do
      expect {
        subject.call
      }.to change { City.count }.by(-1)
    end

    it 'delete all snapshots' do
      expect {
        subject.call
      }.to change { Snapshot.count }.by(-1)
    end
  end
end
