RSpec.describe Ibis::Payments::SetBaseNamespace do
  subject { described_class.new(namespace.id, editor_id) }

  let(:product) { Product.create!(slug: 'product', title: 'Product') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'namespace', title: 'Namespace', start_date: 1.month.ago, end_date: 1.month.from_now) }
  let(:editor_id) { 1 }
  
  describe '#call' do
    before(:each) do
      Namespace.create!(product_id: product.id, slug: 'namespace_base', title: 'Base Namespace', start_date: 1.month.ago, end_date: 1.month.from_now, base: true)
    end

    context 'with valid attributes' do
      it 'set base flag' do
        expect {
          subject.call
        }.to change { namespace.reload; namespace.base }.from(false).to(true)
      end

      it 'remove base flag from other namespaces' do
        old_base = Namespace.base
        expect {
          subject.call
        }.to change { old_base.reload; old_base.base }.from(true).to(false)
      end

      it 'creates 2 snapshots' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(2)
      end
    end
  end
end
