RSpec.describe Ibis::Payments::CreatePaymentKit do
  subject { described_class.new(attributes, editor_id) }

  let(:product) { Product.create!(slug: 'product', title: 'Product') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'namespace', title: 'Namespace', start_date: 1.month.ago, end_date: 1.month.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'subproduct', title: 'Subproduct') }
  let(:period) { Period.create!(slug: 'period', title: 'Period', duration: 1.month) }
  let(:paymethod) { Paymethod.create!(slug: 'paymethod', title: 'Paymenthod') }
  let(:editor_id) { 1 }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        {
          autopay_timeout_hours: 72,
          syncable:      false,
          autoconfirm:   true,
          crossed_price: "0000",
          price:         "0000",
          texts:         {"title"=>"Название", "autopayment"=>"Текст для автоплатежа"},
          product_id:    product.id,
          namespace_id:  namespace.id,
          subproduct_id: subproduct.id,
          period_id:     period.id,
          paymethod_ids: [paymethod.id],
          alternate_ids: nil
        }
      end
      
      it 'saves the payment_kit' do
        expect {
          subject.call
        }.to change { PaymentKit.count }.by(1)
      end      
    end
  end
end
