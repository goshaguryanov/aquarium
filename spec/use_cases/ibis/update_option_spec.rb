RSpec.describe Ibis::UpdateOption do
  before(:each) do
    Ibis::CreateOption.call(type: 'auto', value: 'Volvo')
  end
  
  subject { described_class.new(Option.last.id, attributes) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { value: 'Subaru' }
      end
      
      it 'saves the option' do
        expect {
          subject.call
        }.to change { Option.last.value }.from('Volvo').to('Subaru')
      end
      
      it 'pushes the data to redis' do
        expect(Option).to receive(:save_type_to_redis).with('auto')
        subject.call
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { value: nil }
      end
      
      it "doesn't touch redis" do
        expect(Option).not_to receive(:save_type_to_redis)
        subject.call
      end
    end
  end
end
