RSpec.describe Ibis::CreateMailing do
  let(:editor_id) { 1 }
  let!(:mail_type) { Mailing::MailType.create!(slug: 'test', title: 'Test') }

  subject { described_class.new(attributes, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { type_id: mail_type.id, subject: 'Test', scheduled_at: '2016-02-01 03:02:01', preheader: 'preheader'}
      end
      
      it 'saves the mailing' do
        expect {
          subject.call
        }.to change { Mailing.count }.by(1)
        
        expect(Mailing.last.subject).to eq('Test')
        expect(Mailing.last.data['preheader']).to eq('preheader')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.mailing.snapshots.last
        expect(snapshot.event).to eq('ibis.mailing_created')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end

    context 'with invalid attributes' do
      let(:attributes) do
        {}
      end
      
      it "doesn't saves the mailing" do
        expect {
          subject.call
        }.not_to change { Mailing.count }
      end
    end
  end
end
