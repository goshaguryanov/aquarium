RSpec.describe Ibis::Reports::Create do
  let(:editor_id) { 1 }
  let(:params) do
    { start_date: Time.now.to_s, end_date: Time.now.to_s, kind: 'report' }
  end
  
  subject { described_class.new(params, editor_id) }
  
  describe '#call' do
    it 'create report' do
      expect {
        subject.call
      }.to change { Report.count }.by(1)
    end
    
    it 'create snapshot' do
      expect {
        subject.call
      }.to change { Snapshot.count }.by(1)
    end
  end
end
