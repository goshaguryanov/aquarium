RSpec.describe Ibis::NotReadyMailing do
  let(:editor_id) { 1 }
  let!(:mail_type) { Mailing::MailType.create!(slug: 'test', title: 'Test') }

  before (:each) do
    Ibis::CreateMailing.new({ type_id: mail_type.id, subject: 'Test mailing' }, editor_id).call
  end
    
  subject { described_class.new(Mailing.last.id, editor_id) }
  
  describe '#call' do
    context 'with ready mailing' do
      before(:each) do
        Mailing.last.update(status: 'ready')
      end
      
      it 'saves the mailing with created status' do
        expect {
          subject.call
        }.to change { Mailing.last.status }.from('ready').to('created')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.mailing.snapshots.last
        expect(snapshot.event).to eq('ibis.mailing_not_ready')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end

    context 'with started mailing' do
      before(:each) do
        Mailing.last.update(status: 'started')
      end
      
      it "didn't saves the mailing with created status" do
        expect {
          subject.call
        }.not_to change { Mailing.last.status }
      end

      it "returns false" do
        expect(subject.call).to be_falsey
      end

      it "return correct error" do
        subject.call
        expect(subject.errors).to eq([ "can't change status from 'started' to 'created'" ])
      end

      it "didn't creates a snapshot" do
        expect {
          subject.call
        }.not_to change { Snapshot.count }
      end
    end
  end
end
