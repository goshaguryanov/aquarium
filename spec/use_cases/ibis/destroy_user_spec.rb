RSpec.describe Ibis::DestroyUser do
  let(:editor_id) { 1 }
  
  before(:each) do
    Ibis::CreateUser.call({ email: 'vas@mail.com', nickname: 'vasvas' }, editor_id)
    account = user.accounts.create!(type: 'facebook')
    user.sessions.create!(account_id: account.id, access_token: Session.generate_token, ip: '127.0.0.1')
  end
  
  let(:user) { User.last }
  let(:session) { Session.last }
  
  before(:each) do
    allow(User).to receive(:find).with(user.id).and_return(user)
  end
  
  subject { described_class.new(user.id, editor_id) }
  
  describe '#call' do
    it 'deletes the user' do
      expect(user).to receive(:destroy)
      subject.call
    end
    
    it 'creates a snapshot for user' do
      expect {
        subject.call
      }.to change { Snapshot.where(subject_type: 'User').count }.by(1)
      
      snapshot = subject.user.snapshots.last
      expect(snapshot.event).to eq('ibis.user_destroyed')
      expect(snapshot.author_id).to eq(editor_id)
    end
    
    it 'creates a snapshot for session' do
      expect {
        subject.call
      }.to change { Snapshot.where(subject_type: 'Session').count }.by(1)
      
      snapshot = session.snapshots.last
      expect(snapshot.event).to eq('shark.destroyed_user_unauthenticated')
      expect(snapshot.author_id).to eq(editor_id)
    end
    
    it 'unauthorizes the user' do
      subject.call
      expect(session.user_id).to be_nil
      expect(session.account_id).to be_nil
    end
  end
end
