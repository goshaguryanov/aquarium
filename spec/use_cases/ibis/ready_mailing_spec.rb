RSpec.describe Ibis::ReadyMailing do
  let(:editor_id) { 1 }
  let!(:mail_type) { Mailing::MailType.create!(slug: 'test', title: 'Test') }

  before (:each) do
    Ibis::CreateMailing.new({ type_id: mail_type.id, subject: 'Test mailing' }, editor_id).call
  end
    
  subject { described_class.new(Mailing.last.id, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      it 'saves the mailing with ready status' do
        expect {
          subject.call
        }.to change { Mailing.last.status }.from('created').to('ready')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.mailing.snapshots.last
        expect(snapshot.event).to eq('ibis.mailing_ready')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end
  end
end
