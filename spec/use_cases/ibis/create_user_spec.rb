RSpec.describe Ibis::CreateUser do
  let(:editor_id) { 1 }
  subject { described_class.new(attributes, editor_id) }
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        { email: 'vas@mail.com', nickname: 'vasvas' }
      end
      
      it 'saves the user' do
        expect {
          subject.call
        }.to change { User.count }.by(1)
        
        expect(User.last.email).to eq('vas@mail.com')
      end
      
      it 'creates a snapshot' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(1)
        
        snapshot = subject.user.snapshots.last
        expect(snapshot.event).to eq('ibis.user_created')
        expect(snapshot.author_id).to eq(editor_id)
      end
    end
    
    context 'with invalid attributes' do
      let(:attributes) do
        { nickname: 'vasvas' }
      end
      
      it "doesn't saves the user" do
        expect {
          subject.call
        }.not_to change { User.count }
      end
    end
  end
end
