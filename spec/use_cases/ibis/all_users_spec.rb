RSpec.describe Ibis::AllUsers do
  let(:editor_id) { 1 }
  
  before(:each) do
    Ibis::CreateUser.call({ email: 'vas@mail.com', nickname: 'vasvas' }, editor_id)
  end
  
  describe '#call' do
    it 'returns all users' do
      expect(User.count).to eq(1)
    end
  end
end
