RSpec.describe Ibis::DestroyOption do
  before(:each) do
    Ibis::CreateOption.call(type: 'auto', value: 'Volvo')
  end
  
  subject { described_class.new(Option.last.id) }
  
  describe '#call' do
    it 'deletes the option' do
      expect {
        subject.call
      }.to change { Option.count }.by(-1)
    end
    
    it 'pushes the data to redis' do
      expect(Option).to receive(:save_type_to_redis).with('auto')
      subject.call
    end
  end
end
