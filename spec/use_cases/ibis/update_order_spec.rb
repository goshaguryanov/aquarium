RSpec.describe Ibis::UpdateOrder do
  let(:editor_id) { 1 }
  let(:user) {
    User.create!(email: 'test@test.com', first_name: 'Vasya', last_name: 'Pupkin')
  }
  let(:price_params) do
    {
      product: 'online',
      period:  'for_month'
    }
  end
  let(:create_attributes) do
    {
      reader:            create_reader,
      start_date:        Time.parse('2015-01-01 00:00:00').to_s,
      end_date:          Time.parse('2015-01-31 23:59:59').to_s,
      account:           'VIN-007',
      subscribe_base_id: 12345,
      manager_comment:   'test order, arrrgh!',
      manager_status:    'poor client'
    }.merge(price_params)
  end
  let(:create_reader) do
    {
      reader_type: 'User',
      reader_id: user.id
    }
  end
  let(:order) do
    Ibis::CreateOrder.new(create_attributes, editor_id).call
  end

  subject { described_class.new(order.id, attributes, editor_id) }
  
  before(:each) do
    Price.create!({ since: Time.at(0), value: 1 }.merge(price_params))
    order
  end
  
  describe '#call' do
    context 'with valid attributes' do
      let(:attributes) do
        {
          reader:            create_reader,
          start_date:        Time.parse('2000-01-01 00:00:00').to_s,
          end_date:          Time.parse('2000-01-31 23:59:59').to_s,
          account:           'VIN-008',
          subscribe_base_id: 12346,
          manager_comment:   'test update',
          manager_status:    'rich client'
        }
      end
      
      it 'changes order parameters' do
        subject.call
        order.reload
        expect(order.account).to eq('VIN-008')
        expect(order.subscribe_base_id).to eq(12346)
        expect(order.start_date.to_s).to eq(Time.parse('2000-01-01').beginning_of_day.to_s)
        expect(order.end_date.to_s).to eq(Time.parse('2000-01-31').end_of_day.to_s)
        expect(order.manager_comment).to eq('test update')
        expect(order.manager_status).to eq('rich client')
      end
      
      it 'creates 2 snapshots' do
        expect {
          subject.call
        }.to change { Snapshot.count }.by(2)
        
        snapshot = subject.order.snapshots.last
        expect(snapshot.event).to eq('ibis.order_updated')
        expect(snapshot.author_id).to eq(editor_id)
      end
      
      it 'changes an access right' do
        subject.call
        order.reload
        expect(order.access_rights[0].start_date.to_s).to eq(Time.parse('2000-01-01').beginning_of_day.to_s)
        expect(order.access_rights[0].end_date.to_s).to eq(Time.parse('2000-01-31').end_of_day.to_s)
      end
    end

    context 'cancel order' do
      let(:attributes) do
        {
          reader:            create_reader,
          payment_status:    'canceled',
          start_date:        Time.parse('2000-01-01 00:00:00').to_s,
          end_date:          Time.parse('2000-01-31 23:59:59').to_s,
          account:           'VIN-008',
          subscribe_base_id: 12346,
          manager_comment:   'test update',
          manager_status:    'rich client'
        }
      end
      
      it 'changes order parameters' do
        subject.call
        order.reload
        expect(order.payment_status).to eq('canceled')
      end

      it 'cancel access rights' do
        subject.call
        order.reload
        expect(order.access_rights[0].start_date).to be_nil
        expect(order.access_rights[0].end_date).to be_nil
      end

    end

    context 'with invalid reader' do
      let(:reader) do
        new_user = User.create!(email: 'test1@test.com', first_name: 'Wrong', last_name: 'User')
        {
          reader_type: 'User',
          reader_id: new_user.id
        }
      end
      let(:attributes) do
        {
          reader:            reader,
          start_date:        Time.parse('2000-01-01 00:00:00').to_s,
          end_date:          Time.parse('2000-01-31 23:59:59').to_s,
          account:           'VIN-008',
          subscribe_base_id: 12346,
          manager_comment:   'test update',
          manager_status:    'rich client'
        }
      end
      
      it "doesn't change the order" do
        subject.call
        order.reload
        expect(order.account).to eq('VIN-007')
        expect(order.subscribe_base_id).to eq(12345)
        expect(order.start_date.to_s).to eq(Time.parse('2015-01-01 00:00:00').to_s)
        expect(order.end_date.to_s).to eq(Time.parse('2015-01-31 23:59:59').end_of_day.to_s)
        expect(order.manager_comment).to eq('test order, arrrgh!')
        expect(order.manager_status).to eq('poor client')
      end

      it "doesn't create any snapshots" do
        expect {
          subject.call
        }.not_to change { Snapshot.count }
      end
    end
  end
end
