RSpec.describe Payments::CreatePayment do
  let(:params) do
    {
      paymethod: paymethod,
      payment_kit_path: [product.slug, namespace.slug, subproduct.slug, period.slug].join('.'),
    }
  end
  let(:user) { User.create!(email: 'test@test.com') }
  let(:client_ip) { '127.0.0.1' }
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'online', title: 'Онлайн') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'card', title: 'Банковская карта') }

  subject { described_class.new(params, user, client_ip) }

  before(:each) do
    pk = PaymentKit.create!(
      product:       product,
      namespace:     namespace,
      subproduct:    subproduct,
      period:        period,
      price:         100,
      paymethod_ids: [paymethod.id]
    )
    pk.renewal_id = pk.id
    pk.save!
  end
  
  it 'creates new payment' do
    expect {
      res = subject.call
    }.to change { Payment.count }.by(1)
  end

  it 'set payment renewal' do
    subject.call
    expect(subject.payment.renewal_id).to eq(subject.payment.payment_kit_id)
  end

  it 'creates new access_right' do
    expect {
      subject.call
    }.to change { AccessRight.count }.by(1)

    expect(subject.payment.access_rights.last.reader).to eq(user)
    expect(subject.payment.access_rights.last.start_date).to be_nil
    expect(subject.payment.access_rights.last.end_date).to be_nil
  end

  it 'creates 2 new snapshots' do
    expect {
      subject.call
    }.to change { Snapshot.count }.by(2)
  end
  
  context 'when online payment' do
    let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: Time.parse('1998-12-01'), end_date: Time.parse('2000-02-01')) }

    it 'correct payment dates' do
      allow(Time).to receive(:now).and_return(Time.parse('2000-01-01 13:00:00'))
      subject.call
      expect(subject.payment.start_date).to eq(Time.parse('2000-01-01 13:00:00'))
      expect(subject.payment.end_date).to eq(Time.parse('2000-01-31').end_of_day)
    end

    it 'correct payment date with already exists active accessright' do
      allow(Time).to receive(:now).and_return(Time.new(1999))

      AccessRight.create!(
        reader: user,
        start_date: Time.parse('2000-01-01 13:00:00'),
        end_date: Time.parse('2000-01-02 23:59:59'),
      )

      subject.call
      expect(subject.payment.start_date).to eq(Time.parse('2000-01-03'))
      expect(subject.payment.end_date).to eq(Time.parse('2000-02-02').end_of_day)
    end

  end

  context 'when paper payment' do
    let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'paper', title: 'Газета') }
    let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: Time.parse('1998-12-01'), end_date: Time.parse('2000-02-01')) }

    it 'correct payment date with already exists active paper payment' do
      allow(Time).to receive(:now).and_return(Time.new(1999))

      old_payment = Payments::CreatePayment.new({
        start_date:   Time.parse('2000-01-01 13:00:00'),
        end_date:     Time.parse('2000-01-02 23:59:59'),
        paymethod:    paymethod,
        payment_kit_path: [product.slug, namespace.slug, subproduct.slug, period.slug].join('.')
      }, user, client_ip).call

      old_payment.status = Payment.statuses[:paid]
      old_payment.save!

      subject.call
      expect(subject.payment.start_date).to eq(Time.parse('2000-01-03'))
      expect(subject.payment.end_date).to eq(Time.parse('2000-02-02').end_of_day)
    end

  end
end
