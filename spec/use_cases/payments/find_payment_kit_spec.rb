RSpec.describe Payments::FindPaymentKit do
  let(:product) { Product.create!(slug: 'subscription', title: 'Подписка') }
  let(:namespace) { Namespace.create!(product_id: product.id, slug: 'base', title: 'Базовые цены', start_date: 1.day.ago, end_date: 1.day.from_now) }
  let(:subproduct) { Subproduct.create!(product_id: product.id, slug: 'online', title: 'Онлайн') }
  let(:period) { Period.create!(slug: 'month', title: 'на 1 месяц', duration: 1.month) }
  let(:paymethod) { Paymethod.create(slug: 'card', title: 'Банковская карта') }

  subject { described_class.new(attributes) }

  before(:each) do
    pk = PaymentKit.create!(
      product:       product,
      namespace:     namespace,
      subproduct:    subproduct,
      period:        period,
      price:         100,
      paymethod_ids: [paymethod.id]
    )
    pk.renewal_id = pk.id
    pk.save!
  end
  
  context 'with valid attributes' do
    context 'find by id' do
      let(:attributes) do
        {
          payment_kit_id: PaymentKit.last.id
        }
      end

      it 'success' do
        expect(subject.call).to eq(PaymentKit.last)
      end
    end

    context 'find by slug_path' do
      let(:attributes) do
        {
          payment_kit_path: PaymentKit.last.slug_path
        }
      end

      it 'success' do
        expect(subject.call).to eq(PaymentKit.last)
      end
    end

    context 'find by product, namespace, subproduct, period' do
      let(:attributes) do
        {
          product:    product.slug,
          namespace:  namespace.slug,
          subproduct: subproduct.slug,
          period:     period.slug,
        }
      end

      it 'success' do
        expect(subject.call).to eq(PaymentKit.last)
      end
    end
  end

  context 'with invalid attributes' do
    context 'find by id' do
      let(:attributes) do
        {
          payment_kit_id: 666
        }
      end

      it 'fail' do
        expect(subject.call).to be_nil
      end
    end

    context 'find by slug_path' do
      let(:attributes) do
        {
          payment_kit_path: 'unknown_slug'
        }
      end

      it 'fail' do
        expect(subject.call).to be_nil
      end
    end

    context 'find by product, namespace, subproduct, period' do
      let(:attributes) do
        {
          product:    'unknown_product',
          namespace:  'unknown_namespace',
          subproduct: 'unknown_subproduct',
          period:     'unknown_period',
        }
      end

      it 'fail' do
        expect(subject.call).to be_nil
      end
    end    
  end
end
