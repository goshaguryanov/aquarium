--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: ltree; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;


--
-- Name: EXTENSION ltree; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION ltree IS 'data type for hierarchical tree-like structures';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE access_rights (
    id integer NOT NULL,
    order_id integer,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    reader_id integer NOT NULL,
    reader_type character varying(255) NOT NULL,
    payment_id integer
);


--
-- Name: access_rights_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE access_rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_rights_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE access_rights_id_seq OWNED BY access_rights.id;


--
-- Name: accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE accounts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    type character varying(255) NOT NULL,
    password_digest character varying(255),
    external_id character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accounts_id_seq OWNED BY accounts.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cities (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    capability integer NOT NULL,
    "position" integer NOT NULL,
    contact text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cities_id_seq OWNED BY cities.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contacts (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    user_id integer,
    contact_status integer DEFAULT 0 NOT NULL,
    subscription_status integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    mobile_user_id integer
);


--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;


--
-- Name: feedbacks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE feedbacks (
    id integer NOT NULL,
    access_token character varying(255) NOT NULL,
    user_id integer,
    username character varying(255) NOT NULL,
    company character varying(255),
    email character varying(255) NOT NULL,
    phone character varying(255),
    message text,
    price_id integer,
    callback boolean DEFAULT false NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type integer DEFAULT 0 NOT NULL,
    user_data json DEFAULT '{}'::json NOT NULL
);


--
-- Name: feedbacks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE feedbacks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: feedbacks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE feedbacks_id_seq OWNED BY feedbacks.id;


--
-- Name: import_news; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE import_news (
    id integer NOT NULL,
    import_source_id integer,
    title character varying(255) NOT NULL,
    url character varying(255),
    body text DEFAULT ''::text NOT NULL,
    news_date timestamp without time zone NOT NULL,
    data json NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: import_news_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE import_news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: import_news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE import_news_id_seq OWNED BY import_news.id;


--
-- Name: import_sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE import_sources (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: import_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE import_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: import_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE import_sources_id_seq OWNED BY import_sources.id;


--
-- Name: legal_entities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE legal_entities (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    ips inet[] DEFAULT '{}'::inet[] NOT NULL,
    user_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: legal_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE legal_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: legal_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE legal_entities_id_seq OWNED BY legal_entities.id;


--
-- Name: mailing_banners; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mailing_banners (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    body text DEFAULT ''::text NOT NULL,
    url character varying(255) NOT NULL,
    type_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    "position" integer NOT NULL,
    type character varying(255) DEFAULT 'text'::character varying NOT NULL,
    embed text DEFAULT ''::text NOT NULL,
    picture json DEFAULT '{}'::json NOT NULL
);


--
-- Name: mailing_banners_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mailing_banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mailing_banners_id_seq OWNED BY mailing_banners.id;


--
-- Name: mailing_letters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mailing_letters (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    mailing_id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    error text DEFAULT ''::text,
    mail text DEFAULT ''::text NOT NULL,
    data json DEFAULT '{}'::json NOT NULL
);


--
-- Name: mailing_letters_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mailing_letters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_letters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mailing_letters_id_seq OWNED BY mailing_letters.id;


--
-- Name: mailing_rules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mailing_rules (
    id integer NOT NULL,
    code character varying(255),
    status integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: mailing_rules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mailing_rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mailing_rules_id_seq OWNED BY mailing_rules.id;


--
-- Name: mailing_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mailing_types (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    template_classname character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    slug character varying(255) NOT NULL,
    is_visible boolean DEFAULT true NOT NULL,
    subscribers_only boolean DEFAULT false NOT NULL
);


--
-- Name: mailing_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mailing_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mailing_types_id_seq OWNED BY mailing_types.id;


--
-- Name: mailings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mailings (
    id integer NOT NULL,
    type_id integer,
    template text,
    scheduled_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    status integer DEFAULT 0 NOT NULL,
    data json DEFAULT '{}'::json NOT NULL,
    subject character varying(255),
    contacts_status integer DEFAULT 0 NOT NULL
);


--
-- Name: mailings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mailings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mailings_id_seq OWNED BY mailings.id;


--
-- Name: mobile_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE mobile_users (
    id integer NOT NULL,
    status integer,
    first_name character varying(255),
    last_name character varying(255),
    msisdn bigint NOT NULL,
    operator character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: mobile_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mobile_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobile_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mobile_users_id_seq OWNED BY mobile_users.id;


--
-- Name: namespaces; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE namespaces (
    id integer NOT NULL,
    product_id integer,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    base boolean DEFAULT false NOT NULL,
    redirect_to_id integer,
    picture json DEFAULT '{}'::json NOT NULL,
    texts hstore DEFAULT ''::hstore NOT NULL
);


--
-- Name: namespaces_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE namespaces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: namespaces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE namespaces_id_seq OWNED BY namespaces.id;


--
-- Name: newspapers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE newspapers (
    id integer NOT NULL,
    document_id bigint NOT NULL,
    release_date date NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    urls hstore DEFAULT ''::hstore NOT NULL
);


--
-- Name: newspapers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE newspapers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: newspapers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE newspapers_id_seq OWNED BY newspapers.id;


--
-- Name: options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE options (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE options_id_seq OWNED BY options.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE orders (
    id integer NOT NULL,
    uuid character varying(255) NOT NULL,
    payment_status integer DEFAULT 0 NOT NULL,
    payment_method integer NOT NULL,
    transaction_id character varying(255),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    data json DEFAULT '{}'::json NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    price_id integer,
    synced boolean DEFAULT false NOT NULL,
    managers_data hstore DEFAULT ''::hstore NOT NULL
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: payment_kits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_kits (
    id integer NOT NULL,
    product_id integer NOT NULL,
    namespace_id integer NOT NULL,
    subproduct_id integer NOT NULL,
    period_id integer NOT NULL,
    slug_path ltree NOT NULL,
    price numeric(10,2),
    crossed_price numeric(10,2),
    upsell_id integer,
    renewal_id integer,
    alternate_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    paymethod_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    autopay_timeout interval,
    syncable boolean DEFAULT false NOT NULL,
    texts hstore DEFAULT ''::hstore NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_conditions hstore DEFAULT ''::hstore NOT NULL,
    city_choice boolean DEFAULT false NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    discount integer
);


--
-- Name: payment_kits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_kits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_kits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_kits_id_seq OWNED BY payment_kits.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payments (
    id integer NOT NULL,
    payment_kit_id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    paymethod_id integer NOT NULL,
    price numeric(10,2) NOT NULL,
    renewal_id integer,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    data json DEFAULT '{}'::json NOT NULL,
    managers_data hstore DEFAULT ''::hstore NOT NULL,
    transaction_id character varying(255),
    synced boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payments_id_seq OWNED BY payments.id;


--
-- Name: paymethods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymethods (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    description text
);


--
-- Name: paymethods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE paymethods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: paymethods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE paymethods_id_seq OWNED BY paymethods.id;


--
-- Name: periods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE periods (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    duration interval NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: periods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE periods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: periods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE periods_id_seq OWNED BY periods.id;


--
-- Name: popular_comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE popular_comments (
    id integer NOT NULL,
    widget_id integer NOT NULL,
    xid bigint NOT NULL,
    body json DEFAULT '{}'::json NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: popular_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE popular_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: popular_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE popular_comments_id_seq OWNED BY popular_comments.id;


--
-- Name: pressrelease_banners; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pressrelease_banners (
    id integer NOT NULL,
    document_id bigint NOT NULL,
    banner_id bigint NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pressrelease_banners_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pressrelease_banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pressrelease_banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pressrelease_banners_id_seq OWNED BY pressrelease_banners.id;


--
-- Name: prices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prices (
    id integer NOT NULL,
    product integer NOT NULL,
    period integer NOT NULL,
    student boolean DEFAULT false NOT NULL,
    value numeric(10,2) NOT NULL,
    since timestamp without time zone NOT NULL,
    expire timestamp without time zone,
    upsell_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    crossed_price numeric(10,2),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    renewal_id integer
);


--
-- Name: prices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prices_id_seq OWNED BY prices.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE products (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: quote_sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE quote_sources (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: quote_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE quote_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: quote_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE quote_sources_id_seq OWNED BY quote_sources.id;


--
-- Name: redlines; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE redlines (
    id integer NOT NULL,
    label character varying(255),
    title character varying(255) NOT NULL,
    url character varying(255),
    status integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: redlines_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE redlines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: redlines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE redlines_id_seq OWNED BY redlines.id;


--
-- Name: reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE reports (
    id integer NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    ready boolean DEFAULT false NOT NULL,
    url character varying(255),
    kind character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE reports_id_seq OWNED BY reports.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sessions (
    id integer NOT NULL,
    access_token character varying(255) NOT NULL,
    user_id integer,
    account_id integer,
    ip inet NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    mobile_user_id integer
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- Name: snapshots; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE snapshots (
    id integer NOT NULL,
    subject_id integer NOT NULL,
    subject_type character varying(255) NOT NULL,
    author_id bigint,
    event character varying(255) NOT NULL,
    data json DEFAULT '{}'::json NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    service character varying(255)
);


--
-- Name: snapshots_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE snapshots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: snapshots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE snapshots_id_seq OWNED BY snapshots.id;


--
-- Name: subproducts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE subproducts (
    id integer NOT NULL,
    product_id integer,
    slug character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    phone_text text,
    "position" integer DEFAULT 0 NOT NULL
);


--
-- Name: subproducts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subproducts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subproducts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subproducts_id_seq OWNED BY subproducts.id;


--
-- Name: tickers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tickers (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    quote_source_id integer NOT NULL,
    price_time timestamp without time zone NOT NULL,
    value numeric(15,4) NOT NULL,
    prev_value numeric(15,4),
    data json DEFAULT '{}'::json NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: tickers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tickers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tickers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tickers_id_seq OWNED BY tickers.id;


--
-- Name: user_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_tokens (
    id integer NOT NULL,
    user_id integer,
    token character varying(255) NOT NULL,
    used boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type character varying(255) NOT NULL,
    token_data json DEFAULT '{}'::json NOT NULL
);


--
-- Name: user_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_tokens_id_seq OWNED BY user_tokens.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    nickname character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(255),
    status integer DEFAULT 0 NOT NULL,
    avatar json DEFAULT '{}'::json NOT NULL,
    address json DEFAULT '{}'::json NOT NULL,
    company character varying(255),
    birthday date,
    company_speciality_id integer,
    appointment_id integer,
    income_id integer,
    auto_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    autopay boolean DEFAULT false NOT NULL,
    subscribe_base_id character varying(255),
    accepted_promo hstore DEFAULT ''::hstore NOT NULL,
    data json DEFAULT '{}'::json NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_rights ALTER COLUMN id SET DEFAULT nextval('access_rights_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accounts ALTER COLUMN id SET DEFAULT nextval('accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cities ALTER COLUMN id SET DEFAULT nextval('cities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contacts ALTER COLUMN id SET DEFAULT nextval('contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedbacks ALTER COLUMN id SET DEFAULT nextval('feedbacks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY import_news ALTER COLUMN id SET DEFAULT nextval('import_news_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY import_sources ALTER COLUMN id SET DEFAULT nextval('import_sources_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY legal_entities ALTER COLUMN id SET DEFAULT nextval('legal_entities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_banners ALTER COLUMN id SET DEFAULT nextval('mailing_banners_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_letters ALTER COLUMN id SET DEFAULT nextval('mailing_letters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_rules ALTER COLUMN id SET DEFAULT nextval('mailing_rules_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_types ALTER COLUMN id SET DEFAULT nextval('mailing_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailings ALTER COLUMN id SET DEFAULT nextval('mailings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mobile_users ALTER COLUMN id SET DEFAULT nextval('mobile_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY namespaces ALTER COLUMN id SET DEFAULT nextval('namespaces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY newspapers ALTER COLUMN id SET DEFAULT nextval('newspapers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY options ALTER COLUMN id SET DEFAULT nextval('options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_kits ALTER COLUMN id SET DEFAULT nextval('payment_kits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments ALTER COLUMN id SET DEFAULT nextval('payments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymethods ALTER COLUMN id SET DEFAULT nextval('paymethods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY periods ALTER COLUMN id SET DEFAULT nextval('periods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY popular_comments ALTER COLUMN id SET DEFAULT nextval('popular_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pressrelease_banners ALTER COLUMN id SET DEFAULT nextval('pressrelease_banners_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices ALTER COLUMN id SET DEFAULT nextval('prices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY quote_sources ALTER COLUMN id SET DEFAULT nextval('quote_sources_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY redlines ALTER COLUMN id SET DEFAULT nextval('redlines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reports ALTER COLUMN id SET DEFAULT nextval('reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY snapshots ALTER COLUMN id SET DEFAULT nextval('snapshots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subproducts ALTER COLUMN id SET DEFAULT nextval('subproducts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tickers ALTER COLUMN id SET DEFAULT nextval('tickers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_tokens ALTER COLUMN id SET DEFAULT nextval('user_tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: access_rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_rights
    ADD CONSTRAINT access_rights_pkey PRIMARY KEY (id);


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: cities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: feedbacks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY feedbacks
    ADD CONSTRAINT feedbacks_pkey PRIMARY KEY (id);


--
-- Name: import_news_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY import_news
    ADD CONSTRAINT import_news_pkey PRIMARY KEY (id);


--
-- Name: import_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY import_sources
    ADD CONSTRAINT import_sources_pkey PRIMARY KEY (id);


--
-- Name: legal_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY legal_entities
    ADD CONSTRAINT legal_entities_pkey PRIMARY KEY (id);


--
-- Name: mailing_banners_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_banners
    ADD CONSTRAINT mailing_banners_pkey PRIMARY KEY (id);


--
-- Name: mailing_letters_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_letters
    ADD CONSTRAINT mailing_letters_pkey PRIMARY KEY (id);


--
-- Name: mailing_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_rules
    ADD CONSTRAINT mailing_rules_pkey PRIMARY KEY (id);


--
-- Name: mailing_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailing_types
    ADD CONSTRAINT mailing_types_pkey PRIMARY KEY (id);


--
-- Name: mailings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mailings
    ADD CONSTRAINT mailings_pkey PRIMARY KEY (id);


--
-- Name: mobile_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY mobile_users
    ADD CONSTRAINT mobile_users_pkey PRIMARY KEY (id);


--
-- Name: namespaces_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY namespaces
    ADD CONSTRAINT namespaces_pkey PRIMARY KEY (id);


--
-- Name: newspapers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY newspapers
    ADD CONSTRAINT newspapers_pkey PRIMARY KEY (id);


--
-- Name: options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY options
    ADD CONSTRAINT options_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: payment_kits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_kits
    ADD CONSTRAINT payment_kits_pkey PRIMARY KEY (id);


--
-- Name: payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: paymethods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymethods
    ADD CONSTRAINT paymethods_pkey PRIMARY KEY (id);


--
-- Name: periods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY periods
    ADD CONSTRAINT periods_pkey PRIMARY KEY (id);


--
-- Name: popular_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY popular_comments
    ADD CONSTRAINT popular_comments_pkey PRIMARY KEY (id);


--
-- Name: pressrelease_banners_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pressrelease_banners
    ADD CONSTRAINT pressrelease_banners_pkey PRIMARY KEY (id);


--
-- Name: prices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: quote_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY quote_sources
    ADD CONSTRAINT quote_sources_pkey PRIMARY KEY (id);


--
-- Name: redlines_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY redlines
    ADD CONSTRAINT redlines_pkey PRIMARY KEY (id);


--
-- Name: reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (id);


--
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: snapshots_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY snapshots
    ADD CONSTRAINT snapshots_pkey PRIMARY KEY (id);


--
-- Name: subproducts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY subproducts
    ADD CONSTRAINT subproducts_pkey PRIMARY KEY (id);


--
-- Name: tickers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tickers
    ADD CONSTRAINT tickers_pkey PRIMARY KEY (id);


--
-- Name: user_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_tokens
    ADD CONSTRAINT user_tokens_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_access_rights_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_rights_on_order_id ON access_rights USING btree (order_id);


--
-- Name: index_access_rights_on_payment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_rights_on_payment_id ON access_rights USING btree (payment_id);


--
-- Name: index_access_rights_on_reader_id_and_reader_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_rights_on_reader_id_and_reader_type ON access_rights USING btree (reader_id, reader_type);


--
-- Name: index_accounts_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_accounts_on_user_id ON accounts USING btree (user_id);


--
-- Name: index_contacts_on_contact_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contacts_on_contact_status ON contacts USING btree (contact_status);


--
-- Name: index_contacts_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_contacts_on_email ON contacts USING btree (email);


--
-- Name: index_contacts_on_mobile_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contacts_on_mobile_user_id ON contacts USING btree (mobile_user_id);


--
-- Name: index_contacts_on_subscription_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contacts_on_subscription_status ON contacts USING btree (subscription_status);


--
-- Name: index_contacts_on_type_ids; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contacts_on_type_ids ON contacts USING gin (type_ids);


--
-- Name: index_contacts_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contacts_on_user_id ON contacts USING btree (user_id);


--
-- Name: index_feedbacks_on_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_feedbacks_on_status ON feedbacks USING btree (status);


--
-- Name: index_feedbacks_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_feedbacks_on_user_id ON feedbacks USING btree (user_id);


--
-- Name: index_import_news_on_import_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_import_news_on_import_source_id ON import_news USING btree (import_source_id);


--
-- Name: index_import_sources_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_import_sources_on_slug ON import_sources USING btree (slug);


--
-- Name: index_mailing_letters_on_contact_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_letters_on_contact_id ON mailing_letters USING btree (contact_id);


--
-- Name: index_mailing_letters_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_letters_on_mailing_id ON mailing_letters USING btree (mailing_id);


--
-- Name: index_mailing_rules_on_code; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_mailing_rules_on_code ON mailing_rules USING btree (code);


--
-- Name: index_mailing_types_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_mailing_types_on_slug ON mailing_types USING btree (slug);


--
-- Name: index_mobile_users_on_msisdn; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_mobile_users_on_msisdn ON mobile_users USING btree (msisdn);


--
-- Name: index_namespaces_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_namespaces_on_product_id ON namespaces USING btree (product_id);


--
-- Name: index_namespaces_on_redirect_to_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_namespaces_on_redirect_to_id ON namespaces USING btree (redirect_to_id);


--
-- Name: index_newspapers_on_document_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_newspapers_on_document_id ON newspapers USING btree (document_id);


--
-- Name: index_newspapers_on_release_date; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_newspapers_on_release_date ON newspapers USING btree (release_date);


--
-- Name: index_on_payments_cast_data_subscription_id_as_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_on_payments_cast_data_subscription_id_as_text ON payments USING btree (((data ->> 'subscription_id'::text)));


--
-- Name: index_orders_on_payment_method_and_transaction_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_orders_on_payment_method_and_transaction_id ON orders USING btree (payment_method, transaction_id);


--
-- Name: index_orders_on_price_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_price_id ON orders USING btree (price_id);


--
-- Name: index_payment_kits_on_disabled; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_disabled ON payment_kits USING btree (disabled);


--
-- Name: index_payment_kits_on_namespace_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_namespace_id ON payment_kits USING btree (namespace_id);


--
-- Name: index_payment_kits_on_period_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_period_id ON payment_kits USING btree (period_id);


--
-- Name: index_payment_kits_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_product_id ON payment_kits USING btree (product_id);


--
-- Name: index_payment_kits_on_renewal_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_renewal_id ON payment_kits USING btree (renewal_id);


--
-- Name: index_payment_kits_on_slug_path; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_payment_kits_on_slug_path ON payment_kits USING btree (slug_path);


--
-- Name: index_payment_kits_on_subproduct_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_subproduct_id ON payment_kits USING btree (subproduct_id);


--
-- Name: index_payment_kits_on_upsell_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payment_kits_on_upsell_id ON payment_kits USING btree (upsell_id);


--
-- Name: index_payments_on_payment_kit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_payment_kit_id ON payments USING btree (payment_kit_id);


--
-- Name: index_payments_on_paymethod_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_paymethod_id ON payments USING btree (paymethod_id);


--
-- Name: index_payments_on_renewal_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_renewal_id ON payments USING btree (renewal_id);


--
-- Name: index_popular_comments_on_widget_id_and_xid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_popular_comments_on_widget_id_and_xid ON popular_comments USING btree (widget_id, xid);


--
-- Name: index_pressrelease_banners_on_banner_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_pressrelease_banners_on_banner_id ON pressrelease_banners USING btree (banner_id);


--
-- Name: index_pressrelease_banners_on_document_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_pressrelease_banners_on_document_id ON pressrelease_banners USING btree (document_id);


--
-- Name: index_prices_all_options; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prices_all_options ON prices USING btree (product, period, student, since, expire);


--
-- Name: index_prices_on_renewal_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prices_on_renewal_id ON prices USING btree (renewal_id);


--
-- Name: index_prices_on_since_and_expire; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prices_on_since_and_expire ON prices USING btree (since, expire);


--
-- Name: index_quote_sources_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_quote_sources_on_slug ON quote_sources USING btree (slug);


--
-- Name: index_quote_sources_on_title; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_quote_sources_on_title ON quote_sources USING btree (title);


--
-- Name: index_sessions_on_access_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sessions_on_access_token ON sessions USING btree (access_token);


--
-- Name: index_sessions_on_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_account_id ON sessions USING btree (account_id);


--
-- Name: index_sessions_on_mobile_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_mobile_user_id ON sessions USING btree (mobile_user_id);


--
-- Name: index_sessions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_user_id ON sessions USING btree (user_id);


--
-- Name: index_snapshots_on_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_snapshots_on_author_id ON snapshots USING btree (author_id);


--
-- Name: index_snapshots_on_subject_id_and_subject_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_snapshots_on_subject_id_and_subject_type ON snapshots USING btree (subject_id, subject_type);


--
-- Name: index_subproducts_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subproducts_on_product_id ON subproducts USING btree (product_id);


--
-- Name: index_tickers_on_quote_source_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_tickers_on_quote_source_id_and_slug ON tickers USING btree (quote_source_id, slug);


--
-- Name: index_user_tokens_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_tokens_on_user_id ON user_tokens USING btree (user_id);


--
-- Name: index_users_on_credentials; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_credentials ON users USING btree (first_name, last_name, nickname, company);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_status ON users USING btree (status);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20141006135517');

INSERT INTO schema_migrations (version) VALUES ('20141007133211');

INSERT INTO schema_migrations (version) VALUES ('20141008141242');

INSERT INTO schema_migrations (version) VALUES ('20141020165748');

INSERT INTO schema_migrations (version) VALUES ('20141127144407');

INSERT INTO schema_migrations (version) VALUES ('20141210080012');

INSERT INTO schema_migrations (version) VALUES ('20141216125934');

INSERT INTO schema_migrations (version) VALUES ('20141225143556');

INSERT INTO schema_migrations (version) VALUES ('20141229104119');

INSERT INTO schema_migrations (version) VALUES ('20141229155123');

INSERT INTO schema_migrations (version) VALUES ('20150104115213');

INSERT INTO schema_migrations (version) VALUES ('20150105142654');

INSERT INTO schema_migrations (version) VALUES ('20150120123754');

INSERT INTO schema_migrations (version) VALUES ('20150120155711');

INSERT INTO schema_migrations (version) VALUES ('20150121140218');

INSERT INTO schema_migrations (version) VALUES ('20150121140628');

INSERT INTO schema_migrations (version) VALUES ('20150126090815');

INSERT INTO schema_migrations (version) VALUES ('20150126091059');

INSERT INTO schema_migrations (version) VALUES ('20150202161935');

INSERT INTO schema_migrations (version) VALUES ('20150206165000');

INSERT INTO schema_migrations (version) VALUES ('20150206165100');

INSERT INTO schema_migrations (version) VALUES ('20150206165200');

INSERT INTO schema_migrations (version) VALUES ('20150206165300');

INSERT INTO schema_migrations (version) VALUES ('20150210104818');

INSERT INTO schema_migrations (version) VALUES ('20150219201438');

INSERT INTO schema_migrations (version) VALUES ('20150220184712');

INSERT INTO schema_migrations (version) VALUES ('20150221140806');

INSERT INTO schema_migrations (version) VALUES ('20150221155302');

INSERT INTO schema_migrations (version) VALUES ('20150221161440');

INSERT INTO schema_migrations (version) VALUES ('20150223114644');

INSERT INTO schema_migrations (version) VALUES ('20150224151826');

INSERT INTO schema_migrations (version) VALUES ('20150224154830');

INSERT INTO schema_migrations (version) VALUES ('20150225073342');

INSERT INTO schema_migrations (version) VALUES ('20150227070625');

INSERT INTO schema_migrations (version) VALUES ('20150311114315');

INSERT INTO schema_migrations (version) VALUES ('20150311143239');

INSERT INTO schema_migrations (version) VALUES ('20150331143832');

INSERT INTO schema_migrations (version) VALUES ('20150409123520');

INSERT INTO schema_migrations (version) VALUES ('20150527134307');

INSERT INTO schema_migrations (version) VALUES ('20150618172451');

INSERT INTO schema_migrations (version) VALUES ('20150731124126');

INSERT INTO schema_migrations (version) VALUES ('20150809221224');

INSERT INTO schema_migrations (version) VALUES ('20150824093229');

INSERT INTO schema_migrations (version) VALUES ('20150826143848');

INSERT INTO schema_migrations (version) VALUES ('20150915105009');

INSERT INTO schema_migrations (version) VALUES ('20150915135406');

INSERT INTO schema_migrations (version) VALUES ('20150916072510');

INSERT INTO schema_migrations (version) VALUES ('20150917101445');

INSERT INTO schema_migrations (version) VALUES ('20150917130002');

INSERT INTO schema_migrations (version) VALUES ('20150922095005');

INSERT INTO schema_migrations (version) VALUES ('20150922100848');

INSERT INTO schema_migrations (version) VALUES ('20150928131543');

INSERT INTO schema_migrations (version) VALUES ('20151001145212');

INSERT INTO schema_migrations (version) VALUES ('20151002115223');

INSERT INTO schema_migrations (version) VALUES ('20151005165653');

INSERT INTO schema_migrations (version) VALUES ('20151009120711');

INSERT INTO schema_migrations (version) VALUES ('20151009123610');

INSERT INTO schema_migrations (version) VALUES ('20151012165431');

INSERT INTO schema_migrations (version) VALUES ('20151020121017');

INSERT INTO schema_migrations (version) VALUES ('20151020135116');

INSERT INTO schema_migrations (version) VALUES ('20151020142358');

INSERT INTO schema_migrations (version) VALUES ('20151201140740');

INSERT INTO schema_migrations (version) VALUES ('20151203100040');

INSERT INTO schema_migrations (version) VALUES ('20151208151622');

INSERT INTO schema_migrations (version) VALUES ('20151208161259');

INSERT INTO schema_migrations (version) VALUES ('20151208163056');

INSERT INTO schema_migrations (version) VALUES ('20151209132743');

INSERT INTO schema_migrations (version) VALUES ('20151210153811');

INSERT INTO schema_migrations (version) VALUES ('20151211145329');

INSERT INTO schema_migrations (version) VALUES ('20151215122335');

INSERT INTO schema_migrations (version) VALUES ('20151215141936');

INSERT INTO schema_migrations (version) VALUES ('20151216100005');

INSERT INTO schema_migrations (version) VALUES ('20151216102437');

INSERT INTO schema_migrations (version) VALUES ('20151216111552');

INSERT INTO schema_migrations (version) VALUES ('20151217094202');

INSERT INTO schema_migrations (version) VALUES ('20151218072402');

INSERT INTO schema_migrations (version) VALUES ('20151218104500');

INSERT INTO schema_migrations (version) VALUES ('20151218142100');

INSERT INTO schema_migrations (version) VALUES ('20160113164442');

INSERT INTO schema_migrations (version) VALUES ('20160225110459');

INSERT INTO schema_migrations (version) VALUES ('20160322122251');

INSERT INTO schema_migrations (version) VALUES ('20160324151757');

INSERT INTO schema_migrations (version) VALUES ('20160516101837');

INSERT INTO schema_migrations (version) VALUES ('20160516104809');

INSERT INTO schema_migrations (version) VALUES ('20160628123153');

INSERT INTO schema_migrations (version) VALUES ('20160714125432');

INSERT INTO schema_migrations (version) VALUES ('20160926080943');

INSERT INTO schema_migrations (version) VALUES ('20161007074501');

INSERT INTO schema_migrations (version) VALUES ('20161013073950');

INSERT INTO schema_migrations (version) VALUES ('20161014110904');

INSERT INTO schema_migrations (version) VALUES ('20161129154506');

INSERT INTO schema_migrations (version) VALUES ('20161206102106');

