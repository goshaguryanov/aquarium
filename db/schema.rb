# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161007074501) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "ltree"

  create_table "access_rights", force: true do |t|
    t.integer  "order_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reader_id",   null: false
    t.string   "reader_type", null: false
    t.integer  "payment_id"
  end

  add_index "access_rights", ["order_id"], name: "index_access_rights_on_order_id", using: :btree
  add_index "access_rights", ["payment_id"], name: "index_access_rights_on_payment_id", using: :btree
  add_index "access_rights", ["reader_id", "reader_type"], name: "index_access_rights_on_reader_id_and_reader_type", using: :btree

  create_table "accounts", force: true do |t|
    t.integer  "user_id",         null: false
    t.string   "type",            null: false
    t.string   "password_digest"
    t.string   "external_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "cities", force: true do |t|
    t.string   "title",      null: false
    t.integer  "capability", null: false
    t.integer  "position",   null: false
    t.text     "contact"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: true do |t|
    t.string   "email",                            null: false
    t.integer  "user_id"
    t.integer  "contact_status",      default: 0,  null: false
    t.integer  "subscription_status", default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type_ids",            default: [], null: false, array: true
    t.integer  "mobile_user_id"
  end

  add_index "contacts", ["contact_status"], name: "index_contacts_on_contact_status", using: :btree
  add_index "contacts", ["email"], name: "index_contacts_on_email", unique: true, using: :btree
  add_index "contacts", ["mobile_user_id"], name: "index_contacts_on_mobile_user_id", using: :btree
  add_index "contacts", ["subscription_status"], name: "index_contacts_on_subscription_status", using: :btree
  add_index "contacts", ["type_ids"], name: "index_contacts_on_type_ids", using: :gin
  add_index "contacts", ["user_id"], name: "index_contacts_on_user_id", using: :btree

  create_table "feedbacks", force: true do |t|
    t.string   "access_token",                 null: false
    t.integer  "user_id"
    t.string   "username",                     null: false
    t.string   "company"
    t.string   "email",                        null: false
    t.string   "phone"
    t.text     "message"
    t.integer  "price_id"
    t.boolean  "callback",     default: false, null: false
    t.integer  "status",       default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type",         default: 0,     null: false
    t.json     "user_data",    default: {},    null: false
  end

  add_index "feedbacks", ["status"], name: "index_feedbacks_on_status", using: :btree
  add_index "feedbacks", ["user_id"], name: "index_feedbacks_on_user_id", using: :btree

  create_table "import_news", force: true do |t|
    t.integer  "import_source_id"
    t.string   "title",                         null: false
    t.string   "url"
    t.text     "body",             default: "", null: false
    t.datetime "news_date",                     null: false
    t.json     "data",                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "import_news", ["import_source_id"], name: "index_import_news_on_import_source_id", using: :btree

  create_table "import_sources", force: true do |t|
    t.string   "slug",       null: false
    t.string   "title",      null: false
    t.string   "url",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "import_sources", ["slug"], name: "index_import_sources_on_slug", unique: true, using: :btree

  create_table "legal_entities", force: true do |t|
    t.string   "name",                    null: false
    t.inet     "ips",        default: [], null: false, array: true
    t.integer  "user_ids",   default: [], null: false, array: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mailing_banners", force: true do |t|
    t.string   "title",                       null: false
    t.text     "body",       default: "",     null: false
    t.string   "url",                         null: false
    t.integer  "type_ids",   default: [],     null: false, array: true
    t.datetime "start_date",                  null: false
    t.datetime "end_date",                    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",                    null: false
    t.string   "type",       default: "text", null: false
    t.text     "embed",      default: "",     null: false
    t.json     "picture",    default: {},     null: false
  end

  create_table "mailing_letters", force: true do |t|
    t.integer  "contact_id",              null: false
    t.integer  "mailing_id",              null: false
    t.integer  "status",     default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "error",      default: ""
    t.text     "mail",       default: "", null: false
    t.json     "data",       default: {}, null: false
  end

  add_index "mailing_letters", ["contact_id"], name: "index_mailing_letters_on_contact_id", using: :btree
  add_index "mailing_letters", ["mailing_id"], name: "index_mailing_letters_on_mailing_id", using: :btree

  create_table "mailing_rules", force: true do |t|
    t.string   "code"
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailing_rules", ["code"], name: "index_mailing_rules_on_code", unique: true, using: :btree

  create_table "mailing_types", force: true do |t|
    t.string   "title",                              null: false
    t.string   "template_classname"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",                               null: false
    t.boolean  "is_visible",         default: true,  null: false
    t.boolean  "subscribers_only",   default: false, null: false
  end

  add_index "mailing_types", ["slug"], name: "index_mailing_types_on_slug", unique: true, using: :btree

  create_table "mailings", force: true do |t|
    t.integer  "type_id"
    t.text     "template"
    t.datetime "scheduled_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",          default: 0,  null: false
    t.json     "data",            default: {}, null: false
    t.string   "subject"
    t.integer  "contacts_status", default: 0,  null: false
  end

  create_table "mobile_users", force: true do |t|
    t.integer  "status"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "msisdn",     limit: 8, null: false
    t.string   "operator",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mobile_users", ["msisdn"], name: "index_mobile_users_on_msisdn", unique: true, using: :btree

  create_table "namespaces", force: true do |t|
    t.integer  "product_id"
    t.string   "slug",                           null: false
    t.string   "title",                          null: false
    t.datetime "start_date",                     null: false
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "base",           default: false, null: false
    t.integer  "redirect_to_id"
    t.json     "picture",        default: {},    null: false
    t.hstore   "texts",          default: {},    null: false
  end

  add_index "namespaces", ["product_id"], name: "index_namespaces_on_product_id", using: :btree
  add_index "namespaces", ["redirect_to_id"], name: "index_namespaces_on_redirect_to_id", using: :btree

  create_table "newspapers", force: true do |t|
    t.integer  "document_id",  limit: 8,              null: false
    t.date     "release_date",                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "urls",                   default: {}, null: false
  end

  add_index "newspapers", ["document_id"], name: "index_newspapers_on_document_id", unique: true, using: :btree
  add_index "newspapers", ["release_date"], name: "index_newspapers_on_release_date", unique: true, using: :btree

  create_table "options", force: true do |t|
    t.string   "type",       null: false
    t.string   "value",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string   "uuid",                           null: false
    t.integer  "payment_status", default: 0,     null: false
    t.integer  "payment_method",                 null: false
    t.string   "transaction_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.json     "data",           default: {},    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price_id"
    t.boolean  "synced",         default: false, null: false
    t.hstore   "managers_data",  default: {},    null: false
  end

  add_index "orders", ["payment_method", "transaction_id"], name: "index_orders_on_payment_method_and_transaction_id", unique: true, using: :btree
  add_index "orders", ["price_id"], name: "index_orders_on_price_id", using: :btree

  create_table "payment_kits", force: true do |t|
    t.integer  "product_id",                                               null: false
    t.integer  "namespace_id",                                             null: false
    t.integer  "subproduct_id",                                            null: false
    t.integer  "period_id",                                                null: false
    t.ltree    "slug_path",                                                null: false
    t.decimal  "price",           precision: 10, scale: 2
    t.decimal  "crossed_price",   precision: 10, scale: 2
    t.integer  "upsell_id"
    t.integer  "renewal_id"
    t.integer  "alternate_ids",                            default: [],    null: false, array: true
    t.integer  "paymethod_ids",                            default: [],    null: false, array: true
    t.interval "autopay_timeout"
    t.boolean  "syncable",                                 default: false, null: false
    t.hstore   "texts",                                    default: {},    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "user_conditions",                          default: {},    null: false
    t.boolean  "city_choice",                              default: false, null: false
    t.boolean  "disabled",                                 default: false, null: false
    t.integer  "discount"
  end

  add_index "payment_kits", ["disabled"], name: "index_payment_kits_on_disabled", using: :btree
  add_index "payment_kits", ["namespace_id"], name: "index_payment_kits_on_namespace_id", using: :btree
  add_index "payment_kits", ["period_id"], name: "index_payment_kits_on_period_id", using: :btree
  add_index "payment_kits", ["product_id"], name: "index_payment_kits_on_product_id", using: :btree
  add_index "payment_kits", ["renewal_id"], name: "index_payment_kits_on_renewal_id", using: :btree
  add_index "payment_kits", ["slug_path"], name: "index_payment_kits_on_slug_path", unique: true, using: :btree
  add_index "payment_kits", ["subproduct_id"], name: "index_payment_kits_on_subproduct_id", using: :btree
  add_index "payment_kits", ["upsell_id"], name: "index_payment_kits_on_upsell_id", using: :btree

  create_table "payments", force: true do |t|
    t.integer  "payment_kit_id",                                          null: false
    t.integer  "status",                                  default: 0,     null: false
    t.integer  "paymethod_id",                                            null: false
    t.decimal  "price",          precision: 10, scale: 2,                 null: false
    t.integer  "renewal_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.json     "data",                                    default: {},    null: false
    t.hstore   "managers_data",                           default: {},    null: false
    t.string   "transaction_id"
    t.boolean  "synced",                                  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["payment_kit_id"], name: "index_payments_on_payment_kit_id", using: :btree
  add_index "payments", ["paymethod_id"], name: "index_payments_on_paymethod_id", using: :btree
  add_index "payments", ["renewal_id"], name: "index_payments_on_renewal_id", using: :btree

  create_table "paymethods", force: true do |t|
    t.string   "slug",        null: false
    t.string   "title",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "periods", force: true do |t|
    t.string   "slug",       null: false
    t.string   "title",      null: false
    t.interval "duration",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "popular_comments", force: true do |t|
    t.integer  "widget_id",                         null: false
    t.integer  "xid",        limit: 8,              null: false
    t.json     "body",                 default: {}, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "popular_comments", ["widget_id", "xid"], name: "index_popular_comments_on_widget_id_and_xid", unique: true, using: :btree

  create_table "pressrelease_banners", force: true do |t|
    t.integer  "document_id", limit: 8, null: false
    t.integer  "banner_id",   limit: 8, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pressrelease_banners", ["banner_id"], name: "index_pressrelease_banners_on_banner_id", unique: true, using: :btree
  add_index "pressrelease_banners", ["document_id"], name: "index_pressrelease_banners_on_document_id", unique: true, using: :btree

  create_table "prices", force: true do |t|
    t.integer  "product",                                                null: false
    t.integer  "period",                                                 null: false
    t.boolean  "student",                                default: false, null: false
    t.decimal  "value",         precision: 10, scale: 2,                 null: false
    t.datetime "since",                                                  null: false
    t.datetime "expire"
    t.integer  "upsell_ids",                             default: [],    null: false, array: true
    t.decimal  "crossed_price", precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "renewal_id"
  end

  add_index "prices", ["product", "period", "student", "since", "expire"], name: "index_prices_all_options", using: :btree
  add_index "prices", ["renewal_id"], name: "index_prices_on_renewal_id", using: :btree
  add_index "prices", ["since", "expire"], name: "index_prices_on_since_and_expire", using: :btree

  create_table "products", force: true do |t|
    t.string   "slug",       null: false
    t.string   "title",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quote_sources", force: true do |t|
    t.string   "title",      null: false
    t.string   "slug",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "quote_sources", ["slug"], name: "index_quote_sources_on_slug", unique: true, using: :btree
  add_index "quote_sources", ["title"], name: "index_quote_sources_on_title", unique: true, using: :btree

  create_table "redlines", force: true do |t|
    t.string   "label"
    t.string   "title",                  null: false
    t.string   "url"
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.datetime "start_date",                 null: false
    t.datetime "end_date",                   null: false
    t.boolean  "ready",      default: false, null: false
    t.string   "url"
    t.string   "kind",                       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.string   "access_token",   null: false
    t.integer  "user_id"
    t.integer  "account_id"
    t.inet     "ip",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "mobile_user_id"
  end

  add_index "sessions", ["access_token"], name: "index_sessions_on_access_token", unique: true, using: :btree
  add_index "sessions", ["account_id"], name: "index_sessions_on_account_id", using: :btree
  add_index "sessions", ["mobile_user_id"], name: "index_sessions_on_mobile_user_id", using: :btree
  add_index "sessions", ["user_id"], name: "index_sessions_on_user_id", using: :btree

  create_table "snapshots", force: true do |t|
    t.integer  "subject_id",                          null: false
    t.string   "subject_type",                        null: false
    t.integer  "author_id",    limit: 8
    t.string   "event",                               null: false
    t.json     "data",                   default: {}, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "service"
  end

  add_index "snapshots", ["author_id"], name: "index_snapshots_on_author_id", using: :btree
  add_index "snapshots", ["subject_id", "subject_type"], name: "index_snapshots_on_subject_id_and_subject_type", using: :btree

  create_table "subproducts", force: true do |t|
    t.integer  "product_id"
    t.string   "slug",                   null: false
    t.string   "title",                  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "phone_text"
    t.integer  "position",   default: 0, null: false
  end

  add_index "subproducts", ["product_id"], name: "index_subproducts_on_product_id", using: :btree

  create_table "tickers", force: true do |t|
    t.string   "title",                                                 null: false
    t.string   "slug",                                                  null: false
    t.integer  "quote_source_id",                                       null: false
    t.datetime "price_time",                                            null: false
    t.decimal  "value",           precision: 15, scale: 4,              null: false
    t.decimal  "prev_value",      precision: 15, scale: 4
    t.json     "data",                                     default: {}, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tickers", ["quote_source_id", "slug"], name: "index_tickers_on_quote_source_id_and_slug", unique: true, using: :btree

  create_table "user_tokens", force: true do |t|
    t.integer  "user_id"
    t.string   "token",                      null: false
    t.boolean  "used",       default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",                       null: false
    t.json     "token_data", default: {},    null: false
  end

  add_index "user_tokens", ["user_id"], name: "index_user_tokens_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "nickname",                              null: false
    t.string   "email",                                 null: false
    t.string   "phone"
    t.integer  "status",                default: 0,     null: false
    t.json     "avatar",                default: {},    null: false
    t.json     "address",               default: {},    null: false
    t.string   "company"
    t.date     "birthday"
    t.integer  "company_speciality_id"
    t.integer  "appointment_id"
    t.integer  "income_id"
    t.integer  "auto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "autopay",               default: false, null: false
    t.string   "subscribe_base_id"
    t.hstore   "accepted_promo",        default: {},    null: false
    t.json     "data",                  default: {},    null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["first_name", "last_name", "nickname", "company"], name: "index_users_on_credentials", using: :btree
  add_index "users", ["status"], name: "index_users_on_status", using: :btree

end
