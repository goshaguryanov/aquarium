class AddFieldsToMailingTypes < ActiveRecord::Migration
  def change
    add_column :mailing_types, :is_visible, :boolean, null: false, default: true
    add_column :mailing_types, :subscribers_only, :boolean, null: false, default: false
  end
end
