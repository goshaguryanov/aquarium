class CreateImportNews < ActiveRecord::Migration
  def change
    create_table :import_news do |t|
      t.references :import_source, index: true
      t.string     :title,                      null: false
      t.string     :url
      t.text       :body,                       null: false, default: ''
      t.timestamp  :news_date,     index: true, null: false
      t.json       :data,                       null: false, defailt: {}
      
      t.timestamps
    end
  end
end
