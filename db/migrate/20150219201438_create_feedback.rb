class CreateFeedback < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string  :type,         null: false
      t.string  :access_token, null: false
      t.integer :user_id
      t.string  :username,     null: false
      t.string  :company
      t.string  :email,        null: false
      t.string  :phone,        null: false
      t.string  :message,      null: false
      t.integer :price_id
      t.boolean :callback,     null: false, default: false
      t.integer :status,       null: false, default: 0
      
      t.timestamps
    end
    
    add_index :feedbacks, :user_id
    add_index :feedbacks, :status
  end
end
