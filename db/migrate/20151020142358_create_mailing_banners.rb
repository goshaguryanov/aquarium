class CreateMailingBanners < ActiveRecord::Migration
  def change
    create_table :mailing_banners do |t|
      t.string   :title,      null: false
      t.text     :body,       null: false
      t.string   :url,        null: false
      t.integer  :type_ids,   null: false, default: [], array: true
      t.datetime :start_date, null: false
      t.datetime :end_date,   null: false
      
      t.timestamps
    end
  end
end
