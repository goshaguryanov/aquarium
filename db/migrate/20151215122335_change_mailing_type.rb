class ChangeMailingType < ActiveRecord::Migration
  def change
    add_index :mailing_types, :slug, unique: true
  end
end
