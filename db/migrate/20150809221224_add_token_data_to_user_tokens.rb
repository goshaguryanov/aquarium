class AddTokenDataToUserTokens < ActiveRecord::Migration
  def change
    add_column :user_tokens, :token_data, :json, default: {}, null: false
  end
end
