class RenameColumnInContacts < ActiveRecord::Migration
  def change
    rename_column :contacts, :subscriber_status, :contact_status
  end
end
