class RelinkAccessRights < ActiveRecord::Migration
  def up
    change_table :orders do |t|
      t.remove :user_id
    end
    
    change_table :access_rights do |t|
      t.remove :user_id
      t.references :reader, polymorphic: true, index: true
    end
    
    change_table :sessions do |t|
      t.remove :access_right_id
    end
    
    change_table :legal_entities do |t|
      t.remove :access_type
    end
  end
  
  def down
    change_table :orders do |t|
      t.references :user, index: true
    end
    
    change_table :access_rights do |t|
      t.remove :reader_id
      t.remove :reader_type
      t.references :user, index: true
    end
    
    change_table :sessions do |t|
      t.references :access_right, index: true
    end
    
    change_table :legal_entities do |t|
      t.string :access_type, null: false
    end
  end
end
