class AddSlugToMailingTypes < ActiveRecord::Migration
  def change
    add_column :mailing_types, :slug, :string, null: false
  end
end
