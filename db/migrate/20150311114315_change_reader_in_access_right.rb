class ChangeReaderInAccessRight < ActiveRecord::Migration
  def up
    change_table :access_rights do |t|
      t.change :reader_type, :string,  :null => false
      t.change :reader_id,   :integer, :null => false
    end
  end

  def down
    change_table :access_rights do |t|
      t.change :reader_type, :string,  :null => true
      t.change :reader_id,   :integer, :null => true
    end
  end
end
