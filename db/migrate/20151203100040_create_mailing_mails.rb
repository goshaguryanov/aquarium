class CreateMailingMails < ActiveRecord::Migration
  def change
    create_table :mailing_mails do |t|
      t.references  :contact, null: false, index: true
      t.references  :mailing, null: false, index: true
      t.integer     :status, default: 0, null: false, index: true
      t.timestamps
    end
  end
end
