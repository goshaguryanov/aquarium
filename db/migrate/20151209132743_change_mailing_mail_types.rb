class ChangeMailingMailTypes < ActiveRecord::Migration
  def up
    change_column :mailing_types, :template_classname, :string, null: true
  end

  def down
    change_column :mailing_types, :template_classname, :string, null: false
  end
end
