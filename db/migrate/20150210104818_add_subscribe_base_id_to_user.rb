class AddSubscribeBaseIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :subscribe_base_id, :string
  end
end
