class AddSomethingInNamespace < ActiveRecord::Migration
  def change
    add_column :namespaces, :base, :boolean, default: false, null: false
    add_reference :namespaces, :redirect_to, index: true
  end
end
