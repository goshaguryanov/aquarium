class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string     :access_token, null: false
      t.references :user,         index: true
      t.references :account,      index: true
      t.references :access_right, index: true
      t.inet       :ip,           null: false
      t.integer    :counter,      null: false, default: 0
      
      t.timestamps
      
      t.index :access_token, unique: true
    end
  end
end
