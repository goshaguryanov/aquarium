class CreateAccessRights < ActiveRecord::Migration
  def change
    create_table :access_rights do |t|
      t.references :user, index: true
      t.references :order, index: true
      t.timestamp :start_date
      t.timestamp :end_date
      
      t.timestamps
    end
  end
end
