class RemoveUserTokenExpiresAtColumn < ActiveRecord::Migration
  def change
    remove_column :user_tokens, :expires_at
  end
end
