class ChangePriceInOrders < ActiveRecord::Migration
  def up
    change_table :orders do |t|
      t.remove :price
      t.references :price, index: true
    end
  end
  def down
    change_table :orders do |t|
      t.remove :price_id
      t.decimal :price, precision: 10, scale: 2
    end
  end
end
