class AddPositionToMailingBanners < ActiveRecord::Migration
  def change
    add_column :mailing_banners, :position, :integer, null: false
  end
end
