class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string  :title
      t.integer :document_id, limit: 8
      t.integer :bound_document_id, limit: 8
      t.boolean :processed, default: false
      t.timestamps

      t.index :title, unique: true
      t.index :created_at
      t.index :processed
    end
  end
end
