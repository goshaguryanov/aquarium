class AddDiscountColumnToPaymentKit < ActiveRecord::Migration
  def change
    add_column :payment_kits, :discount, :integer
  end
end
