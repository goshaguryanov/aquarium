class AddMailingIndexes < ActiveRecord::Migration
  def change
    add_index :contacts, :contact_status
    add_index :contacts, :subscription_status
    add_index :contacts, :type_ids, using: :gin
  end
end
