class PopularComments < ActiveRecord::Migration
  def change
    create_table :popular_comments do |t|
      t.integer :widget_id, null: false
      t.integer :xid, null: false, limit: 8
      t.json :body, null: false, default: {}
      t.timestamps
    end
    add_index :popular_comments, [:widget_id, :xid], unique: true
  end
end
