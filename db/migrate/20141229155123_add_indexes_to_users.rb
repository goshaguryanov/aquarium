class AddIndexesToUsers < ActiveRecord::Migration
  def change
    add_index :users, :first_name
    add_index :users, :last_name
    add_index :users, :nickname
    add_index :users, :company
    add_index :users, :status
  end
end
