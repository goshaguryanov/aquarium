class AddCityChoiceToPaymentKit < ActiveRecord::Migration
  def change
    add_column :payment_kits, :city_choice, :boolean, default: false, null: false
  end
end
