class CreateIndexOnPaymentsCastDataSubscriptionIdAsText < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE INDEX index_on_payments_cast_data_subscription_id_as_text ON payments (CAST(data->>'subscription_id' AS text));
    SQL
  end
end
