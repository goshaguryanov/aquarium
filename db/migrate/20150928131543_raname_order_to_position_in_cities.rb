class RanameOrderToPositionInCities < ActiveRecord::Migration
  def change
    rename_column :cities, :order, :position
  end
end
