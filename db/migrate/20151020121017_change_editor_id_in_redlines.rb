class ChangeEditorIdInRedlines < ActiveRecord::Migration
  def change
    remove_column :redlines, :editor_id
  end
end
