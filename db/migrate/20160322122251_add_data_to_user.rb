class AddDataToUser < ActiveRecord::Migration
  def change
    add_column :users, :data, :json, null: false, default: {}
  end
end
