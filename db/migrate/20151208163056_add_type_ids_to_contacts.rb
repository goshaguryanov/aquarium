class AddTypeIdsToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :type_ids, :integer, default: [], null: false, array: true
  end
end
