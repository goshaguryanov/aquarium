class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user, null: false, index: true
      t.string     :type, null: false
      t.string     :password_digest
      t.string     :external_id
      
      t.timestamps
    end
  end
end
