class AddSlxSyncToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :synced, :boolean, null: false, default: false
  end
end
