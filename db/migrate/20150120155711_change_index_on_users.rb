class ChangeIndexOnUsers < ActiveRecord::Migration
  def up
    remove_index :users, :first_name
    remove_index :users, :last_name
    remove_index :users, :nickname
    remove_index :users, :company
    
    add_index    :users, %i(first_name last_name nickname company), name: 'index_users_on_credentials'
  end
  
  def down
    remove_index :users, name: 'index_users_on_credentials'
    
    add_index :users, :first_name
    add_index :users, :last_name
    add_index :users, :nickname
    add_index :users, :company
  end
end
