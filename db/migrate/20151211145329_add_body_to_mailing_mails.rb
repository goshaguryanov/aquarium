class AddBodyToMailingMails < ActiveRecord::Migration
  def change
    add_column :mailing_mails, :mail, :text, default: '', null: false
  end
end
