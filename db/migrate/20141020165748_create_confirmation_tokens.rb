class CreateConfirmationTokens < ActiveRecord::Migration
  def change
    create_table :confirmation_tokens do |t|
      t.references :user, index: true, null: false
      t.string :token,                 null: false
      t.datetime :expires_at,          null: false
      t.boolean :used,                 null: false, default: false
      
      t.timestamps
    end
  end
end
