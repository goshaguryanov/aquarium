class AddIndexToPaymentKit < ActiveRecord::Migration
  def change
    add_index :payment_kits, :slug_path, unique: true
  end
end
