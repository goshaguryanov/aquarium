class CreateQuoteSources < ActiveRecord::Migration
  def change
    create_table :quote_sources do |t|
      t.string :title, null: false
      t.string :slug,  null: false

      t.timestamps
    end
    add_index :quote_sources, :title, unique: true
    add_index :quote_sources, :slug, unique: true
  end
end
