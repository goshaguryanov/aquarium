class AddDisabledToPaymentKit < ActiveRecord::Migration
  def change
    add_column :payment_kits, :disabled, :boolean, null: false, default: false
    add_index :payment_kits, :disabled
  end
end
