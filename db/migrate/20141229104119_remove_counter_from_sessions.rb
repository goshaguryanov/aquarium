class RemoveCounterFromSessions < ActiveRecord::Migration
  def up
    change_table :sessions do |t|
      t.remove :counter
    end
  end
  
  def down
    change_table :sessions do |t|
      t.integer :counter, null: false, default: 0
    end
  end
end
