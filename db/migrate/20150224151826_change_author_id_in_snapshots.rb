class ChangeAuthorIdInSnapshots < ActiveRecord::Migration
  def up
    change_column :snapshots, :author_id, :integer, limit: 8
  end
  
  def down
    change_column :snapshots, :author_id, :integer
  end
end
