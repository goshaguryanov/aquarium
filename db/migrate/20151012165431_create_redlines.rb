class CreateRedlines < ActiveRecord::Migration
  def change
    create_table :redlines do |t|
      t.string  :label
      t.string  :title, null: false
      t.string  :url
      t.integer :status, default: 0, null: false
      t.integer :editor_id, null: false
      
      t.timestamps
    end
  end
end
