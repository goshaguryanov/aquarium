class ChangeFeedbacksType < ActiveRecord::Migration
  def up
    remove_column :feedbacks, :type
    add_column    :feedbacks, :type, :integer, null: false, default: 0
  end
  
  def down
    remove_column :feedbacks, :type
    add_column    :feedbacks, :type, :string, null: false
  end
end
