class AddUserConditionsToPaymentKit < ActiveRecord::Migration
  def change
    add_column :payment_kits, :user_conditions, :hstore, default: {}, null: false
  end
end
