class AddRenewalToPrice < ActiveRecord::Migration
  def change
    add_reference :prices, :renewal, index: true
  end
end
