class RemoveImportStatusFromContact < ActiveRecord::Migration
  def up
    remove_column :contacts, :import_status
  end

  def down
    add_column :contacts, :import_status, :integer, null: false, default: 0
  end
end
