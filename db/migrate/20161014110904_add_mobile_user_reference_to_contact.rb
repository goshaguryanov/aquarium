class AddMobileUserReferenceToContact < ActiveRecord::Migration
  def change
    remove_column :mobile_users, :email
    add_reference :contacts, :mobile_user
    add_index :contacts, :mobile_user_id
  end
end
