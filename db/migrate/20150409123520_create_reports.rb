class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.timestamp :start_date, null: false
      t.timestamp :end_date,   null: false
      t.boolean :ready,        null: false, default: false
      t.string :url
      t.string :kind,          null: false, index: true

      t.timestamps
    end
  end
end
