class AddUrlToNewspapers < ActiveRecord::Migration
  def change
    change_table :newspapers do |t|
      t.remove :url
      t.hstore :urls, null: false, default: {}
    end
  end
end
