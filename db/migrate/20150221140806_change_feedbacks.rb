class ChangeFeedbacks < ActiveRecord::Migration
  def change
    change_column_null :feedbacks, :phone, true
    change_column_null :feedbacks, :message, true
  end
end
