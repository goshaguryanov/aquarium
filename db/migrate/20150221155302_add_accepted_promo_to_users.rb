class AddAcceptedPromoToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.hstore :accepted_promo, null: false, default: {}
    end
  end
end
