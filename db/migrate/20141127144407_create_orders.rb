class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string     :uuid, null: false
      t.references :user, index: true
      t.integer    :payment_status, null: false, default: 0
      t.integer    :payment_method, null: false
      t.decimal    :price, precision: 10, scale: 2
      t.string     :transaction_id
      t.timestamp  :start_date
      t.timestamp  :end_date
      t.json       :data, null: false, default: {}
      
      t.timestamps
    end
  end
end
