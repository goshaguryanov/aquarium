class AddFieldsToMailingBanner < ActiveRecord::Migration
  def change
    add_column :mailing_banners, :type,    :string, null: false, default: 'text'
    add_column :mailing_banners, :embed,   :text,   null: false, default: ''
    add_column :mailing_banners, :picture, :json,   null: false, default: {}
  end
end
