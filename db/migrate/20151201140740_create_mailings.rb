class CreateMailings < ActiveRecord::Migration
  def change
    create_table :mailings do |t|
      t.references  :type
      t.text        :template
      t.datetime    :scheduled_at
      t.timestamps
    end
  end
end
