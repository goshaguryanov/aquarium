class ChangeFeedbacksColumnMessage < ActiveRecord::Migration
  def change
    change_column :feedbacks, :message, :text
  end
end
