class CreateNewspapers < ActiveRecord::Migration
  def change
    create_table :newspapers do |t|
      t.integer :document_id,  null: false, limit: 8
      t.date    :release_date, null: false
      t.string  :url,          null: false
      
      t.index   :document_id,  unique: true
      t.index   :release_date, unique: true
      t.index   :url,          unique: true
      
      t.timestamps
    end
  end
end
