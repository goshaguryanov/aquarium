class CreateImportSources < ActiveRecord::Migration
  def change
    create_table :import_sources do |t|
      t.string :slug,  null: false
      t.string :title, null: false
      t.string :url,  null: false
      
      t.timestamps
    end
    add_index :import_sources, :slug, unique: true
  end
end
