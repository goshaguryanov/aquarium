class CreateTickers < ActiveRecord::Migration
  def change
    create_table :tickers do |t|
      t.string     :title,        null: false
      t.string     :slug,         null: false
      t.references :quote_source, null: false
      t.timestamp  :price_time,   null: false
      t.decimal    :value,        null: false, precision: 15, scale: 4
      t.decimal    :prev_value,                precision: 15, scale: 4
      t.json       :data,         null: false, default: {}

      t.timestamps
    end

    add_index :tickers, %i(quote_source_id slug), unique: true
  end
end
