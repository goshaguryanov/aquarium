class AddDefaultValueForTextInMailingBanners < ActiveRecord::Migration
  def up
    change_column_default :mailing_banners, :body, ''
  end

  def down
    change_column_default :mailing_banners, :body, nil
  end
end
