class RenameSubscribersToContacts < ActiveRecord::Migration
  def change
    rename_table :subscribers, :contacts
  end
end
