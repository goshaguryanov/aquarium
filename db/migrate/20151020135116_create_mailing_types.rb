class CreateMailingTypes < ActiveRecord::Migration
  def change
    create_table :mailing_types do |t|
      t.string  :title, null: false
      t.string  :template_classname, null: false
      
      t.timestamps
    end
  end
end
