class AddImportToContact < ActiveRecord::Migration
  def change
     add_column :contacts, :import_status, :integer, null: false, default: 0
  end
end
