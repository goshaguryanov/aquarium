class AddPositionToSubproducts < ActiveRecord::Migration
  def change
    add_column :subproducts, :position, :integer, null: false, default: 0
  end
end
