class RenameMailingMailToLetter < ActiveRecord::Migration
  def change
    rename_table :mailing_mails, :mailing_letters
  end
end
