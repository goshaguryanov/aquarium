class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string  :title,      null: false
      t.integer :capability, null: false
      t.integer :order,      null: false
      t.text    :contact

      t.timestamps
    end
  end
end
