class CreateNewPayments < ActiveRecord::Migration
  def up
    create_table :products do |t|
      t.string :slug,  null: false
      t.string :title, null: false

      t.timestamps
    end

    create_table :namespaces do |t|
      t.references :product,    index: true
      t.string     :slug,       null: false
      t.string     :title,      null: false
      t.datetime   :start_date, null: false
      t.datetime   :end_date

      t.timestamps
    end

    create_table :periods do |t|
      t.string   :slug,     null: false
      t.string   :title,    null: false
      t.interval :duration, null: false

      t.timestamps
    end

    create_table :subproducts do |t|
      t.references :product, index: true
      t.string     :slug,    null: false
      t.string     :title,   null: false

      t.timestamps
    end

    create_table :paymethods do |t|
      t.string :slug,  null: false
      t.string :title, null: false

      t.timestamps      
    end

    enable_extension :ltree

    create_table :payment_kits do |t|
      t.references :product,        null: false, index: true
      t.references :namespace,      null: false, index: true
      t.references :subproduct,     null: false, index: true
      t.references :period,         null: false, index: true
      t.ltree      :slug_path,      null: false
      t.decimal    :price,          precision: 10, scale: 2
      t.decimal    :crossed_price,  precision: 10, scale: 2
      t.references :upsell,         index: true
      t.references :renewal,        index: true
      t.integer    :alternate_ids,  null: false, array: true, default: []
      t.integer    :paymethod_ids,  null: false, array: true, default: []
      t.boolean    :autoconfirm,    null: false, default: false
      t.interval   :autopay_timeout
      t.boolean    :syncable,       null: false, default: false
      t.hstore     :texts,          null: false, default: {}

      t.timestamps
    end

    create_table :payments do |t|
      t.references :payment_kit,    null: false, index: true
      t.integer    :status,         null: false, index: true, default: 0
      t.references :paymethod,      null: false, index: true
      t.decimal    :price,          null: false, precision: 10, scale: 2
      t.references :renewal,        index: true
      t.datetime   :start_date
      t.datetime   :end_date
      t.json       :data,           null: false, default: {}
      t.hstore     :managers_data,  null: false, default: {}
      t.string     :transaction_id
      t.boolean    :synced,         null: false, default: false

      t.timestamps
    end

    add_reference :access_rights, :payment, index: true
  end

  def down
    remove_column :access_rights, :payment_id
    drop_table :payments
    drop_table :payment_kits
    disable_extension :ltree
    drop_table :paymethods
    drop_table :subproducts
    drop_table :periods
    drop_table :namespaces
    drop_table :products
  end
end
