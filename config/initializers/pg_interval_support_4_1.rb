# Enables PostgreSQL interval datatype support (as ActiveSupport::Duration) in Ruby on Rails 4.1.
# Based on https://gist.github.com/clarkdave/6529610

require 'active_support/duration'
require 'active_record/connection_adapters/postgresql_adapter'

# add a native DB type of :interval
ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::NATIVE_DATABASE_TYPES[:interval] = { name: 'interval' }

# add the interval type to the simplified_type list. because this method is a case statement
# we can't inject anything into it, so we create an alias around it so calls to it will call
# our aliased method first, which (if appropriate) will return our type, otherwise passing
# it along to the original unaliased method (which has the normal case statement)
ActiveRecord::ConnectionAdapters::PostgreSQLColumn.class_eval do

  define_method("simplified_type_with_interval") do |field_type|
    if field_type == 'interval'
      :interval
    else
      send("simplified_type_without_interval", field_type)
    end
  end

  alias_method_chain :simplified_type, 'interval'
end

# add a table definition for migrations, so rails will create 'interval' columns
ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::TableDefinition.class_eval do

  define_method('interval') do |*args|
    options = args.extract_options!
    column(args[0], 'interval', options)
  end
end

# add a table definition for migrations, so rails will create 'interval' columns
ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::Table.class_eval do

  define_method('interval') do |*args|
    options = args.extract_options!
    column(args[0], 'interval', options)
  end
end

# make sure activerecord treats :intervals as 'text'. This won't provide any help with
# dealing with them, but we can handle that ourselves
ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::OID.alias_type 'interval', 'text'

module ActiveRecord
  module ConnectionAdapters
    class PostgreSQLAdapter < AbstractAdapter
      module OID # :nodoc:
        class Interval < ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::OID::Type # :nodoc:

          # Converts PostgreSQL interval value (in +postgres+ format) to ActiveSupport::Duration
          def type_cast(value)
            return nil if value.nil?
            return value if value.is_a?(::ActiveSupport::Duration)
            time = ::Time.now
            if value.kind_of?(::Numeric)
              return ::ActiveSupport::Duration.new(time.advance(seconds: value) - time, seconds: value)
            end

            regex = / # Matches postgres format: -1 year -2 mons +3 days -04:05:06
              (?:(?<years>[\+\-]?\d+)\syear[s]?)?\s* # year part, like +3 years+
              (?:(?<months>[\+\-]?\d+)\smon[s]?)?\s* # month part, like +2 mons+
              (?:(?<days>[\+\-]?\d+)\sday[s]?)?\s*   # day part, like +5 days+
              (?:
                (?<timesign>[\+\-])?
                (?<hours>\d+):(?<minutes>\d+)(?::(?<seconds>\d+))?
              )?  # time part, like -00:00:00
            /x

            results = regex.match(value)
            parts = []
            seconds = 0
            %i(years months days).each do |param|
              next unless results[param]
              parts << [param, results[param].to_i]
              seconds += results[param].to_i.send(param)
            end
            if seconds==0
              %i(hours minutes seconds).each do |param|
                next unless results[param]
                seconds += "#{results[:timesign]}#{results[param]}".to_i.send(param)
              end
              if seconds>0
                parts << [:seconds, seconds]
              end
            end

            ::ActiveSupport::Duration.new(seconds, parts)
          end

        end
      end
    end
  end
end

ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::OID.register_type 'interval', ActiveRecord::ConnectionAdapters::PostgreSQLAdapter::OID::Interval.new

module ActiveRecord
  module ConnectionAdapters
    module PostgreSQL
      module ColumnMethods
        def interval(name, options = {})
          column(name, :interval, options)
        end
      end
    end
  end
end

module ActiveRecord
  module ConnectionAdapters
    class PostgreSQLAdapter < AbstractAdapter
      module Quoting

        alias_method :type_cast_without_interval, :type_cast

        # Converts ActiveSupport::Duration to PostgreSQL interval value (in +Tradition PostgreSQL+ format)
        def type_cast(value, column, array_member = false)
          return super(value, column) unless column
          case value
            when ::ActiveSupport::Duration
              if 'interval' == column.sql_type
                value.parts.
                  reduce(::Hash.new(0)) { |h,(l,r)| h[l] += r; h }.
                  sort_by {|unit,  _ | [:years, :months, :days, :minutes, :seconds].index(unit)}.
                  map     {|unit, val| "#{val} #{val == 1 ? unit.to_s.chop : unit.to_s}"}.
                  join ' '
              else
                super(value, column)
              end
            else
              type_cast_without_interval(value, column, array_member)
          end
        end

      end
    end
  end
end