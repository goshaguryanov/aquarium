require 'resque/failure/multiple'
require 'resque/failure/redis'

Resque::Failure::Multiple.classes = [ Resque::Failure::Redis, Resque::Failure::GelfExt ]
Resque::Failure.backend = Resque::Failure::Multiple

Resque.redis        = Aquarium::Redis.connection

Resque.logger       = Logger.new("#{Rails.root}/log/resque.log")
Resque.logger.level = Logger::INFO
