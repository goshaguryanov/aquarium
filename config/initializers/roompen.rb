Roompen.configure do |config|
  config.base_url = Settings.hosts.tanagra
  config.document_class = Document

  config.coercions = {
    document: {
      image: -> (image_attributes) do
        Roompen::Image.new(image_attributes)
      end,
      
      author: -> (author_attributes) do
        Document.new(author_attributes)
      end,
      
      second_author: -> (author_attributes) do
        Document.new(author_attributes)
      end,
      
      portrait: -> (image_attributes) do
        Roompen::Image.new(image_attributes)
      end,
      
      slider: -> (slider_attributes) do
        Roompen::Image.new(slider_attributes)
      end,

      newsstand: -> (newsstand_attributes) do
        Roompen::Image.new(newsstand_attributes)
      end
    },
    box: {
      image: -> (image_attributes) do
        Roompen::Image.new(image_attributes)
      end,

      bound_document: -> (document_attributes) do
        Document.new(document_attributes)
      end
    }
  }

end
