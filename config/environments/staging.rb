Rails.application.configure do
  # Code is not reloaded between requests.
  config.cache_classes = true
  
  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true
  
  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  
  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_assets = false
  
  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx
  
  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true
  
  # Set to :debug to see everything in the log.
  config.log_level = :info
  
  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true
  
  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify
  
  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new
  
  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
  
  config.action_mailer.delivery_method = :logfile
end
