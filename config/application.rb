require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'

Bundler.require(*Rails.groups)

Settings::Utils.environments << :staging

module Aquarium
  class Application < Rails::Application
    %w(app/serializers app/use_cases app/values app/validators app/segments lib).each do |path|
      config.autoload_paths << config.root.join(path).to_s
    end
    config.gelf_ext = Settings.graylog

    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    I18n.enforce_available_locales = true
    config.action_controller.permit_all_parameters = true

    config.active_record.schema_format = :sql
    
    config.middleware.use Rack::Cors do
      allow do
        origins  '*'
        resource '*', headers: :any, methods: %i(get post put patch delete options)
      end
    end
  end
end

require 'db_config'
